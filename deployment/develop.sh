echo "Deploy started"
cd /var/www/staging
git checkout develop
git pull
php artisan migrate
php artisan optimize
php artisan config:clear
php artisan l5-swagger:generate
echo "Deploy finished"