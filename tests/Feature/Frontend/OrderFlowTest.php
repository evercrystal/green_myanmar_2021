<?php

namespace Tests\Feature\Frontend;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Country\Entities\Country;
use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotRoute;
use Modules\SubRoute\Entities\ProductSubRoute;
use Modules\ProductType\Entities\ProductType;
use Modules\Box\Entities\Box;
use Modules\Order\Entities\OrderPayment;
use Modules\Order\Entities\Order;
use App\Factories\CreateCompletedSubRouteFactory;
use App\Factories\CreateCompletedOrderFactory;
use App\Models\BNFEncryption;
use App\Enums\Table;
use Carbon\Carbon as Carbon;
use App\Enums\Pickup;
use Modules\Order\Enum\PaymentModeType;

class OrderFlowTest extends TestCase
{
    use RefreshDatabase;

    public function testOrderFormSuccess()
    {
        $response = $this->get('/order-form');
        $response->assertStatus(200);
    }

    public function testGetCitySuccess()
    {
    	$country  = factory(Country::class)->create();

    	$state  = factory(State::class)->create([
    		'country_id' => $country->id
    	]);

    	$city = factory(City::class)->create([
    		'state_id' => $state->id
    	]);

        $response = $this->get('/order-form/get-city/'.$country->id);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,count($responseData));
    }

    public function testGetTownshipSuccess()
    {
    	$country  = factory(Country::class)->create();

    	$state  = factory(State::class)->create([
    		'country_id' => $country->id
    	]);

    	$city = factory(City::class)->create([
    		'state_id' => $state->id
    	]);

    	$township = factory(Township::class)->create([
    		'city_id' => $city->id
    	]);
        $response = $this->get('/order-form/get-township/'.$city->id);
        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,count($responseData));
    }

    public function testGetCollectionAddressSuccess()
    {
    	$township  = factory(Township::class)->create();

    	$collectionpoint  = factory(CollectionPoint::class)->create([
    		'township_id' => $township->id
    	]);

    	$collectionpoint = factory(CollectionPoint::class)->create([
    		'township_id' => $township->id
    	]);

        $response = $this->get('/order-form/get-collection-address/'.$collectionpoint->id);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,count($responseData));
    }

    public function testCalculatePriceValidationError()
    {

        $response = $this->post('/order-form/calculate-price', []);
        $response->assertSessionHasErrors(['from_township_id', 'to_township_id','product_type_id','no_of_item','box_id','weight','pickup_schedule_id','pickup_date']);

        $response = $this->post('/order-form/calculate-price', [
            'from_township_id' => 'error',
            'to_township_id' => 'error',
            'product_type_id' => 'error',
            'no_of_item' => 'error',
            'box_id' => 'error',
            'weight' => 'error',
            'pickup_schedule_id' => 'error',
            'pickup_date' => 'error',
        ]);
        $response->assertSessionHasErrors(['from_township_id', 'to_township_id','product_type_id','no_of_item','box_id','weight','pickup_schedule_id','pickup_date']);

    }

    public function testCalculatePriceInSameCitySuccess()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);


        $response = $this->post('/order-form/calculate-price', [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d')
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 1000, $responseData['data']['total_amount']);

    }

    public function testCalculatePriceInDifferentCitySuccess()
    {
        $fromTownship = factory(Township::class)->create();

        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);


        $response = $this->post('/order-form/calculate-price', [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d')
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 1000, $responseData['data']['total_amount']);

    }

    public function testCalculatePriceWithBoxSuccess()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);

        $response = $this->post('/order-form/calculate-price', [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d')
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.frontend.order.valid_route'), $responseData['message']);
        $this->assertEquals(true, $responseData['success']);

    }

    public function testCalculatePriceWithNoBoxSuccess()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $productType = factory(ProductType::class)->create();

        $box = factory(Box::class)->create();

        $route = factory(Route::class)->create([
            'from_city_id' => $fromTownship->city->id,
            'to_city_id' => $toTownship->city->id,
        ]);

        $subroute = factory(SubRoute::class)->create([
            'from_township_id' => $fromTownship->id,
            'to_township_id' => $toTownship->id,
            'box_id' => null,
            'max_weight' => 2,
            'mmk_price' => 1000,
        ]);
        
        $pivotRoute = factory(PivotRoute::class)->create([
            'route_id' => $route->id,
            'sub_route_id' => $subroute->id
        ]);

        $productSubRoute = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType->id,
            'sub_route_id' => $subroute->id
        ]);

        $response = $this->post('/order-form/calculate-price', [
            'from_township_id'=> $fromTownship->id,
            'to_township_id'=> $toTownship->id,
            'product_type_id'=> $productType->id,
            'no_of_item' => 1,
            'box_id'=> $box->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d')
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.frontend.order.valid_route'), $responseData['message']);
        $this->assertEquals(true, $responseData['success']);

    }

    public function testCalculatePriceFail()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $productType = factory(ProductType::class)->create();

        $box = factory(Box::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);


        $response = $this->post('/order-form/calculate-price', [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $productType->id,
            'no_of_item' => 1,
            'box_id'=> $box->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d')
        ]);
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(0, $responseData['data']['total_amount']);

    }

    public function testSaveOrderValidationError()
    {
        $response = $this->post('/order-form/save', []);
        $response->assertSessionHasErrors(['from_township_id', 'to_township_id','product_type_id','no_of_item','box_id','weight','pickup_date','pickup_schedule_id','payment_mode_id','pickup_name','pickup_phone','receiver_name','receiver_phone','from_address','to_address']);

        $response = $this->post('/order-form/save', [
            'from_township_id' => 'error',
            'to_township_id' => 'error',
            'product_type_id' => 'error',
            'no_of_item' => 'error',
            'box_id' => 'error',
            'weight' => 'error',
            'pickup_date' => 'error',
            'pickup_schedule_id' => 'error',
            'payment_mode_id' => 'error',
            'pickup_name' => 'Testing Pickup Name',
            'pickup_phone' => '09987654321',
            'receiver_name' => 'Testing Name',
            'receiver_phone' => '09123456789',
            'from_address' => 'Address1',
            'to_address' => 'Address2',
        ]);
        $response->assertSessionHasErrors(['from_township_id', 'to_township_id','product_type_id','no_of_item','box_id','weight','pickup_date','pickup_schedule_id','payment_mode_id']);

    }

    public function testSaveOrderSuccess()
    {
        $fromTownship = factory(Township::class)->create();
        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000, 2, $fromTownship, $toTownship);

        $response = $this->post('/order-form/save', [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d'),
            'payment_mode_id' => 1,
            'pickup_name' => 'Testing Pickup Name',
            'pickup_phone' => '09123456789',
            'receiver_name' => 'Testing Name',
            'receiver_phone' => '09987654321',
            'from_address' => 'Address1',
            'to_address' => 'Address2',
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('alerts.frontend.order.saved_order'), $responseData['message']);
        $this->assertDatabaseHas(
            Table::ORDER,
            [
                'from_township_id'=> $data['fromTownship']->id,
                'to_township_id'=> $data['toTownship']->id,
                'product_type_id'=> $data['productType']->id,
                'no_of_item' => 1,
                'box_id'=> $data['box']->id,
                'weight'=> 2,
                'pickup_schedule_id'=> 1,
                'payment_mode_id' => 1,
                'pickup_name' => 'Testing Pickup Name',
                'pickup_phone' => '959123456789',
                'receiver_name' => 'Testing Name',
                'receiver_phone' => '959987654321',
                'from_address' => 'Address1',
                'to_address' => 'Address2',
               
            ]
        );

        $this->assertDatabaseHas(
            Table::ORDER_ROUTE,
            [
                'route_id'=> $data['route']->id,
                'sub_route_id'=> $data['subroute']->id,
                'sequence' => 1
            ]
        );
    }

    public function testSaveOrderSuccessWithMerchantLogin()
    {
    	$user= $this->loginAsMerchant();

        $fromTownship = factory(Township::class)->create();
        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000, 2, $fromTownship, $toTownship);

        $response = $this->post('/order-form/save', [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d'),
            'payment_mode_id' => 1,
            'pickup_name' => 'Testing Pickup Name',
            'pickup_phone' => '09123456789',
            'receiver_name' => 'Testing Name',
            'receiver_phone' => '09987654321',
            'from_address' => 'Address1',
            'to_address' => 'Address2'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('alerts.frontend.order.saved_order'), $responseData['message']);
        $this->assertDatabaseHas(
            Table::ORDER,
            [
            	'merchant_id' => $user->merchant->id,
                'from_township_id'=> $data['fromTownship']->id,
                'to_township_id'=> $data['toTownship']->id,
                'product_type_id'=> $data['productType']->id,
                'no_of_item' => 1,
                'box_id'=> $data['box']->id,
                'weight'=> 2,
                'pickup_schedule_id'=> 1,
                'payment_mode_id' => 1,
                'pickup_name' => 'Testing Pickup Name',
                'pickup_phone' => '959123456789',
                'receiver_name' => 'Testing Name',
                'receiver_phone' => '959987654321',
                'from_address' => 'Address1',
                'to_address' => 'Address2'
               
            ]
        );
    }

    public function testSaveOrderFail()
    {
        $fromTownship = factory(Township::class)->create();
        $toTownship = factory(Township::class)->create();

        $newFromTownship = factory(Township::class)->create();
        $newToTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000, 2, $fromTownship, $toTownship);

        $response = $this->post('/order-form/save', [
            'from_township_id'=> $newFromTownship->id,
            'to_township_id'=> $newToTownship->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_schedule_id'=> 1,
            'pickup_date' => Carbon::now()->addDays(1)->format('Y-m-d'),
            'payment_mode_id' => 1,
            'pickup_name' => 'Testing Pickup Name',
            'pickup_phone' => '09123456789',
            'receiver_name' => 'Testing Name',
            'receiver_phone' => '09987654321',
            'from_address' => 'Address1',
            'to_address' => 'Address2'
        ]);
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('alerts.frontend.order.order_does_not_created'), $responseData['message']);

    }

    public function testSaveOrderPyamentValidationError()
    {
        $response = $this->post('/order-form/save-payment', []);
        $response->assertSessionHasErrors(['order_id', 'service_fee','payment_method','amount','payment_mode_id']);

        $response = $this->post('/order-form/save-payment', [
            'order_id' => 'error',
            'service_fee' => 'error',
            'amount' => 'error',
            'payment_mode_id' => 'error'
        ]);
        $response->assertSessionHasErrors(['order_id', 'service_fee','amount','payment_mode_id']);

    }

    public function testSaveOrderPaymentSuccess()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $response = $this->post('/order-form/save-payment', [
            'order_id'=> $data['order']->id,
            'service_fee'=> 0,
            'payment_method'=> 'cod',
            'amount' => 1000,
            'payment_mode_id' => 1,

        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(true, $responseData['success']);
        $this->assertDatabaseHas(
            Table::ORDER_PAYMENT,
            [
                'order_id'=> $data['order']->id,
                'service_fee'=> 0,
                'payment_method'=> 'cod',
                'amount' => 1000,
                'payment_mode_id' => 1
               
            ]
        );
    }

    public function testSaveOrderPaymentFail()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $response = $this->post('/order-form/save-payment', [
            'order_id'=> $data['order']->id,
            'service_fee'=> 0,
            'payment_method'=> 'cod',
            'amount' => 1000,
            'payment_mode_id' => 12345,

        ]);
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('alerts.frontend.order.order_payment_error'), $responseData['message']);
    }

    public function testOrderPaymentConfirmationPageSuccess()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $data['order']->id,
            'payment_method' => 'cod',
            'amount' => 1000
        ]);

        $encryptOrderPaymentId = BNFEncryption::encode($orderPayment->id);

        $response = $this->get('order/payment-confirmation/cod/'.$encryptOrderPaymentId);

        $response->assertStatus(200);
        $response->assertViewIs('frontend.payment_confirmation');

    }

    public function testReceiverPaymentOrderConfirmationPageSuccess()
    {
        $order= factory(Order::class)->create([
            'payment_mode_id' => PaymentModeType::ID_RECEIVER_WILL_PAID_ALL_ROUTE 
        ]);
        $encryptOrderId = BNFEncryption::encode($order->id);
        $response = $this->get( 'order/receiver-payment-confirmation/'.$encryptOrderId);
        $response->assertStatus(200);
        $response->assertViewIs('frontend.order_complete');

    }
    
    public function testOrderPaymentConfirmationPageError()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $data['order']->id,
            'payment_method' => 'cod',
            'amount' => 1000
        ]);

        $encryptOrderPaymentId = BNFEncryption::encode($orderPayment->id);

        $response = $this->get('order/payment-confirmation/none/'.$encryptOrderPaymentId);

        $response->assertStatus(404);

    }

    public function testBankTransferOrderPaymentCompletePageSuccess()
    {
        
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $data['order']->id,
            'payment_method' => 'bank_transfer',
            'amount' => 1000
        ]);

        $encryptOrderPaymentId = BNFEncryption::encode($orderPayment->id);

        $request = [
            "bank_name" => "Testing Bank",
            "remark" => "success"
        ];

        $response = $this->post('/order/payment-complete/bank_transfer/'.$encryptOrderPaymentId, $request);

       $response->assertStatus(200);
       $response->assertViewIs('frontend.payment_complete');
      
    }

    public function testMPUOrderPaymentCompletePageSuccess()
    {
        
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $data['order']->id,
            'payment_method' => 'mpu',
            'amount' => 1000
        ]);

        $encryptOrderPaymentId = BNFEncryption::encode($orderPayment->id);

        $request = [
            "channel_response_code" => "00",
            "channel_response_desc" => "00",
            "user_defined_1" => $encryptOrderPaymentId,
        ];

        $response = $this->post('/order/payment-complete/mpu', $request);

       $response->assertStatus(200);
       $response->assertViewIs('frontend.payment_complete');
      
    }

    public function testOrderPaymentCompletePageFail()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $data['order']->id,
            'payment_method' => 'bank_transfer',
            'amount' => 1000
        ]);

        $encryptOrderPaymentId = BNFEncryption::encode($orderPayment->id);

        $bankTransferRequest = [
            "bank_name" => "Testing Bank",
            "remark" => "success"
        ];

        $response = $this->post('/order/payment-complete/bank_transfer/none', $bankTransferRequest);

        $response->assertRedirect('/');

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $data['order']->id,
            'payment_method' => 'mpu',
            'amount' => 1000
        ]);

        $request = [
            "channel_response_code" => "00",
            "channel_response_desc" => "00",
            "user_defined_1" => "none",
        ];

        $response = $this->post('/order/payment-complete/mpu', $request);

        $response->assertRedirect('/');
      
    }

}
