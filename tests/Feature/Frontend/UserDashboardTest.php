<?php

namespace Tests\Feature\Frontend;

use Tests\TestCase;
use Modules\Merchant\Entities\Merchant;
use Modules\Order\Entities\Order;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDashboardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function unauthenticated_users_cant_access_the_dashboard()
    {
        $this->get('/dashboard')->assertRedirect('/login');
    }

    public function testUserDashboardSuccessWithMerchantAccess()
    { 
        $this->loginAsMerchant();

        $response = $this->get('dashboard');

        $response->assertStatus(200);

    }

    public function testOrderListSuccess()
    { 
        $user = $this->loginAsMerchant();

        $order = factory(Order::class)->create(['merchant_id'=>$user->merchant->id]);

        $response = $this->post('dashboard/get-order-list');

        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1, $responseData['recordsTotal']);

    }
}
