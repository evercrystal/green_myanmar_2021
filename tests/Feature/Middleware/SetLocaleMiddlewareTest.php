<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Kernel;
use Illuminate\Http\Request;
use Modules\Api\Http\Middleware\SetLocale;
use Modules\Page\Entities\Page; 
use Tests\TestCase;
use App\Enums\LanguageType;

class SetLocaleMiddlewareTest extends TestCase
{
    use RefreshDatabase;

    public function testRegisteredInRouteMiddlewareArrayOfTheHttpKernel()
    {
        $routeMiddleware = resolve(Kernel::class)->getRouteMiddleware();

        $this->assertArrayHasKey('locale', $routeMiddleware);

        // Assert it's pointing to the correct namespace
        $this->assertEquals(SetLocale::class, $routeMiddleware['locale']);
    }

    public function testSetsTheAppLocaleAndResponseContentLanguagewithPageExist()
    {
        $page = factory(Page::class)->create([
            'page' => 'aboutus',
            'language_id' => LanguageType::ID_ENGLISH
        ]);

        $response = $this->withHeaders(['Accept-Language' => 'en'])->json('GET', '/api/v1/page/aboutus');

        $responseData = $response->decodeResponseJson();

        $this->assertEquals(1, sizeOf($responseData));

        $this->assertEquals('en', app()->getLocale());

        $this->assertEquals('en',$response->headers->get('Content-Language'));

    }

    public function testSetsTheAppLocaleAndResponseContentLanguagewithPageNoExist()
    {
        $page = factory(Page::class)->create([
            'page' => 'aboutus',
            'language_id' => LanguageType::ID_ENGLISH
        ]);

        $response = $this->withHeaders(['Accept-Language' => 'mm'])->json('GET', '/api/v1/page/aboutus');

        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => trans('alerts.frontend.page.no_page_content_with_accept_language'),
                'errors' => []
        ], $responseData);

        $this->assertEquals('mm', app()->getLocale());

        $this->assertEquals('mm',$response->headers->get('Content-Language'));

    }

}
