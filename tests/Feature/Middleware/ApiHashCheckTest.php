<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Merchant\Entities\Merchant;
use App\Services\HashService;
use Tests\TestCase;

class ApiHashCheckTest extends TestCase
{
    use RefreshDatabase;

    public function testVerifyMerchantFailValidationError()
    {
        $response = $this->post('/api/v1/verify-merchant', []);
        $response->assertStatus(500);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 500,
                'message' => trans("errors.invalid_request_data"),
                'errors' => []
        ], $responseData);
    }

    public function testVerifyMerchantFailError()
    {
        $response = $this->post('/api/v1/verify-merchant', [
            'merchant_ref_id'=>'BNF-WAHBTQ',
            'hash_value'=>'5D85937464DEE96FD16CFEDEBFC4DF186C24147D7B92300DCFB97ED37D9C0AB2'
        ]);
        $response->assertStatus(500);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 500,
                'message' => trans("errors.merchant_does_not_exist"),
                'errors' => []
        ], $responseData);

        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/verify-merchant', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=>'5D85937464DEE96FD16CFEDEBFC4DF186C24147D7B92300DCFB97ED37D9C0AB2'
        ]);
        $response->assertStatus(500);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 500,
                'message' => trans("errors.invalid_hash_value"),
                'errors' => []
        ], $responseData);
    }

    public function testVerifyMerchantSuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/verify-merchant', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService)->encrypt('merchant_ref_id='.$merchant->merchant_ref_id, $merchant->merchant_secret)
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 200,
                'message' => 'success'
        ], $responseData);
    }
}
