<?php

namespace Tests\Enums\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Enums\Pickup;

class PickupTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetPickupNames()
    {
        $pickupName = Pickup::getName(Pickup::ID_TODAY);
        $this->assertSame('Today', $pickupName);

        $pickupName = Pickup::getName(Pickup::ID_TODAY,false);
        $this->assertSame('today', $pickupName);

        $pickupNameTomorrow = Pickup::getName(Pickup::ID_TOMORROW_MORNING);
        $this->assertSame('Tomorrow Morning', $pickupNameTomorrow);

        $pickupNameTomorrow = Pickup::getName(Pickup::ID_TOMORROW_MORNING,false);
        $this->assertSame('tomorrow_morning', $pickupNameTomorrow);

        $pickupNameTomorrowAfternoon = Pickup::getName(Pickup::ID_TOMORROW_AFTERNOON);
        $this->assertSame('Tomorrow Afternoon', $pickupNameTomorrowAfternoon);

        $pickupNameTomorrowAfternoon = Pickup::getName(Pickup::ID_TOMORROW_AFTERNOON,false);
        $this->assertSame('tomorrow_afternoon', $pickupNameTomorrowAfternoon);

        $pickupNameOther = Pickup::getName(Pickup::ID_OTHER);
        $this->assertSame('Other', $pickupNameOther);

        $pickupNameOther = Pickup::getName(Pickup::ID_OTHER,false);
        $this->assertSame('other', $pickupNameOther);

    }
}
