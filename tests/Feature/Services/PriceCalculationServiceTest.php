<?php

namespace Tests\Feature\Services;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Factories\CreateCompletedSubRouteFactory;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotRoute;
use Modules\SubRoute\Entities\ProductSubRoute;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\ProductType\Entities\ProductType;
use Modules\Box\Entities\Box;
use App\Services\PriceCalculationService;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Tests\TestCase;
use Carbon\Carbon as Carbon;
use App\Enums\Pickup;

class PriceCalculationServiceTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPriceCalculationWithSingleSubRoute()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000.0,2,$fromTownship,$toTownship);

        $input = [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            "box_id"=> $data['box']->id,
            "no_of_item" => 1,
            "weight"=> 2,
            "pickup_schedule_id"=> 1,
            "pickup_date" => Carbon::now()->addDays(1)->format('Y-m-d')
        ];

        $result = (new PriceCalculationService())->priceForMerchantOrder($input);

        $this->assertSame(0, $result['total_add_on_weight']);
        $this->assertSame((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 1000, $result['total_amount']);
        $this->assertSame(true, $result['access_delivery_time']);
        $this->assertSame($fromTownship->city->name, $result['from_city']);
        $this->assertSame($toTownship->city->name, $result['to_city']);
        $this->assertSame((string)$data['subroute']->id, $result['sub_routes']);

    }

    public function testPriceCalculationWithMultipleSubRoutes()
    {
        $fromCity = factory(City::class)->create();
        $toCity = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$fromCity->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$toCity->id
        ]);

        $productType = factory(ProductType::class)->create();

        $box = factory(Box::class)->create();

        $route = factory(Route::class)->create([
            'from_city_id' => $fromTownship->city->id,
            'to_city_id' => $toTownship->city->id,
        ]);

        $subRoute1 = factory(SubRoute::class)->create([
            'from_township_id' => $fromTownship->id,
            'to_township_id' => $fromTownship->id,
            'box_id' => $box->id,
            'max_weight' => 2,
            'mmk_price' => 1000
        ]);

        $subRoute2 = factory(SubRoute::class)->create([
            'from_township_id' => $fromTownship->id,
            'to_township_id' => $toTownship->id,
            'box_id' => $box->id,
            'max_weight' => 2,
            'mmk_price' => 2000,
            'is_city_route'=>1
        ]);

        $subRoute3 = factory(SubRoute::class)->create([
            'from_township_id' => $toTownship->id,
            'to_township_id' => $toTownship->id,
            'box_id' => $box->id,
            'max_weight' => 2,
            'mmk_price' => 500
        ]);

        $pivotRoute1 = factory(PivotRoute::class)->create([
            'route_id' => $route->id,
            'sub_route_id' => $subRoute1->id
        ]);

        $pivotRoute2 = factory(PivotRoute::class)->create([
            'route_id' => $route->id,
            'sub_route_id' => $subRoute2->id
        ]);

        $pivotRoute3 = factory(PivotRoute::class)->create([
            'route_id' => $route->id,
            'sub_route_id' => $subRoute3->id
        ]);

        $productSubRoute1 = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType->id,
            'sub_route_id' => $subRoute1->id
        ]);

        $productSubRoute2 = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType->id,
            'sub_route_id' => $subRoute2->id
        ]);

        $productSubRoute3 = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType->id,
            'sub_route_id' => $subRoute3->id
        ]);

        $data = [
            'from_township_id'=> $fromTownship->id,
            'to_township_id'=> $toTownship->id,
            'product_type_id'=> $productType->id,
            "box_id"=> $box->id,
            "no_of_item" => 1,
            "weight"=> 2,
            "pickup_schedule_id"=> 1,
            "pickup_date" => Carbon::now()->addDays(1)->format('Y-m-d')
        ];

        $result = (new PriceCalculationService())->priceForMerchantOrder($data);
        
        $this->assertSame(0, $result['total_add_on_weight']);
        $this->assertSame(true, $result['access_delivery_time']);
        $this->assertSame((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 3500, $result['total_amount']);
        $this->assertSame($fromTownship->city->name, $result['from_city']);
        $this->assertSame($toTownship->city->name, $result['to_city']);
        $this->assertSame($subRoute1->id.','.$subRoute2->id.','.$subRoute3->id, $result['sub_routes']);

    }

    public function testPriceCalculationAccessDeliveryTimeError()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000.0,2,$fromTownship,$toTownship);

        $input = [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            "box_id"=> $data['box']->id,
            "no_of_item" => 1,
            "weight"=> 2,
            "pickup_schedule_id"=> 1,
            "pickup_date" => Carbon::now()->subDays(1)->format('Y-m-d')
        ];

        $result = (new PriceCalculationService())->priceForMerchantOrder($input);

        $this->assertSame(0, $result['total_add_on_weight']);
        $this->assertSame(1000.0, $result['total_amount']);
        $this->assertSame(false, $result['access_delivery_time']);
        $this->assertSame($fromTownship->city->name, $result['from_city']);
        $this->assertSame($toTownship->city->name, $result['to_city']);
        $this->assertSame((string)$data['subroute']->id, $result['sub_routes']);

    }


    public function testPriceCalculationWithNoProductType()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $productType = factory(ProductType::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000.0,2,$fromTownship,$toTownship);

        $input = [
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $productType->id,
            "box_id"=> $data['box']->id,
            "no_of_item" => 1,
            "weight"=> 2,
            "pickup_schedule_id"=> 1,
            "pickup_date" => Carbon::now()->addDays(1)->format('Y-m-d')
        ];

        $result = (new PriceCalculationService())->priceForMerchantOrder($input);

        $this->assertSame(0, $result['total_add_on_weight']);
        $this->assertSame(false, $result['access_delivery_time']);
        $this->assertSame(0, $result['total_amount']);
        $this->assertSame(null, $result['from_city']);
        $this->assertSame(null, $result['to_city']);
        $this->assertSame(null, $result['sub_routes']);

    }

    public function testPriceCalculationWithCollectionPoints()
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $pickupCollection = factory(CollectionPoint::class)->create([
            'township_id' => $fromTownship->id
        ]);
        $deliveryCollection = factory(CollectionPoint::class)->create([
            'township_id' => $toTownship->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000.0,2,$fromTownship,$toTownship,$pickupCollection,$deliveryCollection);

        $input = [
            'from_township_id'=> $data['fromTownship']->id,
            'pickup_collection_point' => $pickupCollection->id,
            'to_township_id'=> $data['toTownship']->id,
            'delivery_collection_point' => $deliveryCollection->id,
            'product_type_id'=> $data['productType']->id,
            "box_id"=> $data['box']->id,
            "no_of_item" => 1,
            "weight"=> 2,
            "pickup_schedule_id"=> 1,
            "pickup_date" => Carbon::now()->addDays(1)->format('Y-m-d')
        ];

        $result = (new PriceCalculationService())->priceForMerchantOrder($input);

        $this->assertSame(0, $result['total_add_on_weight']);
        $this->assertSame((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 1000, $result['total_amount']);
        $this->assertSame(true, $result['access_delivery_time']);
        $this->assertSame($fromTownship->city->name, $result['from_city']);
        $this->assertSame($toTownship->city->name, $result['to_city']);
        $this->assertSame((string)$data['subroute']->id, $result['sub_routes']);

    }

}
