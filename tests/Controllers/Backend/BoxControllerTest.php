<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Box\Entities\Box;
use App\Enums\Table;
use Illuminate\Http\UploadedFile;
use App\Factories\CreateCompletedSubRouteFactory;
use Modules\Township\Entities\Township;
use Storage;

class BoxControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testBoxIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/box');
        $response->assertStatus(200);
    }

    public function testBoxCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/box/create');
        $response->assertStatus(200);
    }

    public function testBoxCreateSuccess()
    {
        $this->loginAsAdmin();
        Storage::fake('uploads');

        $response = $this->post('/admin/box', [
            'name' => 'box1',
            'description' =>  'testing',
            'status'      =>  '1',
            'image'       =>  UploadedFile::fake()->image('image.png')
        ]);

        $this->assertDatabaseHas(
            Table::BOX,
            [
                'name'          => 'box1',
                'description'   => 'testing',
                'status'        => '1',
            ]
        );
        $imageName = Box::where('name','box1')->first()->image;

        Storage::disk('uploads')->assertExists($imageName);


        $response->assertSessionHas(['flash_success' => __('box::alerts.backend.box.created')]);
    }

    public function testBoxCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/box', []);

        $response->assertSessionHasErrors(['name', 'description','image']);

        $this->post('/admin/box', [
            'name' => 'box1',
            'description' => 'testing',
            'image'     => UploadedFile::fake()->image('image.png')
        ]);

        $response = $this->post('/admin/box', [
            'name' => 'box1',
            'description' => 'testing',
            'image'     => UploadedFile::fake()->image('image.png')
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testBoxUpdateSuccess()
    {
        $this->loginAsAdmin();
        
        $box  = factory(Box::class)->create();

        $response = $this->get('/admin/box/'.$box->id.'/edit');

        $response->assertStatus(200);
    }

    public function testBoxUpdateValidationError()
    {
        $this->loginAsAdmin();

        $box = factory(Box::class)->create();

        $response = $this->patch('/admin/box/'.$box->id, []);

        $response->assertSessionHasErrors(['name', 'description']);

        factory(Box::class)->create([
            'name' => 'box1',
            'description' => 'testing',
            'image'     => 'boximage'
        ]);

        $existingBox = factory(Box::class)->create([
            'name' => 'box2',
            'description' => 'testing',
            'image'     => 'boximage'
        ]);

        $this->patch('/admin/box/'.$existingBox->id, [
            'name' => 'box1',
            'description' => 'testing',
            'image'       => 'boximage',
        ]);

        $response->assertSessionHasErrors(['name']);
    }


    public function testBoxUpdate()
    {
        $this->loginAsAdmin();
        Storage::fake('uploads');
        $box = factory(Box::class)->create();

        $response = $this->patch('/admin/box/'.$box->id, [
            'name' => 'box1',
            'description' => 'testing',
            'image'       =>  UploadedFile::fake()->image('imageupdate.png')
        ]);

        $this->assertDatabaseHas(
            Table::BOX,
            [
                'name' => 'box1',
                'description' => 'testing',

            ]
        );
        $imageName = Box::where('name','box1')->first()->image;

        Storage::disk('uploads')->assertExists($imageName);

        $response->assertSessionHas(['flash_success' => __('box::alerts.backend.box.updated')]);
    }

    public function testBoxDeleteSuccess()
    {
        $this->loginAsAdmin();

        $box = factory(Box::class)->create();

        $response = $this->delete("/admin/box/{$box->id}");

        $response->assertSessionHas(['flash_success' => __('box::alerts.backend.box.deleted')]);
        $this->assertDatabaseMissing(Table::BOX, ['id' => $box->id, 'deleted_at' => null]);
    }

    public function testBoxDeleteFail()
    {
        $this->loginAsAdmin();

        $fromTownship = factory(Township::class)->create();

        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);

        $response = $this->delete("/admin/box/".$data['box']->id);
        
        $response->assertSessionHas(['flash_danger' => __('box::alerts.backend.box.not_deleted')]);
    }
}
