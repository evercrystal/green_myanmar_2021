<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Merchant\Entities\Merchant;
use Modules\Merchant\Entities\MerchantLocation;
use Modules\Country\Entities\Country;
use Modules\City\Entities\City;
use Modules\Order\Enum\OrderListType;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\Township\Entities\Township;
use App\Models\Auth\Role;
use App\Models\Auth\User;
use App\Enums\Table;

class MerchantControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMerchantIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/merchant');

        $response->assertStatus(200);
    }

    public function testMerchantCreate()
    {
        $this->loginAsAdmin();
        
        $response = $this->get('/admin/merchant/create');

        $response->assertStatus(200);
    }

    public function testMerchantCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/merchant', []);

        $response->assertSessionHasErrors(['name', 'mobile','type','country_id','city_id','township_id','email','password','password_confirmation','active','confirmed']);

        $township = factory(Township::class)->create();

        $name = 'a';
        for ($i=0 ; $i < 200 ;$i++) {
            $name .= 'a';
        }
        $response = $this->post('/admin/merchant', [
            'name' => $name,
            'mobile' => '09954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'
        ]);

        $response->assertSessionHasErrors(['name']);

        $response = $this->post('/admin/merchant', [
            'name' => 'testing',
            'mobile' => '1234567',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['mobile']);

        $response = $this->post('/admin/merchant', [
            'name' => 'testing',
            'mobile' => '09954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id,
            'email' => 'testing1234',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['email']);

        Role::create(['name' => config('access.users.merchant_role')]);
        $this->post('/admin/merchant', [
            'name' => 'testing',
            'mobile' => '959954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);

        $response = $this->post('/admin/merchant', [
            'name' => 'testing',
            'mobile' => '959954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['email']);
    }

    public function testMerchantCreateSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        Role::create(['name' => config('access.users.merchant_role')]);

        $response = $this->post('/admin/merchant', [
              'name' => 'testing',
              'mobile' => '959954555372',
              'type' => '1',
              'country_id' => $township->city->state->country_id,
              'state_id' => $township->city->state_id,
              'city_id' => $township->city_id,
              'township_id' => $township->id,
              'email' => 'testing@gmail.com',
              'password' => 'secret',
              'password_confirmation' => 'secret',
              'active' => '1',
              'confirmed' => '1'
        ]);

        $this->assertDatabaseHas(
            'users',
            [
                'email'=>'testing@gmail.com'
            ]
        );

        $this->assertDatabaseHas(
            Table::MERCHANT,
            [
                'name' => 'testing',
                'mobile' => '959954555372',
                'type' => 1,
            ]
        );

        $this->assertDatabaseHas(
            Table::MERCHANT_LOCATION,
            [
                'country_id' => $township->city->state->country_id,
                'city_id' => $township->city_id,
                'township_id' => $township->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('merchant::alerts.backend.merchant.created')]);
    }

    public function testMerchantUpdate()
    {
        $this->loginAsAdmin();
        
        $merchant = factory(Merchant::class)->create();

        $response = $this->get('/admin/merchant/'.$merchant->id.'/edit');

        $response->assertStatus(200);
    }

    public function testMerchantUpdateValidationError()
    {
        $this->loginAsAdmin();

        $merchant = factory(Merchant::class)->create();

        $response = $this->patch('/admin/merchant/'.$merchant->id, []);

        $response->assertSessionHasErrors(['name','mobile','email']);

        $user = factory(User::class)->create([
            'email' => 'testing@gmail.com'
        ]);

        factory(Merchant::class)->create([
            'user_id' => $user->id
        ]);

        $existingMerchant = factory(Merchant::class)->create();

        $this->patch('/admin/merchant/'.$existingMerchant->id, [
            'name' => 'Testing',
            'mobile' => '959954555372',
            'email' => 'testing@gmail.com'
        ]);

        $response->assertSessionHasErrors(['email']);
    }

    public function testMerchantUpdateSuccess()
    {
        $this->loginAsAdmin();

        $merchant = factory(Merchant::class)->create();

        $response = $this->patch('/admin/merchant/'.$merchant->id, [
            'name' => 'Merchant1',
            'mobile' => '959954555372',
            'email' => 'testing@gmail.com',
        ]);

        $this->assertDatabaseHas(
            Table::MERCHANT,
            [
                'name' => 'Merchant1',
                'mobile' => '959954555372'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('merchant::alerts.backend.merchant.updated')]);
    }

    public function testMerchantDeleteSuccess()
    {
        $this->loginAsAdmin();

        $merchant = factory(Merchant::class)->create();

        $response = $this->delete("/admin/merchant/{$merchant->id}");

        $response->assertSessionHas(['flash_success' => __('merchant::alerts.backend.merchant.deleted')]);
        $this->assertDatabaseMissing(Table::MERCHANT, ['id' => $merchant->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing(Table::MERCHANT_LOCATION, ['merchant_id' => $merchant->id, 'deleted_at' => null]);
    }

    public function testMerchantShowPage()
    {
        $this->loginAsAdmin();

        $merchant = factory(Merchant::class)->create();
        
        $response = $this->get("/admin/merchant/{$merchant->id}");

        $response->assertStatus(200);
    }

    public function testMerchantNewLocationCreatePage()
    {
        $this->loginAsAdmin();

        $merchant = factory(Merchant::class)->create();
        
        $response = $this->get("/admin/merchant/{$merchant->id}/location-create");

        $response->assertStatus(200);
    }

    public function testMerchantNewLocationStore()
    {
        $this->loginAsAdmin();

        $merchant = factory(Merchant::class)->create();
        
        $response = $this->get("/admin/merchant/{$merchant->id}/location-create");

        $response->assertStatus(200);
    }

    public function testMerchantLocationCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/merchant/{$merchant->id}/location-create', []);

        $response->assertSessionHasErrors(['country_id','city_id','township_id']);
    }

    public function testMerchantNewLocationCreateSuccess()
    {
        $this->loginAsAdmin();
        
        $merchant = factory(Merchant::class)->create();

        $township = factory(Township::class)->create();

        $response = $this->post("/admin/merchant/{$merchant->id}/location-create", [
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id
        ]);

        $this->assertDatabaseHas(
            Table::MERCHANT_LOCATION,
            [
                'merchant_id' => $merchant->id,
                'country_id' => $township->city->state->country_id,
                'state_id' => $township->city->state_id,
                'city_id' => $township->city_id,
                'township_id' => $township->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('merchant::alerts.backend.merchant.location_created')]);
    }

    public function testMerchantNewLocationUpdatePage()
    {
        $this->loginAsAdmin();
        
        $location = factory(MerchantLocation::class)->create();

        $response = $this->get('/admin/merchant-location/'.$location->id.'/edit');

        $response->assertStatus(200);
    }

    public function testMerchantLocationUpdateValidationError()
    {
        $this->loginAsAdmin();

        $location = factory(MerchantLocation::class)->create();

        $response = $this->patch('/admin/merchant-location/'.$location->id, []);

        $response->assertSessionHasErrors(['country_id','city_id','township_id']);
    }

    public function testMerchantLocationUpdateSuccess()
    {
        $this->loginAsAdmin();

        $location = factory(MerchantLocation::class)->create();

        $township = factory(Township::class)->create();

        $response = $this->patch('/admin/merchant-location/'.$location->id, [
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id
        ]);

        $this->assertDatabaseHas(
            Table::MERCHANT_LOCATION,
            [
                'country_id' => $township->city->state->country_id,
                'city_id' => $township->city_id,
                'state_id' => $township->city->state_id,
                'township_id' => $township->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('merchant::alerts.backend.merchant.location_updated')]);
    }

    public function testMerchantLocationDeleteSuccess()
    {
        $this->loginAsAdmin();

        $location = factory(MerchantLocation::class)->create();

        $response = $this->delete("/admin/merchant-location/{$location->id}");

        $response->assertSessionHas(['flash_success' => __('merchant::alerts.backend.merchant.location_deleted')]);
        $this->assertDatabaseMissing(Table::MERCHANT_LOCATION, ['is' => $location->id, 'deleted_at' => null]);
    }

    public function testOrderHistoryListSuccess()
    {
        $this->loginAsAdmin();
        $merchant = factory(Merchant::class)->create();
        $order              = factory(Order::class)->create(
            [
                'merchant_id' => $merchant->id,
            ]
        );
       
        $orderRoute         = factory(OrderRoute::class)->create(['order_id' => $order->id]);
        $response = $this->post('admin/order/get',[
            'merchantId'       => $merchant->id,
            'list_type' => OrderListType::NAME_ALL
        ]);

        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1, $responseData['recordsTotal']);


    }
}
