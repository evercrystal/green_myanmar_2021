<?php

namespace Tests\Controllers\Backend;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\ProductType\Entities\ProductType;
use App\Factories\CreateCompletedSubRouteFactory;
use Modules\Township\Entities\Township;
use App\Enums\Table;

class ProductTypeControllerTest extends TestCase
{
    use RefreshDatabase;

    protected $formData;

    protected function setUp() : void
    {
        parent::setUp();
        $this->formData = '"[\n\t{\n\t\t\"type\": \"text\",\n\t\t\"label\": \"Text Field\",\n\t\t\"subtype\": \"text\",\n\t\t\"className\": \"form-control\",\n\t\t\"name\": \"text-1595999699949\"\n\t}\n]"';
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testProductTypeIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/producttype');
        $response->assertStatus(200);
    }

    public function testProductTypeCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/producttype/create');
        $response->assertStatus(200);
    }

    public function testProductTypeCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/producttype', []);

        $response->assertSessionHasErrors(['name', 'form']);

        $this->post('/admin/producttype', [
            'name' => 'productType1',
            'form' => $this->formData,
            'is_active'  => 1
        ]);

        $response = $this->post('/admin/producttype', [
            'name' => 'productType1',
            'form' => $this->formData,
            'is_active'  => 1
        ]);

        $response->assertSessionHasErrors(['name']);

    }

    public function testRouteCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/producttype', [
            'name' => 'product1',
            'form' => $this->formData,
            'is_active'  => 1
        ]);

        $this->assertDatabaseHas(
            Table::PRODUCT_TYPE,
            [
                'name' => 'product1',
                'form' => $this->formData,
                'is_active'  => 1
            ]
        );

        $response->assertSessionHas(['flash_success' => __('producttype::alerts.backend.producttype.created')]);
    }

    public function testProductTypeUpdate()
    {
        $this->loginAsAdmin();
        
        $productType  = factory(ProductType::class)->create();

        $response = $this->get('/admin/producttype/'.$productType->id.'/edit');

        $response->assertStatus(200);
    }

    public function testProductTypeUpdateValidationError()
    {
        $this->loginAsAdmin();

        $productType = factory(ProductType::class)->create();

        $response = $this->patch('/admin/producttype/'.$productType->id, []);

        $response->assertSessionHasErrors(['name', 'form']);

        factory(ProductType::class)->create([
            'name' => 'productType1',
            'form' => $this->formData,
            'is_active' => 1
        ]);

        $existingProduct = factory(ProductType::class)->create([
            'name' => 'productType2',
            'form' => $this->formData,
            'is_active' => 1
        ]);

        $this->patch('/admin/producttype/'.$existingProduct->id, [
            'name' => 'productType1',
            'form' => $this->formData,
            'is_active' => 1
        ]);

        $response->assertSessionHasErrors(['name']);

    }

    public function testProductTypeUpdateSuccess()
    {
        $this->loginAsAdmin();

        $productType = factory(ProductType::class)->create();

        $response = $this->patch('/admin/producttype/'.$productType->id, [
            'name' => 'product1',
            'form' => $this->formData,
            'is_active'  => 1

        ]);

        $this->assertDatabaseHas(
            Table::PRODUCT_TYPE,
            [
                'name' => 'product1',
                'form' => $this->formData,
                'is_active'  => 1
            ]
        );

        $response->assertSessionHas(['flash_success' => __('producttype::alerts.backend.producttype.updated')]);
    }


    public function testProductTypeDeleteSuccess()
    {
        $this->loginAsAdmin();

        $productType = factory(ProductType::class)->create();

        $response = $this->delete("/admin/producttype/{$productType->id}");

        $response->assertSessionHas(['flash_success' => __('producttype::alerts.backend.producttype.deleted')]);
        $this->assertDatabaseMissing(Table::PRODUCT_TYPE, ['id' => $productType->id, 'deleted_at' => null]);
    }

    public function testProductTypeDeleteFail()
    {
        $this->loginAsAdmin();

        $fromTownship = factory(Township::class)->create();

        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);

        $response = $this->delete("/admin/producttype/".$data['productType']->id);
        
        $response->assertSessionHas(['flash_danger' => __('producttype::alerts.backend.producttype.not_deleted')]);
    }
}
