<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\State\Entities\State;
use Modules\Country\Entities\Country;
use App\Enums\Table;
use Modules\City\Entities\City;

class StateControllerTest extends TestCase
{
    use RefreshDatabase;
    public function testStateIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/state');
        $response->assertStatus(200);
    }
    public function testStateCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/state/create');
        $response->assertStatus(200);
    }
    public function testStateCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/state', []);

        $response->assertSessionHasErrors(['name', 'country_id']);

        $name = 'a';
        $country = factory(Country::class)->create(); 
        for($i=0 ; $i < 200 ;$i++) $name .= 'a';
        $response=$this->post('/admin/state',[
            'name' => $name,
            'country_id' => $country->id ,
            'is_active'  => 1
        ]);
        $response->assertSessionHasErrors(['name']);
    }
    public function testStateCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/state', [
            'name' => 'test',
            'country_id' => '9',
            'is_active'  => 1
        ]);

        $this->assertDatabaseHas(
            Table::STATE,
            [
                'name' => 'test',
                'country_id' => '9',
                'is_active'  => 1
            ]
        );

        $response->assertSessionHas(['flash_success' => __('state::alerts.backend.state.created')]);
    }
    public function testStateUpdate()
    {
        $this->loginAsAdmin();
        
        $state  = factory(State::class)->create();

        $response = $this->get('/admin/state/'.$state->id.'/edit');

        $response->assertStatus(200);
    }
    public function testStateUpdateValidationError()
    {
        $this->loginAsAdmin();

        $state = factory(State::class)->create();

        $response = $this->patch('/admin/state/'.$state->id, []);

        $response->assertSessionHasErrors(['name', 'country_id']);

        factory(State::class)->create([
            'name' => 'bago',
            'country_id' => '9',
            'is_active'  => 1
        ]);

        $existingState = factory(State::class)->create([
            'name' => 'Yangon',
            'country_id' => '1',
            'is_active'  => 1
        ]);

        $this->patch('/admin/state/'.$existingState->id, [
            'name' => 'bago',
            'country_id' => '9',
            'is_active'  => 1
        ]);

        $response->assertSessionHasErrors(['name']);
    }
    public function testStateUpdateSuccess()
    {
        $this->loginAsAdmin();

        $state = factory(State::class)->create();

        $response = $this->patch('/admin/state/'.$state->id, [
            'name' => 'test',
            'country_id' => '2',
            'is_active'  => 1
        ]);

        $this->assertDatabaseHas(
            Table::STATE,
            [
                'name' => 'test',
                'country_id' => '2',
                'is_active'  => 1
            ]
        );

        $response->assertSessionHas(['flash_success' => __('state::alerts.backend.state.updated')]);
    }
    public function testStateDeleteNotSuccess()
    {
        $this->loginAsAdmin();

        $state = factory(State::class)->create();
        factory(City::class)->create([
            'name' => 'Yangon',
            'state_id' => $state->id,
            'is_active'  => 1
        ]);

        $response = $this->delete("/admin/state/{$state->id}");
        
        $response->assertSessionHas(['flash_danger' => __('state::alerts.backend.state.not_deleted')]);
    }
    public function testStateDeleteSuccess()
    {
        $this->loginAsAdmin();

        $state = factory(State::class)->create();

        $response = $this->delete("/admin/state/{$state->id}");

        $response->assertSessionHas(['flash_success' => __('state::alerts.backend.state.deleted')]);
        $this->assertDatabaseMissing(Table::STATE, ['id' => $state->id, 'deleted_at' => null]);
    }
}
