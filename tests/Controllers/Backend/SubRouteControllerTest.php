<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Township\Entities\Township;
use Modules\Box\Entities\Box;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotRoute;
use Modules\SubRoute\Entities\ProductSubRoute;
use Modules\Route\Entities\Route;
use Modules\ProductType\Entities\ProductType;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Modules\SubRoute\Entities\PivotPickupCollection;
use Modules\SubRoute\Entities\PivotDeliveryCollection;
use Tests\TestCase;
use App\Enums\Table;

class SubRouteControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSubRouteIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/subroute');
        $response->assertStatus(200);
    }

    public function testSubRouteCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/subroute/create');
        $response->assertStatus(200);
    }

    public function testSubRouteCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/subroute', []);

        $response->assertSessionHasErrors(['name', 'from_township_id', 'to_township_id', 'type','routes','duration', 'productTypes', 'max_weight','add_on_weight_rate_mmk','add_on_weight_rate_usd', 'mmk_price', 'usd_price']);
    }

    public function testSubRouteCreateSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        $box = factory(Box::class)->create();

        $route = factory(Route::class)->create();

        $routeArr = $route->pluck('id')->toArray();

        $productType = factory(ProductType::class)->create();

        $productTypeArr = $productType->pluck('id')->toArray();

        $response = $this->post('/admin/subroute', [
            'name' => 'Testing',
            'to_township_id' => $township->id,
            'from_township_id' => $township->id,
            'routes'         => $routeArr,
            'productTypes'    => $productTypeArr,
            'type'             => 1,
            'max_weight'       => 2,
            'add_on_weight_rate_mmk' => 500,
            'add_on_weight_rate_usd' => 0.5,
            'mmk_price'        => 3000,
            'usd_price'        => 2,
            'duration'         => 1440
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing',
                'to_township_id' => $township->id,
                'from_township_id' => $township->id,
                'type'             => 1,
                'max_weight'       => 2,
                'add_on_weight_rate_mmk' => 500,
                'add_on_weight_rate_usd' => 0.5,
                'mmk_price'        => 3000,
                'usd_price'        => 2,
                'duration'         => 1440
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PRODUCT_SUB_ROUTE,
            [
                'product_type_id' => $productType->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.created')]);

        $route2 = factory(Route::class)->create();

        $productType2 = factory(ProductType::class)->create();

        $routeArrs = Route::pluck('id')->toArray();

        $productTypeArrs = ProductType::pluck('id')->toArray();

        $response = $this->post('/admin/subroute', [
            'name' => 'Testing1',
            'to_township_id' => $township->id,
            'from_township_id' => $township->id,
            'routes'         => $routeArrs,
            'productTypes'     => $productTypeArrs,
            'type'             => 1,
            'max_weight'       => 2,
            'add_on_weight_rate_mmk' => 500,
            'add_on_weight_rate_usd' => 0.5,
            'mmk_price'        => 3000,
            'usd_price'        => 2,
            'duration'         => 1440
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing1',
                'to_township_id' => $township->id,
                'from_township_id' => $township->id,
                'type'             => 1,
                'max_weight'       => 2,
                'add_on_weight_rate_mmk' => 500,
                'add_on_weight_rate_usd' => 0.5,
                'mmk_price'        => 3000,
                'usd_price'        => 2,
                'duration'         => 1440
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route2->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PRODUCT_SUB_ROUTE,
            [
                'product_type_id' => $productType->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PRODUCT_SUB_ROUTE,
            [
                'product_type_id' => $productType2->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.created')]);
    }

    public function testSubRouteCreateSuccesswithCollectionPoints()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        $pickupCollection = factory(CollectionPoint::class)->create([
            'township_id' => $township->id
        ]);
        $deliveryCollection = factory(CollectionPoint::class)->create([
            'township_id' => $township->id
        ]);

        $box = factory(Box::class)->create();

        $route = factory(Route::class)->create();

        $routeArr = $route->pluck('id')->toArray();

        $productType = factory(ProductType::class)->create();

        $productTypeArr = $productType->pluck('id')->toArray();

        $pickupCollectionArr = $pickupCollection->pluck('id')->toArray();

        $deliveryCollectionArr = $deliveryCollection->pluck('id')->toArray();

        $response = $this->post('/admin/subroute', [
            'name' => 'Testing',
            'to_township_id' => $township->id,
            'from_township_id' => $township->id,
            'routes'         => $routeArr,
            'productTypes'    => $productTypeArr,
            'pickupCollection'=> $pickupCollectionArr,
            'deliveryCollection'=> $deliveryCollectionArr,
            'type'             => 1,
            'box_id'           => $box->id,
            'max_weight'       => 2,
            'add_on_weight_rate_mmk' => 500,
            'add_on_weight_rate_usd' => 0.5,
            'mmk_price'        => 3000,
            'usd_price'        => 2,
            'duration'         => 1440
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing',
                'to_township_id' => $township->id,
                'from_township_id' => $township->id,
                'type'             => 1,
                'box_id'           => $box->id,
                'max_weight'       => 2,
                'add_on_weight_rate_mmk' => 500,
                'add_on_weight_rate_usd' => 0.5,
                'mmk_price'        => 3000,
                'usd_price'        => 2,
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PRODUCT_SUB_ROUTE,
            [
                'product_type_id' => $productType->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_PICKUP_COLLECTION,
            [
                'collection_point_id' => $pickupCollection->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_DELIVERY_COLLECTION,
            [
                'collection_point_id' => $pickupCollection->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.created')]);
    }


    public function testSubRouteUpdateValidationError()
    {
        $this->loginAsAdmin();

        $subroute = factory(SubRoute::class)->create();

        $response = $this->patch('/admin/subroute/'.$subroute->id, []);
        $response->assertSessionHasErrors(['name', 'from_township_id', 'to_township_id', 'type','routes','duration','productTypes', 'max_weight','add_on_weight_rate_mmk','add_on_weight_rate_usd', 'mmk_price', 'usd_price']);
    }

    public function testSubRouteUpdateSuccess()
    {
        $this->loginAsAdmin();

        $route1 = factory(Route::class)->create();
        $route2 = factory(Route::class)->create();

        $productType1 = factory(ProductType::class)->create();
        $productType2 = factory(ProductType::class)->create();

        $pickupCollection = factory(CollectionPoint::class)->create();
        $deliveryCollection = factory(CollectionPoint::class)->create();

        $subroute = factory(SubRoute::class)->create();

        $pivotRoute1 = factory(PivotRoute::class)->create([
            'route_id' => $route1->id,
            'sub_route_id' => $subroute->id
        ]);

        $pivotRoute2 = factory(PivotRoute::class)->create([
            'route_id' => $route2->id,
            'sub_route_id' => $subroute->id
        ]);

        $productSubRoute1 = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType1->id,
            'sub_route_id' => $subroute->id
        ]);

        $productSubRoute2 = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType2->id,
            'sub_route_id' => $subroute->id
        ]);

        $pivotPickupCollection = factory(PivotPickupCollection::class)->create([
            'collection_point_id' => $pickupCollection->id,
            'sub_route_id' => $subroute->id
        ]);

        $pivotDeliveryCollection = factory(PivotDeliveryCollection::class)->create([
            'collection_point_id' => $deliveryCollection->id,
            'sub_route_id' => $subroute->id
        ]);

        $routeArr[] = $route1->id;
        $productTypeArr[] = $productType1->id;

        $response = $this->patch('/admin/subroute/'.$subroute->id, [
            'name' => 'Testing',
            'to_township_id' => $subroute->from_township_id,
            'from_township_id' => $subroute->to_township_id,
            'routes'           => $routeArr,
            'productTypes'     => $productTypeArr,
            'type'             => 1,
            'max_weight'       => 2,
            'add_on_weight_rate_mmk' => 500,
            'add_on_weight_rate_usd' => 0.5,
            'mmk_price'        => 3000,
            'usd_price'        => 2,
            'duration'         => 1440,
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing',
                'to_township_id' => $subroute->from_township_id,
                'from_township_id' => $subroute->to_township_id,
                'type'             => 1,
                'max_weight'       => 2,
                'add_on_weight_rate_mmk' => 500,
                'add_on_weight_rate_usd' => 0.5,
                'mmk_price'        => 3000,
                'usd_price'        => 2,
                'duration'         => 1440,
            ]
        );

        $this->assertDatabaseMissing(Table::PIVOT_ROUTE, ['route_id' => $route2->id,'sub_route_id' => $subroute->id]);
        $this->assertDatabaseMissing(Table::PRODUCT_SUB_ROUTE, ['product_type_id' => $productType2->id,'sub_route_id' => $subroute->id]);
        $this->assertDatabaseMissing(Table::PIVOT_PICKUP_COLLECTION, ['collection_point_id' => $pickupCollection->id,'sub_route_id' => $subroute->id]);
        $this->assertDatabaseMissing(Table::PIVOT_DELIVERY_COLLECTION, ['collection_point_id' => $deliveryCollection->id,'sub_route_id' => $subroute->id]);

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.updated')]);
    }

    public function testSubRouteDeleteSuccess()
    {
        $this->loginAsAdmin();

        $route1 = factory(Route::class)->create();

        $subroute = factory(SubRoute::class)->create();

        $pivotRoute1 = factory(PivotRoute::class)->create([
            'route_id' => $route1->id,
            'sub_route_id' => $subroute->id
        ]);

        $response = $this->delete("/admin/subroute/{$subroute->id}");

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.deleted')]);
        $this->assertDatabaseMissing(Table::SUB_ROUTE, ['id' => $subroute->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing(Table::PIVOT_ROUTE, ['route_id' => $route1->id,'sub_route_id' => $subroute->id]);
    }
}
