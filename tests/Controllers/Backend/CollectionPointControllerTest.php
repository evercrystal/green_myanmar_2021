<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Modules\State\Entities\State;
use Modules\Township\Entities\Township;

use App\Enums\Table;

class CollectionPointControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCollectionPointIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/collectionpoint');
        $response->assertStatus(200);
    }

    public function testCollectionPointCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/collectionpoint/create');
        $response->assertStatus(200);
    }

    public function testCollectionPointCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/collectionpoint', []);

        $response->assertSessionHasErrors(['name', 'township_id','address']);

        $name = 'a';
        $township = factory(Township::class)->create(); 
        for($i=0 ; $i < 200 ;$i++) $name .= 'a';
        $response=$this->post('/admin/collectionpoint',[
            'name' => $name,
            'township_id' => $township->id,
            'address'     => 'Test Street'
        ]);


        $response->assertSessionHasErrors(['name']);

        $township = factory(Township::class)->create(); 

        $this->post('/admin/collectionpoint', [
            'name' => 'CollectionPoint',
            'township_id' => $township->id,
             'address'     => 'Test Street'
        ]);

        $response = $this->post('/admin/collectionpoint', [
            'name' => 'CollectionPoint',
            'township_id' => '9',
            'address'     => 'Test Street'
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testCollectionPointCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/collectionpoint', [
            'name' => 'test',
            'township_id' => '9',
            'address'     => 'Test Street'
        ]);

        $this->assertDatabaseHas(
            Table::COLLECTION_POINT,
            [
                'name' => 'test',
                'township_id' => '9',
                'address'     => 'Test Street'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('collectionpoint::alerts.backend.collectionpoint.created')]);
    }
    public function testCollectionPointUpdate()
    {
        $this->loginAsAdmin();
        
        $collectionpoint  = factory(CollectionPoint::class)->create();
        $response = $this->get('/admin/collectionpoint/'.$collectionpoint->id.'/edit');
        $response->assertStatus(200);
    }

    public function testCollectionPointUpdateValidationError()
    {
        $this->loginAsAdmin();

        $collectionpoint = factory(CollectionPoint::class)->create();

        $response = $this->patch('/admin/collectionpoint/'.$collectionpoint->id, []);

        $response->assertSessionHasErrors(['name', 'township_id','address']);

        factory(CollectionPoint::class)->create([
            'name' => 'Point1',
            'township_id' => '9',
            'address'     => 'Test Street'
        ]);

        $existingCollectionPoint = factory(CollectionPoint::class)->create([
            'name' => 'Point2',
            'township_id' => '1',
            'address'     => 'Test Street'
        ]);

        $this->patch('/admin/collectionpoint/'.$existingCollectionPoint->id, [
            'name' => 'Point1',
            'township_id' => '1',
            'address'     => 'Test Street'
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testCollectionPointUpdateSuccess()
    {
        $this->loginAsAdmin();

        $collectionpoint = factory(Collectionpoint::class)->create();

        $response = $this->patch('/admin/collectionpoint/'.$collectionpoint->id, [
            'name' => 'point1',
            'township_id' => '2',
            'address'     => 'Test Street'
        ]);

        $this->assertDatabaseHas(
            Table::COLLECTION_POINT,
            [
                'name' => 'point1',
                'township_id' => '2',
                'address'     => 'Test Street'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('collectionpoint::alerts.backend.collectionpoint.updated')]);
    }

    public function testCollectionPointDeleteSuccess()
    {
        $this->loginAsAdmin();

        $collectionpoint = factory(CollectionPoint::class)->create();

        $response = $this->delete("/admin/collectionpoint/{$collectionpoint->id}");

        $response->assertSessionHas(['flash_success' => __('collectionpoint::alerts.backend.collectionpoint.deleted')]);
        $this->assertDatabaseMissing(Table::COLLECTION_POINT, ['id' => $collectionpoint->id, 'deleted_at' => null]);
    }
}
