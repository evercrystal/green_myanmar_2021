<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;

use App\Enums\Table;


class TownshipControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testTownshipIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/township');
        $response->assertStatus(200);
    }
    public function testTownshipCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/township/create');
        $response->assertStatus(200);
    }

    public function testTownshipCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/township', []);

        $response->assertSessionHasErrors(['name', 'city_id']);

        $name = 'a';
        $city = factory(City::class)->create(); 

        for($i=0 ; $i < 200 ;$i++) $name .= 'a';

        $response = $this->post('/admin/township', [
            'name' => $name,
            'city_id' => $city->id,
            'is_active' => 1
        ]);

        $response->assertSessionHasErrors(['name']);

        $township = factory(Township::class)->create(); 
        $city = factory(City::class)->create(); 

        $this->post('/admin/township', [
            'name' => 'Myanmar',
            'city_id' => $city->id,
            'is_active' => 1
        ]);

        $response = $this->post('/admin/township', [
            'name' => 'Myanmar',
            'city_id' => $city->id,
            'is_active' => 1
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testTownshipCreateSuccess()
    {
        $this->loginAsAdmin();
        $city = factory(City::class)->create(); 

        $response = $this->post('/admin/township', [
            'name' => 'test',
            'city_id' => $city->id,
            'is_active' => 1
        ]);

        $this->assertDatabaseHas(
            Table::TOWNSHIP,
            [
                'name' => 'test',
                'city_id' => $city->id,
                'is_active' => 1
            ]
        );

        $response->assertSessionHas(['flash_success' => __('township::alerts.backend.township.created')]);
    }
    public function testTownshipUpdate()
    {
        $this->loginAsAdmin();
        
        $township  = factory(Township::class)->create();

        $response = $this->get('/admin/township/'.$township->id.'/edit');

        $response->assertStatus(200);
    }

    public function testTownshipUpdateValidationError()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();
        $city = factory(City::class)->create();

        $response = $this->patch('/admin/township/'.$township->id, []);

        $response->assertSessionHasErrors(['name', 'city_id']);

        factory(Township::class)->create([
            'name' => 'bago',
            'city_id' => $city->id,
            'is_active' => 1
        ]);

        $existingTownship = factory(Township::class)->create([
            'name' => 'Yangon',
            'city_id' => '1',
            'is_active' => 1
        ]);

        $this->patch('/admin/township/'.$existingTownship->id, [
            'name' => 'bago',
            'city_id' => $city->id,
            'is_active' => 1
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testTownshipUpdateSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();
        $city = factory(City::class)->create();

        $response = $this->patch('/admin/township/'.$township->id, [
            'name' => 'Yangon',
            'city_id' => '2',
            'is_active' => 1
        ]);

        $this->assertDatabaseHas(
            Table::TOWNSHIP,
            [
                'name' => 'Yangon',
                'city_id' => $city->id,
                'is_active' => 1
            ]
        );

        $response->assertSessionHas(['flash_success' => __('township::alerts.backend.township.updated')]);
    }

    public function testTownshipDeleteSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        $response = $this->delete("/admin/township/{$township->id}");

        $response->assertSessionHas(['flash_success' => __('township::alerts.backend.township.deleted')]);
        $this->assertDatabaseMissing(Table::TOWNSHIP, ['id' => $township->id, 'deleted_at' => null]);
    }
}
