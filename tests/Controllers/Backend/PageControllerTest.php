<?php

namespace Tests\Controllers\Backend;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Page\Entities\Page;
use App\Enums\Table;

/**
 * Class PageControllerTest.
 */
class PageControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testPageIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/page');

        $response->assertStatus(200);
    }

    public function testPageCreate()
    {
        $this->loginAsAdmin();
        
        $response = $this->get('/admin/page/create');

        $response->assertStatus(200);
    }

    public function testPageCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/page', []);

        $response->assertSessionHasErrors(['page', 'title', 'content', 'language_id']);

        $this->post('/admin/page', [
            'page' => 'aboutus',
            'title' => 'About Us',
            'content' => 'Test page content ',
            'language_id' => '1'
        ]);

        $response = $this->post('/admin/page', [
            'page' => 'aboutus',
            'title' => 'About Us',
            'content' => 'Test page content ',
            'language_id' => '1'
        ]);

        $response->assertSessionHasErrors(['page']);
    }

    public function testPageCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/page', [
            'page' => 'aboutus',
            'title' => 'About Us',
            'content' => 'Test page content',
            'language_id' => "1"
        ]);
        
        $this->assertDatabaseHas(
            Table::PAGE,
            [
                'page' => 'aboutus',
                'title' => 'About Us',
                'content' => 'Test page content',
                'language_id' => "1"
            ]
        );

        $response->assertSessionHas(['flash_success' => __('page::alerts.backend.page.created')]);
    }

    public function testPageUpdate()
    {
        $this->loginAsAdmin();
        
        $page = factory(Page::class)->create();

        $response = $this->get('/admin/page/'.$page->id.'/edit');

        $response->assertStatus(200);
    }

    public function testPageUpdateValidationError()
    {
        $this->loginAsAdmin();

        $page = factory(Page::class)->create();

        $response = $this->patch('/admin/page/'.$page->id, []);

        $response->assertSessionHasErrors(['page', 'title', 'content', 'language_id']);

        $existingPage = factory(Page::class)->create([
            'page' => 'aboutus',
            'title' => 'About Us',
            'content' => 'Test page content ',
            'language_id' => '1'
        ]);

        $this->patch('/admin/page/'.$page->id, [
            'page' => 'aboutus',
            'title' => 'About Us',
            'content' => 'Test page content ',
            'language_id' => '1'
        ]);

        $response->assertSessionHasErrors(['page']);
    }

    public function testPageUpdateSuccess()
    {
        $this->loginAsAdmin();

        $page = factory(Page::class)->create();

        $response = $this->patch('/admin/page/'.$page->id, [
            'page' => 'aboutus',
            'title' => 'About Us',
            'content' => 'Test page content',
            'language_id' => '1'
        ]);

        $this->assertDatabaseHas(
            Table::PAGE,
            [
                'page' => 'aboutus',
                'title' => 'About Us',
                'content' => 'Test page content',
                'language_id' => '1'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('page::alerts.backend.page.updated')]);
    }

    public function testPageDeleteSuccess()
    {
        $this->loginAsAdmin();

        $page = factory(Page::class)->create();

        $response = $this->delete("/admin/page/{$page->id}");

        $response->assertSessionHas(['flash_success' => __('page::alerts.backend.page.deleted')]);
        $this->assertDatabaseMissing('page', ['id' => $page->id, 'deleted_at' => null]);
    }
}
