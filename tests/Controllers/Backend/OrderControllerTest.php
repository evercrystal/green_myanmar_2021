<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\Order\Entities\OrderPayment;
use Modules\Order\Entities\OrderSupplier;
use Modules\DeliverySupplier\Entities\DeliverySupplier;

use App\Enums\Table;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetOrderRoute()
    {
        $this->loginAsAdmin();
        $order          = factory(Order::class)->create();
        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
        ]);
        $response = $this->get('/admin/get-order-route/'.$order->id);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1, count($responseData));
   
    }

    public function testAssignDeliverySupplierSuccess()
    {
        $this->loginAsAdmin();
        $order              = factory(Order::class)->create();
        $orderRoute         = factory(OrderRoute::class)->create();
        $deliverySupplier   = factory(DeliverySupplier::class)->create();

        $response = $this->post('/admin/assign-delivery-supplier',[
            'order_id'               =>      $order->id,
            'delivery_supplier_id'    =>      $deliverySupplier->id,
            'order_route_id'          =>      $orderRoute->id,
        ]);
        $response->assertStatus(200);   
        $this->assertDatabaseHas(
            Table::ORDER_SUPPLIER,
            [
                'order_id' => $order->id,
                'delivery_supplier_id' => $deliverySupplier->id,
            ]
        );
    }

    public function testAssignDeliverySupplierValidationError()
    {
        $this->loginAsAdmin();
        $response = $this->post('/admin/assign-delivery-supplier', []);

        $response->assertSessionHasErrors(['order_id','delivery_supplier_id','order_route_id']);

        $order          = factory(Order::class)->create();
        $deliverySupplier   = factory(DeliverySupplier::class)->create();
        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id' => $deliverySupplier->id
        ]);

        $response = $this->post('/admin/assign-delivery-supplier', [
            'order_id' => $order->id,
            'order_route_id' => $orderRoute->id,
            'delivery_supplier_id' =>$deliverySupplier->id 
        ]);

        $response->assertSessionHasErrors(['delivery_supplier_id']);

    }

    public function testOrderRouteListSuccess()
    {
        $this->loginAsAdmin();

        $order              = factory(Order::class)->create();
       
        $orderRoute         = factory(OrderRoute::class)->create(['order_id' => $order->id]);
        $response = $this->post('admin/order/get-order-route',[
            'orderId'       => $order->id
        ]);

        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1, $responseData['recordsTotal']);


    }

    public function testOrderPaymentListSuccess()
    {
        $this->loginAsAdmin();

        $order                  = factory(Order::class)->create();
        $orderRoute             = factory(OrderRoute::class)->create();
        $orderPayment           = factory(OrderPayment::class)->create(['order_id' => $order->id]);
        $response = $this->post('admin/order/get-order-payment',[
            'orderId'       => $order->id
        ]);

        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1, $responseData['recordsTotal']);


    }

    public function testAddInformationSuccess()
    {
        $this->loginAsAdmin();

        $order = factory(Order::class)->create();
        $response = $this->post('admin/order/add-information/'.$order->id,[
            'information'  => 'testing'
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('order::alerts.backend.order.successful_add_information'), $responseData['message']);

    }

    public function testAddInformationFail()
    {
        $this->loginAsAdmin();

        $order = factory(Order::class)->create();
        $response = $this->post('admin/order/add-information/'.$order->id,[
            'error'  => 'testing'
        ]);

        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('order::alerts.backend.order.fail_add_infromation'), $responseData['message']);

    }

     public function testChangeOrderStatusSuccess()
    {
        $this->loginAsAdmin();
        $order = factory(Order::class)->create();

        $response = $this->post('/admin/change-order-status',[
            'orderId'     =>      $order->id,
            'statusID'    =>      2        
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('order::alerts.backend.order.status_change_success'), $responseData['message']);
        $this->assertDatabaseHas(
            Table::ORDER,
            [
                'id' => $order->id,
                'status' => 2
            ]
        );
    }

    public function testStoreAndUpdatePaymentValidationError()
    {
        $this->loginAsAdmin();
        $response = $this->post('/admin/order/payment', []);

        $response->assertSessionHasErrors(['order_id','amount','payment_method','payment_complete','balance','service_fee']);

        $data     = [
            'order_id' => 'error',
            'amount' => 'error',
            'payment_method' => 'cod',
            'payment_complete' => 1,
            'balance' => 'error',
            'service_fee' => 'error'
        ];

        $response = $this->post('/admin/order/payment', $data);

        $response->assertSessionHasErrors(['order_id','amount','balance','service_fee']);

    }

    public function testStorePaymentSuccess()
    {
        $this->loginAsAdmin();

        $order = factory(Order::class)->create([
            'total_amount'  => 2000
        ]);

        $response = $this->post('admin/order/payment',[
            'order_id'  => $order->id,
            'amount'  => 2000,
            'payment_method'  => 'cod',
            'payment_complete'  => 1,
            'remark' => 'testing',
            'balance' => 2000,
            'service_fee' => 0
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('order::alerts.backend.order.successful_add_payment'), $responseData['message']);
        $this->assertDatabaseHas(
            Table::ORDER_PAYMENT,
            [
                'order_id'  => $order->id,
                'amount'  => 2000,
                'payment_method'  => 'cod',
                'payment_complete'  => 1,
                'remark' => 'testing'
            ]
        );
    }

    public function testUpdatePaymentSuccess()
    {
        $this->loginAsAdmin();

        $order = factory(Order::class)->create([
            'total_amount'  => 2000
        ]);

        $orderPayment = factory(OrderPayment::class)->create([
            'order_id' => $order->id,
            'amount' => 2300,
            'payment_method' => 'bank_transfer',
            'payment_complete' => 0,
            'remark' =>'',
            'service_fee' => 300
        ]);

        $response = $this->post('admin/order/payment',[
            'order_payment_id'  => $orderPayment->id,
            'order_id'  => $order->id,
            'amount'  => 2300,
            'payment_method'  => 'bank_transfer',
            'payment_complete'  => 1,
            'remark' => 'testing',
            'balance' => 2000,
            'service_fee' => 300,
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('order::alerts.backend.order.successful_add_payment'), $responseData['message']);
        $this->assertDatabaseHas(
            Table::ORDER_PAYMENT,
            [
                'id' => $orderPayment->id,
                'order_id'  => $order->id,
                'amount'  => 2300,
                'payment_method'  => 'bank_transfer',
                'payment_complete'  => 1,
                'remark' => 'testing'
            ]
        );
    }

    public function testStorePaymentFail()
    {
        $this->loginAsAdmin();

        $order = factory(Order::class)->create([
            'total_amount'  => 2000
        ]);

        $response = $this->post('admin/order/payment',[
            'order_payment_id' => 12345,
            'order_id'  => $order->id,
            'amount'  => 2000,
            'payment_method'  => 'cod',
            'payment_complete'  => 1,
            'remark' => 'testing',
            'balance' => 2000,
            'service_fee' => 0
        ]);
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('order::alerts.backend.order.fail_add_payment'), $responseData['message']);

    }

    public function testUpdatePaymentWithPaidFail()
    {
        $this->loginAsAdmin();

        $order = factory(Order::class)->create([
            'total_amount'  => 2000
        ]);

        factory(OrderPayment::class)->create([
            'order_id' => $order->id,
            'amount' => 2000,
            'payment_method' => 'cod',
            'payment_complete' => 0,
            'remark' =>'',
            'service_fee' => 0
        ]);

        $newOrderPayment = factory(OrderPayment::class)->create([
            'order_id' => $order->id,
            'amount' => 2000,
            'payment_method' => 'cod',
            'payment_complete' => 0,
            'remark' =>'',
            'service_fee' => 0
        ]);

        $response = $this->post('admin/order/payment',[
            'order_payment_id'  => $newOrderPayment->id,
            'order_id'  => $order->id,
            'amount'  => 2000,
            'payment_method'  => 'bank_transfer',
            'payment_complete'  => 1,
            'remark' => 'testing',
            'balance' => 0,
            'service_fee' => 0,
        ]);

        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('order::alerts.backend.order.all_payment_has_been_fully_charged'), $responseData['message']);

    }

    public function testAdmincanRestoreOrder()
    {
        $this->loginAsAdmin();
        $order = factory(Order::class)->states('softDeleted')->create();
        $response = $this->get("/admin/order/restore/{$order->id}");
        $response->assertSessionHas(['flash_success' => __('order::alerts.backend.order.restore')]);

        $this->assertNull($order->fresh()->deleted_at);
    }

}
