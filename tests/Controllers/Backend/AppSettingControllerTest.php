<?php

namespace Tests\Controllers\Backend;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\File;

/**
 * Class AppSettingControllerTest.
 */
class AppSettingControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testAppSettingIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/appsetting');

        $response->assertStatus(200);
    }

    public function testAppSettingUpdateSuccess()
    {
        $this->loginAsAdmin();
        $content = File::get(base_path('.env.testing'));

        $fileMock = new File;
        $fileMock->shouldReceive('get')
                    ->with(base_path() . '/.env')
                    ->andReturn($content);
        //mock for replace update setting data
        $newContent = str_replace('APP_NAME="BNF Delivery"', 'APP_NAME="New BNF Delivery"', $content);
        $newContent = str_replace('QR_PREFIX="BNF"', 'QR_PREFIX="New BNF"', $newContent);
        $newContent = str_replace('META_KEYWORDS="BNF DELIVERY"', 'META_KEYWORDS="New BNF DELIVERY"', $newContent);
        $newContent = str_replace('META_DESCRIPTION="BNF Delivery System"', 'META_DESCRIPTION="NEW BNF Delivery System"', $newContent);

        $fileMock->shouldReceive('put')
                    ->with(base_path() . '/.env', $newContent)
                    ->andReturn(true);      

        $response = $this->post('/admin/appsetting', [
            'tab' => 'basic',
            'appstore' => 'www.google.com',
            'playstore' => 'www.google.com',
            'youtubedemo' => 'https://youtube.com',
            'app_name' => 'New BNF Delivery',
            'facebook' => 'www.facebook.com',
            'app_email' => 'bnfdelivery@gmail.com',
            'qr_prefix' => 'New BNF',
            'app_phone' => '09954555372',
            'app_address' => 'No. 391 Waizayantar Road, Za/South Quarter, Thingangyun Township, Yangon, Myanmar.',
            'app_dollar_rate' => '1450',
            'google_map' => 'AIzaSyCfXOeoxQMK3CXttc2l-qSz1chohv9UXN4',
            'meta_keywords' => 'New BNF DELIVERY',
            'meta_description' => 'NEW BNF Delivery System',
            'date_format' => 'd-m-Y',
            'time_format' => 'H:i',
            'datetime_format' => 'd-m-Y H:i'
        ]);

        $response->assertSessionHas(['flash_success' => __('appsetting::alerts.backend.appsetting.updated')]);
    }

    public function testAppSettingUpdateFail()
    {
        $this->loginAsAdmin();
        $content = File::get(base_path('.env.testing'));

        $fileMock = new File;
        $fileMock->shouldReceive('get')
                    ->with(base_path() . '/.env')
                    ->andReturn($content);

        $response = $this->post('/admin/appsetting', [
            'number_test' => '200'
        ]);

        if (strpos($content, 'NUMBER_TEST="200"') === false) {
            $response->assertSessionHas(['flash_danger' => __('appsetting::alerts.backend.appsetting.updated_error')]);
        }
    }
}
