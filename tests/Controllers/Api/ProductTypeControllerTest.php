<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\ProductType\Entities\ProductType;
use Tests\TestCase;

class ProductTypeControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testGetProductTypeDataSuccess()
    {
        $productType = factory(ProductType::class)->create();

        $response = $this->get('/api/v1/product-type');
        
        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();

        $this->assertEquals(1, sizeof($responseData['data']));
    }
}
