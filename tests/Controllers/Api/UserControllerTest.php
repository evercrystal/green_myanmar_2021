<?php

namespace Tests\Controllers\Api;

use App\Models\Auth\Role;
use App\Models\Auth\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Tests\TestCase;
use GMBF\PhoneNumber;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    private function createUser($phone, $email, $password, $active = true)
    {
        $user = factory(User::class)->create([
            'email' => $email,
            'password' => $password,
            'active' => $active
        ]);
        if ($phone) {
            $phoneNumber = new PhoneNumber();
            $phone = $phoneNumber->add_prefix($phone);
            factory(DeliverySupplier::class)->create([
                'user_id' => $user->id,
                'mobile' => $phone
            ]);
        }
        return $user;
    }

    public function testForgotPasswordUserNotFound()
    {
        $this->post(
            '/api/v1/user/forgot-password',
            ['email' => rand() . '@bnfdelivery.com']
        )
            ->assertStatus(404);
    }

    public function testForgotPasswordEmailInvalid()
    {
        $this->post(
            '/api/v1/user/forgot-password',
            ['email' => 'fooblah']
        )
            ->assertStatus(400);
    }

    public function testForgotPasswordNoEmail()
    {
        $this->post(
            '/api/v1/user/forgot-password',
            []
        )
            ->assertStatus(400);
    }

    public function testForgotPasswordSuccess()
    {
        $userEmail = 'demo@bnfdelivery.com';
        $this->createUser('+9594545454', $userEmail, '123456');
        $this->post(
            '/api/v1/user/forgot-password',
            ['email' => $userEmail]
        )
            ->assertStatus(200);

        $this->assertDatabaseHas('password_resets', ['email' => $userEmail]);
    }

    public function testUserShow(): void
    {
        $user = $this->createUser('+9594545454', 'demo@bnfdelivery.com', '123456');

        Passport::actingAs($user, ['full-access']);

        $response = $this->get('/api/v1/user');

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        # Assertion
        $response = $responseData['data'];
        $this->assertEquals($response['first_name'], $user->first_name);
        $this->assertEquals($response['last_name'], $user->last_name);
        $this->assertEquals($response['full_name'], $user->full_name);
        $this->assertEquals($response['email'], $user->email);
        $user = User::find($response['id']);
        $this->assertEquals($user->deliverySupplier->mobile, '9594545454');
    }

    public function testUserUpdateProfileValidationError()
    {
        $user = $this->createUser('+9594545454', 'demo@bnfdelivery.com', '123456');

        Passport::actingAs($user, ['full-access']);

        $response = $this->post('/api/v1/user/profile', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'name' => ['The name field is required.']
                ]
        ], $responseData);

        $name = 'a';
        for($i=0 ; $i < 200 ;$i++) $name .= 'a';
        $response = $this->post('/api/v1/user/profile', [
            'name'=>$name,

        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'name' => ['The name may not be greater than 191 characters.'],
                ]
        ], $responseData);
    }

    public function testUserUpdateProfileSuccess()
    {
        $user = $this->createUser('+9594545454', 'demo@bnfdelivery.com', '123456');

        Passport::actingAs($user, ['full-access']);

        $response = $this->post('/api/v1/user/profile', [
            'name'=>'Testing Name',

        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 200,
                'message' => trans('alerts.successfully_update_profile')
        ], $responseData);
    }

    public function testUserChangePasswordValidationError()
    {
        $user = $this->createUser('+9594545454', 'demo@bnfdelivery.com', '123456');

        Passport::actingAs($user, ['full-access']);

        $response = $this->post('/api/v1/user/change-password', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'old_password' => ['The old password field is required.'],
                    'password' => ['The password field is required.']
                ]
        ], $responseData);

        $response = $this->post('/api/v1/user/change-password', [
            'old_password' => '123456',
            'password' => '1234'
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'password' => ['The password must be at least 6 characters.']
                ]
        ], $responseData);
    }

    public function testUserChangePasswordMismatchError()
    {
        $user = $this->createUser('+9594545454', 'demo@bnfdelivery.com', '123456');

        Passport::actingAs($user, ['full-access']);

        $response = $this->post('/api/v1/user/change-password', [
            'old_password' => '111111',
            'password' => '222222'
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => trans('exceptions.frontend.auth.password.change_mismatch'),
                'errors' => []
        ], $responseData);
    }

    public function testUserChangePasswordSuccess()
    {
        $user = $this->createUser('+9594545454', 'demo@bnfdelivery.com', '123456');

        Passport::actingAs($user, ['full-access']);

        $response = $this->post('/api/v1/user/change-password', [
            'old_password' => '123456',
            'password' => '222222'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 200,
                'message' => trans('alerts.successfully_changed_password')
        ], $responseData);
    }
}
