<?php

namespace Tests\Controllers\Api;

use App\Models\Auth\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Api\Entities\OAuthRefreshToken;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Tests\TestCase;
use Laravel\Passport\Passport;
use GMBF\PhoneNumber;

class AccessTokenControllerTest extends TestCase
{
    use RefreshDatabase;

    private function createUser($phone, $email, $password, $active = true)
    {
        $user = factory(User::class)->create([
            'email' => $email,
            'password' => $password,
            'active' => $active
        ]);
        if ($phone) {
            $phoneNumber = new PhoneNumber();
            $phone = $phoneNumber->add_prefix($phone);
            factory(DeliverySupplier::class)->create([
                'user_id' => $user->id,
                'mobile' => $phone
            ]);
        }
        return $user;
    }

    public function testRevokeAccessToken()
    {
        $accessTokenId = rand();
        $refreshToken = new OAuthRefreshToken;
        $refreshToken->id = rand();
        $refreshToken->access_token_id = $accessTokenId;
        $refreshToken->revoked = false;
        $refreshToken->save();
        $this->assertNotNull($refreshToken);
        $this->assertFalse($refreshToken->revoked);

        $mockToken = $user = $this->mock(
            'Laravel\Passport\Token[revoke]',
            function ($mock) use ($accessTokenId) {
                $mock->shouldReceive('revoke')
                    ->andReturn(true)
                    ->set('id', $accessTokenId);
            }
        );

        $user = $this->mock(
            'App\Models\Auth\User[token]',
            function ($mock) use ($mockToken) {
                $mock->shouldReceive('token')
                    ->andReturn($mockToken);
            }
        );

        \Auth::shouldReceive('user')->andReturn($user);

        $this->withoutMiddleware()
            ->post('/api/oauth/token/revoke')
            ->assertStatus(204);
    }

    public function testIssueTokenSuccessUsingEmail()
    {
        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $user = factory(User::class)->create([
            'email' => 'demo@bnfdelivery.com',
            'password' => 'bnfdeliverydemo'
        ]);

        $response = $this->post('/api/oauth/token', [
                'username' => 'demo@bnfdelivery.com',
                'password' => 'bnfdeliverydemo',
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['token_type','expires_in','access_token','refresh_token']);
    }

    public function testIssueTokenSuccessUsingPhone()
    {
        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $user = $this->createUser('+123456789', 'demo@bnfdelivery.com', 'bnfdelivery');

        $this->post('/api/oauth/token', [
                'username' => '+123456789',
                'password' => 'bnfdelivery',
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ])
            ->assertStatus(200);
    }

    public function testIssueTokenInvalidPassword()
    {
        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $user = $this->createUser('+12345679', 'demo@bnfdelivery.com', 'bnfdelivery');

        $this->post('/api/oauth/token', [
                'username' => 'demo@bnfdelivery.com',
                'password' => 'wrongbnfdelivery',
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ])
            ->assertStatus(400);
    }

    public function testIssueTokenErrorInactiveUserWithPhone()
    {
        $phone = '+6598765432';
        $password = 'bnfdeliverytest';
        $this->createUser($phone, 'demo@bnfdelivery.com', $password, false);

        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $this->post('/api/oauth/token', [
                'username' => $phone,
                'password' => $password,
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ])
            ->assertStatus(403);
    }

    public function testIssueTokenFailedInactiveUserWithEmail()
    {
        $phone = '+6598765432';
        $password = 'bnfdeliverytest';
        $user = $this->createUser($phone, 'demo@bnfdelivery.com', $password, false);

        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $this->post('/api/oauth/token', [
                'username' => $user->email,
                'password' => $password,
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ])
            ->assertStatus(403);
    }

    public function testRefreshTokenSuccess()
    {
        $user = $this->createUser(null, 'demo@bnfdelivery.com', 'bnfdeliverydemo');

        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $response = $this->post('/api/oauth/token', [
                'username' => 'demo@bnfdelivery.com',
                'password' => 'bnfdeliverydemo',
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ]);
        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();
        $this->assertArrayHasKey('refresh_token', $responseData);

        $this->post('/api/oauth/token', [
                'refresh_token' => $response['refresh_token'],
                'grant_type' => 'refresh_token',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ])
            ->assertStatus(200);
    }

    public function testIncorrectCredentials()
    {
        $user = $this->createUser(null, 'demo@bnfdelivery.com', 'bnfdeliverydemo');

        $client = $this->generateOAuthClient();
        $this->assertNotNull($client);

        $response = $this->post('/api/oauth/token', [
                'username' => 'demo@bnfdelivery.com',
                'password' => 'surewrongone',
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
            ]);
        $response->assertStatus(400);
    }
}