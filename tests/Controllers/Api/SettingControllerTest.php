<?php

namespace Tests\Controllers\Api;

use App\Enums\PickupTimeSchedule;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Township\Entities\Township;
use Tests\TestCase;

class SettingControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testGetLocationSuccess()
    {

        factory(Township::class)->create();

        $response = $this->get('/api/v1/setting');
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(1, sizeof($responseData['countries']['data']));
        $this->assertEquals(1, sizeof($responseData['states']['data']));
        $this->assertEquals(1, sizeof($responseData['cities']['data']));
        $this->assertEquals(1, sizeof($responseData['townships']['data']));
        $this->assertEquals(PickupTimeSchedule::getSettings(), $responseData['pickup_time_schedules']);
    }
}
