<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Merchant\Entities\Merchant;
use Modules\SubRoute\Entities\SubRoute;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\ProductType\Entities\ProductType;
use Modules\Box\Entities\Box;
use Modules\Order\Entities\OrderSupplier;
use App\Factories\CreateCompletedSubRouteFactory;
use App\Factories\CreateCompletedOrderFactory;
use App\Services\HashService;
use Tests\TestCase;
use App\Enums\Table;
use Carbon\Carbon as Carbon;
use App\Enums\Pickup;
use Illuminate\Support\Facades\Storage;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\Order\Entities\OrderRouteProof;
use Modules\Order\Entities\OrderPayment;
use Modules\Order\Enum\OrderRouteLocationType;
use Modules\Order\Enum\OrderProofType;
use Modules\Order\Enum\OrderStatusType;
use Modules\Order\Enum\PaymentMethodType;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase,WithoutMiddleware;

    protected $tomorrow;
    protected $previousDay;

    protected function setUp() : void
    {
        parent::setUp();

        $this->tomorrow = Carbon::now()->addDays(1)->format('Y-m-d');
        $this->previousDay = Carbon::now()->subDays(1)->format('Y-m-d');
    }

    public function testCalculatePriceForMerchantOrderValidationError()
    {
        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/order/price', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id, $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'from_township_id' => ['The from township id field is required.'],
                    'to_township_id' => ['The to township id field is required.'],
                    'product_type_id' => ['The product type id field is required.'],
                    'no_of_item' => ['The no of item field is required.'],
                    'box_id' => ['The box id field is required.'],
                    'weight' => ['The weight field is required.'],
                    'pickup_date' => ['The pickup date field is required.'],
                    'pickup_schedule_id' => ['The pickup schedule id field is required.']
                ]
        ], $responseData);

         $response = $this->post('/api/v1/order/price', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'from_township_id' => 12345,
            'to_township_id' => 12345,
            'product_type_id' => 12345,
            'no_of_item' => 'error',
            'box_id' => 12345,
            'weight' => 'error',
            'pickup_date' => 'error',
            'pickup_schedule_id' => 1234,
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id=12345&to_township_id=12345&product_type_id=12345&no_of_item=error&box_id=12345&weight=error&pickup_date=error&pickup_schedule_id=1234', $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'from_township_id' => ['The selected from township id is invalid.'],
                    'to_township_id' => ['The selected to township id is invalid.'],
                    'product_type_id' => ['The selected product type id is invalid.'],
                    'no_of_item' => ['The no of item must be a number.'],
                    'box_id' => ['The selected box id is invalid.'],
                    'weight' => ['The weight must be a number.'],
                    'pickup_date' => ['The pickup date is not a valid date.'],
                    'pickup_schedule_id' => ['The selected pickup schedule id is invalid.']
                ]
        ], $responseData);
    }

    public function testCalculatePriceForMerchantOrderInSameCitySuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$data['fromTownship']->id.'&to_township_id='.$data['toTownship']->id.'&product_type_id='.$data['productType']->id.'&no_of_item=1&box_id='.$data['box']->id.'&weight=2&pickup_date='.$this->tomorrow.'&pickup_schedule_id=1';

        $response = $this->post('/api/v1/order/price', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_date'=> $this->tomorrow,
            'pickup_schedule_id'=> 1,
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 1000, $responseData['total_amount']);

    }

    public function testCalculatePriceForMerchantOrderInDifferentCitySuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $fromTownship = factory(Township::class)->create();

        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$data['fromTownship']->id.'&to_township_id='.$data['toTownship']->id.'&product_type_id='.$data['productType']->id.'&no_of_item=1&box_id='.$data['box']->id.'&weight=2&pickup_date='.$this->tomorrow.'&pickup_schedule_id=1';

        $response = $this->post('/api/v1/order/price', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_date'=> $this->tomorrow,
            'pickup_schedule_id'=> 1,
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        
        $this->assertEquals((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON] + 1000, $responseData['total_amount']);

    }

    public function testCalculatePriceForMerchantOrderFail()
    {
        $merchant = factory(Merchant::class)->create();

        $fromTownship = factory(Township::class)->create();

        $toTownship = factory(Township::class)->create();

        $productType = factory(ProductType::class)->create();

        $box = factory(Box::class)->create();

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$fromTownship->id.'&to_township_id='.$toTownship->id.'&product_type_id='.$productType->id.'&no_of_item=1&box_id='.$box->id.'&weight=2&pickup_date='.$this->tomorrow.'&pickup_schedule_id=1';

        $response = $this->post('/api/v1/order/price', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $fromTownship->id,
            'to_township_id'=> $toTownship->id,
            'product_type_id'=> $productType->id,
            'no_of_item' => 1,
            'box_id'=> $box->id,
            'weight'=> 2,
            'pickup_date'=> $this->tomorrow,
            'pickup_schedule_id'=> 1,
        ]);
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('alerts.frontend.order.invalid_route'), $responseData['message']);

    }

    public function testCalculatePriceWithInvalidPickupSchedule()
    {
        $merchant = factory(Merchant::class)->create();

        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000,2,$fromTownship,$toTownship);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$data['fromTownship']->id.'&to_township_id='.$data['toTownship']->id.'&product_type_id='.$data['productType']->id.'&no_of_item=1&box_id='.$data['box']->id.'&weight=2&pickup_date='.$this->previousDay.'&pickup_schedule_id=1';

        $response = $this->post('/api/v1/order/price', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_date'=> $this->previousDay,
            'pickup_schedule_id'=> 1,
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans('alerts.frontend.order.invalid_pickup_schedule'), $responseData['message']);

    }

    public function testSaveOrderValidationError()
    {
        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/order/save-order', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id, $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'from_township_id' => ['The from township id field is required.'],
                    'to_township_id' => ['The to township id field is required.'],
                    'product_type_id' => ['The product type id field is required.'],
                    'no_of_item' => ['The no of item field is required.'],
                    'box_id' => ['The box id field is required.'],
                    'weight' => ['The weight field is required.'],
                    'pickup_date' => ['The pickup date field is required.'],
                    'pickup_schedule_id' => ['The pickup schedule id field is required.'],
                    'payment_mode_id' => ['The payment mode id field is required.'],
                    'receiver_name' => ['The receiver name field is required.'],
                    'receiver_phone' => ['The receiver phone field is required.'],
                    'pickup_name' => ['The pickup name field is required.'],
                    'pickup_phone' => ['The pickup phone field is required.'],
                    'from_address' => ['The from address field is required.'],
                    'to_address' => ['The to address field is required.']
                ]
        ], $responseData);

        $response = $this->post('/api/v1/order/save-order', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'from_township_id' => 12345,
            'to_township_id' => 12345,
            'product_type_id' => 12345,
            'no_of_item' => 'error',
            'box_id' => 12345,
            'weight' => 'error',
            'pickup_date' => 'error',
            'pickup_schedule_id' => 12345,
            'payment_mode_id' =>12345,
            'receiver_name' => 'Testing',
            'receiver_phone' => '09123456789',
            'pickup_name' => 'Testing Pickup',
            'pickup_phone' => '09987654321',
            'from_address' => 'From Address Name',
            'to_address' => 'To Address Name',
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id=12345&to_township_id=12345&product_type_id=12345&no_of_item=error&box_id=12345&weight=error&pickup_date=error&pickup_schedule_id=12345&payment_mode_id=12345&receiver_name=Testing&receiver_phone=09123456789&pickup_name=Testing Pickup&pickup_phone=09987654321&from_address=From Address Name&to_address=To Address Name', $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'from_township_id' => ['The selected from township id is invalid.'],
                    'to_township_id' => ['The selected to township id is invalid.'],
                    'product_type_id' => ['The selected product type id is invalid.'],
                    'no_of_item' => ['The no of item must be a number.'],
                    'box_id' => ['The selected box id is invalid.'],
                    'weight' => ['The weight must be a number.'],
                    'pickup_date' => ['The pickup date is not a valid date.'],
                    'pickup_schedule_id' => ['The selected pickup schedule id is invalid.'],
                    'payment_mode_id' => ['The selected payment mode id is invalid.'],

                ]
        ], $responseData);
    }

    public function testSaveOrderWithCustomerOrderNumberValidationError()
    {
        $merchant = factory(Merchant::class)->create();

        $order = factory(Order::class)->create([
            'user_id' => $merchant->user_id,
            'merchant_id' => $merchant->id,
            'customer_order_no' => '11111'
        ]);

        $fromTownship = factory(Township::class)->create();
        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000, 2, $fromTownship, $toTownship);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$data['fromTownship']->id.'&to_township_id='.$data['toTownship']->id.'&product_type_id='.$data['productType']->id.'&no_of_item=1&box_id='.$data['box']->id.'&weight=2&pickup_date='.$this->tomorrow.'&pickup_schedule_id=1&payment_mode_id=1&pickup_name=Testing Merchant&pickup_phone=09123456789&receiver_name=MgMg&receiver_phone=09987654321&from_address=From Address Name&to_address=To Address Name&customer_order_no=11111';

        $response = $this->post('/api/v1/order/save-order', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'merchant' => $merchant,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_date'=> $this->tomorrow,
            'pickup_schedule_id'=> 1,
            'payment_mode_id' => 1,
            'pickup_name' => 'Testing Merchant',
            'pickup_phone' => '09123456789',
            'receiver_name' => 'MgMg',
            'receiver_phone' => '09987654321',
            'from_address' => 'From Address Name',
            'to_address' => 'To Address Name',
            'customer_order_no' => '11111'
        ]);

        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'customer_order_no' => ['The customer order no has already been taken.'],


                ]
        ], $responseData);
    }

    public function testSaveOrderSuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $fromTownship = factory(Township::class)->create();
        $toTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000, 2, $fromTownship, $toTownship);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$data['fromTownship']->id.'&to_township_id='.$data['toTownship']->id.'&product_type_id='.$data['productType']->id.'&no_of_item=1&box_id='.$data['box']->id.'&weight=2&pickup_date='.$this->tomorrow.'&pickup_schedule_id=1&payment_mode_id=1&pickup_name=Testing Merchant&pickup_phone=09123456789&receiver_name=MgMg&receiver_phone=09987654321&from_address=From Address Name&to_address=To Address Name';

        $response = $this->post('/api/v1/order/save-order', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $data['fromTownship']->id,
            'to_township_id'=> $data['toTownship']->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_date'=> $this->tomorrow,
            'pickup_schedule_id'=> 1,
            'payment_mode_id' => 1,
            'pickup_name' => 'Testing Merchant',
            'pickup_phone' => '09123456789',
            'receiver_name' => 'MgMg',
            'receiver_phone' => '09987654321',
            'from_address' => 'From Address Name',
            'to_address' => 'To Address Name',
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals((float)config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON]+1000, $responseData['meta']['sender_clearance_amount']);

        $this->assertDatabaseHas(
            Table::ORDER,
            [
                'from_township_id'=> $data['fromTownship']->id,
                'to_township_id'=> $data['toTownship']->id,
                'product_type_id'=> $data['productType']->id,
                'no_of_item' => 1,
                'box_id'=> $data['box']->id,
                'weight'=> 2,
                'pickup_date'=> $this->tomorrow,
                'pickup_schedule_id'=> 1,
                'payment_mode_id' => 1,
                'pickup_name' => 'Testing Merchant',
                'pickup_phone' => '959123456789',
                'receiver_name' => 'MgMg',
                'receiver_phone' => '959987654321',
                'from_address' => 'From Address Name',
                'to_address' => 'To Address Name',
               
            ]
        );

    }

    public function testSaveOrderFail()
    {
        $merchant = factory(Merchant::class)->create();

        $fromTownship = factory(Township::class)->create();
        $toTownship = factory(Township::class)->create();

        $newFromTownship = factory(Township::class)->create();
        $newToTownship = factory(Township::class)->create();

        $data = CreateCompletedSubRouteFactory::createSubRoute(1000, 2, $fromTownship, $toTownship);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&from_township_id='.$newFromTownship->id.'&to_township_id='.$newToTownship->id.'&product_type_id='.$data['productType']->id.'&no_of_item=1&box_id='.$data['box']->id.'&weight=2&pickup_date='.$this->tomorrow.'&pickup_schedule_id=1&payment_mode_id=1&pickup_name=Testing Merchant&pickup_phone=09123456789&receiver_name=MgMg&receiver_phone=09987654321&from_address=From Address Name&to_address=To Address Name';

        $response = $this->post('/api/v1/order/save-order', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'from_township_id'=> $newFromTownship->id,
            'to_township_id'=> $newToTownship->id,
            'product_type_id'=> $data['productType']->id,
            'no_of_item' => 1,
            'box_id'=> $data['box']->id,
            'weight'=> 2,
            'pickup_date'=> $this->tomorrow,
            'pickup_schedule_id'=> 1,
            'payment_mode_id' => 1,
            'pickup_name' => 'Testing Merchant',
            'pickup_phone' => '09123456789',
            'receiver_name' => 'MgMg',
            'receiver_phone' => '09987654321',
            'from_address' => 'From Address Name',
            'to_address' => 'To Address Name',
        ]);
        $response->assertStatus(500);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans("errors.order_does_not_created"), $responseData['message']);
    }

    public function testSaveOrderPaymentValidationError()
    {
        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/order/save-order-payment', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id, $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'order_id' => ['The order id field is required.'],
                    'remark' => ['The remark field is required.']
                ]
        ], $responseData);

        $response = $this->post('/api/v1/order/save-order-payment', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'order_id' => 12345,
            'remark' => 'testing',
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id.'&order_id=12345&remark=testing', $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'order_id' => ['The selected order id is invalid.'],

                ]
        ], $responseData);
    }

    public function testSaveOrderPaymentSuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $data = $data = CreateCompletedOrderFactory::createOrder(1000,2,3);

        $dataString = 'merchant_ref_id='.$merchant->merchant_ref_id.'&order_id='.$data['order']->id. '&remark=testing';

        $response = $this->post('/api/v1/order/save-order-payment', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService())->encrypt($dataString, $merchant->merchant_secret),
            'order_id'=> $data['order']->id,
            'remark' => 'testing'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();


        $this->assertDatabaseHas(
            Table::ORDER_PAYMENT,
            [
                'order_id'=> $data['order']->id,
                'payment_method' => PaymentMethodType::NAME_PAY_BY_MERCHANT,
                'remark' => 'testing'
            ]
        );

    }

    public function testSupplierOrderValidationError()    
    {
        $response = $this->get('/api/v1/supplier/order?status_ids=20');
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'status_ids_array' => ['The selected status ids array is invalid.']
                ]
        ], $responseData);
    }

    public function testSupplierOrderSuccess()
    {
        $this->loginAsSupplier();

        $data = CreateCompletedOrderFactory::createOrder(1000,2,3);

        $orderSupplier = factory(OrderSupplier::class)->create([
            'order_id' => $data['order']->id,
            'delivery_supplier_id' => auth()->user()->deliverySupplier->id
        ]);

        $response = $this->get('/api/v1/supplier/order?status_ids=3,4');

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['data']));
        $response = $this->get('/api/v1/supplier/order?date='.$data['order']->created_at.' - '.$data['order']->created_at);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['data']));
    }

    public function testSupplierAllocationOrderSuccess()
    {
        $this->loginAsSupplier();

        $data = CreateCompletedOrderFactory::createOrder(1000,2,OrderStatusType::ID_ALLOCATION);

        $response = $this->get('/api/v1/supplier/order?status_ids=2');

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(1,sizeOf($responseData['data']));

    }

    public function testSupplierOrderNoData()
    {
        $this->loginAsSupplier();

        $response = $this->get('/api/v1/supplier/order');

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(0,sizeOf($responseData['data']));

        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $orderSupplier = factory(OrderSupplier::class)->create([
            'order_id' => $data['order']->id,
            'delivery_supplier_id' => auth()->user()->deliverySupplier->id
        ]);
        $response = $this->get('/api/v1/supplier/order?status_ids=2');

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(0,sizeOf($responseData['data']));
    }

    public function testAssignDeliverySupplierValidationError()    
    {

        $response = $this->post('/api/v1/supplier/assign-delivery', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'ref_no' => ['The ref no field is required.']

                ]
        ], $responseData);

        $response = $this->post('/api/v1/supplier/assign-delivery', [
            'ref_no' => 'invalid ref no'
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'ref_no' => ['The selected ref no is invalid.']
                ]
        ], $responseData);
    }

    public function testGetSupplieOrderRouteValidationError()
    {
        $this->loginAsSupplier();

        $response = $this->get('/api/v1/supplier/order-route/111');
        
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 404,
                'message' => 'Not Found',
                'errors' => [
                    'order_id' => ['The selected order id is invalid.']
                ]
        ], $responseData);
    }


    public function testAssignDeliverySupplierFail()
    {        
        $this->loginAsMerchant();

        $order = factory(Order::class)->create([
            'status' => 2,
            'ref_no' => 'BNF-AAAAEF'
        ]);

        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => $order->ref_no
        ]);

        $response->assertStatus(404);        
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans("errors.unable_to_assign_order"), $responseData['message']);

        $this->loginAsAdmin();

        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => $order->ref_no
        ]);

        $response->assertStatus(404);        
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans("errors.unable_to_assign_order"), $responseData['message']);

        $this->loginAsSupplier();

        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_DELIVERING,
            'ref_no' => 'BNF-AAAAAF'
        ]);

        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id' => auth()->user()->deliverySupplier->id,
            'location_status_id' => OrderRouteLocationType::ID_PICKUP
        ]);
        $order->orderRoute = $orderRoute;
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => $order->ref_no
        ]);
        $response->assertStatus(400);        
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans("errors.order_route_not_complete",['route'=> $orderRoute->subroute->name]), $responseData['message']);    
    }

    public function testAssignSupplierFailDependingOnSatus()
    {        
        $this->loginAsSupplier();

        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ON_PROGRESS,
            'ref_no'   => 'BNF-AAAAAF'
        ]);
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => $order->ref_no
        ]);

        $response->assertStatus(400);        
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'ref_no' => [trans('errors.order_not_allow_status',['order_status'=> OrderStatusType::AVAILABLES[$order->status]])]
                ]
        ], $responseData);

    }

    public function testAssignSupplierFailWhenGotWrongRefNo()
    {        
        $this->loginAsSupplier();

        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ON_PROGRESS,
            'ref_no'   => 'BNF-AAAAAF'
        ]);
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => 'aaaaaaaaa'
        ]);

        $response->assertStatus(400);        
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'ref_no' => ['The selected ref no is invalid.']
                ]
        ], $responseData);

    }

    public function testAssignSupplierCanAssignWithSmallLetter()
    {        
        $this->loginAsSupplier();

        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ALLOCATION,
            'ref_no'   => 'BNF-AAAAAF'
        ]);
        $orderRoute     = factory(OrderRoute::class)->create([
                                'order_id' => $order->id,
                            ]);
        $order->orderRoute = $orderRoute;
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => 'bnf-aaaaaf'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();    

        $this->assertEquals('success',$responseData['message']);
    }

    public function testAssignSupplierCanAssignWithoutPrefix()
    {        
        $this->loginAsSupplier();

        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ALLOCATION,
            'ref_no'   => 'BNF-AAAAAF'
        ]);
        $orderRoute     = factory(OrderRoute::class)->create([
                                'order_id' => $order->id,
                            ]);
        $order->orderRoute = $orderRoute;
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => 'aaaaaf'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();    

        $this->assertEquals('success',$responseData['message']);
        
        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ALLOCATION,
            'ref_no'   => 'BNF-AAAAAE'
        ]);
        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
        ]);
        $order->orderRoute = $orderRoute;
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => 'AAAAAE'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();    

        $this->assertEquals('success',$responseData['message']);
    }
    public function testAssignDeliverySupplierSuccess()
    {
        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ALLOCATION,
            'ref_no'   => 'BNF-AAAAAF'
        ]);
        
        $this->loginAsSupplier();
        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
        ]);
        $order->orderRoute = $orderRoute;
        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => $order->ref_no
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();    

        $this->assertEquals('success',$responseData['message']);
        
        $this->assertDatabaseHas(
            Table::ORDER_ROUTE,
            [
                'order_id'=> $order->id,
                'id'=> $orderRoute->id,
                'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
                'sequence' => 1       
            ]
        );

        $this->assertDatabaseHas(
            Table::ORDER,
            [
                'id'=> $order->id,
                'status'=> OrderStatusType::ID_ASSIGNED              
            ]
        );
    }

    public function testGetSupplierOrderRouteSuccess()
    {
        $this->loginAsSupplier();

        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $response = $this->get('/api/v1/supplier/order-route/'.$data['order']->id);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['data']));
    }

    public function testChangeOrderRouteLocationStartSuccess()
    {
        $this->loginAsSupplier();

        $data = CreateCompletedOrderFactory::createOrder(1000,2,OrderStatusType::ID_ASSIGNED);

        $data['orderRoute']->update(['delivery_supplier_id'=>auth()->user()->deliverySupplier->id]);
        $response = $this->put('/api/v1/supplier/order-route-location',[
            'order_id' => $data['orderRoute']->order_id,
            'sub_route_id' => $data['orderRoute']->sub_route_id
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.successfully_changed_order_route',['route'=>$data['orderRoute']->subroute->name]),$responseData['message']);             
        
        $this->assertDatabaseHas(
            Table::ORDER_ROUTE,
            [
                'order_id'=> $data['orderRoute']->order_id,
                'id'=> $data['orderRoute']->id,
                'sub_route_id'=> $data['orderRoute']->sub_route_id,
                'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
                'location_status_id'=> OrderRouteLocationType::ID_PICKUP               
            ]
        );

        $this->assertDatabaseHas(
            Table::ORDER,
            [
                'id'=> $data['order']->id,
                'status'=> OrderStatusType::ID_DELIVERING              
            ]
        );
    }
    
    public function testChangeOrderRouteLocationCompleteSuccess()
    {
        $this->loginAsSupplier();

        $data = CreateCompletedOrderFactory::createOrder(1000,2,OrderStatusType::ID_DELIVERING);

        $data['orderRoute']->update(['delivery_supplier_id'=>auth()->user()->deliverySupplier->id,'location_status_id'=>OrderRouteLocationType::ID_PICKUP]);
        
        $response = $this->put('/api/v1/supplier/order-route-location',[
            'order_id' => $data['orderRoute']->order_id,
            'sub_route_id' => $data['orderRoute']->sub_route_id
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.successfully_changed_order_route',['route'=>$data['orderRoute']->subroute->name]),$responseData['message']);             
        
        $this->assertDatabaseHas(
            Table::ORDER_ROUTE,
            [
                'order_id'=> $data['orderRoute']->order_id,
                'id'=> $data['orderRoute']->id,
                'sub_route_id'=> $data['orderRoute']->sub_route_id,
                'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
                'location_status_id'=> OrderRouteLocationType::ID_DELIVER               
            ]
        );

    }

    public function testChangeOrderRouteLocationValidationError()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2,2);

        $response = $this->put('/api/v1/supplier/order-route-location', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'order_id' => ['The order id field is required.'],
                    'sub_route_id' => ['The sub route id field is required.']
                ]
        ], $responseData);

        $response = $this->put('/api/v1/supplier/order-route-location', ['sub_route_id'=>$data['orderRoute']->sub_route_id]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [                   
                    'order_id' => ['The order id field is required.']
                ]
        ], $responseData);

        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=>$data['orderRoute']->order_id]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [                   
                    'sub_route_id' => ['The sub route id field is required.']
                ]
        ], $responseData);

        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=> 1234,'sub_route_id'=>1234]);

        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [                   
                    'order_id' => ['The selected order id is invalid.'],
                    'sub_route_id' => ['The selected sub route id is invalid.']
                ]
        ], $responseData);
        
        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=>$data['orderRoute']->order_id,'sub_route_id'=> 1234]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [                   
                    'sub_route_id' => ['The selected sub route id is invalid.']
                ]
        ], $responseData);
    }

    public function testChangeOrderRouteLocationFail()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2,OrderStatusType::ID_ALLOCATION);

        $this->loginAsMerchant();

        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=>$data['orderRoute']->order_id,'sub_route_id'=>$data['orderRoute']->sub_route_id]);
        $response->assertStatus(404);        
        $responseData = $response->decodeResponseJson();        
        $this->assertEquals(trans("errors.unable_to_assign_order"), $responseData['message']);

        $this->loginAsSupplier();

        $data['orderRoute']->update(['delivery_supplier_id'=>5,'location_status_id'=>OrderRouteLocationType::ID_PICKUP]);

        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=>$data['orderRoute']->order_id,'sub_route_id'=>$data['orderRoute']->sub_route_id]);
        $response->assertStatus(404);        
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans("errors.mismatch_delivery_supplier"), $responseData['message']);

        $data['orderRoute']->update(['delivery_supplier_id'=>auth()->user()->deliverySupplier->id,'location_status_id'=>OrderRouteLocationType::ID_DELIVER]);
         
        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=>$data['orderRoute']->order_id,'sub_route_id'=>$data['orderRoute']->sub_route_id]);        
        $response->assertStatus(404);        
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans("errors.order_route_completed"), $responseData['message']);
    }

    public function testChangeOrderRouteLocationFailDependingOnSatus()
    {        
        $this->loginAsSupplier();

        $data = CreateCompletedOrderFactory::createOrder(1000,2,OrderStatusType::ID_ON_PROGRESS,'BNF-AAAAAA');

        $response = $this->put('/api/v1/supplier/order-route-location', ['order_id'=>$data['orderRoute']->order_id,'sub_route_id'=>$data['orderRoute']->sub_route_id]);

        $response->assertStatus(400);        
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'order_id' => [trans('errors.order_not_allow_status',['order_status'=> OrderStatusType::AVAILABLES[$data['order']->status]])]
                ]
        ], $responseData);

    }

    public function testProofOrderRouteLocationValidationError()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2);

        $response = $this->post('/api/v1/supplier/order-route-location/proof', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'order_route_id' => ['The order route id field is required.'],
                    'location_status_id' => ['The location status id field is required.'],
                    'proof_type_id' => ['The proof type id field is required.'],
                    'proof' => ['The proof field is required.'],
                ]
        ], $responseData);

         $response = $this->post('/api/v1/supplier/order-route-location/proof', ['order_route_id'=> 1234, 'location_status_id'=>1234, 'proof_type_id' =>1234, 'proof' => 'Testing']);

        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();
        
        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [                   
                    'order_route_id' => ['The selected order route id is invalid.'],
                    'location_status_id' => ['The selected location status id is invalid.'],
                    'proof_type_id' => ['The selected proof type id is invalid.']
                ]
        ], $responseData);
    }

    public function testProofOrderRouteLocationFail()
    {
        $this->loginAsSupplier();

        $order = factory(Order::class)->create();

        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
            'location_status_id'=> OrderRouteLocationType::ID_PICKUP
        ]);

        $response = $this->post('/api/v1/supplier/order-route-location/proof',[
            'order_route_id' => $orderRoute->id,
            'location_status_id'=> OrderRouteLocationType::ID_PICKUP,
            'proof_type_id' => OrderProofType::ID_IMAGE,
            'proof' =>'data:image/pdf;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/'
        ]);
        
        $responseData = $response->decodeResponseJson();
            $this->assertEquals([
                'http_code' => 415,
                'message' => trans('errors.incorrect_image_format'),
                'errors' => []
        ], $responseData); 
    }

    public function testProofOrderRouteLocationPickUpSuccess()
    {
        $this->loginAsSupplier();

        $order = factory(Order::class)->create();

        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
            'location_status_id'=> OrderRouteLocationType::ID_PICKUP
        ]);
        
        $response = $this->post('/api/v1/supplier/order-route-location/proof',[
            'order_route_id' => $orderRoute->id,
            'location_status_id'=> OrderRouteLocationType::ID_PICKUP,
            'proof_type_id' => OrderProofType::ID_PIN,
            'proof' =>'Pin Proof'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.order_route_location_proof_success'),$responseData['message']);             
        
        $this->assertDatabaseHas(
            Table::ORDER_ROUTE_PROOF,
            [
                'order_route_id' => $orderRoute->id,
                'location_status_id'=> OrderRouteLocationType::ID_PICKUP,
                'proof_type_id' => OrderProofType::ID_PIN,
                'proof' =>'Pin Proof'             
            ]
        );

        $order = factory(Order::class)->create();

        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
            'location_status_id'=> OrderRouteLocationType::ID_PICKUP
        ]);

        $response = $this->post('/api/v1/supplier/order-route-location/proof',[
            'order_route_id' => $orderRoute->id,
            'location_status_id'=> OrderRouteLocationType::ID_PICKUP,
            'proof_type_id' => OrderProofType::ID_IMAGE,
            'proof' =>'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxAPDhAPDw8QDxAVEBUPDw8PDRAPFRUWFhUSFRYYHSggGBolHRYVITEhJSkrLi4uFyAzODMsNygtLisBCgoKDg0OGhAQGi0lICUuLS4rKystLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABAECAwUGBwj/xABIEAACAgACBQYICwcDBAMAAAAB'
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.order_route_location_proof_success'),$responseData['message']);             
        
        $imageName = OrderRouteProof::where('order_route_id',$orderRoute->id)->where('location_status_id',OrderRouteLocationType::ID_PICKUP)->where('proof_type_id',OrderProofType::ID_IMAGE)->first()->proof;

        Storage::disk('uploads')->assertExists($imageName);


        $this->assertDatabaseHas(
            Table::ORDER_ROUTE_PROOF,
            [
                'order_route_id' => $orderRoute->id,
                'location_status_id'=> OrderRouteLocationType::ID_PICKUP,
                'proof_type_id' => OrderProofType::ID_IMAGE,
                'proof' =>$imageName             
            ]
        );

    } 
    
    public function testAssignDeliverySupplierNotAvailable()
    {
        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ALLOCATION,
            'ref_no'   => 'BNF-AAAAAA'
        ]);
        $user = $this->loginAsSupplier();
        $supplier = $user->deliverySupplier;
        $supplier->update(['available' => 0]);
        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
        ]);
        $order->orderRoute = $orderRoute;

        $response = $this->post('/api/v1/supplier/assign-delivery',[
            'ref_no' => $order->ref_no
        ]);

        $response->assertStatus(404);
             
        $responseData = $response->decodeResponseJson();    
        $this->assertEquals(trans('errors.unable_to_assign_order'),$responseData['message']);             

        
     
    }

    public function testProofOrderRouteLocationDeliverSuccess()
    {
        $this->loginAsSupplier();

        $order = factory(Order::class)->create();

        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
            'location_status_id'=> OrderRouteLocationType::ID_DELIVER
        ]);
        
        $response = $this->post('/api/v1/supplier/order-route-location/proof',[
            'order_route_id' => $orderRoute->id,
            'location_status_id'=> OrderRouteLocationType::ID_DELIVER,
            'proof_type_id' => OrderProofType::ID_PIN,
            'proof' =>'Pin Proof'
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.order_route_location_proof_success'),$responseData['message']);             
        
        $this->assertDatabaseHas(
            Table::ORDER_ROUTE_PROOF,
            [
                'order_route_id' => $orderRoute->id,
                'location_status_id'=> OrderRouteLocationType::ID_DELIVER,
                'proof_type_id' => OrderProofType::ID_PIN,
                'proof' =>'Pin Proof'             
            ]
        );

        $order = factory(Order::class)->create();

        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'delivery_supplier_id'=> auth()->user()->deliverySupplier->id,
            'location_status_id'=> OrderRouteLocationType::ID_DELIVER
        ]);

        $response = $this->post('/api/v1/supplier/order-route-location/proof',[
            'order_route_id' => $orderRoute->id,
            'location_status_id'=> OrderRouteLocationType::ID_DELIVER,
            'proof_type_id' => OrderProofType::ID_IMAGE,
            'proof' =>'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxAPDhAPDw8QDxAVEBUPDw8PDRAPFRUWFhUSFRYYHSggGBolHRYVITEhJSkrLi4uFyAzODMsNygtLisBCgoKDg0OGhAQGi0lICUuLS4rKystLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABAECAwUGBwj/xABIEAACAgACBQYICwcDBAMAAAAB'
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(trans('alerts.order_route_location_proof_success'),$responseData['message']);             
        
        $imageName = OrderRouteProof::where('order_route_id',$orderRoute->id)->where('location_status_id',OrderRouteLocationType::ID_DELIVER)->where('proof_type_id',OrderProofType::ID_IMAGE)->first()->proof;

        Storage::disk('uploads')->assertExists($imageName);


        $this->assertDatabaseHas(
            Table::ORDER_ROUTE_PROOF,
            [
                'order_route_id' => $orderRoute->id,
                'location_status_id'=> OrderRouteLocationType::ID_DELIVER,
                'proof_type_id' => OrderProofType::ID_IMAGE,
                'proof' =>$imageName             
            ]
        );

        $this->assertDatabaseHas(
            Table::ORDER,
            [
                'id'=> $order->id,
                'status'=> OrderStatusType::ID_DELIVERED              
            ]
        );

    } 

    public function testMechantOrderValidationError()    
    {
        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/merchant/order',[
            'merchant_ref_id' => $merchant->merchant_ref_id,
            'filter' => 'error,1',
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id.'&filter=error,1', $merchant->merchant_secret)
        ]);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'filter_field.0' => ['The selected filter_field.0 is invalid.']
                ]
        ], $responseData);
    }

    public function testMecahntOrderSuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $order = factory(Order::class)->create([
            'status'   => OrderStatusType::ID_ON_PROGRESS,
            'user_defined_1' => '1',
            'merchant_id' => $merchant->id,
            'ref_no' => 'BNF-AAAAAF'
        ]);
        $orderRoute     = factory(OrderRoute::class)->create([
            'order_id' => $order->id
        ]);

        $orderRoute     = factory(OrderPayment::class)->create([
            'order_id' => $order->id,
            'payment_method' => PaymentMethodType::NAME_PAY_BY_MERCHANT,
            'payment_complete' => 1
        ]);

        $response = $this->post('/api/v1/merchant/order',[
            'merchant_ref_id' => $merchant->merchant_ref_id,
            'merchant' => $merchant,
            'filter' => 'status_id,1|user_defined_1,1',
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id.'&filter=status_id,1|user_defined_1,1', $merchant->merchant_secret)
        ]);

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['data']));

        $response = $this->post('/api/v1/merchant/order',[
            'merchant_ref_id' => $merchant->merchant_ref_id,
            'merchant' => $merchant,
            'hash_value'=> (new HashService())->encrypt('merchant_ref_id='.$merchant->merchant_ref_id, $merchant->merchant_secret)
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['data']));

    }

}
