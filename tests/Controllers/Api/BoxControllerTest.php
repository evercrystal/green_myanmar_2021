<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Box\Entities\Box;
use Tests\TestCase;

class BoxControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testGetBoxDataSuccess()
    {
        $box = factory(Box::class)->create();

        $response = $this->get('/api/v1/box');
        
        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();

        $this->assertEquals(1, sizeof($responseData['data']));
    }
}
