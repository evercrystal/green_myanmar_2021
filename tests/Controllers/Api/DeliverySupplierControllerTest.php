<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Tests\TestCase;

class DeliverySupplierControllerTest extends TestCase
{
    use RefreshDatabase,WithoutMiddleware;
    
    public function testChangeAvailableSuccess()
    {
        $this->loginAsSupplier();

        $response = $this->put('/api/v1/supplier/change-available');

        $response->assertStatus(200);        
        $response->assertJsonStructure(['available']);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(false, $responseData['available']);
    }
    
    public function testChangeAvailableFail()
    {
        $this->loginAsMerchant();

        $response = $this->put('/api/v1/supplier/change-available');

        $response->assertStatus(404);        

        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans("errors.unable_to_assign_order"), $responseData['message']);
        

        $this->loginAsAdmin();

        $response = $this->put('/api/v1/supplier/change-available');
        
        $response->assertStatus(404);        

        $responseData = $response->decodeResponseJson();

        $this->assertEquals(trans("errors.unable_to_assign_order"), $responseData['message']);

    }

}
