<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Merchant\Entities\Merchant;
use App\Services\HashService;
use Tests\TestCase;

class MerchantControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testVerifyMerchantSuccess()
    {
        $merchant = factory(Merchant::class)->create();

        $response = $this->post('/api/v1/verify-merchant', [
            'merchant_ref_id'=>$merchant->merchant_ref_id,
            'hash_value'=> (new HashService)->encrypt('merchant_ref_id='.$merchant->merchant_ref_id, $merchant->merchant_secret)
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 200,
                'message' => 'success'
        ], $responseData);
    }
}
