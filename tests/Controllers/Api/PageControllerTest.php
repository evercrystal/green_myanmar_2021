<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Page\Entities\Page;
use App\Enums\LanguageType;
use Tests\TestCase;

class PageControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testGetPageRouteValidationError()
    {
        $this->loginAsSupplier();

        $response = $this->get('/api/v1/page/error_page');
        
        $response->assertStatus(404);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 404,
                'message' => 'Not Found',
                'errors' => [
                    'page_name' => ['The selected page name is invalid.']
                ]
        ], $responseData);
    }

    public function testGetPageDataSuccess()
    {
        $page = factory(Page::class)->create([
            'page' => 'aboutus',
        ]);
        $response = $this->get('/api/v1/page/aboutus');
        
        $response->assertStatus(200);

        $responseData = $response->decodeResponseJson();

        $this->assertEquals($response['data']['page'], $page->page);
        $this->assertEquals($response['data']['title'], $page->title);
        $this->assertEquals($response['data']['content'], $page->content);
    }

    public function testGetPageDataFail()
    {
        $page = factory(Page::class)->create([
            'page' => 'aboutus',
            'language_id' => LanguageType::ID_MYANUNI
        ]);
        $response = $this->get('/api/v1/page/aboutus');
        
        $response->assertStatus(400);

        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => trans('alerts.frontend.page.no_page_content_with_accept_language'),
                'errors' => []
        ], $responseData);
    }
}
