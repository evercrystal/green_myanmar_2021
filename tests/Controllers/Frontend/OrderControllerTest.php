<?php

namespace Tests\Controllers\Frontend;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Order\Entities\Order;
use App\Factories\CreateCompletedOrderFactory;


class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testUserRecentOrderSuccess()
    {
        $user = $this->loginAsMerchant();
        $order = factory(Order::class)->create([
            'ref_no' => 'AAAAAA',
            'user_id' => $user->id,
            'merchant_id' => $user->merchant->id
        ]);
        $response = $this->get('/order-form/get-recent-order?value=AAAAAA');

        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['data']));
    }

    public function testUserRecentOrderFail()
    {
        $this->loginAsMerchant();
        $response = $this->get('/order-form/get-recent-order?value=AAAAAA');
        
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(0,sizeOf($responseData['data']));
    }

    public function testTrackOrderSearchValidatonError()
    {
        $response = $this->get('/order/track?search_value=aaa');

        $response->assertSessionHasErrors(['ref_no']);

        $response = $this->get('/order/track?search_value=1234');

        $response->assertSessionHasErrors(['phone']);

    }

    public function testTrackOrderSearchSuccessWithRefNumber()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2,3);
        $response = $this->get('/order/track?search_value='.$data['order']->ref_no);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['order']));
    }

    public function testTrackOrderSearchSuccessWithPhone()
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2,3);
        $response = $this->get('/order/track?search_value='.$data['order']->receiver_phone);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals(1,sizeOf($responseData['order']));
    }

    public function testTrackOrderDetailSuccess() 
    {
        $data = CreateCompletedOrderFactory::createOrder(1000,2,3);
        $response = $this->get('/order/track-detail?ref_no='.$data['order']->ref_no);
        $response->assertStatus(200);
        $this->assertEquals($data['order']->ref_no,$response->original['order']->ref_no);
    }

    public function testTrackOrderDetailFail()
    {
        $response = $this->get('/order/track-detail?ref_no=BNF-AAAAA');
        $response->assertStatus(200);
    
        $this->assertEquals(null,$response->original['order']);    
    }


}
