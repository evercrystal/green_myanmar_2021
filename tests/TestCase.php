<?php

namespace Tests;

use App\Models\Auth\Role;
use App\Models\Auth\User;
use DB;
use Modules\Merchant\Entities\Merchant;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase.
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Create the admin role or return it if it already exists.
     *
     * @return mixed
     */
    protected function getAdminRole()
    {
        if ($role = Role::whereName(config('access.users.admin_role'))->first()) {
            return $role;
        }

        $adminRole = factory(Role::class)->create(['name' => config('access.users.admin_role')]);
        $adminRole->givePermissionTo(factory(Permission::class)->create(['name' => 'view backend']));

        return $adminRole;
    }


    /**
    * Create the user role or return it if it already exists.
    *
    * @return mixed
    */
    protected function getUserRole()
    {
        if ($role = Role::whereName(config('access.users.default_role'))->first()) {
            return $role;
        }

        $memberRole = factory(Role::class)->create(['name' => config('access.users.default_role')]);

        return $memberRole;
    }

    /**
    * Create the admin role or return it if it already exists.
    *
    * @return mixed
    */
    protected function getMemberRole()
    {
        if ($role = Role::whereName(config('access.users.default_role'))->first()) {
            return $role;
        }

        $memberRole = factory(Role::class)->create(['name' => config('access.users.default_role')]);

        return $memberRole;
    }

    /**
     * Create an administrator.
     *
     * @param array $attributes
     *
     * @return mixed
     */
    
    protected function createAdmin(array $attributes = [])
    {
        $adminRole = $this->getAdminRole();
        $admin = factory(User::class)->create($attributes);
        $admin->assignRole($adminRole);

        return $admin;
    }

    /**
     * Create an member.
     *
     * @param array $attributes
     *
     * @return mixed
     */
    protected function createMerchant(array $attributes = [])
    {

        $memberRole = $this->getUserRole();
        $user = factory(User::class)->create($attributes);
        factory(Merchant::class)->create(['user_id'=>$user->id]);
        $user->assignRole($memberRole);

        return $user;
    }

    protected function createSupplier(array $attributes = [])
    {

        $memberRole = $this->getUserRole();
        $user = factory(User::class)->create($attributes);
        factory(DeliverySupplier::class)->create(['user_id'=>$user->id]);
        $user->assignRole($memberRole);

        return $user;
    }

    /**
     * Login the given administrator or create the first if none supplied.
     *
     * @param bool $admin
     *
     * @return bool|mixed
     */
    protected function loginAsAdmin($admin = false)
    {
        if (! $admin) {
            $admin = $this->createAdmin();
        }

        $this->actingAs($admin);

        return $admin;
    }

    protected function loginAsMerchant($member = false)
    {
        if (! $member) {
            $member = $this->createMerchant();
        }

        $this->actingAs($member);

        return $member;

    }

    protected function loginAsSupplier($member = false)
    {
        if (! $member) {
            $member = $this->createSupplier();
        }

        $this->actingAs($member);

        return $member;

    }

    public function generateOAuthClient()
    {
        $this->artisan('passport:install');
        return DB::table('oauth_clients')->where('password_client', 1)->first();
    }
}
