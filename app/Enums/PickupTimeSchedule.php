<?php

namespace App\Enums;

class PickupTimeSchedule
{

    /**
     * @SWG\Definition(
     *   definition="PickupTimeSchedules",
     *   type="object",
     *   required={"id", "name"},
     *   @SWG\Property(
     *       property="id",
     *       description="Pickup Time Schedule ID",
     *       type="integer",
     *       example="1"
     *   ),
     *   @SWG\Property(
     *       property="name",
     *       description="Pickup Time Schedule Name",
     *       type="string",
     *       example="Anytime (9am to 5pm)"
     *   )
     * )
    */
    const ID_ANYTIME = 1;
    const ID_MORNING = 2;
    const ID_AFTERNOON = 3;

    const NAME_ANYTIME = '9-5';
    const NAME_MORNING = '9-12';
    const NAME_AFTERNOON = '1-5';

    const NAMES = [
        self::ID_ANYTIME => self::NAME_ANYTIME,
        self::ID_MORNING => self::NAME_MORNING,
        self::ID_AFTERNOON => self::NAME_AFTERNOON,
    ];

    public static function getName(int $id, bool $translate = true) : string
    {
        $name = self::NAMES[$id];
        return $translate ? trans('labels.pickup_schedule.' . $name) : $name;
    }

    public static function getSettings() : array
    {
        $pickupSettings = [];
        foreach(self::NAMES as $key=>$name)
        {
            $pickupSetting['id'] = $key;
            $pickupSetting['name'] = self::getName($key);
            $pickupSettings[] = $pickupSetting;
        }
        return $pickupSettings;
    }
}
