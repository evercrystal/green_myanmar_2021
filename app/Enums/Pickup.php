<?php

namespace App\Enums;

class Pickup
{
    const ID_TODAY = 1;
    const ID_TOMORROW_MORNING = 2;
    const ID_TOMORROW_AFTERNOON = 3;
    const ID_OTHER = 4;

    const NAME_TODAY = 'today';
    const NAME_TOMORROW_MORNING = 'tomorrow_morning';
    const NAME_TOMORROW_AFTERNOON = 'tomorrow_afternoon';
    const NAME_OTHER = 'other';

    const NAMES = [
        self::ID_TODAY => self::NAME_TODAY,
        self::ID_TOMORROW_MORNING => self::NAME_TOMORROW_MORNING,
        self::ID_TOMORROW_AFTERNOON => self::NAME_TOMORROW_AFTERNOON,
        self::ID_OTHER => self::NAME_OTHER
    ];

    public static function getName(int $id, bool $translate = true) : string
    {
        $name = self::NAMES[$id];
        return $translate ? trans('labels.pickup.' . $name) : $name;
    }

    public static function getSettings() : array
    {
        $pickupPrices = config('pickup.prices');
        $pickupSettings = [];
        foreach(self::NAMES as $key=>$name)
        {
            $pickupSetting['id'] = $key;
            $pickupSetting['name'] = self::getName($key);
            $pickupSetting['price'] = $pickupPrices[$key];
            $pickupSettings[] = $pickupSetting;
        }
        return $pickupSettings;
    }
}
