<?php
namespace App\Enums;

class RecipientTypes
{
    const ID_DELIVERY_CONTACT = 1;
    const ID_PICKUP_CONTACT = 2;

    const NAME_DELIVERY_CONTACT = "Delivery Contact";
    const NAME_PICKUP_CONTACT = "Pickup Contact";

    const NAMES = [
        self::ID_DELIVERY_CONTACT => self::NAME_DELIVERY_CONTACT,
        self::ID_PICKUP_CONTACT => self::NAME_PICKUP_CONTACT
    ];

}
