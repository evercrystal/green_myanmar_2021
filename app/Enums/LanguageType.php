<?php
namespace App\Enums;

class LanguageType
{
    const ID_ENGLISH = 1;
    const ID_MYANUNI = 2;

    const PREFIX_ENGLISH = "en";
    const PREFIX_MYANUNI = "mm";

    const AVAILABLES = [
        self::ID_ENGLISH => self::PREFIX_ENGLISH,
        self::ID_MYANUNI => self::PREFIX_MYANUNI,
    ];

    const AVAILABLE_PREFIXIDS = [
        self::PREFIX_ENGLISH => self::ID_ENGLISH,
        self::PREFIX_MYANUNI => self::ID_MYANUNI,
    ];

    public static function getName(string $languageKey)
    {
        return trans('menus.language-picker.langs.'.$languageKey);
    }

    public static function getLocalKey()
    {
        return LanguageType::AVAILABLE_PREFIXIDS[app()->getLocale()];  
    }
}
