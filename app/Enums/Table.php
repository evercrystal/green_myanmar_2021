<?php

namespace App\Enums;

class Table
{
    const User = 'users';
    const RECYCLE_BIN = 'recycle_bin';
    const COUNTRY = 'countries';
    const DESTINATION = 'destinations';
    const CARRENTAL = 'car_rentals';
    const ITINERARY = 'itinerary';
    const Blog = 'blogs';

}
