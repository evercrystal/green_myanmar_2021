<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use Laravel\Passport\HasApiTokens;
use GMBF\PhoneNumber;
use App\Enums\Table;
/**
 * Class User.
 */
class User extends BaseUser
{
    use HasApiTokens,
    	  UserAttribute,
        UserMethod,
        UserRelationship,
		    UserScope;
		
	protected $table = Table::User;
	
	protected $primaryKey = 'user_id';

  // protected $fillable = ["user_id","uuid","user_name","email","mobile","avatar_type","avatar_location","password","password_changed_at","active","confirmation_code","confirmed","timezone","last_login_at","last_login_ip","to_be_logged_out"];

    /*
	   * Find the user identified by the given $identifier.
	   *
	   * @param $identifier email|phone
	   * @return mixed
   */
  	public function findForPassport($identifier)
  	{
      	$phoneNumber = new PhoneNumber();
      	$mobile = $phoneNumber->add_prefix($identifier);

      	return self::select(['users.*'])
          	->leftjoin('delivery_supplier', 'delivery_supplier.user_id', '=', 'users.id')
          	->orWhere('users.email', $identifier)
          	->orWhere('delivery_supplier.mobile', $mobile)
          	->first();
  	}
}
