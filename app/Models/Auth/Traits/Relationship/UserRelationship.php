<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\SocialAccount;
use App\Models\Auth\PasswordHistory;
/**
 * Class UserRelationship.
 */
trait UserRelationship
{
 
    public function providers()
    {
        return $this->hasMany(SocialAccount::class,'user_id', 'user_id');
    }

    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class,'user_id', 'user_id');
    }
}
