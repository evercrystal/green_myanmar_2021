<?php
namespace App\Services;
use Modules\DeliverySupplier\Entities\DeliverySupplier;

use Illuminate\Support\Facades\DB;

class DeliverySupplierService
{
    public function changeAvailable(DeliverySupplier $deliverySupplier) : bool
    {
        try {
            $deliverySupplier->toggleAvailable()->save();
            return true;            
        }catch (\Exception $e) {
            return false;
        }        
    }    
}
