<?php
namespace App\Services;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\Order\Entities\OrderSupplier;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Illuminate\Support\Facades\DB;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Modules\Order\Enum\OrderRouteLocationType;
use Modules\Order\Enum\OrderStatusType;
use Modules\Township\Entities\Township;

class OrderService
{
    public function assignDeliverySupplier(array $input) : bool
    {
        \DB::beginTransaction();
        try {
            $order = Order::find($input['order_id']);
            if(!$order->delivery_supplier_id){
                $order->status = OrderStatusType::ID_ASSIGNED;
            }
            $order->delivery_supplier_id = $input['delivery_supplier_id'];
            $order->save();
            $orderRoute = OrderRoute::find($input['order_route_id']);
            $orderRoute->delivery_supplier_id = $input['delivery_supplier_id'];
            $orderRoute->remark = isset($input['remark'])? $input['remark'] : null;
            $orderRoute->save();

            $orderSupplier = OrderSupplier::create([
                'order_id' => $input['order_id'],
                'delivery_supplier_id' => $input['delivery_supplier_id'],
            ]);
            \Log::info('Order '.$order->ref_no.' .Order was Assigned by ' . auth()->user()->name);

        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }

        \DB::commit();

       return true;
    }

    public function getOrderRouteData(int $orderId)
    {
        $orderRoutes = OrderRoute::with('subRoute')->where('order_id',$orderId)->orderBy('sequence','asc')->get();
        $orderRoutes = $orderRoutes->map(function ($orderRoute, $key)  {
            $orderRoute['sub_route_name'] = $orderRoute->subRoute->name;
            return $orderRoute;
        });
        return $orderRoutes;
    }

    public function getRecentOrder(string $search)
    {
        $orders = Order::with('fromTownship','toTownship')->where('user_id',auth()->user()->id)
                ->where(function ($query) use ($search) {
                    $query->where('ref_no', 'like', $search.'%')
                          ->orWhereHas('fromTownship', function ($query) use ($search) {
                                $query->where('name', 'like','%'.$search.'%');
                            })
                          ->orWhereHas('toTownship', function ($query) use ($search) {
                                $query->where('name', 'like','%'.$search.'%');
                            })
                          ->orWhere('user_id', 'like', '%'.$search.'%')
                          ->orWhere('id', $search);
                })->paginate(10);

        return $orders;
    }

    public function getCollectionPointAddress(int $id)
    {
        $collectionAddress = CollectionPoint::Where('id', $id)->pluck('address');
        return $collectionAddress;
    }

    public function getTownshipAndCollection(string $search , int $cityId) 
    {
        $allTownships = Township::with('city')->where('city_id', $cityId);
        $townshipIds = $allTownships->pluck('id')->toArray();

        $townships = $allTownships->where(function ($query) use ($search) {
                        $query->where('name', 'like', $search.'%')
                              ->orWhereHas('city', function ($query) use ($search) {
                                    $query->where('name', 'like','%'.$search.'%');
                                });
                    })->limit(10)->get();   

        $collections =  CollectionPoint::with('township')->whereIn('township_id', $townshipIds)
                        ->where(function ($query) use ($search) {
                            $query->where('name', 'like', $search.'%')
                                  ->orWhere('address', 'like', '%'.$search.'%')
                                  ->orWhereHas('township', function ($query) use ($search) {
                                        $query->where('name', 'like','%'.$search.'%');
                                    });
                        })->limit(10)->get();

        return  $collections->concat($townships)->paginate(10);

    }

}
