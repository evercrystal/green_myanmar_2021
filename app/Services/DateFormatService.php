<?php
namespace App\Services;
use Carbon\Carbon;

class DateFormatService
{
    public static function changeDateTimeFormat(Carbon $dateTime) : string
    {
        return Carbon::parse($dateTime)->format(config('appsetting.basic.datetime_format'));
    }

    
}
