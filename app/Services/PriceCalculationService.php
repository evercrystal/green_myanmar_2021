<?php
namespace App\Services;
use Modules\Merchant\Entities\Merchant;
use Modules\Box\Entities\Box;
use Modules\Order\Entities\Order;
use Modules\ProductType\Entities\ProductType;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;
use Modules\Township\Entities\Township;
use Modules\Order\Enum\PaymentModeType;
use App\Enums\PickupTimeSchedule;
use App\Enums\Pickup;
use Carbon\Carbon as Carbon;

class PriceCalculationService
{

    public function priceForMerchantOrder(array $input) : array
    {
        $pickupScheduleId = $input['pickup_schedule_id'];
        $pickupDate = $input['pickup_date'];
        $weight = $input['weight'];
        $pickupCollectionId = isset($input['pickup_collection_point']) ? $input['pickup_collection_point'] : null;
        $deliveryCollectionId = isset($input['delivery_collection_point']) ? $input['delivery_collection_point'] : null;
    	$fromTownship = Township::find($input['from_township_id']);
    	$toTownship = Township::find($input['to_township_id']);
        $productType = ProductType::find($input['product_type_id']);
        $box = Box::find($input['box_id']);
        $data['total_amount'] = 0;
        $data['total_add_on_weight'] = 0;
        $data['sub_routes'] = null;
        $data['from_city'] = null;
        $data['to_city'] = null;
        $data['access_delivery_time'] = false;

        $subRoutes = SubRoute::with('pickupCollectionPoint','deliveryCollectionPoint','route','productType','box');
        if($fromTownship->city->name == $toTownship->city->name)
        {
            $subRoutes = $subRoutes->where(function($query) use ($fromTownship,$pickupCollectionId){
                $query->where('from_township_id',$fromTownship->id);
                if($pickupCollectionId) {
                    $query->whereHas('pickupCollectionPoint', function ($query) use ($pickupCollectionId) {
                        return  $query->where('collection_point_id', $pickupCollectionId);
                    });
                } else {
                    $query->whereDoesntHave('pickupCollectionPoint');
                }
            });

            $subRoutes = $subRoutes->where(function($query) use ($toTownship,$deliveryCollectionId){
                $query->where('to_township_id',$toTownship->id);
                if($deliveryCollectionId) {
                    $query->whereHas('deliveryCollectionPoint', function ($query) use ($deliveryCollectionId) {
                        return  $query->where('collection_point_id', $deliveryCollectionId);
                    });
                } else {
                    $query->whereDoesntHave('deliveryCollectionPoint');
                }
            });

        } else {
            $subRoutes = SubRoute::with('route','productType','box');

            $subRoutes = $subRoutes->whereHas('route', function ($query) use ($fromTownship,$toTownship) {
                return  $query->where('from_city_id',$fromTownship->city->id)->where('to_city_id',$toTownship->city->id)->where('is_active',true);
            });

            $subRoutes = $subRoutes->where(function($subRoute) use ($fromTownship,$toTownship,$pickupCollectionId,$deliveryCollectionId) {
                $subRoute->where(function($query) use ($fromTownship,$pickupCollectionId){
                    $query->where('from_township_id',$fromTownship->id);
                    if($pickupCollectionId){
                        $query->whereHas('pickupCollectionPoint', function ($query) use ($pickupCollectionId) {
                            return  $query->where('collection_point_id', $pickupCollectionId);
                        });
                    }else {
                        $query->whereDoesntHave('pickupCollectionPoint');
                    }
                });
                $subRoute->orWhere('is_city_route',true);
                $subRoute->orWhere(function($query) use ($toTownship,$deliveryCollectionId){
                    $query->where('to_township_id',$toTownship->id);
                    if($deliveryCollectionId){
                        $query->whereHas('deliveryCollectionPoint', function ($query) use ($deliveryCollectionId) {
                            return  $query->where('collection_point_id', $deliveryCollectionId);
                        });
                    }else {
                        $query->whereDoesntHave('deliveryCollectionPoint');
                    }
                });
            });

        }

    	$subRoutes->whereHas('productType', function ($query) use ($productType) {
            return  $query->where('name', $productType->name);
        });

        $boxIdArrs = $subRoutes->pluck('box_id')->toArray();

        if(in_array($input['box_id'], $boxIdArrs)){
            $subRoutes = $subRoutes->where('box_id',$input['box_id']);
        } else {
            $subRoutes = $subRoutes->where('box_id',null);
        }

        if($subRoutes->count()) {
            $subRoutes= $subRoutes->get();
            $subRoutes = $subRoutes->map(function ($subRoute, $key) use ($weight) {
                if($subRoute->max_weight < $weight) {
                    $subRoute['add_on_weight'] = $subRoute->add_on_weight_rate_mmk * ($weight - $subRoute->max_weight);
                } else {
                    $subRoute['add_on_weight'] = 0;
                }
                return $subRoute;
            });
            $totalAddOnWeight = $subRoutes->sum('add_on_weight');
            $totalDurationMins = $subRoutes->sum('duration');
            $totalBlockMins = $totalDurationMins + config('appsetting.order.before_block_minutes');
    		$totalAmount = $subRoutes->sum('mmk_price') + $totalAddOnWeight;
            $subRouteIds = implode(',',$subRoutes->pluck('id')->toArray());

            $accessDeliveryTime = $this->accessDeliveryScheduleTime($totalBlockMins,$pickupScheduleId,$pickupDate);

            if($accessDeliveryTime){
                $totalAmount += $this->pickupPriceOnDate($pickupDate,$pickupScheduleId);
            }

            $data['total_amount'] = $totalAmount;
            $data['access_delivery_time'] = $accessDeliveryTime;
            $data['total_add_on_weight'] = $totalAddOnWeight;
            $data['sub_routes'] = $subRouteIds;
            $data['from_city'] = $fromTownship->city->name;
            $data['to_city'] = $toTownship->city->name;
        }
            

    	return $data;

    }

    public function priceForSenderClearenceAmount(Order $order, array $input) : float
    {
        $paymentModeId = $input['payment_mode_id'];
        $pickupScheduleId = $input['pickup_schedule_id'];
        $pickupDate = $input['pickup_date'];
        $orderRoute = $order->orderRoute;
        $price = 0;

        if($orderRoute->count() == 1) {
            if($paymentModeId == PaymentModeType::ID_SENDER_WILL_PAID_ALL_ROUTE || $paymentModeId == PaymentModeType::ID_SENDER_WILL_PAID_TO_GATE || $paymentModeId == PaymentModeType::ID_SENDER_WILL_PAID_TO_RECEIVER_GATE) {
                $price = $orderRoute->first()->subRoute->mmk_price + $input['total_add_on_weight'] + $this->pickupPriceOnDate($pickupDate,$pickupScheduleId);
            }
        } else {
            $subRouteIdArr = $orderRoute->pluck('sub_route_id')->toArray();
            if($paymentModeId == PaymentModeType::ID_SENDER_WILL_PAID_ALL_ROUTE) {
                $subRoutePrice = SubRoute::whereIn('id',$subRouteIdArr)->sum('mmk_price');
                $price = $subRoutePrice + $input['total_add_on_weight'] + $this->pickupPriceOnDate($pickupDate,$pickupScheduleId);

            } else if($paymentModeId == PaymentModeType::ID_SENDER_WILL_PAID_TO_GATE){
                $subRoutePrice = SubRoute::whereIn('id',$subRouteIdArr)->where('from_township_id',$order->from_township_id)->sum('mmk_price');
                $price = $subRoutePrice + $input['total_add_on_weight'] + $this->pickupPriceOnDate($pickupDate,$pickupScheduleId);

            } else if($paymentModeId == PaymentModeType::ID_SENDER_WILL_PAID_TO_RECEIVER_GATE){
                $subRoutePrice = SubRoute::whereIn('id',$subRouteIdArr)->where(function($query) use ($order){
                    $query->where('from_township_id',$order->from_township_id)->orWhere('is_city_route',1);
                })->sum('mmk_price');
                $price = $subRoutePrice + $input['total_add_on_weight'] + $this->pickupPriceOnDate($pickupDate,$pickupScheduleId);

            } else {
                $price = 0;
            }
        }

        return $price;
    }


    public function accessDeliveryScheduleTime(int $totalBlockMins, int $pickupScheduleId, string $pickupDate) : bool
    {
        $now = Carbon::now();
        $routeAccessDateTime = $now->addMinutes($totalBlockMins);

        $pickupScheduleHours = config('pickupSchedule.hours')[$pickupScheduleId];

        $pickupDate = Carbon::create($pickupDate);
        $pickupDateTime = $pickupDate->addHours($pickupScheduleHours);

        if($pickupDateTime->gt($routeAccessDateTime)) {
            return true;
        }
        return false; 
    }

    public function pickupPriceOnDate(string $pickupDate, int $pickupScheduleId) : float
    {
        if($pickupDate == Carbon::now()->format('Y-m-d')){
           $pickupPrice =  config('pickup.prices')[Pickup::ID_TODAY];
        } else if (($pickupDate == Carbon::now()->addDays(1)->format('Y-m-d')) && ($pickupScheduleId == PickupTimeSchedule::ID_MORNING)) {
            $pickupPrice = config('pickup.prices')[Pickup::ID_TOMORROW_MORNING];
        } else if (($pickupDate == Carbon::now()->addDays(1)->format('Y-m-d')) && ($pickupScheduleId == PickupTimeSchedule::ID_AFTERNOON || $pickupScheduleId == PickupTimeSchedule::ID_ANYTIME)) {
            $pickupPrice = config('pickup.prices')[Pickup::ID_TOMORROW_AFTERNOON];
        } else {
            $pickupPrice = config('pickup.prices')[Pickup::ID_OTHER];
        }

        return $pickupPrice;
    }

    public function getOrderRemainingBalance(Order $order) : float
    {
        $amount = 0;
        $completeOrderPayment = $order->orderPayment->where('payment_complete',1);
        $amount = $completeOrderPayment->sum('amount') - $completeOrderPayment->sum('service_fee');

        return $order->total_amount - $amount;
    }

    
}
