<?php
namespace App\Services;

use App\Models\BNFEncryption;
use Modules\Order\Entities\OrderPayment;
use Modules\Order\Enum\PaymentMethodType;

class PaymentService
{
    public function getServiceFees($amount) : array
    {
        $serviceFee['mpu'] = config('appsetting.payment.mpu.enable');
        $serviceFee['transfer'] = config('appsetting.payment.transfer.enable');
        $serviceFee['visa_master'] = config('appsetting.payment.visa_master.enable');
        $serviceFee['bnf_topup'] = config('appsetting.payment.topup.enable');
        $serviceFee['cod'] = config('appsetting.payment.cod.enable');

        $serviceFee['mpu_fee'] = '';
        $serviceFee['transfer_fee'] = '';
        $serviceFee['visa_master_fee'] = '';
        $serviceFee['bnf_topup_fee'] = '';
        $serviceFee['cod_fee'] = '';

        if (config('appsetting.payment.mpu.enable') == "true") {
            if (config('appsetting.payment.mpu.charge_type') == "percentage") {
                $serviceFee['mpu_fee'] = floatval((config('appsetting.payment.mpu.charge') / 100) * $amount);
            } else {
                $serviceFee['mpu_fee'] = floatval(config('appsetting.payment.mpu.charge'));
            }
        }

        if (config('appsetting.payment.transfer.enable') == "true") {
            if (config('appsetting.payment.transfer.charge_type') == "percentage") {
                $serviceFee['transfer_fee'] = floatval((config('appsetting.payment.transfer.charge') / 100) * $amount);
            } else {
                $serviceFee['transfer_fee'] = floatval(config('appsetting.payment.transfer.charge'));
            }
        }

        if (config('appsetting.payment.visa_master.enable') == "true") {
            if (config('appsetting.payment.visa_master.charge_type') == "percentage") {
                $serviceFee['visa_master_fee'] = floatval((config('appsetting.payment.visa_master.charge') / 100) * $amount);
            } else {
                $serviceFee['visa_master_fee'] = floatval(config('appsetting.payment.visa_master.charge'));
            }
        }

        if (config('appsetting.payment.topup.enable') == "true") {
            if (config('appsetting.payment.topup.charge_type') == "percentage") {
                $serviceFee['bnf_topup_fee'] = floatval((config('appsetting.payment.topup.charge') / 100) * $amount);
            } else {
                $serviceFee['bnf_topup_fee'] = floatval(config('appsetting.payment.topup.charge'));
            }
        }

        if (config('appsetting.payment.cod.enable') == "true") {
            if (config('appsetting.payment.cod.charge_type') == "percentage") {
                $serviceFee['cod_fee'] = floatval((config('appsetting.payment.cod.charge') / 100) * $amount);
            } else {
                $serviceFee['cod_fee'] = floatval(config('appsetting.payment.cod.charge'));
            }
        }

        return $serviceFee;
    }

    public function mpu($encryptId, $totalAmount) : array
    {
        $id = BNFEncryption::decode($encryptId);

        $amount = $totalAmount . "00";

        $amount = str_pad($amount, 12, '0', STR_PAD_LEFT);

        $secretKey = config('appsetting.payment.mpu.hash_key');   //Get SecretKey from 2C2P Merchant Interface

        $version = config('appsetting.payment.mpu.version');
        $merchantId = config('appsetting.payment.mpu.merchant_id');
        $currency = "104";
        $customerEmail = "";
        $payCategoryId = "";
        $promotion = "";
        $userDefined_1 = $encryptId;

        $orderPayment = OrderPayment::find($id);

        $paymentDescription = app_name() . ' ref: ' . $orderPayment->order->ref_no;
        $invoiceNo = $orderPayment->order->ref_no.$id;
        $userDefined_2 = "order";
        $userDefined_3 = "";
        $orderId = str_pad($orderPayment->id, 20, '0', STR_PAD_LEFT);

        $resultUrl_1 = url("/order/payment-complete/mpu");
        $resultUrl_2 = url("/order/payment-status/mpu");

        $userDefined_4 = "";
        $userDefined_5 = "";

        $paymentOption = "M";
        $request_3ds = "";
        $stringToHash = $version . $merchantId . $paymentDescription . $orderId . $invoiceNo . $currency . $amount . $customerEmail . $payCategoryId . $promotion . $userDefined_1 . $userDefined_2 . $userDefined_3 . $userDefined_4 . $userDefined_5 . $resultUrl_1 . $resultUrl_2 . $paymentOption . $request_3ds ;
        $hash = strtoupper(hash_hmac('sha1', $stringToHash, $secretKey, false));

        $form['url'] = config('appsetting.payment.mpu.payment_url');
        $form['values'] =
              "<input type='hidden' id='version' name='version' value='" .$version. "'/>".
              "<input type='hidden' id='merchant_id' name='merchant_id' value='" .$merchantId. "'/>".
              "<input type='hidden' id='payment_description' name='payment_description' value='" .$paymentDescription. "' /> ".
              "<input type='hidden' id='order_id' name='order_id' value='" .$orderId. "' />    ".
              "<input type='hidden' id='invoice_no' name='invoice_no' value='" .$invoiceNo. "' />".
              "<input type='hidden' id='currency' name='currency' value='" .$currency. "'/>".
              "<input type='hidden' id='amount' name='amount' value='" .$amount. "'/>".
              "<input type='hidden' id='customer_email' name='customer_email' value='" .$customerEmail. "'/>".
              "<input type='hidden' id='pay_category_id' name='pay_category_id' value='" .$payCategoryId. "'/>".
              "<input type='hidden' id='promotion' name='promotion' value='" .$promotion. "'/>".
              "<input type='hidden' id='user_defined_1' name='user_defined_1' value='" .$userDefined_1. "'/>".
              "<input type='hidden' id='user_defined_2' name='user_defined_2' value='" .$userDefined_2. "'/>".
              "<input type='hidden' id='user_defined_3' name='user_defined_3' value='" .$userDefined_3. "'/>".
              "<input type='hidden' id='user_defined_4' name='user_defined_4' value='" .$userDefined_4. "'/>".
              "<input type='hidden' id='user_defined_5' name='user_defined_5' value='" .$userDefined_5. "'/>".
              "<input type='hidden' id='result_url_1' name='result_url_1' value='" .$resultUrl_1. "'/>".
              "<input type='hidden' id='result_url_2' name='result_url_2' value='" .$resultUrl_2. "'/>".
              "<input type='hidden' id='payment_option' name='payment_option' value='" .$paymentOption. "'/>".
              "<input type='hidden' id='request_3ds' name='request_3ds' value='" .$request_3ds. "'/>".
              "<input type='hidden' id='hash_value' name='hash_value' value='" .$hash. "'/>";

        return $form;
    }

    public function transfer($encryptId, $totalAmount) : array
    {
        $orderPaymentId = BNFEncryption::decode($encryptId);
        $orderPayment = OrderPayment::find($orderPaymentId);
        $form['url'] = url('order/payment-complete/bank_transfer/' . $encryptId);
        $form['values'] = trans('labels.frontend.payment.transfer_info', [ 'amount' => $totalAmount, 'payment_description' => 'Payment for Ticket No. ' . $orderPayment->order->ref_no]).'<input type="hidden" name="booking_ref" value="' . $orderPayment->order->ref_no.$orderPaymentId . '"> 
                            <div class="control-group" >
                                <label class="control-label">' . trans('labels.frontend.payment.bank_transfer') . '</label>
                            <div class="controls">
                                <select class="form-control" name="bank_name">';

        if (config('appsetting.payment.transfer.transfer_1')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_1') . '">' . config('appsetting.payment.transfer.transfer_1') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_2')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_2') . '">' . config('appsetting.payment.transfer.transfer_2') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_3')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_3') . '">' . config('appsetting.payment.transfer.transfer_3') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_4')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_4') . '">' . config('appsetting.payment.transfer.transfer_4') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_5')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_5') . '">' . config('appsetting.payment.transfer.transfer_5') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_6')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_6') . '">' . config('appsetting.payment.transfer.transfer_6') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_7')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_7') . '">' . config('appsetting.payment.transfer.transfer_7') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_8')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_8') . '">' . config('appsetting.payment.transfer.transfer_8') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_9')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_9') . '">' . config('appsetting.payment.transfer.transfer_9') . '</option>';
        }
        if (config('appsetting.payment.transfer.transfer_10')) {
            $form['values'] .= '<option value="' . config('appsetting.payment.transfer.transfer_10') . '">' . config('appsetting.payment.transfer.transfer_10') . '</option>';
        }

        $form['values'] .= '</select>
                                </div>
                            </div>

                            <div class="control-group" >
                                <label class="control-label">' . trans('labels.frontend.payment.remark') . '</label>
                                <div class="controls">
                                    <textarea class="form-control" name="remark" placeholder="Optional" ></textarea>
                                </div>
                            </div>';
        return $form;
    }

    public function bnfTopup($encryptId, $totalAmount) : array
    {
        $orderPaymentId = BNFEncryption::decode($encryptId);
        $orderPayment = OrderPayment::find($orderPaymentId);
        $order = $orderPayment->order;

        $secret = config('appsetting.payment.topup.secretkey');
        $appId = config('appsetting.payment.topup.appid');
        $version = '1.0';

        $name = app_name().' Delivery';
        $orderNo = 'BNF-' .$order->ref_no;
        $serviceFee = ($orderPayment->service_fee > 0) ? $orderPayment->service_fee : null;
        $amount = $totalAmount;

        $itemName = $order->fromTownship->name .' - '. $order->toTownship->name .' delivery';
        $itemDescription = 'Receiver - '.$order->receiver_name .' , '.$order->receiver_phone.',weight '.$order->weight.'Kg';
        $expiryDatetime = '';
        $userDefined_1 = $encryptId;
        $userDefined_2 = 'order';
        $userDefined_3 = null;
        $userDefined_4 = null;
        $userDefined_5 = null;
        $userDefined_6 = null;
        $resultUrl_1 = url("/order/payment-complete/bnf_topup");
        $resultUrl_2 = url("/order/payment-status/bnf_topup");

        $requestData = [
            'app_id' => $appId,
            'version' => $version,
            'name' => $name,
            'order_no' => $orderNo,
            'amount' => $amount,
            'service_fee' => $serviceFee,
            'item_name' => $itemName,
            'item_description' => $itemDescription,
            'expiry_datetime' => $expiryDatetime,
            'user_defined_1' => $userDefined_1,
            'user_defined_2' => $userDefined_2,
            'user_defined_3' => $userDefined_3,
            'user_defined_4' => $userDefined_4,
            'user_defined_5' => $userDefined_5,
            'user_defined_6' => $userDefined_6,
            'result_url_1'   => $resultUrl_1,
            'result_url_2'   => $resultUrl_2,
        ];
        ksort($requestData);
        $hashValue = strtolower(hash_hmac('sha256', @implode('', array_values($requestData)), $secret, false));

        $form['url'] = config('appsetting.payment.topup.topup_url');
        $form['values'] =
            '<input type="hidden" name="app_id" value="' .$appId. '">
             <input type="hidden" name="version" value="' .$version. '">
             <input type="hidden" name="hash_value" value="' .$hashValue. '">
             <input type="hidden" name="name" value="' .$name. '">
             <input type="hidden" name="order_no" value="' .$orderNo. '">
             <input type="hidden" name="amount" value="' .$amount. '">
             <input type="hidden" name="service_fee" value="' .$serviceFee. '">
             <input type="hidden" name="item_name" value="' .$itemName. '">
             <input type="hidden" name="item_description" value="' .$itemDescription. '">
             <input type="hidden" name="expiry_datetime" value="' .$expiryDatetime. '">
             <input type="hidden" name="user_defined_1" value="' .$userDefined_1. '">
             <input type="hidden" name="user_defined_2" value="' .$userDefined_2. '">
             <input type="hidden" name="user_defined_3" value="' .$userDefined_3. '">
             <input type="hidden" name="user_defined_4" value="' .$userDefined_4. '">
             <input type="hidden" name="user_defined_5" value="' .$userDefined_5. '">
             <input type="hidden" name="user_defined_6" value="' .$userDefined_6. '">
             <input type="hidden" name="result_url_1" value="' .$resultUrl_1. '">
             <input type="hidden" name="result_url_2" value="' .$resultUrl_2. '">';

        return $form;
    }

    public function cod($encryptId, $totalAmount) : array
    {
        $orderPaymentId = BNFEncryption::decode($encryptId);
        $orderPayment = OrderPayment::find($orderPaymentId);
        $order = $orderPayment->order;

        $amount = $totalAmount;
        $ref = $order->ref_no;
        $response = ['method' => 'cod','ref' =>$ref];

        return $response;
    }

    public function saveTransfer($encryptId, array $input) : array
    {
        $orderPaymentId = BNFEncryption::decode($encryptId);
        $orderPayment = OrderPayment::find($orderPaymentId);
        $orderPayment->bank_name = $input['bank_name'];
        $orderPayment->bank_remark = $input['remark'] ? $input['remark'] : ' ';
        $orderPayment->save();

        $response['amount'] = $orderPayment->amount;
        $response['ref'] = $orderPayment->order->ref_no;
        $response['bank_name'] = $orderPayment->bank_name;
        $response['bank_remark'] = $orderPayment->bank_remark;

        return $response;
    }

    public function saveMPU($encryptId, array $input) : array
    {
        $orderPaymentId = BNFEncryption::decode($encryptId);

        $channelResponseCode = @$input["channel_response_code"];
        $channelResponseDesc = @$input["channel_response_desc"];

        $orderPayment = OrderPayment::find($orderPaymentId);

        $ref = $orderPayment->order->ref_no;

        if ($channelResponseCode === '00' && OrderPayment::where('id', $orderPaymentId)->count()) {
            $this->paymentStatusChange($orderPaymentId, $input);

            $response['paid'] = true;
        } else {
            $response['paid'] = false;
            $response['message'] = $channelResponseDesc;
        }

        $response['ref'] = $ref;

        return $response;
    }

    public function saveBnfTopup($encryptId, array $input) : array
    {
        $orderPaymentId = BNFEncryption::decode($encryptId);
        $orderPayment = OrderPayment::find($orderPaymentId);
        $ref = $orderPayment->order->ref_no;
        \Log::info(json_encode($input));
        if (!$this->checkBnfTopupResponseHash($input)) {
            $response['paid'] = false;
            $response['message'] = "Invalid hash value";
            $response['ref'] = $ref;

            return $response;
        }

        if (($input['status'] == 'verified' || $input['status'] == 'success') && OrderPayment::where('id', $orderPaymentId)->count()) {
            $this->paymentStatusChange( $orderPaymentId, $input);

            $response['paid'] = true;
        } else {
            $response['paid'] = false;
            $response['message'] = "cancelled";
        }
        $response['ref'] = $ref;

        return $response;
    }

    public function checkBnfTopupResponseHash($request) : bool
    {
        $requestData = array_except($request, ['hash_value']);
        ksort($requestData);
        $hashValue = strtolower(hash_hmac('sha256', @implode('', array_values($requestData)), config('appsetting.payment.topup.secretkey'), false));

        $response = ($hashValue == $request['hash_value']) ? true : false;

        return $response;
    }

    public function paymentStatusChange(int $orderPaymentId, array $input) : bool
    {
        $orderPayment = OrderPayment::where('id', $orderPaymentId)->first();
        $orderPayment->remark = json_encode($input);
        $orderPayment->payment_complete = 1;
        $orderPayment->save();

        return true;
    }  
}
