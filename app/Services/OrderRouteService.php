<?php
namespace App\Services;

use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\Order\Entities\OrderRouteProof;
use Modules\Order\Enum\OrderRouteLocationType;
use App\Services\SMSService;
use Modules\Order\Enum\OrderStatusType;

class OrderRouteService
{

    public function setOrderRouteSequence(Order $order) : bool
    {
        $orderRoutes = OrderRoute::where('order_id',$order->id)->get();
        $orderRoutesCount = $orderRoutes->count();
        $checkFirstTownshipId = $order->from_township_id;

        $count = 1;
        while($count <= $orderRoutesCount) {
            foreach($orderRoutes as $orderRoute)
            {            
                if($orderRoute->subRoute->from_township_id == $checkFirstTownshipId)
                {
                    $orderRoute->update(['sequence' => $count]);
                    $checkFirstTownshipId = $orderRoute->subroute->to_township_id;
                    break;
                }
            }
            $count++;
        }               

        return true;
    }

    public function getOrderRouteWithRefNo(string $refNo, string $orderBy = 'sequence', string $sort = 'asc')
    {
        $order = Order::where('ref_no',$refNo)->first();
        $orderRoutes = OrderRoute::where('order_id',$order->id)->orderBy($orderBy,$sort)->get();
        return $orderRoutes;
    }

    public function getOrderRoute(int $orderId)
    {
        $orderRoutes = OrderRoute::with('subRoute')->where('order_id',$orderId)->get();
        $orderRoutes = $orderRoutes->map(function ($orderRoute, $key)  {
            $orderRoute['sub_route_name'] = $orderRoute->subRoute->name;
            return $orderRoute;
        });
        return $orderRoutes;
    }

    public function addOrderRouteProof(array $input) : bool 
    {
        try {
            $orderRouteProof = OrderRouteProof::create($input);

            $order = $orderRouteProof->orderRoute->order;
            $latestOrderRoute = $order->orderRoute()->orderBy('sequence','desc')->first();

            if(OrderRouteProof::where('order_route_id',$latestOrderRoute->id)->where('location_status_id',OrderRouteLocationType::ID_DELIVER)->count()){
                $order->update(['status' => OrderStatusType::ID_DELIVERED]);
                SMSService::sendDeliveredSms($order->receiver_name,$order->pickup_phone,$order->pickup_name,$order->ref_no);
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;

    }
}
