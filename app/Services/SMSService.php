<?php
namespace App\Services;

use GuzzleHttp\Exception\BadResponseException;
use Notification;
use App\Notifications\SMSPohApproved;

class SMSService
{
    public static function sendDeliveredSms(string $receiverName,string $pickupPhone,string $pickupName,string $orderRef) : bool
    {
        if(config('message_templates.delivery_location_completed.is_enabled')){
            $message = config('message_templates.delivery_location_completed.message');
            $message = str_replace('{DELIVERY_RECIPIENT_NAME}',$receiverName, $message);
            $message = str_replace('{ORDER_REFERENCE_NUMBER}',$orderRef, $message);

            return self::sendSms($pickupPhone,$message,config('appsetting.sms.sms_sender_name'));
        }
        return false;
    }
    public static function sendConfirmationSms(string $receiverName,string $pickupPhone,string $pickupName,string $orderRef) : bool
    {
        if(config('message_templates.order_confirmation.is_enabled') && !auth()->user()){
            $message = config('message_templates.order_confirmation.message');
            $message = str_replace('{DELIVERY_PICKUP_NAME}',$pickupName, $message);
            $message = str_replace('{ORDER_REFERENCE_NUMBER}',$orderRef, $message);
            $longUrl = url("order/track-detail?ref_no={$orderRef}");
            $shortLink = app('url')->shorten($longUrl);
            $message = str_replace('{SHORT_LINK}',$shortLink, $message);

            return self::sendSms($pickupPhone,$message,config('appsetting.sms.sms_sender_name'));
        }
        return false;
    }

    public static function sendSms($mobile,$message,$sender) : bool
    {
        if(config('appsetting.sms.sms_poh_enable') == 'true'){

            try{
                $result = Notification::route('smspoh', $mobile)->notify(new SMSPohApproved($message,$sender));
                  
                \Log::notice(' Send Delivered SMS To '.$mobile);
     
                return true;
            }
            catch(\Exception $e){
              $response = $e->getMessage();        
                                 
              \Log::error(' Send SMS Error Response : '.$mobile.' '. json_encode($response));

              return false;
            }
                
        }
    }


    
}
