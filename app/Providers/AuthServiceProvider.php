<?php

namespace App\Providers;

use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Carbon\Carbon; 

/**
 * Class AuthServiceProvider.
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();

        // Define OAuth2 scopes
        Passport::tokensCan([
            'full-access' => 'Full API Access',
        ]);
        // Set access tokens expiry
        Passport::tokensExpireIn(Carbon::now()->addHours(env('OAUTH2_ACCESS_TOKEN_EXPIRY_IN_HOURS')));
        // Set refresh tokens expiry
        Passport::refreshTokensExpireIn(Carbon::now()->addMonths(env('OAUTH2_REFRESH_TOKEN_EXPIRY_IN_MONTHS')));

        // Implicitly grant "Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user) {
            return $user->hasRole(config('access.users.admin_role')) ? true : null;
        });
    }
}
