<?php
namespace App\Factories;

use Modules\ProductType\Entities\ProductType;
use Modules\Box\Entities\Box;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotRoute;
use Modules\SubRoute\Entities\ProductSubRoute;
use Modules\SubRoute\Entities\PivotPickupCollection;
use Modules\SubRoute\Entities\PivotDeliveryCollection;

class CreateCompletedSubRouteFactory {

    public static function createSubRoute($price,$weight,$fromTownship,$toTownship,$pickupCollection = null,$deliveryCollection = null)
    {
        $productType = factory(ProductType::class)->create();

        $box = factory(Box::class)->create();

        $route = factory(Route::class)->create([
            'from_city_id' => $fromTownship->city->id,
            'to_city_id' => $toTownship->city->id,
        ]);

        $subroute = factory(SubRoute::class)->create([
            'from_township_id' => $fromTownship->id,
            'to_township_id' => $toTownship->id,
            'box_id' => $box->id,
            'max_weight' => $weight,
            'mmk_price' => $price,
        ]);
        

        $pivotRoute = factory(PivotRoute::class)->create([
            'route_id' => $route->id,
            'sub_route_id' => $subroute->id
        ]);

        $productSubRoute = factory(ProductSubRoute::class)->create([
            'product_type_id' => $productType->id,
            'sub_route_id' => $subroute->id
        ]);

        if($pickupCollection) {
            $pivotPickupCollection = factory(PivotPickupCollection::class)->create([
                'collection_point_id' => $pickupCollection->id,
                'sub_route_id' => $subroute->id
            ]);  
        }

        if($deliveryCollection) {
            $pivotDeliveryCollection = factory(PivotDeliveryCollection::class)->create([
                'collection_point_id' => $deliveryCollection->id,
                'sub_route_id' => $subroute->id
            ]);
        }

        $data = [
            'route' => $route,
            'subroute' => $subroute,
            'fromTownship' => $subroute->fromTownship,
            'toTownship' => $subroute->toTownship,
            'productType' => $productType,
            'box' => $box,
            'pivotRoute' => $pivotRoute,
            'productSubRoute' => $productSubRoute
        ];

        return $data;
        
    }
}




