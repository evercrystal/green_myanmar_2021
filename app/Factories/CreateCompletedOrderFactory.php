<?php
namespace App\Factories;

use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use App\Factories\CreateCompletedSubRouteFactory;
use Carbon\Carbon as Carbon;

class CreateCompletedOrderFactory {

    public static function createOrder($amount,$weight,$status = 1,$refNo='AAAAAA')
    {
        $city = factory(City::class)->create();

        $fromTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $toTownship = factory(Township::class)->create([
            'city_id'=>$city->id
        ]);

        $data = CreateCompletedSubRouteFactory::createSubRoute($amount,$weight,$fromTownship,$toTownship);

        $order = factory(Order::class)->create([
            'from_city_id'                  =>  $data['fromTownship']->city->id,
            'to_city_id'                    =>  $data['toTownship']->city->id,
            'from_township_id'              =>  $data['fromTownship']->id,
            'to_township_id'                =>  $data['toTownship']->id,
            'product_type_id'               =>  $data['productType']->id,
            'box_id'                        =>  $data['box']->id,
            'receiver_name'                 =>  'Testing Name',
            'receiver_phone'                =>  '959123456789',
            'no_of_item'                    =>  1,
            'weight'                        =>  $weight,
            'pickup_date'                   =>  Carbon::now()->addDays(1)->format('Y-m-d'),
            'pickup_schedule_id'            =>  1,
            'payment_mode_id'               =>  1,
            'ref_no'                        =>  $refNo,
            'status'                        =>  $status,
            'total_amount'                  =>  $amount
        ]);

        $orderRoute = factory(OrderRoute::class)->create([
            'order_id' => $order->id,
            'route_id' => $data['route']->id,
            'sub_route_id' => $data['subroute']->id
        ]);

        $data['order'] = $order;
        $data['orderRoute'] = $orderRoute;

        return $data;
        
    }
}




