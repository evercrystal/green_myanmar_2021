<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Modules\State\Entities\State;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderPayment;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Modules\CollectionPoint\Repositories\CollectionPointRepository;
use Modules\Country\Repositories\CountryRepository;
use Modules\ProductType\Repositories\ProductTypeRepository;
use Modules\Box\Repositories\BoxRepository;
use Modules\Order\Repositories\OrderRepository;
use App\Http\Requests\Frontend\Order\OrderPriceRequest;
use App\Http\Requests\Frontend\Order\SaveOrderRequest;
use App\Http\Requests\Frontend\Order\SaveOrderPaymentRequest;
use App\Http\Requests\Frontend\Order\TrackOrderRequest;
use App\Services\PriceCalculationService;
use App\Services\PaymentService;
use App\Services\OrderService;
use App\Enums\Pickup;
use App\Enums\PickupTimeSchedule;
use Modules\Order\Enum\PaymentModeType;
use Modules\Order\Enum\OrderStatusType;
use GMBF\PhoneNumber;
use App\Models\BNFEncryption;
use App\Services\SMSService;
use Illuminate\Http\Request;
use Carbon\Carbon as Carbon;

/**
 * Class OrderController.
 */
class OrderController extends Controller
{
	protected $country;
	protected $productType;
	protected $box;
    protected $priceService;
    protected $order;
    protected $paymentService;
    protected $collection;

	public function __construct(CountryRepository $country, ProductTypeRepository $productType, BoxRepository $box, PriceCalculationService $priceService, OrderRepository $order, PaymentService $paymentService, OrderService $orderService, CollectionPointRepository $collection)
    {
        $this->country = $country;
        $this->productType = $productType;
        $this->box = $box;
        $this->priceService = $priceService;
        $this->order = $order;
        $this->paymentService = $paymentService;
        $this->orderService = $orderService;
        $this->collection = $collection;
    }   
    /**
     * @return \Illuminate\View\View
     */
    public function orderForm()
    {
        $countries = $this->country->getAll();
    	$productTypes = $this->productType->getActiveAll();
    	$boxes = $this->box->getActiveAll();
    	$pickupTimeScheduleSettings = PickupTimeSchedule::getSettings();
        $paymentModes = PaymentModeType::getSettings();
        $location = null;
        if(isset(auth()->user()->merchant)) {
            $location = auth()->user()->merchant->merchantLocation->first();
        }
        return view('frontend.order_form',compact('countries','productTypes','boxes','pickupTimeScheduleSettings','paymentModes','location'));
    }

    public function getCities($countryId)
    {
    	$stateIdsArr = State::Where('country_id',$countryId)->pluck('id')->toArray();
    	$cities = City::whereIn('state_id',$stateIdsArr)->get();
        return $cities;
    }
    public function getCitiesinRegister($stateId)
    {
    	$cities = City::where('state_id',$stateId)->get();
        return $cities;
    }
    public function getStates($countryId)
    {
        $states = State::Where('country_id', $countryId)->get();
        return $states;
    }
    public function getTownships($cityId)
    {
        $townships = Township::Where('city_id', $cityId)->get();
        return $townships;
    }

    public function getCollection($township_id)
    {
        $pickupCollection = CollectionPoint::Where('township_id', $township_id)->get();
        return $pickupCollection;
    }

    public function getAllCollection()
    {
        $collections = $this->collection->getAll();
        return $collections;
    }

    public function getCollectionPointAddress(int $collectionPointId)
    {
        return $this->orderService->getCollectionPointAddress($collectionPointId);
    }

    public function getCollectionPointDetail(int $collectionPointId)
    {
        $collections =  CollectionPoint::with('township')->where('id',$collectionPointId)->get();
        $collections = $collections->map(function ($collection, $key)  {
            $collection['country_id'] = $collection->township->city->state->country_id;
            return $collection;
        });
        return $collections->first();
    }

    public function getRecentOrder(Request $request)
    {
        $search = $request->get('value');

        $orders = $this->orderService->getRecentOrder($search);

        return response()->json($orders, 200);

    }

    public function getTownshipAndCollection(Request $request)
    {
        $cityId = $request->cityId;
        $searchValue = $request->value;
        $data = $this->orderService->getTownshipAndCollection($searchValue,$cityId);

        return $data;
    }


    public function calculatePrice(OrderPriceRequest $request)
    {
        $input = $request->all();
        $data = $this->priceService->priceForMerchantOrder($input);

        if ($data['total_amount'] > 0) {
            if(!$data['access_delivery_time']) {
                return response(['success' => false, 'message' =>trans('alerts.frontend.order.invalid_pickup_schedule'), 'data'=> $data  ], 400);
            }
            return response(['success' => true, 'message' =>trans('alerts.frontend.order.valid_route'), 'data'=> $data ], 200);
        } else {
            return response(['success' => false, 'message' =>trans('alerts.frontend.order.invalid_route'), 'data'=> $data  ], 404);
        }
    }

    public function saveOrder(SaveOrderRequest $request)
    {
        $input = $request->all();
        $priceData = $this->priceService->priceForMerchantOrder($input);
        $input['total_amount'] = $priceData['total_amount'];
        $input['total_add_on_weight'] = $priceData['total_add_on_weight'];
        $input['sub_routes_ids'] = $priceData['sub_routes'];
        if(auth()->user() && auth()->user()->merchant) {
            $input['merchant_ref_id'] = auth()->user()->merchant->merchant_ref_id;
        }

        $order = $this->order->saveOrder($input);
        if (!$order) {
            return response(['success' => false, 'message' =>trans('alerts.frontend.order.order_does_not_created') ], 404);
        }
        $encryptOrderId = BNFEncryption::encode($order->id);
        $ReceiverPaymentRedirectUrl = url('order/receiver-payment-confirmation/'.$encryptOrderId);
        $senderClearenceAmount = $this->priceService->priceForSenderClearenceAmount($order, $input);
        $receiverClearenceAmount = $priceData['total_amount'] - $senderClearenceAmount;
        $paymentServiceFees = $this->paymentService->getServiceFees($senderClearenceAmount);
        return response(['success' => true, 'message' =>trans('alerts.frontend.order.saved_order'), 'receiver_payment_redirect_url' => $ReceiverPaymentRedirectUrl,'order'=> $order, 'sender_clearance_amount' => $senderClearenceAmount, 'receiver_clearance_amount'=> $receiverClearenceAmount,'payment_service_fee' => $paymentServiceFees,'payment_mode_id'=>$input['payment_mode_id'] ], 200);
    }

    public function saveOrderPayment(SaveOrderPaymentRequest $request)
    {
        $input = $request->all();
        $orderPayment = $this->order->saveOrderPayment($input);

        if($orderPayment) {
            $encryptOrderPaymentId = BNFEncryption::encode($orderPayment->id);
            $paymentRedirectUrl = url('order/payment-confirmation/'.$orderPayment->payment_method.'/'.$encryptOrderPaymentId);
            return response(['success' => true, 'payment_redirect_url' => $paymentRedirectUrl],200);
        }
        return response(['success' => false, 'message' => trans('alerts.frontend.order.order_payment_error')],404);
    }

    public function receiverPaymentConfirmation($encryptOrderId)
    {  
        $orderPaymentId = BNFEncryption::decode($encryptOrderId);
        $order = Order::find($orderPaymentId);
        SMSService::sendConfirmationSms($order->receiver_name,$order->pickup_phone,$order->pickup_name,$order->ref_no);                        

        return view('frontend.order_complete', compact('order'));
    }
    
    public function paymentConfirmation($method, $encryptOrderPaymentId)
    {
        if ($this->order->checkValidOrderPayment($method, $encryptOrderPaymentId)) {
            $orderPaymentId = BNFEncryption::decode($encryptOrderPaymentId);
            $orderPayment = OrderPayment::find($orderPaymentId);
            $amount = $orderPayment->amount;
            $order = $orderPayment->order()->first();

            $currency = 'MMK';
            $usdRate = config('appsetting.basic.app_dollar_rate');

            $usdAmount = round($amount / floatval($usdRate), 2);
            $mmkAmount = $amount;
            switch ($method) {
                case 'bank_transfer' : $formValues = $this->paymentService->transfer($encryptOrderPaymentId, $mmkAmount);
                    break;
                case 'mpu' : $formValues = $this->paymentService->mpu($encryptOrderPaymentId, $mmkAmount);
                    break;
                case 'bnf_topup' : $formValues = $this->paymentService->bnfTopup($encryptOrderPaymentId, $mmkAmount);
                    break;
                case 'cod': $formValues = $this->paymentService->cod($encryptOrderPaymentId, $mmkAmount);
                            SMSService::sendConfirmationSms($order->receiver_name,$order->pickup_phone,$order->pickup_name,$order->ref_no);                                            
                    break;
                default:  app()->abort('404');
            }
            return view('frontend.payment_confirmation', compact('usdAmount', 'mmkAmount', 'currency', 'formValues', 'orderPayment', 'amount', 'method', 'encryptOrderPaymentId'));
        }
        app()->abort('404');
    }

    public function paymentComplete(Request $request, $method, $encryptId=null)
    {
        if (!$encryptId) {
            if ($method == 'visa_master' || $method == 'mpu' || $method == 'bnf_topup') {
                $encryptId = $request->input('user_defined_1');
            }
        }
        $request = $request->all();
        if (($this->order->checkValidOrderPayment($method, $encryptId)) && $request) {
            $orderPaymentId = BNFEncryption::decode($encryptId);
            $orderPayment = OrderPayment::find($orderPaymentId);
            $order = $orderPayment->order()->first();
            
            switch ($method) {
                case 'bank_transfer' : $response = $this->paymentService->saveTransfer($encryptId, $request);
                    break;
                case 'mpu' : $response = $this->paymentService->saveMPU($encryptId, $request);
                    break;
                case 'bnf_topup':  $response = $this->paymentService->saveBnfTopup($encryptId, $request);
                                    break;
                default: app()->abort('404');
            }
            SMSService::sendConfirmationSms($order->receiver_name,$order->pickup_phone,$order->pickup_name,$order->ref_no);                

            return view('frontend.payment_complete', compact('response', 'method', 'encryptId' ));
        } else {
            return redirect('/');
        }
    }

    public function paymentStatus(Request $request, $method, $encryptId=null)
    {
        if (!$encryptId) {
            if ($method == 'visa_master' || $method == 'mpu' || $method == 'bnf_topup') {
                $encryptId = $request->input('user_defined_1');
            }
        }
        $request = $request->all();
        if (($this->order->checkValidOrderPayment($method, $encryptId)) && $request) {
            switch ($method) {
                case 'bank_transfer' : $response = $this->paymentService->saveTransfer($encryptId, $request);
                    break;
                case 'mpu' : $response = $this->paymentService->saveMPU($encryptId, $request);
                    break;
                case 'bnf_topup':  $response = $this->paymentService->saveBnfTopup($encryptId, $request);
                                    break;
                default: app()->abort('404');
            }
            return response()->json(['success'=>true], 200);
        } else {
            return response()->json(['success'=>false], 403);
        }
    }

    public function trackOrder(TrackOrderRequest $request)
    {
        $input = $request->all();

        $order = Order::with(['fromTownship','toTownship']);

        if(isset($input['ref_no'])) {
            $order = $order->where('ref_no',$input['ref_no']);
        } else {
            $phone= (new PhoneNumber())->add_prefix($input['phone']);
            $order = $order->whereNotIn('status',[
                OrderStatusType::ID_ON_PROGRESS,
                OrderStatusType::ID_ALLOCATION,
                OrderStatusType::ID_DELIVERED
            ])->where('receiver_phone',$phone);
        }
        $order = $order->orderBy('created_at','desc')->get();

        return response(['success' => true, 'order' => $order ],200);
    }

    public function trackDetail(Request $request)
    {
      
        $order = Order::with(['orderRoute' => function ($q) {
           $q->orderBy('sequence', 'asc');
        }])->where('ref_no',$request->ref_no)->get();
        if($order) {
            $order = $order->first();
            $refNo = $request->ref_no;
        }
        return view('frontend.track_order', compact('order','refNo'));
    }

}
