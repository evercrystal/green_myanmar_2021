<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __construct()
    {
        
    }

    public function index()
    {
        return view('frontend.user.dashboard');
    }

   
}
