<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Frontend\Auth\UserRepository;
use Modules\Merchant\Enum\MerchantType;
use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\Merchant\Http\Requests\CreateMerchantRequest;
use Modules\Country\Repositories\CountryRepository;
use Modules\City\Repositories\CityRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\State\Repositories\StateRepository;
use Modules\Merchant\Repositories\MerchantRepository;
use Modules\Merchant\Entities\Merchant;
use Modules\Merchant\Entities\MerchantLocation;
use GMBF\PhoneNumber;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $userRepository
     */
    protected $country;
    protected $city;
    protected $township;
    protected $state;
    protected $merchant;

    public function __construct(UserRepository $userRepository, CountryRepository $country, CityRepository $city, StateRepository $state, TownshipRepository $township, MerchantRepository $merchant)
    {
        $this->merchant = $merchant;
        $this->userRepository = $userRepository;
        $this->country = $country;
        $this->city = $city;
        $this->township = $township;
        $this->state = $state;

    }

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectPath()
    {
        return route(home_route());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        abort_unless(config('access.registration'), 404);
        $types = MerchantType::AVAILABLES;
        $countries = $this->country->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id','asc');
        $states = $this->state->where('country_id',1)->get();
        $townships = $this->township->getAll('id','asc');
        return view('frontend.auth.register',compact('countries', 'cities', 'townships', 'types','states'));
    }

    /**
     * @param RegisterRequest $request
     *
     * @throws \Throwable
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {
        abort_unless(config('access.registration'), 404);
        
        $input = $request->all();
        $merchantName = explode(' ', $input['name']);
        $input['first_name'] = $merchantName[0];
        unset($merchantName[0]);
        $input['last_name']  = implode(' ', $merchantName);

        $phoneNumber = new PhoneNumber();
        $input['mobile'] = $phoneNumber->add_prefix($input['mobile']);
        $input['merchant_ref_id'] = $this->merchant->generateReferenceId();
        $input['merchant_secret'] = $this->merchant->generateSecretKey();

        $user = $this->userRepository->create($input);

        $input['user_id'] = $user->user_id;
        $merchant = Merchant::create($input);

        $input['merchant_id'] = $merchant->id;
        MerchantLocation::create($input);

        // If the user must confirm their email or their account requires approval,
        // create the account but don't log them in.
        if (config('access.users.confirm_email') || config('access.users.requires_approval')) {
            event(new UserRegistered($user));

            return redirect($this->redirectPath())->withFlashSuccess(
                config('access.users.requires_approval') ?
                    __('exceptions.frontend.auth.confirmation.created_pending') :
                    __('exceptions.frontend.auth.confirmation.created_confirm')
            );
        }

        auth()->login($user);

        event(new UserRegistered($user));

        return redirect($this->redirectPath());
    }
}
