<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\UserRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use DataTables;

/**
 * Class UserTableController.
 */
class UserTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @param UserRepository $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageUserRequest $request)
    {
        $input = $request->all();

        return Datatables::of($this->users->getForDataTable($input))
        ->escapeColumns(['user_name'])
        ->editColumn('confirmed', function ($user) {
            return view('backend.auth.user.includes.confirm',compact('user'));
        })
        ->addColumn('roles', function ($user) {
            return $user->roles_label;
        })
        ->addColumn('permissions', function ($user) {
            return $user->permissions_label;
        })
        ->addColumn('social', function ($user) {
            return  view('backend.auth.user.includes.social-buttons',compact('user'));
        })
        ->editColumn('updated_at', function ($user) {
            return $user->updated_at->diffForHumans();
        })
        ->addColumn('actions', function ($user) {
            return view('backend.auth.user.includes.actions',compact('user'));
        })
        ->make(true);
    }
}
