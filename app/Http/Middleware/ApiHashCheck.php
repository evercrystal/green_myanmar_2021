<?php

namespace App\Http\Middleware;
use Modules\Merchant\Entities\Merchant;
use App\Services\HashService;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Api\Http\Requests\V1\MerchantRequest;
use Closure;

class ApiHashCheck extends ApiController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->merchant_ref_id && $request->hash_value) {
            
            $requestData = $request->all();
            $hashValue = $requestData['hash_value'];
            unset($requestData['hash_value']);
            $dataString = (new HashService())->mappedImplode('&',$requestData,'=');

            $merchant = Merchant::where('merchant_ref_id',$request->merchant_ref_id)->first();

            if($merchant && $merchant->user->active) {

                $hashEqual = (new HashService())->hashEqual($dataString,$merchant->merchant_secret,$hashValue);

                $request->request->add(['merchant' => $merchant]);
                if($hashEqual) {
                    return $next($request);
                }else {
                    return $this->respondWithUnexpectedError('invalid_hash_value');
                }
            }
            return $this->respondWithUnexpectedError('merchant_does_not_exist');
        }
        return $this->respondWithUnexpectedError('invalid_request_data');
    }
}
