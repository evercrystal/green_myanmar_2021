<?php

namespace App\Http\Swagger\V1;

/**
 * @SWG\Swagger(
 *     schemes={"https", "http"},
 *     basePath="/api",
 *     host=L5_SWAGGER_CONST_HOST,
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title=API_TITLE,
 *         description=API_DESCRIPTION,
 *         @SWG\Contact(
 *             email=SUPPORT_CONTACT
 *         )
 *     )
 * )
 */

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="passport",
 *   type="oauth2",
 *   tokenUrl="/oauth/token",
 *   flow="password",
 *   scopes={"full-access":"Full API Access"}
 * )
 */

// ====== Urls
/**
 *
 * POST /oauth/token
 * POST /oauth/token/revoke
 *
 */
/**
 * @SWG\Post(
 *      path="/oauth/token",
 *      summary="Request for access token or refreshes the access token",
 *      tags={"Authentication"},
 *      operationId="authenticateUser",
 *      @SWG\Parameter(
 *         name="request",
 *         in="body",
 *         description="If “grant_type” is “password”, access token is returned after authenticating the username and password.
If “grant_type” is “refresh_token”, a new access token is generated based on the access token specified in the Authorization header.",
 *         required=true,
 *         @SWG\Schema(
 *             required={"grant_type", "client_id", "client_secret", "scope"},
 *             @SWG\Property(property="grant_type", type="string", description="refresh_token or password", example="refresh_token"),
 *             @SWG\Property(property="username", type="string", description="Username for login. If 'grant_type' is 'password', the 'username' field is required."),
 *             @SWG\Property(property="password", type="string", description="Password for login. If 'grant_type' is 'password', the 'password' field is required."),
 *             @SWG\Property(property="refresh_token", type="string", description="Refresh_token from the authorization response. If 'grant_type' is 'refresh_token', the 'refresh_token' field is required."),
 *             @SWG\Property(property="client_id", type="string", description="OAuth Client ID"),
 *             @SWG\Property(property="client_secret", type="string", description="OAuth Client Secret"),
 *             @SWG\Property(property="scope", type="string", description="What scopes are requested, for all, use 'full-access'", example="full-access"),
 *         )
 *     ),
 *      @SWG\Response(
 *         response=200,
 *         description="Success",
 *         @SWG\Schema(ref="#/definitions/Authentication")
 *     ),
 *     @SWG\Response(
 *         response=401,
 *         description="“Invalid credentials",
 *         @SWG\Schema(ref="#/definitions/Error")
 *     ),
 *     @SWG\Response(
 *         response=403,
 *         description="Unverified user",
 *         @SWG\Schema(ref="#/definitions/Error")
 *     ),
 *     @SWG\Response(
 *         response=400,
 *         description="Grant type not supported",
 *         @SWG\Schema(ref="#/definitions/Error")
 *     ),
 *     @SWG\Response(
 *         response=500,
 *         description="Internal Server Error",
 *     ),
 * )
 */
/**
 * @SWG\Post(
 *      path="/oauth/token/revoke",
 *      summary="Requests token to revoke",
 *      tags={"Authentication"},
 *      operationId="revokeToken",
 *      security={
 *          {"passport": {}}
 *      },
 *      @SWG\Response(
 *         response=204,
 *         description="Success",
 *     ),
 *     @SWG\Response(
 *         response=401,
 *         description="Unauthorized",
 *         @SWG\Schema(ref="#/definitions/Error")
 *     ),
 *     @SWG\Response(
 *         response=500,
 *         description="Internal Server Error",
 *     ),
 * )
 */
/**
 * @SWG\Definition(
 *   definition="Authentication",
 *   type="object",
 *   required={"token_type", "expires_in", "access_token", "refresh_token"},
 *   @SWG\Property(
 *       property="token_type",
 *       type="string"
 *   ),
 *  @SWG\Property(
 *       property="expires_in", 
 *       type="integer",
 *       format="int64"
 *   ),
 *   @SWG\Property(
 *       property="access_token",
 *       type="string"
 *   ),
 *   @SWG\Property(
 *       property="refresh_token",
 *       type="string"
 *   ),
 * )
 */
 /**
 * @SWG\Definition(
 *   definition="Response",
 *   type="object",
 *   @SWG\Property(
 *       property="data",
 *       type="object"
 *   )
 * )
 */
/**
 * @SWG\Definition(
 *   definition="Error",
 *   type="object",
 *   required={"http_code", "message", "errors"},
 *   @SWG\Property(
 *       property="http_code",
 *       type="integer",
 *       format="int64"
 *   ),
 *   @SWG\Property(
 *       property="message",
 *       type="string",
 *   ),
 *   @SWG\Property(
 *       property="errors",
 *       type="array",
 *       @SWG\Items(
 *           type="string"
 *       )
 *   )
 * )
 */
