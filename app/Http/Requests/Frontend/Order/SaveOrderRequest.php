<?php

namespace App\Http\Requests\Frontend\Order;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

/**
 * Class SaveOrderRequest.
 */
class SaveOrderRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'from_township_id' => 'required|numeric',
            'to_township_id' => 'required|numeric',
            'product_type_id' => 'required|numeric',
            'no_of_item' => 'required|numeric',
            'box_id' => 'required|numeric',
            'weight' => 'required|numeric',
            'pickup_date' => 'required|date',
            'pickup_schedule_id' => 'required|numeric',
            'payment_mode_id' => 'required|numeric',
            'receiver_name' => 'required',
            'receiver_phone' => 'required',
            'pickup_name' => 'required',
            'pickup_phone' => 'required',
            'from_address' => 'required',
            'to_address' => 'required',
        ];
        if($this->input('customer_order_no') && auth()->user())
        {
            $rules['customer_order_no'] = 'nullable|string|unique:'.Table::ORDER.',customer_order_no,' . $this->id . ',id,user_id,'.auth()->user()->id;
        }

        return $rules;
    }

}
