<?php

namespace App\Http\Requests\Frontend\Order;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SaveOrderPaymentRequest.
 */
class SaveOrderPaymentRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|numeric',
            'service_fee' => 'required|numeric',
            'payment_method' => 'required',
            'amount' => 'required|numeric',
            'payment_mode_id' => 'required|numeric',
        ];
    }

}
