<?php

namespace App\Http\Requests\Frontend\Order;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

/**
 * Class TrackOrderRequest.
 */
class TrackOrderRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'search_value' => 'required'
        ];

        if($this->input('phone'))
        {
            $rules['phone'] = 'required|valid_phone_number';

        }else {
            $rules['ref_no'] = 'required|exists:'.Table::ORDER.',ref_no';
        }
        
        return $rules;
    }

    protected function prepareForValidation()
    {
        if(is_numeric($this->input('search_value')))
        {
            $this->merge([
                'phone' => $this->input('search_value'),
            ]);
        } else {
            $this->merge([
                'ref_no' => $this->input('search_value'),
            ]);
        }
    }

    public function messages()
    {
        return [
            'valid_phone_number' => 'Invalid Mobile No. or Not Support Mobile No.'
        ];
    }

}
