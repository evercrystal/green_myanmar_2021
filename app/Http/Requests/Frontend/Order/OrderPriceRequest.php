<?php

namespace App\Http\Requests\Frontend\Order;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class OrderPriceRequest.
 */
class OrderPriceRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from_township_id' => 'required|numeric',
            'to_township_id' => 'required|numeric',
            'product_type_id' => 'required|numeric',
            'no_of_item' => 'required|numeric',
            'box_id' => 'required|numeric',
            'weight' => 'required|numeric',
            'pickup_date' => 'required|date',
            'pickup_schedule_id' => 'required|numeric',
        ];
    }

}
