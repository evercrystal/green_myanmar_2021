<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Search and Sortable Order Column Fields
    |--------------------------------------------------------------------------
    |
    | This option controls the default search fields that will be used to
    | search when order search.
    |
    */

    'status_id' => [
        'searchable' => true,
        'search_field' => 'status',
    ],

    'date' => [
        'searchable' => true,
        'search_field' => 'created_at',
    ],

    'ref_no' => [
        'searchable' => true,
        'search_field' => 'ref_no',
    ],

    'from_township_id' => [
        'searchable' => true,
        'search_field' => 'from_township_id',
    ],

    'to_township_id' => [
        'searchable' => true,
        'search_field' => 'to_township_id',
    ],

    'user_defined_1' => [
        'searchable' => true,
        'search_field' => 'user_defined_1',
    ],

    'user_defined_2' => [
        'searchable' => true,
        'search_field' => 'user_defined_2',
    ],

    'user_defined_3' => [
        'searchable' => true,
        'search_field' => 'user_defined_3',
    ],

    'user_defined_5' => [
        'searchable' => true,
        'search_field' => 'user_defined_5',
    ],
];