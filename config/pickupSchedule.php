<?php

use App\Enums\PickupTimeSchedule;

return [
    /*
    |--------------------------------------------------------------------------
    | Default Pickup Time Schedule
    |--------------------------------------------------------------------------
    |
    | This option controls the default pick up schedule that will be used in
    | calculation when order created/update.
    |
    */

    'hours' => [
        PickupTimeSchedule::ID_ANYTIME => env('PICKUP_ANYTIME_LAST_HOUR', 17),
        PickupTimeSchedule::ID_MORNING => env('PICKUP_MORNING_LAST_HOUR', 12),
        PickupTimeSchedule::ID_AFTERNOON => env('PICKUP_AFTERNOON_LAST_HOUR', 17),
    ]
];
