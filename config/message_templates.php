<?php

use App\Enums\RecipientTypes;

return [
    /*
    |--------------------------------------------------------------------------
    | Default Message Templates
    |--------------------------------------------------------------------------
    |
    | This option controls the default message template that will be used to
    | send when order delivered.
    |
    */

    'delivery_location_completed' => [
        'is_enabled' => 1,
        'recipient_type_id' => RecipientTypes::ID_DELIVERY_CONTACT,
        'message' => 'The package ref: {ORDER_REFERENCE_NUMBER} has been delivered to the desirable address. Receiver Name: {DELIVERY_RECIPIENT_NAME} . BNF Express ph: 09969910109 / 09969910110'
    ],
    'order_confirmation' => [
        'is_enabled' => 1,
        'recipient_type_id' => RecipientTypes::ID_DELIVERY_CONTACT,
        'message' => 'Hi {DELIVERY_PICKUP_NAME} , This is your package ref: {ORDER_REFERENCE_NUMBER} . You can keep track of your package here: {SHORT_LINK} . BNF Express ph: 09969910109 / 09969910110'
    ],
];
