<?php

use App\Enums\Pickup;

return [
    /*
    |--------------------------------------------------------------------------
    | Default Pickup Price
    |--------------------------------------------------------------------------
    |
    | This option controls the default pick up price that will be used in
    | calculation when order created/update.
    |
    */

    'prices' => [
        Pickup::ID_TODAY => env('PICKUP_TODAY_PRICE', 1000),
        Pickup::ID_TOMORROW_MORNING => env('PICKUP_TOMORROW_MORNING_PRICE', 500),
        Pickup::ID_TOMORROW_AFTERNOON => env('PICKUP_TOMORROW_AFTERNOON_PRICE', 500),
        Pickup::ID_OTHER => env('PICKUP_OTHER_PRICE', 500)
    ]
];
