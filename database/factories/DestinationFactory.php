<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\Destination\Entities\Destination;
use Modules\Country\Entities\Country;
use Faker\Generator as Faker;

$factory->define(Destination::class, function (Faker $faker) {
    $country = factory(Country::class)->create();
    return [
        'name' => $faker->name(),
        'country_id' => $country->id
    ];
});
