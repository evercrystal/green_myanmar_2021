<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Modules\State\Entities\State;
use Modules\Country\Entities\Country;
use Faker\Generator as Faker;
use App\Enums\Table;

$factory->define(State::class, function (Faker $faker) {
    $country = factory(Country::class)->create();
    return [
        'name' => $faker->name(),
        'country_id' => $country->id,
        'is_active'  => 1
    ];
});
