<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Order\Entities\Order;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\ProductType\Entities\ProductType;
use Modules\Box\Entities\Box;

$factory->define(Order::class, function (Faker $faker) {
    $city               = factory(City::class)->create();
    $fromTownship       = factory(Township::class)->create();
    $toTownship         = factory(Township::class)->create();
    $productType        = factory(ProductType::class)->create();
    $box                = factory(Box::class)->create();
    return [
        'from_city_id'                  =>  $city->id,
        'to_city_id'                    =>  $city->id,
        'from_township_id'              =>  $fromTownship->id,
        'to_township_id'                =>  $toTownship->id,
        'product_type_id'               =>  $productType->id,
        'box_id'                        =>  $box->id,
        'receiver_name'                 =>  $faker->name(),
        'receiver_phone'                =>  $faker->phoneNumber,
        'pickup_name'                   =>  $faker->name(),
        'pickup_phone'                  =>  $faker->phoneNumber,
        'no_of_item'                    =>  $faker->randomNumber(),
        'weight'                        =>  $faker->randomNumber(),
        'pickup_date'                   =>  $faker->date(),
        'pickup_schedule_id'            =>  1,
        'payment_mode_id'               =>  1,
        'ref_no'                        =>  $faker->randomNumber(),
        'status'                        =>  1,
        'total_amount'                  =>  1000,
        'from_address'                  => $faker->address(),
        'to_address'                    => $faker->address(),
    ];
});
$factory->state(Order::class, 'softDeleted', function () {
    return [
        'deleted_at' => now(),
    ];
});
