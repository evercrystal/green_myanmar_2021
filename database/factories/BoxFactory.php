<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Box\Entities\Box;

$factory->define(Box::class, function (Faker $faker) {

    return [
        'name'  => $faker->name(),
        'description' => $faker->text(),
        'image' => $faker->imageUrl(400, 300),
        'status'=> 1
    ];
});
