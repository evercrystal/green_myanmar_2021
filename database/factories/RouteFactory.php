<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Route\Entities\Route;
use Modules\City\Entities\City;

$factory->define(Route::class, function (Faker $faker) {
    $fromcity = factory(City::class)->create();
    $tocity = factory(City::class)->create();

    return [
        'from_city_id' => $fromcity->id,
        'to_city_id' => $tocity->id,
        'is_active'    => '1',
    ];
});
