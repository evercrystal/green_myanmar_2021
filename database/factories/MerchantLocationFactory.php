<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Merchant\Entities\Merchant;
use Modules\Merchant\Entities\MerchantLocation;
use Modules\Country\Entities\Country;
use Modules\State\Entities\State;

$factory->define(MerchantLocation::class, function (Faker $faker) {
    $merchant = factory(Merchant::class)->create();
    $country = factory(Country::class)->create();
    $state = factory(State::class)->create();

    return [
        'merchant_id' => $merchant->id,
        'country_id' => $country->id,
        'state_id' => $state->id,
        'city_id' => 1,
        'township_id' => 1
        
    ];
});
