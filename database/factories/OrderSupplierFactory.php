<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Order\Entities\OrderSupplier;
use Modules\Order\Entities\Order;
use Modules\DeliverySupplier\Entities\DeliverySupplier;

$factory->define(OrderSupplier::class, function (Faker $faker) {
    $order = factory(Order::class)->create();
    $deliverySupplier = factory(DeliverySupplier::class)->create();
 
    return [
        'order_id' =>  $order->id,
        'delivery_supplier_id' =>  $deliverySupplier->id,
    ];
});
