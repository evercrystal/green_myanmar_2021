<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\SubRoute\Entities\SubRoute;
use Modules\Township\Entities\Township;
use Modules\Box\Entities\Box;
use Modules\SubRoute\Enum\DeliveryType;

$factory->define(SubRoute::class, function (Faker $faker) {
    $fromtownship = factory(Township::class)->create();
    $totownship = factory(Township::class)->create();
    $box = factory(Box::class)->create();

    return [
        'name' => $faker->name(),
        'type' => DeliveryType::ID_CAR,
        'from_township_id' => $fromtownship->id,
        'to_township_id' => $totownship->id,
        'box_id' => $box->id,
        'max_weight' => $faker->randomNumber(),
        'duration' => 0,
        'add_on_weight_rate_mmk' => $faker->randomNumber(),
        'add_on_weight_rate_usd' => $faker->randomNumber(),
        'usd_price' => $faker->randomNumber(),
        'mmk_price' => $faker->randomNumber()
    ];
});
