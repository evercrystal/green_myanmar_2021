<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\ProductType\Entities\ProductType;

$factory->define(ProductType::class, function (Faker $faker) {

    return [
        'name' => $faker->name(),
        'form' => $faker->randomLetter(),
        'is_active'    => '1',
    ];
});
