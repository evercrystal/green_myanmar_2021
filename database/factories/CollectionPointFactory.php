<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\CollectionPoint\Entities\CollectionPoint;
use Modules\Township\Entities\Township;
use Faker\Generator as Faker;

$factory->define(CollectionPoint::class, function (Faker $faker) {
    $township = factory(Township::class)->create();
    return [
        'name' => $faker->name(),
        'township_id' => $township->id,
        'address' => $faker->text(),

    ];
});
