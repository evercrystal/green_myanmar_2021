<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\SubRoute\Entities\PivotRoute;
use Modules\SubRoute\Entities\SubRoute;
use Modules\Route\Entities\Route;

$factory->define(PivotRoute::class, function (Faker $faker) {
    return [
        'route_id' => factory(Route::class)->create()->id,
        'sub_route_id' => factory(SubRoute::class)->create()->id
    ];
});
