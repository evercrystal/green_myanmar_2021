<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderRoute;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Modules\ProductType\Entities\ProductType;
use Modules\Box\Entities\Box;

$factory->define(OrderRoute::class, function (Faker $faker) {
    $sub_route          = factory(SubRoute::class)->create();
    $route              = factory(Route::class)->create();
    $order              = factory(Order::class)->create();
    $deliverySupplier   = factory(DeliverySupplier::class)->create();
 
    return [
        'order_id'                      =>  $order->id,
        'route_id'                      =>  $route->id,
        'sub_route_id'                  =>  $sub_route->id,
        'delivery_supplier_id'          =>  null,
        'location_status_id'            =>  null,
        'sequence'                      =>  1
    ];
});
