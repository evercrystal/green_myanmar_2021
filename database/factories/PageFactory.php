<?php

use Faker\Generator as Faker;
use Modules\Page\Entities\Page;
use App\Enums\LanguageType;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'page' => $faker->name(),
        'title' => $faker->title(),
        'content' => $faker->paragraph(),
        'language_id' => LanguageType::ID_ENGLISH
    ];
});