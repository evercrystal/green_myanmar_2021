<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Order\Entities\OrderPayment;
use Modules\Order\Entities\Order;
use Modules\Order\Enum\PaymentModeType;

$factory->define(OrderPayment::class, function (Faker $faker) {
    $order = factory(Order::class)->create();
    return [
        'order_id' =>  $order->id,
        'service_fee'=> 0,
        'payment_method'=> $faker->name(),
        'amount' => $faker->randomNumber(),
        'payment_mode_id' =>  PaymentModeType::ID_SENDER_WILL_PAID_ALL_ROUTE,
    ];
});
