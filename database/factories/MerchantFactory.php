<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Auth\User;
use Modules\Merchant\Entities\Merchant;
use Modules\Merchant\Enum\MerchantType;

$factory->define(Merchant::class, function (Faker $faker) {
	$user = factory(User::class)->create();
    return [
        'user_id' => $user->id,
        'name' => $faker->firstName,
        'mobile' => $faker->phoneNumber,
        'merchant_ref_id' => $faker->randomLetter,
        'merchant_secret' => $faker->randomLetter,
        'type' => MerchantType::ID_GENERAL
    ];
});
