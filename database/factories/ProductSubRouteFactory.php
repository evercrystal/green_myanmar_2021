<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\SubRoute\Entities\ProductSubRoute;
use Modules\SubRoute\Entities\SubRoute;
use Modules\ProductType\Entities\ProductType;

$factory->define(ProductSubRoute::class, function (Faker $faker) {
    return [
        'product_type_id' => factory(ProductType::class)->create()->id,
        'sub_route_id' => factory(SubRoute::class)->create()->id
    ];
});
