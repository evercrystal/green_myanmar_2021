<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\CollectionPoint\Entities\CollectionPoint;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotDeliveryCollection;

$factory->define(PivotDeliveryCollection::class, function (Faker $faker) {
    return [
        'collection_point_id' => factory(CollectionPoint::class)->create()->id,
        'sub_route_id' => factory(SubRoute::class)->create()->id
    ];
});
