<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\Destination\Entities\Destination;

class DestinationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $destinations = [
            ['id' => '1', 'name' => 'Yangon','name_mm' => 'ရန်ကုန်' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '2', 'name' => 'Mandalay','name_mm' => 'မန္တလေး' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '3', 'name' => 'Nay Pyi Taw','name_mm' => 'နေပြည်တော်' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '4', 'name' => 'Mawlamyine','name_mm' => 'မော်လမြိုင်' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '5', 'name' => 'Kyain Seikgyi Township','name_mm' => 'Kyain Seikgyi Township' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '6', 'name' => 'Bago','name_mm' => 'ပဲခူး' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '7', 'name' => 'Pathein','name_mm' => 'Pathein' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '8', 'name' => 'Monywa','name_mm' => 'Monywa' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '9', 'name' => 'Sittwe','name_mm' => 'Sittwe' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '10', 'name' => 'Meiktila','name_mm' => 'Meiktila' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '11', 'name' => 'Myeik','name_mm' => 'Myeik' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '12', 'name' => 'Taunggyi','name_mm' => 'တောင်ကြီး' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '13', 'name' => 'Myingyan','name_mm' => 'Myingyan' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '14', 'name' => 'Dawei','name_mm' => 'Dawei' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '15', 'name' => 'Pyay','name_mm' => 'Pyay' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '16', 'name' => 'Hinthada','name_mm' => 'Hinthada' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '17', 'name' => 'Lashio','name_mm' => 'Lashio' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '18', 'name' => 'Pakokku','name_mm' => 'Pakokku' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '19', 'name' => 'Thaton','name_mm' => 'Thaton' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '20', 'name' => 'Pyin Oo Lwin','name_mm' => 'ပြင်ီဥးလွင်' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '21', 'name' => 'Yenangyaung','name_mm' => 'Yenangyaung' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '22', 'name' => 'Taungoo','name_mm' => 'Taungoo' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '23', 'name' => 'Thayetmyo','name_mm' => 'Thayetmyo' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '24', 'name' => 'Pyinmana','name_mm' => 'Pyinmana' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '25', 'name' => 'Magway','name_mm' => 'Magway' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '26', 'name' => 'Myitkyina','name_mm' => 'Myitkyina' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '27', 'name' => 'Chauk','name_mm' => 'Chauk' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '28', 'name' => 'Mogok','name_mm' => 'Mogok' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '29', 'name' => 'Nyaunglebin','name_mm' => 'Nyaunglebin' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '30', 'name' => 'Mudon','name_mm' => 'Mudon' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '31', 'name' => 'Shwebo','name_mm' => 'Shwebo' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '32', 'name' => 'Sagaing','name_mm' => 'Sagaing' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '33', 'name' => 'Taungdwingyi','name_mm' => 'Taungdwingyi' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '34', 'name' => 'Syriam','name_mm' => 'Syriam' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '35', 'name' => 'Bogale','name_mm' => 'Bogale' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '36', 'name' => 'Pyapon','name_mm' => 'Pyapon' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '37', 'name' => 'Yamethin','name_mm' => 'Yamethin' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '38', 'name' => 'Kanbe','name_mm' => 'Kanbe' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '39', 'name' => 'Kawthoung','name_mm' => 'Kawthoung' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '40', 'name' => 'Myaydo','name_mm' => 'Myaydo' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '41', 'name' => 'Minbu','name_mm' => 'Minbu' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '42', 'name' => 'Tharyarwady','name_mm' => 'Tharyarwady' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '43', 'name' => 'Thongwa','name_mm' => 'Thongwa' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '44', 'name' => 'Kyaiklat','name_mm' => 'Kyaiklat' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '45', 'name' => 'Tachilek','name_mm' => 'Tachilek' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '46', 'name' => 'Maubin','name_mm' => 'Maubin' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '47', 'name' => 'Kyaukse','name_mm' => 'Kyaukse' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '48', 'name' => 'Hpa-An','name_mm' => 'Hpa-An' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '49', 'name' => 'Kyaikto','name_mm' => 'Kyaikto' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '50', 'name' => 'Martaban','name_mm' => 'Martaban' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '51', 'name' => 'Kyaikkami','name_mm' => 'Kyaikkami' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '52', 'name' => 'Bhamo','name_mm' => 'Bhamo' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '53', 'name' => 'Twante','name_mm' => 'Twante' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '54', 'name' => 'Myawadi','name_mm' => 'Myawadi' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '55', 'name' => 'Mawlaik','name_mm' => 'Mawlaik' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '56', 'name' => 'Wakema','name_mm' => 'Wakema' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '57', 'name' => 'Myanaung','name_mm' => 'Myanaung' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '58', 'name' => 'Pyu','name_mm' => 'Pyu' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '59', 'name' => 'Kayah','name_mm' => 'Kayah' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '60', 'name' => 'Nyaungdon','name_mm' => 'Nyaungdon' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '61', 'name' => 'Mawlamyinegyunn','name_mm' => 'Mawlamyinegyunn' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '62', 'name' => 'Letpandan','name_mm' => 'Letpandan' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '63', 'name' => 'Thanatpin','name_mm' => 'Thanatpin' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '64', 'name' => 'Paungde','name_mm' => 'Paungde' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '65', 'name' => 'Hakha','name_mm' => 'Hakha' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '66', 'name' => 'Loikaw','name_mm' => 'Loikaw' , 'country_id' => '1', 'is_active' => '1','created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '67', 'name' => 'Falam','name_mm' => 'Falam' , 'country_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        Destination::insert($destinations);
    }
}
