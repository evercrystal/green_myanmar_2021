<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\Country\Entities\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
          ['id' => '1','name' => 'Myanmar', 'name_mm' => 'မြန်မာ' , 'country_code' => '959', 'is_active' => '1','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '2','name' => 'Singapore', 'name_mm' =>'စင်္ကာပူ' , 'country_code' => '5', 'is_active' => '1','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '3','name' => 'Thailand', 'name_mm' =>'ထိုင်း' , 'country_code' => '66', 'is_active' => '1','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '4','name' => 'China', 'name_mm' =>'တရုတ်' , 'country_code' => '86', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '5','name' => 'India', 'name_mm' =>'အိန္ဒိယ' , 'country_code' => '91', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '6','name' => 'Japan', 'name_mm' =>'ဂျပန်' , 'country_code' => '81', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '7','name' => 'United States', 'name_mm' =>'အမေရိကန်' , 'country_code' => '1', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '8','name' => 'Brunei', 'name_mm' =>'ဘရူနိုင်း' , 'country_code' => '673', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '9','name' => 'Cambodia', 'name_mm' =>'ကမ္ဘောဒီးယား' , 'country_code' => ' 855', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '10','name' => 'Hong Kong', 'name_mm' =>'ဟောင်ကောင်' , 'country_code' => '852', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '11','name' => 'Laos', 'name_mm' =>'လာအို' , 'country_code' => '856', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '12','name' => 'Malaysia', 'name_mm' =>'မလေးရှား' , 'country_code' => '60', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '13','name' => 'Philippines', 'name_mm' =>'ဖိလစ်ပိုင်' , 'country_code' => '63', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '14','name' => 'South Korea', 'name_mm' =>'တောင်ကိုရီးယား' , 'country_code' => '82', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '15','name' => 'United Kingdom', 'name_mm' =>'ယူနိုက်တက်ကင်းဒမ်း' , 'country_code' => '44', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '16','name' => 'Vietnam', 'name_mm' =>'ဗီယက်နမ်' , 'country_code' => '84', 'is_active' => '0','created_at' => Carbon::now(),'updated_at' => Carbon::now()]
        ];

        Country::insert($countries);
    }
}
