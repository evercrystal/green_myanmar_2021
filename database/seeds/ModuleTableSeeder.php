<?php

use Illuminate\Database\Seeder;
use App\Enums\Table;

class ModuleTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

         $this->truncateMultiple([
            Table::COUNTRY,
            Table::DESTINATION,
        ]);

        $this->call([
            CountryTableSeeder::class,
            DestinationTableSeeder::class,
	    ]);

        $this->enableForeignKeys();
    }
}
