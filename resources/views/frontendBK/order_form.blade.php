@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.order_form.box_title'))

@push('after-styles')
 {{ style('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}
 {{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
<style type="text/css">
  .row:before,.row:after {
      display: none !important;
      }

  #from_township,#to_township,#box,#pickup_date {
    cursor : pointer;
  }
  .error {
    color:red;
  }

  /* The radio */
.radio {
 
     display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 20px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none
}

/* Hide the browser's default radio button */
.radio input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkround {
    position: absolute;
    top: 6px;
    left: 0;
    height: 20px;
    width: 20px;
    border-color:#2074f8;
    border-style:solid;
    border-width:2px;
     border-radius: 50%;
}


/* When the radio button is checked, add a blue background */
.radio input:checked ~ .checkround {
    background-color: #fff;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkround:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.radio input:checked ~ .checkround:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.radio .checkround:after {
     left: 2px;
    top: 2px;
    width: 12px;
    height: 12px;
    border-radius: 50%;
    background:#2074f8;
 
}

/* IMAGE STYLES */
.boxData + img {
  cursor: pointer;
}

/* CHECKED STYLES */
.boxData:checked + img {
  outline: 2px solid #f00;
}

tr,th,td{ padding:0; } 
</style>
@endpush

@section('content')

    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg4.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">@lang('labels.frontend.order_form.box_title')</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row justify-content-center">
        <div class="col col-sm-8 align-self-center">
            <div class="card">
                <div class="card-body">
                    @include('includes.partials.messages')
                    {{ html()->form('', '#')->id('calculatePrice')->open() }}
                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.from').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('from_township_id') }}
                        <div class="col-sm-10">
                          <input type="hidden" id="from_township_id" name="from_township_id" @if($location) value="{{$location->township_id}}" @endif>
                          <input type="hidden" id="pickup_collection_point" name="pickup_collection_point">
                          <input type="text" name="from_township" id="from_township" data-toggle="modal" data-target="#fromToTownship" data-whatever="From" class="form-control" placeholder="From" required readonly="" @if($location) value="@if($location->address_detail) {{$location->address_detail}},@endif {{$location->township->name.','.$location->city->name}}" @endif>
                        </div>
                      </div>

                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.to').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('to_township_id') }}

                        <div class="col-sm-10">
                          <input type="hidden" id="to_township_id" name="to_township_id">
                          <input type="hidden" id="delivery_collection_point" name="delivery_collection_point">
                          <input type="text" name="to_township" id="to_township" data-toggle="modal" data-target="#fromToTownship" data-whatever="To" class="form-control" placeholder="To" required readonly="">
                        </div>
                      </div>
                      <input type="hidden" id="from_address" name="from_address" @if($location) value="{{$location->address_detail}}" @endif>
                      <input type="hidden" id="to_address" name="to_address">
                      <input type="hidden" id="from_country_id" name="from_country_id" @if($location) value="{{$location->country_id}}" @endif>
                      <input type="hidden" id="to_country_id" name="to_country_id">
                      <input type="hidden" id="from_city_id" name="from_city_id" @if($location) value="{{$location->city_id}}" @endif>
                      <input type="hidden" id="to_city_id" name="to_city_id">
                      <input type="hidden" id="from_township_or_collection" name="from_township_or_collection">
                      <input type="hidden" id="to_township_or_collection" name="to_township_or_collection">
                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.product_type').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('product_type_id') }}

                        <div class="col-sm-10">
                          <select name="product_type_id" id="product_type_id" class="form-control">
                               <option value=''>Select Product Type</option>
                                @foreach($productTypes as $productType)
                                    <option value="{{$productType->id}}">{{ $productType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>

                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.no_of_item').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('no_of_item') }}

                        <div class="col-sm-10">
                          {{ html()->number('no_of_item')
                                        ->class('form-control')
                                        ->id('no_of_item')
                                        ->placeholder(__('labels.frontend.order_form.no_of_item'))
                                        ->attribute('maxlength', 191)
                                        ->attribute('min', 1)
                                        ->required()}}
                        </div>
                      </div>

                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.select_box').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('box_id') }}

                        <div class="col-sm-10">
                            <input type="hidden" id="box_id" name="box_id">
                            <input type="text" name="box" id="box" data-toggle="modal" data-target="#boxModal" data-whatever="Choose Box" class="form-control" placeholder="Box" required readonly="">
                        </div>
                      </div>

                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.weight').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('weight') }}

                        <div class="col-sm-10">
                          {{ html()->number('weight')
                                        ->class('form-control')
                                        ->id('weight')
                                        ->placeholder(__('labels.frontend.order_form.weight'))
                                        ->attribute('maxlength', 191)
                                        ->attribute('min', 1)
                                        ->required()}}
                        </div>
                      </div>

                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.pickup_time').'<span class="text-danger">*</span>')->class('col-sm-2 col-form-label')->for('pickup_time') }}

                        <div class="col-sm-5">
                          {{ html()->text('pickup_date')
                                ->class('form-control pickup_date')
                                ->id('pickup_date')
                                ->readonly('readonly')
                                ->required()}}
                        </div>
                        <div class="col-sm-5">
                          <select name="pickup_schedule_id" id="pickup_schedule_id" class="form-control select2" requried>
                                @foreach($pickupTimeScheduleSettings as $pickup)
                                    <option value="{{$pickup['id']}}">{{ $pickup['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      @if(auth()->user())
                      <div class="form-group row">
                        {{ html()->label(__('labels.frontend.order_form.same_pickup_delivery'))->class('col-sm-2 col-form-label')->for('same_pickup_delivery') }}
                        <div class="col-sm-10">
                          <select class="js-data-order-ajax form-control select2" name="same_pickup_delivery" id='same_pickup_delivery'>
                            <option></option>
                          </select>
                        </div>
                      </div>
                      @endif
                      <div class="form-group">
                        <div class="form-check" style="text-align:center;">
                          <input class="form-check-input" type="checkbox" value="" id="invalidCheck">
                          <label class="form-check-label text-danger ml-4" for="invalidCheck" >
                            {{ trans('labels.frontend.order_form.order_info') }}
                          </label>
                        </div>
                      </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix text-center">
                                    <button type="submit" class="btn btn-success" id="calculatePriceButton" disabled="disabled">{{ trans('labels.frontend.order_form.calculate_price') }}</button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                </div><!--card-body-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
    <div class="modal fade" id="fromToTownship" tabindex="-1" role="dialog" aria-labelledby="fromToTownshipLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="fromToTownshipLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="country_id" class="col-form-label">{{ trans('labels.frontend.order_form.country') }} <span class="text-danger">*</span> :</label>
                <select name="country_id" id="country_id" class="form-control select-country">
                    @foreach($countries as $country)
                        <option value="{{$country->id}}">{{ $country->name}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="city_id" class="col-form-label">{{ trans('labels.frontend.order_form.city') }} <span class="text-danger">*</span> :</label>
                <select name="city_id" id="city_id" class="form-control select-city" requried>
                    <option value=""></option>
                </select>
                <span style="color:red;display:none;" id="city_text">{{ trans('labels.frontend.order_form.city_not_found') }}</span>
              </div>

              <input type="hidden" id="township_id" name="township_id">
              <input type="hidden" id="collection_id" name="collection_id">
              <div class="form-group">
                <label for="township-name" class="col-form-label">{{ trans('labels.frontend.order_form.enter_township_or_collection') }} <span class="text-danger">*</span> :</label>
                <select class="js-data-township-colletion-ajax form-control select2" name="township_or_collection" id='township_or_collection'>
                  <option></option>
                </select>
                <span style="color:red;display:none;" id="township_collection_text">{{ trans('labels.frontend.order_form.township_and_collection_not_found') }}</span>
              </div>


              <div class="form-group">
                <label for="address_name" class="col-form-label">{{ trans('labels.frontend.order_form.address') }} <span class="text-danger">*</span>:</label>
                <textarea type="text" name="address_name" class="form-control" id="address_name" required>
                </textarea>
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('labels.frontend.order_form.close') }}</button>
            <button type="button" class="btn btn-primary" id="setFromToTownship" disabled="disabled">{{ trans('labels.frontend.order_form.set') }}</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="boxModal" tabindex="-1" role="dialog" aria-labelledby="boxLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="boxLabel">{{ trans('labels.frontend.order_form.choose_box') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              @foreach($boxes as $box)
              <div class="custom-control custom-radio">
                <label>
                  <input type="radio" name="box_data" value="{{$box->id}}" class="custom-control-input boxData">
                  <img src="{{asset('uploads/'.$box->image) }}" alt="" class="img-thumbnail" width="80px">&nbsp {{ $box->name }}
                <label>
              </div>
              @endforeach
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('labels.frontend.order_form.close') }}</button>
            <button type="button" class="btn btn-primary" id="setBox" disabled="disabled">{{ trans('labels.frontend.order_form.set') }}</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="saveOrderModal" tabindex="-1" role="dialog" aria-labelledby="saveOrderModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="saveOrderModalLabel">{{ trans('labels.frontend.order_form.save_order_form') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {{ html()->form('', '#')->id('saveOrder')->open() }}
          <div class="modal-body">
              <div class="row font-weight-bold">
                <div class="col-md-6 col-sm-6 col-4">{{ trans('labels.frontend.order_form.from') }}</div>
                <div class="col-md-6 col-sm-6 col-6" >: <span id="from_township_order"></span></div>
              </div>

              <div class="row font-weight-bold">
                <div class="col-md-6 col-sm-6 col-4">{{ trans('labels.frontend.order_form.to') }}</div>
                <div class="col-md-6 col-sm-6 col-6" >: <span id="to_township_order"></span></div>
              </div>

              <div class="row font-weight-bold">
                <div class="col-md-6 col-sm-6 col-4">{{ trans('labels.frontend.order_form.product_type') }}</div>
                <div class="col-md-6 col-sm-6 col-6">: <span id="product_type_order"></span></div>
              </div>

              <div class="row font-weight-bold">
                <div class="col-md-6 col-sm-6 col-4">{{ trans('labels.frontend.order_form.no_of_item') }}</div>
                <div class="col-md-6 col-sm-6 col-6">: <span id="no_of_item_order"></span></div>
              </div>

              <div class="row font-weight-bold">
                <div class="col-md-6 col-sm-6 col-4">{{ trans('labels.frontend.order_form.delivery_fee') }}</div>
                <div class="col-md-6 col-sm-6 col-6">: <span id="delivery_fee_order"></span></div>
              </div>

              <div class="form-group">
                <label for="receiver-name" class="col-form-label">{{ trans('labels.frontend.order_form.receiver_name') }} <span class="text-danger">*</span>  :</label>
                <input type="text" name="receiver_name" class="form-control" id="receiver_name" required placeholder="Reciever Name">
              </div>

              <div class="form-group">
                <label for="receiver-phone" class="col-form-label">{{ trans('labels.frontend.order_form.receiver_phone') }} <span class="text-danger">*</span>  :</label>
                <input type="text" name="receiver_phone" class="form-control" id="receiver_phone" required placeholder="Reciever Phone">
              </div>

              <div class="form-group">
                <label for="pickup-name" class="col-form-label">{{ trans('labels.frontend.order_form.pickup_name') }}  :</label>
                <input type="text" name="pickup_name"  class="form-control"  @if(isset(auth()->user()->merchant))
                      value="{{ auth()->user()->merchant->name}}" readonly
                    @endif id="pickup_name"  placeholder="Pickup Name" required >
              </div>
              
              <div class="form-group">
                <label for="pickup-phone" class="col-form-label">{{ trans('labels.frontend.order_form.pickup_phone') }}   :</label>
                <input type="text" name="pickup_phone" class="form-control"      
                  @if(isset(auth()->user()->merchant))
                    value="{{ auth()->user()->merchant->mobile}}" readonly
                  @endif id="pickup_phone" placeholder="Pickup Phone" required>
              </div>

              <div class="form-group">
                <label for="remark" class="col-form-label">{{ trans('labels.frontend.order_form.customer_order_number') }}  :</label>
                <input type="text" name="customer_order_no" class="form-control" id="customer_order_no" placeholder="Customer Order No. (Optional)">
              </div>

              <div class="form-group">
                <label for="remark" class="col-form-label">{{ trans('labels.frontend.order_form.remark') }}  :</label>
                <textarea name="remark" class="form-control" id="remark"  placeholder="Remark (Optional)"></textarea>
              </div>

              <div class="form-group">
                <label for="receiver-name" class="col-form-label">{{ trans('labels.frontend.order_form.payment_mode') }} :</label>
                <select name="payment_mode_id" id="payment_mode_id" class="form-control select2">
                      @foreach($paymentModes as $paymentMode)
                          <option value="{{$paymentMode['id']}}">{{ $paymentMode['name'] }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="advanceOrderCheck">
                  <label class="form-check-label ml-4" for="advanceOrderCheck">
                    {{ trans('labels.frontend.order_form.required_cash_on_delivery') }}
                  </label>
                </div>
              </div>

              <div class="form-group d-none" id="advanceOrderPrice">
                <label for="advance_order_price" class="col-form-label">{!! trans('labels.frontend.order_form.max_order_price', ['max_order_price'=>config('appsetting.order.max_order_price')]) !!} :</label>
                <input type="text" name="advance_order_price" id="max_order_price" class="form-control"  placeholder="Advance Order Price">
              </div>


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('labels.frontend.order_form.close') }}</button>
            <button type="submit" class="btn btn-primary" id="proceedToNextStep">{{ trans('labels.frontend.order_form.proceed_to_next_step') }}</button>
          </div>
          {{ html()->form()->close() }}

        </div>
      </div>
    </div>

    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="paymentModalLabel">{{ trans('labels.frontend.order_form.choose_payment_method') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {{ html()->form('POST', route('frontend.save_payment'))->id('savePayment')->open() }}
          <input id="service_fee" name="service_fee" value="" type="hidden">
          <input id="payment_method" name="payment_method" value="" type="hidden">
          <input id="amount" name="amount" value="" type="hidden">
          <input id="order_id" name="order_id" value="" type="hidden">
          <div class="modal-body">
            <div class="">
              <div class="text-center">
                <p>Sender Payable Delivery Fee <span id="sender_clearance_amount"></span> MMK</p>
                <p>Receiver Payable Delivery Fee <span id="receiver_clearance_amount"></span> MMK</p>
              </div>
              <p class="text-danger">Sender Can Pay with below payments.</p>
              @if(config('appsetting.payment.cod.enable') == 'true')
                <label class="radio">Cash
                  <input type="radio" checked="checked" name="payment" value="cod">
                  <span class="checkround"></span>
                </label>
              @endif
              @if(config('appsetting.payment.transfer.enable') == 'true')
                <label class="radio">Bank Tranfer
                  <input type="radio" name="payment" value="bank_transfer">
                  <span class="checkround"></span>
                </label>
              @endif
              @if(config('appsetting.payment.topup.enable') == 'true')
                <label class="radio">Bnf Topup
                  <input type="radio" name="payment" value="bnf_topup">
                  <span class="checkround"></span>
                </label>
              @endif
              @if(config('appsetting.payment.cod.enable') == 'true')
                <label class="radio">MPU
                  <input type="radio" name="payment" value="mpu">
                  <span class="checkround"></span>
                </label>
              @endif
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="proceedToPayment">{{ trans('labels.frontend.order_form.proceed_to_next_step') }}</button>
          </div>
          {{ html()->form()->close() }}

        </div>
      </div>
    </div>
@endsection

@push('after-scripts')
{{ script("assets/plugins/moment.min.js") }}
{{ script("assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js") }}
{{ script('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}
{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script('assets/plugins/bootbox/bootbox.min.js')}}
{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $(".select-country").select2({
          placeholder: 'Select Country',
          width: '100%'
      });
      $(".select-city").select2({
          placeholder: 'Select City',
          width: '100%'
      });

      function refresh_address(){
        $('#address_name'). removeAttr('disabled','disabled');
        if($('#address_name').val()) {
          $('#address_name').keyup();
        }
      }

      $('#pickup_date').daterangepicker({
        singleDatePicker: true,
        minDate: moment(),
        autoUpdateInput: true,
        locale: {
                  format: 'YYYY-MM-DD'
              },
        }, function(chosen_date) {
          calculatePriceButtonEnable();
        $('#pickup_date').val(chosen_date.format('YYYY-MM-DD'));
      });

      var recipient;
      $('#fromToTownship').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text(recipient)
        if(recipient == 'From' && $('#from_township_id').val()) {
          $("#country_id").val($('#from_country_id').val());
          $("#country_id").select2().trigger("change");              
        }
        else if(recipient == 'To' && $('#to_township_id').val()) {
           $("#country_id").val($('#to_country_id').val());
           $("#country_id").select2().trigger("change");
        } else {
           $("#city_id").val(null).trigger("change");
        }
      });

      var countryId = $('#country_id').val();
      $.ajax({
            url: "{{ url('order-form/get-city') }}/"+countryId,
            type: 'GET',
            success: function (data){
                $('#township_or_collection').attr('disabled','disabled');

                refresh_address();
                $.each(data, function(i, value) {
                    $('#city_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                });    
            },
        });

      var selectCityOption = new Option("Select City", "");

      $('#country_id').on('change', function(){
        run_waitMe();
        countryId = $(this).val();
        if(recipient == 'From'){
          $('#from_country_id').val(countryId);
        }else{
          $('#to_country_id').val(countryId);
        }
        $('#city_id').empty();
        $('#city_id').removeAttr('disabled','disabled');
        selectCityOption.selected = true;
        $('#setFromToTownship').attr('disabled','disabled');
        $.ajax({
            url: "{{ url('order-form/get-city') }}/"+countryId,
            type: 'GET',
            success: function (data){
              if(data.length == 0) {
                    $('#city_id').attr('disabled','disabled');
                    $('#city_id > option').prop("selected",false);
                    $("#city_id").append(selectCityOption);
                    $("#city_id").trigger("change");
                    $('#city_text').show();
                    refresh_address()
                    stop_waitMe();

                }else {
                    $('#city_id').removeAttr('disabled','disabled');
                    $('#city_id').find('option').remove().end();
                    $('#city_id > option').prop("selected",false);
                    $("#city_id").append(selectCityOption);
                    $('#city_text').hide();
                    refresh_address()
                    $.each(data, function(i, value) {
                        $('#city_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                    });
                    if(recipient == 'From' && $('#from_township_id').val() ) {
                      $("#city_id").val($('#from_city_id').val());
                    }
                    if(recipient == 'To' && $('#to_township_id').val() ) {
                      $("#city_id").val($('#to_city_id').val());
                    }
                    $("#city_id").select2().trigger("change");
                    stop_waitMe();
                }  
            }, 
        });
    
      });

      $('#city_id').on('change', function(){
        run_waitMe();
        var cityId = $(this).val();
        if(recipient == 'From'){
          $('from_city_id').val(cityId);
        }else{
          $('to_city_id').val(cityId);
        }
        if(cityId){
            $('#township_id').empty();
            $('#township_id').removeAttr('disabled','disabled');
            $('#setFromToTownship').attr('disabled','disabled');
            $.ajax({
                url: "{{ url('order-form/get-township/') }}/"+cityId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {
                        $("#township_or_collection").val(null).trigger("change");
                        $('#township_or_collection').attr('disabled','disabled');
                        $('#township_collection_text').show();
                        refresh_address()
                        stop_waitMe();

                    }else {
                        $('#township_or_collection').removeAttr('disabled','disabled');
                        $('#township_collection_text').hide();

                        refresh_address()
                        
                        if(recipient == 'From' && $('#from_township_id').val() ) {
                          $("#township_id").val($('#from_township_id').val());
                          $("#collection_id").val($('#pickup_collection_point').val());
                          if(cityId == $('#from_city_id').val()) {
                            $("#township_or_collection").val($('#from_township_or_collection').val()).trigger("change"); 
                          }else {
                            $("#township_or_collection").val(null).trigger("change");
                          }
                        }
                         if(recipient == 'To' && $('#to_township_id').val() ) {
                          $("#township_id").val($('#to_township_id').val());
                          $("#collection_id").val($('#delivery_collection_point').val());
                          if(cityId == $('#to_city_id').val()) {
                            $("#township_or_collection").val($('#to_township_or_collection').val()).trigger("change"); 
                          }else {
                            $("#township_or_collection").val(null).trigger("change");
                          }
                        } 
                        
                        stop_waitMe();
                    }  
                }
            });
        }else{
          $("#township_or_collection").val(null).trigger("change");
          $('#township_or_collection').attr('disabled','disabled');
          $('#township_collection_text').hide(); 
        } 
    });

    $('#township_or_collection').on('change', function(){
        var selectedData = $(this).val();
        if(selectedData) {
          var dataArr = selectedData.split("-");

          if(dataArr[1] != 'undefined') {
            $("#township_id").val(dataArr[1]);
            $("#collection_id").val(dataArr[0]);
            $('#address_name').val(dataArr[2]);
            $('#address_name'). attr('disabled','disabled');
            $('#address_name').keyup();

          } else {
            $("#township_id").val(dataArr[0]);
            $("#collection_id").val('');
            $('#address_name').val('');

            refresh_address()
          }
          if(recipient == 'From' && $('#from_township_id').val() && (selectedData == $('#from_township_or_collection').val()) ) {
              $('#address_name').val($('#from_address').val()); 
              $('#address_name').keyup();
          }
          if(recipient == 'To' && $('#to_township_id').val() && (selectedData == $('#to_township_or_collection').val()) ) {
              $('#address_name').val($('#to_address').val()); 
              $('#address_name').keyup();
          }
        } else {
          stop_waitMe();
          $('#address_name').val('');
          $('#address_name').keyup();
        }
       
      });

      $('#address_name').keyup(function(){
        if($(this).val() &&  $('#township_or_collection').val()) {
          $('#setFromToTownship').removeAttr('disabled','disabled');
        } else {
          $('#setFromToTownship').attr('disabled','disabled');
        }
      })

      $('#setFromToTownship').click(function(){
        var townshipId = $('#township_id').val();
        if($.isNumeric(townshipId)) {
          if(recipient == 'From'){
            $('#from_township_id').val(townshipId);  
            $('#from_country_id').val($('#country_id').val());  
            $('#from_city_id').val($('#city_id').val());  
            $('#pickup_collection_point').val($('#collection_id').val());    
            $('#from_address').val($('#address_name').val());
            var fromAddress = ($('#address_name').val()) ? $('#address_name').val() + ', ' : '';
            var pickup = $('#township_or_collection').select2('data')[0].name + ', ' ;
            $('#from_township').val(fromAddress+pickup+$( "#city_id option:selected" ).text());
            $('#from_township_or_collection').val($('#township_or_collection').val());

          } else {
            $('#to_township_id').val(townshipId);
            $('#to_country_id').val($('#country_id').val());  
            $('#to_city_id').val($('#city_id').val());
            $('#delivery_collection_point').val($('#collection_id').val());    
            $('#to_address').val($('#address_name').val());  
            var toAddress = ($('#address_name').val()) ? $('#address_name').val() + ', ' : '';
            var delivery = $('#township_or_collection').select2('data')[0].name + ', '; 
            $('#to_township').val(toAddress+delivery+$( "#city_id option:selected" ).text()); 
            $('#to_township_or_collection').val($('#township_or_collection').val());
          }
          calculatePriceButtonEnable();
          $('#fromToTownship').modal('hide');
        }
      })

      $('#invalidCheck').click(function(){
          if($(this).prop("checked") == true){
            $('#calculatePriceButton').prop('disabled', false);
          }
          else if($(this).prop("checked") == false){
            $('#calculatePriceButton').prop('disabled', true);
          }
      });

      $('#no_of_item,#weight').keyup(function() {
        calculatePriceButtonEnable();
      });

      $( "#product_type_id,#box_id").change(function() {
        calculatePriceButtonEnable();
      });

      function calculatePriceButtonEnable() {
        if($('#no_if_item').val() != '' && $('#weight').val() != '' && $('#from_township').val() != '' && $('#to_township').val() != '' && $('#product_type_id').val() != '' && $('#box_id').val() != '' && ($('#invalidCheck').prop("checked") == true) )  {
            $('#calculatePriceButton').prop('disabled', false);
          }else {
            $('#calculatePriceButton').prop('disabled', true);
          }
      }

      $("#calculatePrice").validate({
          rules: {
              'from_township': {
                  required: true
              },
              'to_township': {
                  required: true
              },
              'no_of_item': {
                  required: true,
                  number:true,
                  min: 1,
              },
              'weight': {
                  required: true,
                  number:true,
                  min: 1,
              },
              'product_type_id': {
                  required: true
              },
              'box': {
                  required: true
              },
          },
          messages: {
              'from_township': {
                  required: "{{trans('validation.required',['attribute' => 'from township'])}}"
              },
             'to_township': {
                  required: "{{trans('validation.required',['attribute' => 'to township'])}}"
              },
              'no_of_item': {
                  required: "{{trans('validation.required',['attribute' => 'no of item'])}}",
                  number: "{{trans('validation.numeric',['attribute' => 'no of item'])}}"
              },
              'weight': {
                  required: "{{trans('validation.required',['attribute' => 'weight'])}}",
                  number: "{{trans('validation.numeric',['attribute' => 'weight'])}}"
              },
              'product_type_id': {
                  required: "{{trans('validation.required',['attribute' => 'product type'])}}"
              },
              'box': {
                  required: "{{trans('validation.required',['attribute' => 'box size'])}}"
              },
          },
          submitHandler: function(form) {
            run_waitMe();
            $.ajax({
                url: '{{ route('frontend.claculate_price') }}',
                type: 'POST',
                data: {
                    from_township_id: $('#from_township_id').val(),
                    to_township_id: $('#to_township_id').val(),
                    pickup_collection_point: $('#pickup_collection_point').val(),
                    delivery_collection_point: $('#delivery_collection_point').val(),
                    product_type_id: $('#product_type_id').val(),
                    no_of_item: $('#no_of_item').val(),
                    box_id: $('#box_id').val(),
                    weight: $('#weight').val(),
                    pickup_schedule_id: $('#pickup_schedule_id').val(),
                    pickup_date: $('#pickup_date').val(),
                    "_token": "{{ csrf_token() }}",
                },
                success: function (data, textStatus, jQxhr) {
                    $('#from_township_order').text($('#from_township').val());
                    $('#to_township_order').text($('#to_township').val());
                    $('#product_type_order').text($( "#product_type_id option:selected" ).text());
                    $('#no_of_item_order').text($('#no_of_item').val());
                    $('#delivery_fee_order').text(data.data.total_amount);
                    stop_waitMe();
                    $('#saveOrderModal').modal('show');
                },
                error: function (error) {
                    stop_waitMe();
                    bootbox.alert(error.responseJSON.message);

                }
            });
          }
      });

      $('#receiver_name,#receiver_phone,#pickup_name,#pickup_phone,#max_order_price').keyup(function() {
          if($('#receiver_name').val() != '' && $('#receiver_phone').val() != '') {
             $('#proceedToNextStep').prop('disabled', false);
          }
       });

      var mpuFee,transferFee,bnfTopupFee,codFee,senderAmount;
      var maxOrderPrice = "{{config('appsetting.order.max_order_price')}}" ;
      var receiverPaymentId = {{ Modules\Order\Enum\PaymentModeType::ID_RECEIVER_WILL_PAID_ALL_ROUTE }};
      
      $("#saveOrder").validate({
          rules: {
              'receiver_name': {
                  required: true
              },
              'receiver_phone': {
                  required: true,
                  number: true,
              },
              'pickup_name': {
                  required: true
              },
              'pickup_phone': {
                  required: true,
                  number: true,
              },
              'advance_order_price': {
                  number: true,
                  min: 1,
                  max: parseInt(maxOrderPrice),
              },
          },
          messages: {
              'receiver_name': {
                  required: "{{trans('validation.required',['attribute' => 'receiver name'])}}",
              },
             'receiver_phone': {
                  required: "{{trans('validation.required',['attribute' => 'receiver phone'])}}",
                  number: "{{trans('validation.numeric',['attribute' => 'receiver phone'])}}"
              },
              'pickup_name': {
                  required: "{{trans('validation.required',['attribute' => 'pickup name'])}}",
              },
             'pickup_phone': {
                  required: "{{trans('validation.required',['attribute' => 'pickup phone'])}}",
                  number: "{{trans('validation.numeric',['attribute' => 'pickup phone'])}}"
              },
          },
          submitHandler: function(form) {
            run_waitMe();
            $.ajax({
              url: '{{ route('frontend.save_order') }}',
              type: 'POST',
              data: {
                  from_township_id: $('#from_township_id').val(),
                  to_township_id: $('#to_township_id').val(),
                  pickup_collection_point: $('#pickup_collection_point').val(),
                  delivery_collection_point: $('#delivery_collection_point').val(),
                  from_address: $('#from_address').val(),
                  to_address: $('#to_address').val(),
                  product_type_id: $('#product_type_id').val(),
                  no_of_item: $('#no_of_item').val(),
                  box_id: $('#box_id').val(),
                  weight: $('#weight').val(),
                  pickup_schedule_id: $('#pickup_schedule_id').val(),
                  pickup_date: $('#pickup_date').val(),
                  payment_mode_id: $('#payment_mode_id').val(),
                  receiver_name: $('#receiver_name').val(),
                  receiver_phone: $('#receiver_phone').val(),
                  pickup_name: $('#pickup_name').val(),
                  pickup_phone: $('#pickup_phone').val(),
                  remark: $('#remark').val(),
                  link: $('#same_pickup_delivery').val(),
                  advance_order_price: $('#max_order_price').val(),
                  customer_order_no: $('#customer_order_no').val(),
                  "_token": "{{ csrf_token() }}",
              },
              success: function (data, textStatus, jQxhr) {
                  stop_waitMe();
                  $('#saveOrderModal').modal('hide');
                  $('#sender_clearance_amount').text(data.sender_clearance_amount);
                  $('#receiver_clearance_amount').text(data.receiver_clearance_amount);
                  $('#order_id').val(data.order.id);
                  
                  mpuFee = parseFloat(data.payment_service_fee.mpu_fee);
                  transferFee = parseFloat(data.payment_service_fee.transfer_fee);
                  bnfTopupFee = parseFloat(data.payment_service_fee.bnf_topup_fee);
                  codFee = parseFloat(data.payment_service_fee.cod_fee);
                  senderAmount = parseFloat(data.sender_clearance_amount);
                  codAmount =parseFloat(codFee) + parseFloat(senderAmount);
                  $('#service_fee').val(parseFloat(codFee));
                  $('#payment_method').val('cod');
                  $('#amount').val(codAmount);

                  if($('#payment_mode_id').val() ==  receiverPaymentId)
                  {
                    window.location = data.receiver_payment_redirect_url;
                  }
                  else{
                    $('#paymentModal').modal('show');
                  }

              },
              error: function (error) {
                  stop_waitMe();
                  $('#saveOrderModal').modal('hide');
                    // this Error is an Object
                    var errors = error.responseJSON.errors;
                    var showErrors = '';
                    // Loop this object and pring Key or value or both
                    for (const [key, value] of Object.entries(errors)) {
                         showErrors += `${value}`;
                    }
                  if(showErrors) {
                    bootbox.alert(showErrors);
                  }else {
                    bootbox.alert(error.responseJSON.message);
                  }
              }
          });
          }
      });
      
      $('#payment_mode_id').change(function(){

        if($('#payment_mode_id').val() == receiverPaymentId)
        {
          $("#proceedToNextStep").html("{{ trans('labels.frontend.order_form.proceed_to_order') }}");
        }
        else{
          $("#proceedToNextStep").html("{{ trans('labels.frontend.order_form.proceed_to_next_step') }}");

        }
      })

      $('input[type=radio][name=payment]').change(function(){
          if($(this).val() == "mpu"){
              var mpuAmount =parseFloat(mpuFee) + parseFloat(senderAmount);
              $('#service_fee').val(parseFloat(mpuFee));
              $('#payment_method').val('mpu');
              $('#amount').val(mpuAmount);
          }
          else if($(this).val() == "bank_transfer"){
              var transferAmount =parseFloat(transferFee) + parseFloat(senderAmount);
              $('#service_fee').val(parseFloat(transferFee));
              $('#payment_method').val('bank_transfer');
              $('#amount').val(transferAmount);   
          }
          else if($(this).val() == "cod"){
              var codAmount =parseFloat(codFee) + parseFloat(senderAmount);
              $('#service_fee').val(parseFloat(codFee));
              $('#payment_method').val('cod');
              $('#amount').val(codAmount);   
          }
          else {
              topupAmount =parseFloat(bnfTopupFee) + parseFloat(senderAmount);
              $('#service_fee').val(parseFloat(bnfTopupFee));
              $('#payment_method').val('bnf_topup');
              $('#amount').val(topupAmount);
          }
      });

      $('#savePayment').submit(function(e){
          e.preventDefault();
          run_waitMe();
          $.ajax({
              url: $('#savePayment').attr('action'),
              type: "POST",
              data: $('#savePayment').serialize() + '&payment_mode_id=' + $('#payment_mode_id').val(),
              success: function(data) {
                  stop_waitMe();
                  window.location = data.payment_redirect_url;
              },
              error: function(error,status){
                stop_waitMe();
                $('#paymentModal').modal('hide');
                bootbox.alert(error.responseJSON.message);
              }           
          });

      });

      $('input[type=radio][name=box_data]').change(function(){
          if($(this).val() > 0) {
            $('#setBox').prop('disabled', false);
          }
      });

      $('#setBox').click(function(){
        var boxId = $('[name="box_data"]:checked').val();
        $('#box_id').val(boxId);
        $('#box').val($('[name="box_data"]:checked').closest('label').text());
        $('#boxModal').modal('hide');
        calculatePriceButtonEnable();
      });

      $('#advanceOrderCheck').click(function(){
        if($(this).prop("checked") == true){
          $('#advanceOrderPrice').removeClass('d-none');
        }
        else if($(this).prop("checked") == false){
          $('#advanceOrderPrice').addClass('d-none');
        }
      });

    })
  </script>
@endpush