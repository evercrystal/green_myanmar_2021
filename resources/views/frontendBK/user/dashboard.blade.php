@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )
@push('after-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
{{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
<style>
    .row:before,.row:after {
        display: none !important;
        }
</style>
@endpush
@section('content')

<div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg1.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                            @lang('navs.frontend.dashboard')
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-sm-12 col-md-2 mb-4">
                            <div class="card mb-4 bg-light">
                                <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture">

                                <div class="card-body">
                                    <h4 class="card-title">
                                        {{ $logged_in_user->name }}<br/>
                                    </h4>

                                    <p class="card-text">
                                        <small>
                                            <i class="fas fa-envelope"></i> {{ $logged_in_user->email }}<br/>
                                            <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined') {{ timezone()->convertToLocal($logged_in_user->created_at, 'F jS, Y') }}
                                        </small>
                                    </p>

                                    <p class="card-text">

                                        <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm mb-1">
                                            <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
                                        </a>

                                        @can('view backend')
                                            &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-danger btn-sm mb-1">
                                                <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
                                            </a>
                                        @endcan
                                    </p>
                                </div>
                            </div>

                            <div class="card mb-4">
                                <div class="list-group" id="list-tab" role="tablist">
                                    <a class="list-group-item list-group-item-action active show" id="list-clear-list" data-toggle="tab" href="#order_list" role="tab" aria-controls="list-clear" aria-selected="false">Order list</a>
                                </div>
                            </div><!--card-->
                        </div><!--col-md-4-->

                        <div class="col-md-10 col-sm-12">
                            <div class="tab-pane fade in active show" id="order_list" role="tabpanel" aria-labelledby="list-clear-list">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        Order List
                                    </div>

                                    <div class="card-body table-responsive" id="transaction_clear">
                                        <div class="row mb-4">

                                            <div class="col-md-3">
                                                <select name="from_city" id="fromcity_id" class=" select form-control">
                                                        <option value="" selected>Select City</option>

                                                    @foreach($cities as $city)
                                                        <option value="{{ $city->id }}"> {{ $city->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <select name="to_city" id="tocity_id" class="form-control select">
                                                        <option value="" selected>Select City</option>

                                                        @foreach($cities as $city)
                                                            <option value="{{ $city->id }}"> {{ $city->name }}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                         
                                            <div class="col-md-3">
                                                <select name="status" id="status_id" class="form-control select">
                                                        <option value="" selected>Select status</option>

                                                    @foreach($listTypes as $key=>$value)
                                                        <option value="{{ $key }}"> {{ $value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            

                                            <div class="col-md-3">
                                                <input name="ref_id" id="ref_id" class="form-control" type="text" placeholder="Ref No">
                                            </div>

                                            <div class="col-md-6 col-lg-4 mt-4">
                                                <input name="date" class="form-control choose_date_range" type="text" placeholder="Choose Date Range" id="date_range">
                                            </div>
                                            <div class="col-md-2 col-lg-4"><br>
                                                 <button type="button" id="orderSearch" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                                            </div>
                                        </div>  

                                        <div class="table-responsive">
                                            <table id="order-table" class="table table-condensed table-hover">
                                                <thead>
                                                <tr>
                                                    <th>{{ __('order::labels.backend.order.table.created') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.ref_no') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.from_city') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.to_city') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.from_township') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.to_township') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.product_type') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.total_amount') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.status') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.ref_qr') }}</th>
                                                    <th>{{ __('order::labels.backend.order.table.last_updated') }}</th> 
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                        </div><!--col-md-8-->
                    </div><!-- row -->
                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
@push('after-scripts')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
{{ script('https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js')}}
{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
{{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
{{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $('.choose_date_range').daterangepicker({
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                "autoUpdateInput": false,
                locale: {
                      format: 'YYYY-MM-DD HH:mm:ss'
                    },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function(start, end, label) {

            }).attr('readOnly','true').css('cursor','pointer'); 

            $('.choose_date_range').on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm:ss') + ' - ' + picker.endDate.format('YYYY-MM-DD HH:mm:ss'));
              });

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            $(".select").select2({
            
            });
            var order_table;
            LoadOrder();

            $('#orderSearch').click(function () {
                order_table.destroy();
                LoadOrder();
            });

            function LoadOrder() {
                
              order_table = $('#order-table').DataTable({           
                language:
                    {
                        processing: "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
                    },
                processing: true,
                serverSide: true,
                searching: false,
                bFilter: false,
                ajax: {
                    url: '{!! route("frontend.user.dashboard.get_order") !!}',
                    type: "POST",
                    data: { 
                            'status_id': $('#status_id').val(),
                            'from_city_id': $('#fromcity_id').val(),
                            'to_city_id': $('#tocity_id').val(),
                            'ref_id': $('#ref_id').val(),
                            'date': $('#date_range').val(),
                    },
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [

                    {data: 'created_at', name: 'created_at'},
                    {data: 'ref_no', name: 'ref_no'},
                    {data: 'from_city_id', name: 'from_city_id'},
                    {data: 'to_city_id', name: 'to_city_id'},
                    {data: 'from_township_id', name: 'from_township_id'},
                    {data: 'to_township_id', name: 'to_township_id'},
                    {data: 'product_type_id', name: 'product_type_id'},
                    {data: 'total_amount', name: 'total_amount'},
                    {data: 'status', name: 'status'},
                    {data: 'ref_qr', name: 'ref_qr'},
                    {data: 'updated_at', name: 'updated_at'},

                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                }
            }); 
            }      
        });

    </script>

@endpush