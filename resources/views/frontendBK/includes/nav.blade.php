<header class="header mob-header cart-true fixed-true nz-clearfix">
    <div class="mob-header-top nz-clearfix">
        <div class="container">
            <div class="logo logo-mob">
                <a href="#" title="{{ app_name() }}">
                    <img id="img_97a1_0" src="{{ config('appsetting.basic.main_logo') }}" alt="{{ app_name() }}">
                </a>
            </div>
            <span class="mob-menu-toggle"></span>
        </div>
    </div>
    <div class="mob-header-content nz-clearfix">
        <div class="container">
            <nav class="mob-menu nz-clearfix">
                <ul id="mob-header-menu" class="menu">
                    <li class="menu-item {{ active_class(Route::is('frontend.index'), 'current-menu-item') }}">
                        <a href="{{ url('/') }}">
                            <span class="mi"></span>
                            <span class="txt">@lang('navs.general.home')</span>
                            <span class="di icon-arrow-down9"></span>
                        </a>
                    </li>
                    <li class="menu-item {{ active_class(Route::is('frontend.order_form'), 'current-menu-item') }}">
                        <a href="{{route('frontend.order_form')}}">  
                            <span class="mi"></span>
                            <span class="txt">@lang('navs.frontend.delivery')</span>
                            <span class="di icon-arrow-down9"></span>
                        </a>
                    </li>
                    <li class="menu-item {{ active_class(Route::is('frontend.contact'), 'current-menu-item') }}">
                        <a href="{{route('frontend.contact')}}">  
                            <span class="mi"></span>
                            <span class="txt">@lang('navs.frontend.contact')</span>
                            <span class="di icon-arrow-down9"></span>
                        </a>
                    </li>

                    @auth
                        <li class="menu-item {{ active_class(Route::is('frontend.user.dashboard'), 'current-menu-item') }}">
                            <a href="{{route('frontend.user.dashboard')}}">
                                <span class="mi"></span>
                                <span class="txt">@lang('navs.frontend.dashboard')</span>
                                <span class="di icon-arrow-down9"></span>
                            </a>
                        </li>
                    @endauth

                    @guest
                        <li class="menu-item {{ active_class(Route::is('frontend.auth.login'), 'current-menu-item') }}" >
                            <a href="{{route('frontend.auth.login')}}">
                                <span class="mi"></span>
                                <span class="txt">@lang('navs.frontend.login')</span>
                                <span class="di icon-arrow-down9"></span>
                            </a>
                        </li>

                        @if(config('access.registration'))
                            <li class="menu-item {{ active_class(Route::is('frontend.auth.register'), 'current-menu-item') }}" >
                                <a href="{{route('frontend.auth.register')}}">
                                    <span class="mi"></span>
                                    <span class="txt">@lang('navs.frontend.register')</span>
                                    <span class="di icon-arrow-down9"></span>
                                </a>
                            </li>
                        @endif
                    @else
                        <li class="menu-item">
                            <a href="#">
                                <span class="mi"></span>
                                <span class="txt">{{ $logged_in_user->name }}</span>
                                <span class="di icon-arrow-down9"></span>
                            </a>
                            <ul class="sub-menu">
                                <li  class="icon-home menu-item">
                                        @can('view backend')
                                            <a href="{{ route('admin.dashboard') }}">
                                                <span class="mi"></span>
                                                <span class="txt">@lang('navs.frontend.user.administration')</span>
                                                <span class="di icon-arrow-down9"></span>
                                            </a>
                                        @endcan
                                </li>
                                <li class="icon-dashboard menu-item">
                                    <a href="{{ route('frontend.user.account') }}">
                                        <span class="mi"></span>
                                        <span class="txt">@lang('navs.frontend.user.account')</span>
                                    </a>
                                </li>
                                <li class="icon-lock menu-item">
                                    <a href="{{ route('frontend.auth.logout') }}">
                                        <span class="mi"></span>
                                        <span class="txt">@lang('navs.general.logout')</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </nav>
            <div class="ls nz-clearfix">
                <div id="lang_sel">
                    <?php
                        $lang = app()->getLocale();
                    ?>
                    <ul>
                        <li><a href="#" class="lang_sel_sel icl-en">@lang('menus.language-picker.langs.'.$lang)</a>
                            <ul>
                                @if($lang != 'en')
                                    <li class="icl-en"><a href="{{ '/lang/en' }}">@lang('menus.language-picker.langs.en')</a></li>
                                @endif
                                @if($lang != 'mm')
                                    <li class="icl-mm"><a href="{{ '/lang/mm' }}">@lang('menus.language-picker.langs.mm')</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="search nz-clearfix">

                    <fieldset>
                        <input type="text"  name="ref_no"  placeholder="Track with Ref No or Phone"  style="width:100%;"value="" required />
                        <input type="submit"  class="search_track_ticket" value="Search" />
                    </fieldset>

            </div>
        </div>
    </div>
</header>

<header class="header desk desk-ls-true fixed-true cart-true search-true">
    <div class="header-content">
        <div class="container nz-clearfix">

            <div class="logo logo-desk">
                <a href="{{ url('/') }}" title="{{ app_name() }}">
                    <img id="img_97a1_1" src="{{ config('appsetting.basic.main_logo') }}" alt="{{ app_name() }}">
                </a>
            </div>

            <div class="search-toggle">&nbsp;</div>
            <div class="search">
                <fieldset>
                    <input type="text" id="track_value" name="ref_no"  placeholder="Track with Ref No or Phone" style="width:100%;" value="" required />
                    <input type="submit"  class="search_track_ticket" value="Search" />
                </fieldset>
            </div>

            <div class="ls nz-clearfix">
                <div id="lang_sel">
                    <?php
                        $lang = app()->getLocale();
                    ?>
                    <ul>
                        <li><a href="#" class="lang_sel_sel icl-en">@lang('menus.language-picker.langs.'.$lang)</a>
                            <ul>
                                @if($lang != 'en')
                                    <li class="icl-en"><a href="{{ '/lang/en' }}">@lang('menus.language-picker.langs.en')</a></li>
                                @endif
                                @if($lang != 'mm')
                                    <li class="icl-mm"><a href="{{ '/lang/mm' }}">@lang('menus.language-picker.langs.mm')</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <nav class="header-menu desk-menu nz-clearfix">
                <ul id="header-menu" class="menu">
                    <li class="menu-item {{ active_class(Route::is('frontend.index'), 'current-menu-item') }} ">
                        <a href="{{ url('/') }}">
                            <span class="mi"></span>
                            <span class="txt">@lang('navs.general.home')</span>
                            <span class="di icon-arrow-down9"></span>
                        </a>
                    </li>
                    <li class="menu-item {{ active_class(Route::is('frontend.order_form'), 'current-menu-item') }}">
                        <a href="{{route('frontend.order_form')}}">  
                            <span class="mi"></span>
                            <span class="txt">@lang('navs.frontend.delivery')</span>
                            <span class="di icon-arrow-down9"></span>
                        </a>
                    </li>
                    <li class="menu-item {{ active_class(Route::is('frontend.contact'), 'current-menu-item') }}">
                        <a href="{{route('frontend.contact')}}">  
                            <span class="mi"></span>
                            <span class="txt">@lang('navs.frontend.contact')</span>
                            <span class="di icon-arrow-down9"></span>
                        </a>
                    </li>


                    @auth
                        <li class="menu-item {{ active_class(Route::is('frontend.user.dashboard'), 'current-menu-item') }}">
                            <a href="{{route('frontend.user.dashboard')}}">
                                <span class="mi"></span>
                                <span class="txt">@lang('navs.frontend.dashboard')</span>
                                <span class="di icon-arrow-down9"></span>
                            </a>
                        </li>
                    @endauth

                    @guest
                        <li class="menu-item {{ active_class(Route::is('frontend.auth.login'), 'current-menu-item') }}" >
                            <a href="{{route('frontend.auth.login')}}">
                                <span class="mi"></span>
                                <span class="txt">@lang('navs.frontend.login')</span>
                                <span class="di icon-arrow-down9"></span>
                            </a>
                        </li>

                        @if(config('access.registration'))
                            <li class="menu-item {{ active_class(Route::is('frontend.auth.register'), 'current-menu-item') }}" >
                                <a href="{{route('frontend.auth.register')}}">
                                    <span class="mi"></span>
                                    <span class="txt">@lang('navs.frontend.register')</span>
                                    <span class="di icon-arrow-down9"></span>
                                </a>
                            </li>
                        @endif
                    @else
                        <li class="menu-item">
                            <a href="#">
                                <span class="mi"></span>
                                <span class="txt">{{ $logged_in_user->name }}</span>
                                <span class="di icon-arrow-down9"></span>
                            </a>
                            <ul class="sub-menu">
                                <li  class="icon-home menu-item">
                                    @can('view backend')
                                        <a href="{{ route('admin.dashboard') }}">
                                            <span class="mi"></span>
                                            <span class="txt">@lang('navs.frontend.user.administration')</span>
                                            <span class="di icon-arrow-down9"></span>
                                        </a>
                                    @endcan
                                </li>
                                <li class="icon-dashboard menu-item">
                                    <a href="{{ route('frontend.user.account') }}">
                                        <span class="mi"></span>
                                        <span class="txt">@lang('navs.frontend.user.account')</span>
                                    </a>
                                </li>
                                <li class="icon-lock menu-item">
                                    <a href="{{ route('frontend.auth.logout') }}">
                                        <span class="mi"></span>
                                        <span class="txt">@lang('navs.general.logout')</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </nav>
        </div>
        
    </div>

</header>
