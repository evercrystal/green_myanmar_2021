<!-- footer start -->
<footer class='footer'>
    <div class="container nz-clearfix">
        <aside class="footer-widget-area widget-area">
            <div id="text-2" class="widget widget_text">
                <div class="textwidget"><img id="img_40b0_0" src="{{ config('appsetting.basic.main_logo') }}" style="width:23%;" alt="{{ app_name() }}">
                    <div class="gap nz-clearfix" id="div_40b0_34">&nbsp;</div>
                    {!! \Illuminate\Support\Str::words(@$aboutUs->content, 90, '...') !!}
                    <a href="{{ url('page/aboutus') }}">{{ trans('labels.frontend.read_more') }} >></a>
                    <div class="gap nz-clearfix" id="div_40b0_35">&nbsp;</div>
                    <div id="div_40b0_36">{{ app_name() }} | {{ date('Y') }} &copy; All Rights Reserved.</div>                                    
                    <div class="gap nz-clearfix" id="div_40b0_37">&nbsp;</div>
                    <div class="nz-sl social-links nz-clearfix left">
                        <a class="icon-facebook" href="{{ config('app.app_setting.facebook') }}" target="_blank"></a>
                        <a class="icon-apple" href="{{ config('app.app_setting.appstore') }}" target="_blank"></a>
                        <a class="icon-android" href="{{ config('app.app_setting.playstore') }}" target="_blank"></a>
                    </div>
                </div>
            </div>
        </aside>
        <nav class="footer-menu nz-clearfix">
            <div class="text-white" >
                <p class="text-color">Have Questions?</p>
                <p class="text-color"><i class="fas fa-phone"></i>&nbsp; {{config('appsetting.basic.phone')}}</p>
                <p><a href="mailto:hotel@green-myanmar.com" class="text-color"> <i class="fa fa-envelope"></i> &nbsp; {{config('appsetting.basic.email')}}</a></p>
                <p class="text-color"> <i class="fa fa-map-marker"></i>&nbsp; {{config('appsetting.basic.address')}}</p>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3818.9853305403844!2d96.1781965144958!3d16.82708382315208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c193313e13e7d1%3A0x57cb7e587ff2177f!2sGreen%20Myanmar%20Travels%20%26%20Tours!5e0!3m2!1sen!2ssg!4v1601444515655!5m2!1sen!2ssg" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
        </nav>
    </div>
</footer>
<!-- footer end -->

<style type="text/css">
    .text-color{
        color:white;
    }
</style>