@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.payment.order_payment_confirmation'))

@section('content')
    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg1.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                            {{ trans('labels.frontend.payment.order_payment_confirmation') }}                        
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class=" align-self-center">
            <div id="content">
                <div class="container">
                  <div class="bg-light shadow-md rounded">
                    <div class="p-4">
                        @if($method == "cod")
                            <h5  style="text-align:center;"> {!! trans('labels.frontend.payment.cod_payment_confirmation_info') !!}</h5  ><br>
                            <h5  style="text-align:center;"> {{ trans('labels.frontend.payment.your_ref_number_is') }}<span style="color: red"> {{ $formValues['ref'] }} </span>.</h5    ><br>
                              
                            <div class="text-center">
                                <a href="/" class="btn btn-primary"><i class="fas fa-home"></i> {{ trans('labels.frontend.payment.back_to_home') }}</a>
                            </div>
                        @else 
                            @if($method == "mpu")
                                {!! trans('labels.frontend.payment.payment_confirmation_info') !!}
                            @endif

                            {{ html()->form('POST', $formValues['url'])->open() }} 

                            {!! $formValues['values'] !!}

                            <h5 class="text-4 mb-4 text-center">{{ trans('labels.frontend.payment.review_order_and_responsed') }} </h5>

                            @if($currency == 'USD')
                                <h3 class="text-center">{{ $usdAmount }} {{ $currency }} {{ trans('labels.frontend.payment.confirm_to_pay') }} <br><br>
                            @else
                                <h3 class="text-center">{{ $mmkAmount }} {{ $currency }} {{ trans('labels.frontend.payment.confirm_to_pay') }} <br><br>
                            @endif

                            <button type="submit" class="btn btn-primary rounded-pill">Order Now!!</button>
                            </h3> 
                            {{ html()->closeModelForm() }}
                        @endif
                       
                    </div>
                  </div>
                </div>
              </div><!-- Content end -->

        </div><!--col-->
    </div><!--row-->
@endsection