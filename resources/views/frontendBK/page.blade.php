@extends('frontend.layouts.app')

@section('title',  app_name() . ' | ' . strip_tags($page->title))

@section('meta_keyword', $page->keywords)
@section('meta_description', strip_tags($page->title))

@section('meta')
{!! $page->tags !!}
@endsection

@section('content')

<?php $imgNo = rand(1,5); ?>

<div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
    <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('../frontend/upload/section_bg{{ $imgNo }}.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
    <div class="container">
        <div class="nz-row">
            <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                <div class="col-inner" style="">
                    <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                        {{ $page->title }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- content start -->
<div id="nz-content" class='content nz-clearfix padding-false'>
    <div class='container page-full-width'>
        <!-- post start -->
        <div id="post-2057" class="post-2057 page type-page status-publish hentry">
            <section class="page-content nz-clearfix">
                <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false">
                    <div class="container">
                        <div class="nz-row">
                            <div class="col col12  col-animate-false" style="" data-effect="fade" data-align="left">
                                <div class="col-inner">
                                    {!! $page->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- post end -->
    </div>
</div>
<!-- content end -->
@endsection