@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.payment.order_payment_confirmation'))

@section('content')
<div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg1.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                          {{ trans('labels.frontend.payment.order_payment_confirmation') }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class=" align-self-center">
            <div id="content">
                <div class="container">
                  <div class="bg-light shadow-md rounded">
                    <div class="p-4">

                        @if($method == 'bank_transfer')
                          <h5>
                            {!! trans('labels.frontend.payment.transfer_detail', ['ref'=>$response['ref'],'amount'=>$response['amount'],'bankname' => $response['bank_name']]) !!}
                          </h5>
                        @endif

                        @if($method == 'mpu' || $method == "bnf_topup") 

                          @if($response['paid'])

                            <h5> {{  trans('labels.frontend.payment.order_payment_successful') }} </h5>
                                  <h5> {{  trans('labels.frontend.payment.your_ref_number_is') }} <span style="color: red">{{ $response['ref'] }} </span></h5>

                          @else
                            <h5> {{  trans('labels.frontend.payment.order_payment_status_was') }} {{ $response['message'] }}.</h5>
                           <h5> {{  trans('labels.frontend.payment.your_ref_number_is') }}
                           <span style="color: red">{{ $response['ref'] }} </span>.<br> 
                            {{  trans('labels.frontend.payment.payment_manually_later') }}</h5>

                          @endif

                      @endif
                      <br>
                      <h5>{!! trans('labels.frontend.payment.thank_info') !!}</h5>
                      <div class="text-center">
                        <a href="/" class="btn btn-primary"><i class="fas fa-home"></i> {{ trans('labels.frontend.payment.back') }}</a>
                      </div>
                    </div>
                </div>
              </div>
            </div><!-- Content end -->
        </div><!--col-->
    </div><!--row-->
@endsection
