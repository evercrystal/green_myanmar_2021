<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <!-- META TAGS -->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- LINK TAGS -->
        <link rel="stylesheet" href="{{ url('frontend/css/style.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ url('frontend/css/js_composer.min.css') }}" type="text/css" media="all" />

        <link rel="shortcut icon" href="{{ config('appsetting.basic.favicon') }}}" type="image/x-icon" />
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', app_name())">
        <meta name="author" content="@yield('meta_author', 'GMBF Group of Companies Limited')">
        @yield('meta')

        <link rel="stylesheet" id="vc_google_fonts_abril_fatfaceregular-css" href='http://fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&amp;ver=4.4.8' type='text/css' media='all' />

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->

            {{ style(mix('css/frontend.css')) }}

        {{ style('assets/plugins/waitme/waitMe.min.css') }}
        @stack('after-styles')
        <style>
            .border{
                border-bottom: 1px solid #00000030;
            }
            
            .track_button{
                color:white;
                background-color: #ef5417;
            }
            .track_button:hover{
                color:white;
                background-color: rgb(189,34,-27);
            }
            .modal_style{
                border-radius: 0;
                border: 0px;
            }
            .border_zero{
                border: 0px;
            }
        </style>
      
    </head>
    <body class="home page page-id-10 page-template-default wpb-js-composer js-comp-ver-4.9 vc_responsive">
        <!-- general wrap start -->
        <div id="gen-wrap">
            @include('includes.partials.read-only')
            <!-- wrap start -->
            <div id="wrap" class="nz-wide">

                @include('includes.partials.logged-in-as')
                @include('frontend.includes.nav')

                <div class="page-content-wrap inactive-true" id="app">
                    @yield('content')
                    <!-- Modal -->
                    <div class="modal fade" id="orderListModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content modal_style">
                                <div class="modal-header border_zero">
                                    <h5 class="modal-title" id="staticBackdropLabel"> @lang('labels.frontend.order.order_list')</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="container text-center ">
                                    <br>
                                <div class="order-list-modal-body">

                                </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- container -->
                @include('frontend.includes.footer')
            </div>
            <!-- wrap end -->
           
        </div>
        <a class="icon-arrow-up2" id="top" href="#wrap"></a>
        <!-- general wrap end -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script(mix('js/frontend.js')) !!}
        {{ script('assets/plugins/waitme/waitMe.min.js')}}
        <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=true&amp;ver=4.4.8'></script>
        <script type='text/javascript' src='{{ url("frontend/js/modernizr.js") }}'></script>
        <script type='text/javascript' src='{{ url("frontend/js/jquery.blockUI.min.js") }}'></script>
        <script type='text/javascript' src='{{ url("frontend/js/woocommerce.min.js") }}'></script>
        <script type='text/javascript' src='{{ url("frontend/js/jquery.shuffle.js") }}'></script>
        <script type='text/javascript' src='{{ url("frontend/js/controller.js") }}'></script>
        {{ script('assets/plugins/select2/js/select2.full.min.js')}}
        {{ script('assets/plugins/bootbox/bootbox.min.js')}}
        {{ script('assets/plugins/select2/js/select2.full.min.js')}}
        {{ script("assets/plugins/select2/component/components-select2.js") }}
        <script type="text/javascript">
            function run_waitMe() {
                $('body').waitMe({
                    effect: 'win8',
                    text: 'Please Wait..',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000',
                    sizeW: '',
                    sizeH: ''
                });
            }
            function stop_waitMe() {
                $('body').waitMe('hide');
            }
            $().ready(function() {
                $('.search_track_ticket').click(function(){
                    var search_value = $('#track_value').val();
                    if(search_value){
                        run_waitMe();
                        $.ajax({
                            url: "{{ route('frontend.order.track') }}",
                            type: 'get',
                            data: {search_value: search_value},
                            success: function (data){
                                stop_waitMe();
                               if(data.order.length == 1){
                                 window.location="{{ url('order/track-detail') }}?ref_no="+data.order[0]['ref_no'];

                               } else if(data.order.length == 0){
                                 $('#paymentModal').modal('hide');
                                 bootbox.alert("{{trans('alerts.frontend.order.no_order_found')}}");
                               }
                               else{
                                    $('#orderListModal').modal('show');
                                    $.each(data.order, function(i, value) {
                                        $('.order-list-modal-body').append(
                                            "<div class='row border'>  <div class='col-6'><br></br><h5>"+ value.ref_no +"</h5> <p>  "+value.created_at+" </p><p>"+value.from_township.name+" to "+value.to_township.name+"</p></div><div class='col-6'><br></br> <a class='track_button btn' href='{{ url('order/track-detail') }}?ref_no="+value.ref_no+"'>Show More</a></div>  "
                                        );
                                        stop_waitMe();
                                    });    
                               }    
                            },
                            error: function(error,status){
                                stop_waitMe();
                                $('#paymentModal').modal('hide');
                                bootbox.alert(error.responseJSON.message);
                            }           
                        });
                    }
                  
                })
            })
        </script>
        @stack('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>
