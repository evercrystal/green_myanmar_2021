@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))
    
@section('content')

    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg1.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                            @lang('labels.frontend.contact.box_title')
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_4dba_15">
        <div class="container">
            <div class="nz-row">
                <div class="col col6  col-animate-false" data-effect="fade" data-align="left">
                    <div class="col-inner">
                        <p>{{@$contact->content}}</p>
                    </div>
                </div>
                <div class="col col6  col-animate-false" data-effect="fade" data-align="left">
                    <div class="col-inner">
                        <div class="nz-column-text nz-clearfix ">
                            <div class="row justify-content-center">
                                <div class="col  align-self-center">
                                    <div class="card">
                                        <div class="card-body">
                                            @include('includes.partials.messages')
                                            {{ html()->form('POST', route('frontend.contact.send'))->open() }}
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            {{ html()->label(__('validation.attributes.frontend.name'))->for('name') }}

                                                            {{ html()->text('name', optional(auth()->user())->name)
                                                                ->class('form-control')
                                                                ->placeholder(__('validation.attributes.frontend.name'))
                                                                ->attribute('maxlength', 191)
                                                                ->required()
                                                                ->autofocus() }}
                                                        </div><!--form-group-->
                                                    </div><!--col-->
                                                </div><!--row-->

                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                                                            {{ html()->email('email', optional(auth()->user())->email)
                                                                ->class('form-control')
                                                                ->placeholder(__('validation.attributes.frontend.email'))
                                                                ->attribute('maxlength', 191)
                                                                ->required() }}
                                                        </div><!--form-group-->
                                                    </div><!--col-->
                                                </div><!--row-->

                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            {{ html()->label(__('validation.attributes.frontend.phone'))->for('phone') }}

                                                            {{ html()->text('phone')
                                                                ->class('form-control')
                                                                ->placeholder(__('validation.attributes.frontend.phone'))
                                                                ->attribute('maxlength', 191)
                                                                ->required() }}
                                                        </div><!--form-group-->
                                                    </div><!--col-->
                                                </div><!--row-->

                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            {{ html()->label(__('validation.attributes.frontend.message'))->for('message') }}

                                                            {{ html()->textarea('message')
                                                                ->class('form-control')
                                                                ->placeholder(__('validation.attributes.frontend.message'))
                                                                ->attribute('rows', 3)
                                                                ->required() }}
                                                        </div><!--form-group-->
                                                    </div><!--col-->
                                                </div><!--row-->

                                                @if(config('access.captcha.contact'))
                                                    <div class="row">
                                                        <div class="col">
                                                            @captcha
                                                            {{ html()->hidden('captcha_status', 'true') }}
                                                        </div><!--col-->
                                                    </div><!--row-->
                                                @endif

                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group mb-0 clearfix">
                                                            {{ form_submit(__('labels.frontend.contact.button')) }}
                                                        </div><!--form-group-->
                                                    </div><!--col-->
                                                </div><!--row-->
                                            {{ html()->form()->close() }}
                                        </div><!--card-body-->
                                    </div><!--card-->
                                </div><!--col-->
                            </div><!--row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

    
@endsection

@push('after-scripts')
    @if(config('access.captcha.contact'))
        @captchaScripts
    @endif
@endpush