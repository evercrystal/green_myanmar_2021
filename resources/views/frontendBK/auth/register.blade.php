@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
{{ style('assets/css/google-map.css') }}
<link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" type="text/css"/>

<style>
    .width-button {
        width: 100%;
    }
</style>
@endpush

@section('content')
    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg5.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                            <strong>
                                @lang('labels.frontend.auth.register_box_title')
                            </strong>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="">
        <div class="container">
            {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
            <div class="row">

                <!--- Account INformation -------->
                <div class=" col-lg-6">
                    <br>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                {{ html()->label(__('merchant::labels.backend.merchant.table.name').'<span class="text-danger">*</span>')->class('form-control-label')->for('name') }}
                                {{ html()->text('name')
                                    ->class('form-control')
                                    ->placeholder(__('merchant::labels.backend.merchant.table.name'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('merchant::labels.backend.merchant.table.mobile').'<span class="text-danger">*</span>')->class(' form-control-label')->for('mobile') }}
                                {{ html()->text('mobile')
                                    ->class('form-control')
                                    ->placeholder(__('merchant::labels.backend.merchant.table.mobile'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('merchant::labels.backend.merchant.table.type').'<span class="text-danger">*</span>')->class(' form-control-label')->for('type') }}
                                <select name="type" id="type" class="form-control select2">
                                    @foreach($types as $key=>$value)
                                        <option value="{{$key}}">{{ $value}}</option>
                                    @endforeach
                                </select>
                            </div><!--form-group--> 
                    
                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.users.email').'<span class="text-danger">*</span>')->class(' form-control-label')->for('email') }}
                                {{ html()->email('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.users.password').'<span class="text-danger">*</span>')->class(' form-control-label')->for('password') }}
                                {{ html()->password('password')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.password'))
                                    ->required() }}
                            </div><!--form-group-->

                            <div class="form-group row">
                                {{ html()->label(__('validation.attributes.backend.access.users.password_confirmation').'<span class="text-danger">*</span>')->class('form-control-label')->for('password_confirmation') }}
                                {{ html()->password('password_confirmation')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.password_confirmation'))
                                    ->required() }}
                            </div><!--form-group-->

                              <div class="form-group row">
                                {{ html()->label(__('merchant::labels.backend.merchant.table.address_detail'))->class('form-control-label')->for('address_detail') }}
                                {{ html()->textarea('address_detail')
                                    ->class('form-control')
                                    ->placeholder(__('merchant::labels.backend.merchant.table.address_detail'))
                                    ->attribute('maxlength', 191) }}  
                            </div><!--form-group-->
                        </div>
                    </div>
                </div>
                <div class=" col-lg-6">  
                    <br>   
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    {{ html()->label(__('merchant::labels.backend.merchant.table.country').'<span class="text-danger">*</span>')->class(' form-control-label')->for('country') }}

                                    <select name="country_id" id="country_id" class="form-control select2">
                                        <option selected>Choose Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if( old('country_id') == $country->id ) selected @elseif($country->id == 1) selected @endif>{{ $country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="col-md-6">
                                    {{ html()->label(__('merchant::labels.backend.merchant.table.state').'<span class="text-danger">*</span>')->class(' form-control-label')->for('city') }}
                                        <select name="state_id" id="state_id" class="form-control select2">
                                            <option selected>Choose State</option>
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}" @if( old('state_id') == $state->id ) selected @endif>{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                        <span style="color:red;display:none;" id="state_text">State Not found in this country</span>
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <div class="col-md-6">
                                    {{ html()->label(__('merchant::labels.backend.merchant.table.city').'<span class="text-danger">*</span>')->class(' form-control-label')->for('city') }}
                                    @if( old('city_id'))
                                        <select name="city_id" id="city_id" class="form-control select2">
                                            <option selected>Choose City</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}" @if( old('city_id') == $city->id ) selected @endif>{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select name="city_id" id="city_id" class="form-control select2" disabled>
                                            <option value=""></option>
                                        </select>
                                    @endif
                                    
                                    <span style="color:red;display:none;" id="city_text">City Not found in this State</span>
                                </div>

                                <div class="col-md-6">
                                    {{ html()->label(__('merchant::labels.backend.merchant.table.township').'<span class="text-danger">*</span>')->class('form-control-label')->for('township') }}
                                    @if( old('township_id'))
                                        <select name="township_id" id="township_id" class="form-control select2">
                                            <option selected>Choose Township</option>
                                            @foreach($townships as $township)
                                                <option value="{{$township->id}}" @if( old('township_id') == $township->id ) selected @endif>{{ $township->name }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select name="township_id" id="township_id" class="form-control select2" disabled>
                                            <option value=""></option>                              
                                        </select>
                                    @endif
                                    <span style="color:red;display:none;" id="township_text">Township Not found in this City</span>
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Place" style="right:0;">
                                    <div id="map-canvas"
                                        style="width:97%;height:400px;"></div>
                                    <div id="ajax_msg"></div>
                                </div><!--col-->
                            </div><!--form-group-->
                            
                            <div class="form-group row">
                                <div class="col-md-12" style="display: flex;">
                                    <div class="col-md-6">
                                        {{ html()->text('latitude')
                                        ->class('form-control')
                                        ->id('input-latitude')
                                        ->placeholder(__('merchant::labels.backend.merchant.table.latitude')) }}
                                    </div>

                                    <div class="col-md-6">
                                        {{ html()->text('longitude')
                                        ->class('form-control')
                                        ->id('input-longitude')
                                        ->placeholder(__('merchant::labels.backend.merchant.table.longitude')) }}
                                    </div>
                                    
                                </div><!--col-->
                            </div><!--form-group-->

                          
                        </div><!--card-body-->
                    </div><!--card-->
                </div>
                
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <div class="form-group mb-0 clearfix">
                        <input type="submit" class="btn btn-primary btn-lg width-button" value="{{ __('labels.frontend.auth.register_button')}}">
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
            {{ html()->form()->close() }}
        </div><!-- container -->
    </div>
  
@endsection

@push('after-scripts')

{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('appsetting.basic.map_key') }}&libraries=weather,geometry,visualization,places,drawing&callback=initMap" async defer></script>
<script>
     function run_waitMe() {
        $('body').waitMe({
            effect : 'bounce',
            text: 'Please wait',
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            sizeW: '',
            sizeH: ''
        });
    }
    function stop_waitMe() {
        $('body').waitMe('hide');
    }
$(document).ready(function(){
    $('#country_id').on('change', function(){
        run_waitMe();
        var countryId = $(this).val();
        if(countryId){
            var state_id= $('#state_id').val();
            $('#state_id').empty();
        
            $('#state_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{  url('order-form/get-state') }}/"+countryId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {
                        $('#state_id').attr('disabled','disabled');
                        $('#state_id').find('option').remove().end();
                        $('#state_id').hide();
                        $('#state_text').show();
                        $('#city_text').show();
                        $('#township_text').show();
                        stop_waitMe();

                    }else {
                        $('#state_id').find('option').remove().end();
                        $('#state_id').show();
                        $('#state_text').hide();
                        $('#city_text').hide();
                        $('#township_text').hide();
                        $('#state_id').append($('<option></option>') .attr('selected',true).text('Seletct state'));

                        $.each(data, function(i, value) {
                            $('#state_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#state_id').trigger('change');  
                        });    
                            $('#state_id').trigger('change');
                            stop_waitMe();
                    }  
                }, 
            });
        }else{
        $('#state_id').attr('disabled','disabled');  
        } 
    });
    $('#state_id').on('select2:select', function(){
        run_waitMe();
        var stateId = $(this).val();
        if(stateId){
            var city_id= $('#city_id').val();
            $('#city_id').empty();
        
            $('#city_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{  url('order-form/get-city-in-register') }}/"+stateId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {
                        $('#city_id').attr('disabled','disabled');
                        $('#township_id').attr('disabled','disabled');
                        $('#city_id').remove().end();
                        $('#city_id').hide();
                        $('#city_text').show();
                        $('#township_text').show();
                        stop_waitMe();
                    }else {
                        $('#city_id').find('option').remove().end();
                        $('#city_id').removeAttr('disabled','disabled');
                        $('#city_id').show();
                        $('#city_text').hide();
                        $('#township_text').hide();
                        $('#city_id').append($('<option></option>') .attr('selected',true).text('Seletct City'));
                        $.each(data, function(i, value) {                            
                            $('#city_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#city_id').trigger('change');  
                            stop_waitMe();
                        });    
                            $('#city_id').trigger('change');
                    }  
                }, 
            });
        }else{
        $('#city_id').attr('disabled','disabled');  
        } 
    });
    $('#city_id').on('select2:select', function(){
        run_waitMe();
        var cityId = $(this).val();
        if(cityId){
            var township_id= $('#township_id').val();
            $('#township_id').empty();
        
            $('#township_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{ url('order-form/get-township/') }}/"+cityId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {

                        $('#township_id').attr('disabled','disabled');
                        $('#township_id').find('option').remove().end();
                        $('#township_id').hide();
                        $('#township_text').show();
                        stop_waitMe();

                    }else {
                        $('#township_id').show();
                        $('#township_id').removeAttr('disabled','disabled');
                        $('#township_text').hide();
                        $('#township_id').find('option').remove().end();
                        $('#township_id').append($('<option></option>') .attr('selected',true).text('Seletct Township'));

                        $.each(data, function(i, value) {

                            $('#township_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#township_id').trigger('change');  
                            stop_waitMe();
                        });    
                        $('#township_id').removeAttr('disabled','disabled');

                            $('#township_id').trigger('change');
                            stop_waitMe();
                    }  
                }, 
            });
        }else{
        $('#township_id').attr('disabled','disabled');  
        } 
    });

});
    $(".select2, .select2-multiple").select2({
        placeholder: 'Choose ',
        width: '100%'
    });

    function initMap() {
        var mapOptions = {
            center: new google.maps.LatLng(16.798703652839684, 96.14947007373053),
            zoom: 13
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

        var marker_position = new google.maps.LatLng(16.798703652839684, 96.14947007373053);
        var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: marker_position,
            draggable: true,
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });


        google.maps.event.addListener(marker, "mouseup", function (event) {
            $('#input-latitude').val(this.position.lat());
            $('#input-longitude').val(this.position.lng());
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setIcon(/** @type {google.maps.Icon} */({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#input-latitude').val(place.geometry.location.lat());
            $('#input-longitude').val(place.geometry.location.lng());

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            $('input[name=address]').val(place.formatted_address);

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);
        });


        google.maps.event.addListener(marker, 'dragend', function() {

            $('#input-latitude').val(place.geometry.location.lat());
            $('#input-longitude').val(place.geometry.location.lng());

        });

    }


</script>
@endpush
