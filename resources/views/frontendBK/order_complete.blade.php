@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.payment.receiver_order_complete'))

@section('content')
    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg1.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                          {{ trans('labels.frontend.payment.receiver_order_complete') }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class=" align-self-center">
            <div id="content">
                <div class="container">
                    <div class="p-4">
                            <h5  style="text-align:center;"> {!! trans('labels.frontend.payment.receiver_order_complete_info') !!}</h5  ><br>
                            <h5  style="text-align:center;"> {{ trans('labels.frontend.payment.your_ref_number_is') }}<span style="color: red"> {{ $order->ref_no }} </span>.</h5    ><br>
                              
                            <div class="text-center">
                                <a href="/" class="btn btn-primary"><i class="fas fa-home"></i> {{ trans('labels.frontend.payment.back_to_home') }}</a>
                            </div>
                    </div>
                  </div>
                </div>
              </div><!-- Content end -->

        </div><!--col-->
    </div><!--row-->
@endsection