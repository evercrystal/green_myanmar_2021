@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.order.track_order'))

@push('after-styles')
<style type="text/css">
    .toggle-title{
        background:#e1dcdc!important;
    }
    tr{
        vertical-align: top;
    }
    .event-value{
        font-size: 28px !important;
    }
</style>
    
@endpush

@section('content')

    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" style="background-color:#272e35;padding-top:125px;">
        <div style="-webkit-background-size: cover; -moz-background-size: cover; background-size: cover;background-image:url('frontend/upload/section_bg1.jpg');background-repeat:no-repeat;background-position:center top;background-attachment:scroll;" class="parallax-container"></div>
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-true" style="" data-effect="flip" data-align="left">
                    <div class="col-inner" style="">
                        <h2 style="font-size: 46px;color: #ffffff;line-height: 56px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading">
                             {{$refNo}} 's @lang('labels.frontend.order.order_detail')
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($order)
    <br><br>
    <div class="container">
        <div class="nz-row">
            <div class="col col12  col-animate-false" data-effect="fade" data-align="left">
                <div class="col-inner">
                    <div class="sep-wrap left nz-clearfix">
                        <div class="nz-separator solid" id="div_2b92_21">&nbsp;</div>
                    </div>
                    <div id="nz-timeline-1" class="nz-timeline nz-clearfix">
                        <div class="nz-event-container">
                            <div class="nz-event">
                                <div class="nz-event-wrap nz-clearfix"><span class="event-icon icon-search2"></span><span class="event-value">{{ trans('labels.frontend.order.qr') }}</span><span class="event-title">{{ trans('labels.frontend.order.scan_here') }}</span></div>
                            </div>
                            <div class="nz-event-content nz-clearfix">
                                {!! QrCode::size(200)->generate($refNo); !!}
                            </div>
                        </div>
                        <div class="nz-event-container">
                            <div class="nz-event">
                                <div class="nz-event-wrap nz-clearfix"><span class="event-icon icon-paperplane"></span><span class="event-value">{{ trans('labels.frontend.order.pickup') }}</span><span class="event-title">{{ trans('labels.frontend.order.address') }}</span></div>
                            </div>
                            <div class="nz-event-content nz-clearfix">
                                 <p>@if($order->from_address) {{ $order->from_address }} , @endif {{ $order->fromTownship->name }} , {{ $order->fromCity->name }}</p>
                                <p>{{ trans('labels.frontend.order.sender_name') }} - {{$order->pickup_name}}</p>
                                <p>{{ trans('labels.frontend.order.sender_mobile') }} - {{$order->pickup_phone}}</p>
                            </div>
                        </div>
                        <div class="nz-event-container">
                            <div class="nz-event">
                                <div class="nz-event-wrap nz-clearfix"><span class="event-icon icon-paperplane"></span><span class="event-value">{{ trans('labels.frontend.order.deliver') }}</span><span class="event-title">{{ trans('labels.frontend.order.address') }}</span></div>
                            </div>
                            <div class="nz-event-content nz-clearfix">
                                <p>@if($order->to_address) {{ $order->to_address }} , @endif{{ $order->toTownship->name }} , {{ $order->toCity->name }}</p>
                                <p>{{ trans('labels.frontend.order.receiver_name') }} - {{$order->receiver_name}}</p>
                                <p>{{ trans('labels.frontend.order.receiver_mobile') }} - {{$order->receiver_phone}}</p>
                            </div>
                        </div>
                        <div class="nz-event-container">
                            <div class="nz-event">
                                <div class="nz-event-wrap nz-clearfix"><span class="event-icon icon-wallet"></span><span class="event-value">{{ trans('labels.frontend.order.order') }}</span><span class="event-title">{{ trans('labels.frontend.order.detail') }}</span></div>
                            </div>
                            <div class="nz-event-content nz-clearfix">
                                <p>{{ trans('labels.frontend.order.status') }}- {!! $order->status_label !!}</p>
                                <p>{{ trans('labels.frontend.order.no_of_item') }} - {{$order->no_of_item}}</p>
                                <p>{{ trans('labels.frontend.order.weight') }} - {{$order->weight. config('appsetting.basic.weight_unit')}}</p>
                                <p>{{ trans('labels.frontend.order.pickup_date') }} - {{$order->pickup_date}}</p>
                                <p>{{ trans('labels.frontend.order.pickup_schedule') }} - {{App\Enums\PickupTimeSchedule::getName($order->pickup_schedule_id)}}</p>
                                <p>{{ trans('labels.frontend.order.product_type') }} - {{$order->productType->name}}</p>
                                <p>{{ trans('labels.frontend.order.box_size') }} - {{$order->box->name}}</p>
                                <p>{{ trans('labels.frontend.order.total_amount') }} - <span class="text-danger">{{$order->total_amount}} MMK</span></p>
                                @if($order->advance_order_price)
                                    <p>{{ trans('labels.frontend.order.to_cash_on_delivery_amount') }}- {{$order->advance_order_price}}</p>
                                @endif
                                @if($order->remark)
                                    <p>{{ trans('labels.frontend.order.remark') }} - {{$order->remark}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($order->orderRoute as $orderRoute)
    <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_2b92_3">
        <div class="container">
            <div class="nz-row">
                <div class="col col12  col-animate-false" data-effect="fade" data-align="left">
                    <div class="col-inner">
                        <div class="sep-wrap left nz-clearfix">
                            <div class="nz-separator solid" id="div_2b92_4">&nbsp;</div>
                        </div>
                        <div class="nz-accordion" data-collapsible="false">
                            <div class="false toggle-title nz-clearfix" ><span class="toggle-title-header"><span>{{ trans('labels.frontend.order.route') }}( {{$orderRoute->sequence}} ) - {{$orderRoute->subroute->name}}</span></span><span class="arrow"></span></div>
                            <div id="we-create-awesome-websites-for-your-business" class="toggle-content nz-clearfix" style="display: none;">
                                <p>{{ trans('labels.frontend.order.price') }} : {{$orderRoute->subroute->mmk_price}} MMK</p>
                                <p>{{ trans('labels.frontend.order.remark') }} : {!! $orderRoute->location_status_label !!}</p>
                                <p>{{ trans('labels.frontend.order.pickup_time') }} : {!! $orderRoute->pickup_time !!}</p>
                                <p>{{ trans('labels.frontend.order.delivery_time') }} : {!! $orderRoute->deliver_time !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @else
        <div class="col col12  col-animate-false" data-effect="fade" data-align="left">
            <div class="col-inner">
                <h4 id="h3_e9a6_1" class="vc_custom_heading">{{ trans('alerts.frontend.order.no_order_found') }}</h4>
                <div class="sep-wrap center nz-clearfix">
                    <div class="nz-separator solid" id="div_e9a6_7">&nbsp;</div>
                </div>
                <div id="div_e9a6_8" class="nz-column-text nz-clearfix no-padding">
                    <p></p>
                    <p id="p_e9a6_1"><span id="span_e9a6_1">{{ trans('alerts.frontend.order.no_order_with_ref',['ref_no' => $refNo]) }}</span></p>
                    <p>
                </p></div>
            </div>
        </div>
    @endif

@endsection

@push('after-scripts')

@endpush