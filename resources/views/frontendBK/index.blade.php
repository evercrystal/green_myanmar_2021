@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@push('after-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />

@endpush

@section('content')
        <div id="ninzio-slider" data-navigation="light" data-autoheight="true" data-arrow="false" data-effect="none" data-transition="fade" data-layout="wide" data-height="750" data-mobile="false" data-autoplaydelay="5000" data-autoplay="false" data-bullets="false" data-arrows="true">

            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>

            <div id="slider-arrow" data-target="#nz-content" data-offset="90" class="i-separator animate nz-clearfix">
                <i class="icon-arrow-down10"></i>
            </div>

            <ul class="ninzio-slides">
                <li class="post-14 ninzio-slider type-ninzio-slider status-publish hentry" data-video="false" id="post-14">



                    <div class="slider-canvas container">

                        <div class="canvas-content">



                            <div id="ninzio-layer-14-1" class="ninzio-layer ghostleft" data-direction="ghostleft" data-posx="840" data-posy="230" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <p><span style="font-family: georgia, palatino, serif; font-size: 22px; color: #ffffff;"><em>Always Best Worldwide Shipping</em></span></p>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-14-2" class="ninzio-layer ghostleft" data-direction="ghostleft" data-posx="620" data-posy="270" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-1" class="nz-colorbox nz-clearfix" style="width:550px;padding:0px;">
                                            <h3 style="text-align: right;"><span style="font-size: 56px; color: #ffffff; line-height: 66px; font-weight: bold;">{{ app_name() }}</span></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-14-3" class="ninzio-layer ghostbottom" data-direction="ghostbottom" data-posx="835" data-posy="440" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap"><a class="button button-normal default full-false medium square icon-true animate- anim-type-ghost hover-fill" href="{{route('frontend.order_form')}}" ><span class="txt">Make a delivery</span><span class="btn-icon icon-rocket"></span></a>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-14-5" class="ninzio-layer ghostright" data-direction="ghostright" data-posx="695" data-posy="410" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-2" class="nz-colorbox nz-clearfix" style="width:470px;padding:2px;background-color:#ffffff;"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </li>

                <li class="post-17 ninzio-slider type-ninzio-slider status-publish hentry" data-video="false" id="post-17">



                    <div class="slider-canvas container">

                        <div class="canvas-content">



                            <div id="ninzio-layer-17-1" class="ninzio-layer ghostright" data-direction="ghostright" data-posx="0" data-posy="230" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <p><span style="font-family: georgia, palatino, serif; font-size: 22px; color: #ffffff;"><em> The Most Accurate Delivery In The World</em></span></p>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-17-2" class="ninzio-layer ghostright" data-direction="ghostright" data-posx="0" data-posy="270" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-3" class="nz-colorbox nz-clearfix" style="width:550px;padding:0px;">
                                            <h3 style="text-align: left;"><span style="font-size: 56px; color: #ffffff; line-height: 66px; font-weight: bold;">SAVE UP TO 47% FOR SHIPPING</span></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-17-3" class="ninzio-layer ghostleft" data-direction="ghostleft" data-posx="0" data-posy="410" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-4" class="nz-colorbox nz-clearfix" style="width:430px;padding:2px;background-color:#ffffff;"></div>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-17-4" class="ninzio-layer ghostbottom" data-direction="ghostbottom" data-posx="0" data-posy="440" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap"><a class="button button-normal default full-false medium square icon-true animate- anim-type-ghost hover-fill" href="{{route('frontend.order_form')}}" ><span class="txt">Make a delivery</span><span class="btn-icon icon-rocket"></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </li>

                <li class="post-20 ninzio-slider type-ninzio-slider status-publish hentry" data-video="false" id="post-20">



                    <div class="slider-canvas container">

                        <div class="canvas-content">



                            <div id="ninzio-layer-20-1" class="ninzio-layer ghostbottom" data-direction="ghostbottom" data-posx="0" data-posy="230" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-5" class="nz-colorbox nz-clearfix" style="width:1170px;">
                                            <p style="text-align: center;"><span style="font-family: georgia, palatino, serif; font-size: 22px; color: #ffffff;"><em>Over 280000 Happy Clients WorldWide</em></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-20-2" class="ninzio-layer ghostbottom" data-direction="ghostbottom" data-posx="260" data-posy="270" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-6" class="nz-colorbox nz-clearfix" style="width:650px;padding:0px;">
                                            <h3 style="text-align: center;"><span style="font-size: 56px; color: #ffffff; line-height: 66px; font-weight: bold;">BECOME A PART OF {{ app_name() }}</span></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-20-3" class="ninzio-layer ghostleft" data-direction="ghostleft" data-posx="292" data-posy="410" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap">
                                        <div data-id="nz-colorbox-7" class="nz-colorbox nz-clearfix" style="width:585px;padding:2px;background-color:#ffffff;"></div>
                                    </div>
                                </div>
                            </div>

                            <div id="ninzio-layer-20-4" class="ninzio-layer ghostbottom" data-direction="ghostbottom" data-posx="489" data-posy="440" data-mouse="-1" data-speed="0">
                                <div class="layer-mousemove">
                                    <div class="layer-wrap"><a class="button button-normal default full-false medium square icon-true animate- anim-type-ghost hover-fill" href="{{route('frontend.auth.register')}}" ><span class="txt">Click To Sign Up</span><span class="btn-icon icon-login"></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </li>
            </ul>

        </div>

        <!-- content start -->
        <div id="nz-content" class='content nz-clearfix padding-false'>
            <div class='container page-full-width'>
                <div class="slider-banner nz-clearfix container">
                    <p>
                        <div id="nz-slider-banner-1"  class="nz-slider-banner nz-clearfix" data-columns="4">

                            <div class="inner-wrap">
                                <div id="nz-banner-1" class="nz-banner">
                                    <div class="banner-icon icon-phone3"></div>
                                    <div class="banner-data" style="padding-bottom: 19px;">
                                        <h4 id="h4_40b0_0">24/7 PHONE:</h4>
                                        <p id="p_40b0_0">{!! str_replace(',', '<br>', config('appsetting.basic.phone')) !!}</p>
                                    </div>
                                </div>
                            <div id="nz-banner-2" class="nz-banner">
                                <div class="banner-icon icon-map3"></div>
                                <div class="banner-data">
                                    <p id="p_40b0_1">{{ config('appsetting.basic.address') }}</p>
                                </div>
                            </div>
                            <div id="nz-banner-3" class="nz-banner">
                                <div class="banner-icon icon-clock5"></div>
                                <div class="banner-data" style="padding-bottom: 45px;">
                                    <h4 id="h4_40b0_2">WORKING HOURS:</h4>
                                    <p id="p_40b0_2">09:00 - 20:00</p>
                                </div>
                            </div>
                            <div id="nz-banner-4" class="nz-banner">
                                <div class="banner-icon icon-paperplane2"></div>
                                <div class="banner-data" style="padding-bottom: 45px;">
                                    <h4 id="h4_40b0_3">EMAIL:</h4>
                                    <p id="p_40b0_3">{{ config('appsetting.basic.email') }}</p>
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
                <!-- post start -->
                <div id="post-10" class="post-10 page type-page status-publish hentry">
                    <section class="page-content nz-clearfix">
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_0">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <h3 id="h3_40b0_0" class="vc_custom_heading">Need Shipping Wordlwide?</h3>
                                            <div  class="nz-column-text nz-clearfix ">
                                                
                                                <p id="p_40b0_4">Explore Our Services</p>
                                                <p>
                                            </div>
                                            <div class='gap nz-clearfix' id="div_40b0_1">&nbsp;</div><img class="aligncenter size-full wp-image-2199 " src="{{ url('frontend/upload/arrow_grey.png') }}" alt="2199" width="109" height="58">
                                            <div class='gap nz-clearfix' id="div_40b0_2">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_3">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <div class="nz-content-box-2 nz-clearfix animate-none" data-columns="3" data-animate="false">
                                                <div id="nz-box-1" class="nz-box animate-item">
                                                    <div class="nz-box-image nz-thumbnail"><img src="{{ url('frontend/upload/content_box1.jpg') }}" alt="{{ $pages[0]->title }}">
                                                        <div class="ninzio-overlay">
                                                            <a class="nz-overlay-before" href="#"></a>
                                                        </div>
                                                    </div>
                                                    <div class="box-data">
                                                        <div class="box-title"><span>01.</span>
                                                            <h3>{{ $pages[0]->title }}</h3>
                                                        </div>
                                                        <div class="box-content nz-clearfix">
                                                            {{ \Illuminate\Support\Str::words($pages[0]->content, 30, '...') }}
                                                        </div>
                                                        <a class="button medium button-ghost grey hover-fill animate-false" href="{{ url('page/'.$pages[0]->page) }}">Read more</a></div>
                                                </div>
                                                <div id="nz-box-2" class="nz-box animate-item">
                                                    <div class="nz-box-image nz-thumbnail"><img src="{{ url('frontend/upload/content_box2.jpg') }}" alt="{{ $pages[1]->title }}">
                                                        <div class="ninzio-overlay">
                                                            <a class="nz-overlay-before" href="#"></a>
                                                        </div>
                                                    </div>
                                                    <div class="box-data">
                                                        <div class="box-title"><span>02.</span>
                                                            <h3>{{ $pages[1]->title }}</h3>
                                                        </div>
                                                        <div class="box-content nz-clearfix">
                                                            {{ \Illuminate\Support\Str::words($pages[1]->content, 30, '...') }}
                                                        </div>
                                                        <a class="button medium button-ghost grey hover-fill animate-false" href="{{ url('page/'.$pages[1]->page) }}">Read more</a></div>
                                                </div>
                                                <div id="nz-box-3" class="nz-box animate-item">
                                                    <div class="nz-box-image nz-thumbnail"><img src="{{ url('frontend/upload/content_box3.jpg') }}" alt="{{ $pages[2]->title }}">
                                                        <div class="ninzio-overlay">
                                                            <a class="nz-overlay-before" href="#"></a>
                                                        </div>
                                                    </div>
                                                    <div class="box-data">
                                                        <div class="box-title"><span>03.</span>
                                                            <h3>{{ $pages[2]->title }}</h3>
                                                        </div>
                                                        <div class="box-content nz-clearfix">
                                                            {{ \Illuminate\Support\Str::words($pages[2]->content, 30, '...') }}
                                                        </div>
                                                        <a class="button medium button-ghost grey hover-fill animate-false" href="{{ url('page/'.$pages[2]->page) }}">Read more</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_10">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <h3 id="h3_40b0_2" class="vc_custom_heading">About {{ app_name() }}</h3>
                                            <div  class="nz-column-text nz-clearfix ">
                                                
                                                <p id="p_40b0_7">Explore Our Services</p>
                                                <p>
                                            </div>
                                            <div class='gap nz-clearfix' id="div_40b0_11">&nbsp;</div><img class="aligncenter size-full wp-image-2199 " src="{{ url('frontend/upload/arrow_grey.png') }}" alt="2199" width="109" height="58">
                                            <div class='gap nz-clearfix' id="div_40b0_12">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_13">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col6  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <div class="nz-tabs  horizontal full-false">
                                                <div data-target="tab-about-prime" class="tab">About {{ app_name() }}</div>
                                                <div id="tab-about-prime" class="tab-content">
                                                    
                                                    <h5 id="h5_40b0_0">About {{ app_name() }}</h5>
                                                    <div class='gap nz-clearfix' id="div_40b0_14">&nbsp;</div>
                                                    <span id="span_40b0_0">{{ \Illuminate\Support\Str::words(@$aboutUs->content, 40, '...') }}</span>
                                                    <div class='gap nz-clearfix' id="div_40b0_15">&nbsp;</div><a class="button button-ghost grey full-false medium square animate-false anim-type-ghost hover-fill" href="{{ url('page/aboutus') }}" target="_self"><span class="txt">Read more</span></a>
                                                </div>
                                                <div data-target="tab-prime-history" class="tab">History</div>
                                                <div id="tab-prime-history" class="tab-content">
                                                    
                                                    <h5 id="h5_40b0_1">{{ app_name() }} History</h5>
                                                    <div class='gap nz-clearfix' id="div_40b0_16">&nbsp;</div>
                                                    <span id="span_40b0_1">{{ \Illuminate\Support\Str::words($pages[3]->content, 40, '...') }}</span>
                                                    <div class='gap nz-clearfix' id="div_40b0_17">&nbsp;</div><a class="button button-ghost grey full-false medium square animate-false anim-type-ghost hover-fill" href="{{ url('page/'.$pages[3]->page) }}" target="_self"><span class="txt">Read more</span></a>
                                                </div>
                                                <div data-target="tab-prime-mission" class="tab">Mission</div>
                                                <div id="tab-prime-mission" class="tab-content">
                                                    
                                                    <h5 id="h5_40b0_2">{{ app_name() }} Mission</h5>
                                                    <div class='gap nz-clearfix' id="div_40b0_18">&nbsp;</div>
                                                    <span id="span_40b0_2">{{ \Illuminate\Support\Str::words($pages[4]->content, 40, '...') }}</span>
                                                    <div class='gap nz-clearfix' id="div_40b0_19">&nbsp;</div><a class="button button-ghost grey full-false medium square animate-false anim-type-ghost hover-fill" href="{{ url('page/'.$pages[4]->page) }}" target="_self"><span class="txt">Read more</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col6  col-animate-false"  data-effect="fade" data-align="left">
                                        <div class="col-inner" >
                                            <div data-effect="fade" data-bullets="true" data-autoplay="true" data-navigation="true" class="lazy nz-media-slider">
                                                <ul class="slides">
                                                    <li><img src="{{ url('frontend/upload/portfolio1.jpg') }}" alt="Image"></li>
                                                    <li><img src="{{ url('frontend/upload/portfolio8.jpg') }}" alt="Image"></li>
                                                    <li><img src="{{ url('frontend/upload/portfolio3.jpg') }}" alt="Image"></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="1600" data-img-height="871" data-animation-speed="35000" data-parallax="true" id="div_40b0_20">
                            <div id="div_40b0_21" class="parallax-container"></div>
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <div id="nz-counter-1" class="nz-counter nz-clearfix" data-columns="4">
                                                <div class="nz-count">
                                                    <div class="nz-count-wrap nz-clearfix"><span class="count-icon icon-box"></span><span class="count-value">3570</span><span class="count-title">Shipping Sent</span></div>
                                                </div>
                                                <div class="nz-count">
                                                    <div class="nz-count-wrap nz-clearfix"><span class="count-icon icon-dropbox"></span><span class="count-value">8735</span><span class="count-title">Cargo Finished</span></div>
                                                </div>
                                                <div class="nz-count">
                                                    <div class="nz-count-wrap nz-clearfix"><span class="count-icon icon-users4"></span><span class="count-value">9450</span><span class="count-title">Team Members</span></div>
                                                </div>
                                                <div class="nz-count">
                                                    <div class="nz-count-wrap nz-clearfix"><span class="count-icon icon-compass2"></span><span class="count-value">4150</span><span class="count-title">Miles Traveled</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_22">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <h3 id="h3_40b0_3" class="vc_custom_heading">Let's Ship Worldwide</h3>
                                            <div  class="nz-column-text nz-clearfix ">
                                                
                                                <p id="p_40b0_8">Explore Our Services</p>
                                                <p>
                                            </div>
                                            <div class='gap nz-clearfix' id="div_40b0_23">&nbsp;</div><img class="aligncenter size-full wp-image-2199 " src="{{ url('frontend/upload/arrow_grey.png') }}" alt="2199" width="109" height="58">
                                            <div class='gap nz-clearfix' id="div_40b0_24">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_25">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col6  col-animate-false"  data-effect="fade" data-align="left">
                                        <div class="col-inner" >
                                            <div class="nz-accordion " data-collapsible=true>
                                                <div class="active toggle-title nz-clearfix"><span class="toggle-title-header"><span>We Are The Best Company</span></span><span class="arrow"></span></div>
                                                <div id="we-are-the-best-company" class="toggle-content nz-clearfix">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc claram.</div>
                                                <div class="false toggle-title nz-clearfix"><span class="toggle-title-header"><span>Why Choose Us Today</span></span><span class="arrow"></span></div>
                                                <div id="why-choose-us-today" class="toggle-content nz-clearfix">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc claram.</div>
                                                <div class="false toggle-title nz-clearfix"><span class="toggle-title-header"><span>Smart Business With Us</span></span><span class="arrow"></span></div>
                                                <div id="smart-business-with-us" class="toggle-content nz-clearfix">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc claram.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col6  col-animate-false"  data-effect="fade" data-align="left">
                                        <div class="col-inner" >
                                            <div id="nz-testimonials-1" data-columns="1" data-autoplay="false" class="none nz-testimonials nz-carousel lazy">
                                                <div class="testimonial nz-clearfix">
                                                    <div class="text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. </div>
                                                    <div class="nz-clearfix test-info"><img src="{{ url('frontend/upload/person1.jpg') }}" alt="John Smith" />
                                                        <div class="test-data"><span class="name">John Smith</span><span class="title">CEO</span></div>
                                                    </div>
                                                </div>
                                                <div class="testimonial nz-clearfix">
                                                    <div class="text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. </div>
                                                    <div class="nz-clearfix test-info"><img src="{{ url('frontend/upload/person3-1.jpg') }}" alt="Alex Smith" />
                                                        <div class="test-data"><span class="name">Alex Smith</span><span class="title">Financial Director</span></div>
                                                    </div>
                                                </div>
                                                <div class="testimonial nz-clearfix">
                                                    <div class="text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram. </div>
                                                    <div class="nz-clearfix test-info"><img src="{{ url('frontend/upload/person2.jpg') }}" alt="Ann Smith" />
                                                        <div class="test-data"><span class="name">Ann Smith</span><span class="title">Marketing</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-img-width="20" data-img-height="20" data-animation-speed="35000" data-parallax="false" id="div_40b0_26">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <h3 id="h3_40b0_4" class="vc_custom_heading">Featured Projects</h3>
                                            <div  class="nz-column-text nz-clearfix ">
                                                
                                                <p id="p_40b0_9">Explore Our Services</p>
                                                <p>
                                            </div>
                                            <div class='gap nz-clearfix' id="div_40b0_27">&nbsp;</div><img class="aligncenter size-full wp-image-2199 " src="{{ url('frontend/upload/arrow_grey.png') }}" alt="2199" width="109" height="58">
                                            <div class='gap nz-clearfix' id="div_40b0_28">&nbsp;</div>
                                            <div id="nz-recent-portfolio-1" data-animate="false" data-autoplay="false" data-columns="3" class="lazy nz-clearfix nz-recent-portfolio nz-carousel">
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio1-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio1" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Jurong Logistics Hub</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio2-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio2" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Mapletree Benoi Logistics Hub</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio3-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio3" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Mapletree Wuxi Logistics Park</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio4-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio4" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Mapletree Logistics Centre &#8211; Namanseong</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio5-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio5" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">ISH WaiGaoQiao</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio6-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio6" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Mapletree AIP</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio7-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio7" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Mapletree Wuxi Logistics Park</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio8-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio8" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Ouluo Logistics Centre</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portfolio nz-clearfix" data-grid="ninzio_01">
                                                    <div class="nz-thumbnail"><img width="585" height="480" src="{{ url('frontend/upload/portfolio9-585x480.jpg') }}" class="attachment-Ninzio-Half size-Ninzio-Half wp-post-image" alt="portfolio9" />
                                                        <div class="ninzio-overlay">
                                                            <div class="ninzio-overlay-content">
                                                                <h3 class="project-title">Aichi Miyoshi Centre</h3><a href="#" class="button button-ghost square medium animate-false white hover-fill">Read more</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nz-section horizontal autoheight-false animate-false full-width-false " data-animation-speed="35000" data-parallax="false" id="div_40b0_33">
                            <div class="container">
                                <div class="nz-row">
                                    <div class="col col12  col-animate-false"  data-effect="fade" data-align="left" data-margin="false">
                                        <div class="col-inner" >
                                            <div id="nz-clients-1" class="lazy nz-clients nz-carousel animate-none" data-columns="5" data-animate="false" data-autoplay="false">
                                                <div class="client animate-item">
                                                    <div class="client-inner">
                                                        <a href="#" target="_blank"><img src="{{ url('frontend/upload/logo2.png') }}" alt="Client"></a>
                                                    </div>
                                                </div>
                                                <div class="client animate-item">
                                                    <div class="client-inner">
                                                        <a href="#" target="_blank"><img src="{{ url('frontend/upload/logo3.png') }}" alt="Client"></a>
                                                    </div>
                                                </div>
                                                <div class="client animate-item">
                                                    <div class="client-inner">
                                                        <a href="#" target="_blank"><img src="{{ url('frontend/upload/logo4.png') }}" alt="Client"></a>
                                                    </div>
                                                </div>
                                                <div class="client animate-item">
                                                    <div class="client-inner">
                                                        <a href="#" target="_blank"><img src="{{ url('frontend/upload/logo5.png') }}" alt="Client"></a>
                                                    </div>
                                                </div>
                                                <div class="client animate-item">
                                                    <div class="client-inner">
                                                        <a href="#" target="_blank"><img src="{{ url('frontend/upload/logo3.png') }}" alt="Client"></a>
                                                    </div>
                                                </div>
                                                <div class="client animate-item">
                                                    <div class="client-inner">
                                                        <a href="#" target="_blank"><img src="{{ url('frontend/upload/logo2.png') }}" alt="Client"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- post end -->
            </div>
        </div>
        <!-- content end -->
@endsection
