<?php

return [
    'bad_request' => 'Bad Request',
    'not_found' => 'Not Found',
    'user_not_active' => 'Your account has been invalid or banned by system admin!',
    'user_reset_password' => 'Your password reset mail had been sent to your registered mail.',
    'user_not_found' => 'Can\'t found your email in Our system !',
    'invalid_hash_value' => 'Invalid Hash Valule !',
    'merchant_does_not_exist' => 'Merchant Does Not Exist !',
    'invalid_request_data' => 'Invalid Data , please check your API request Informations.',
    'order_does_not_created' => 'Your Order does not successfully success! Please Try Again!',
    'user_not_active' => 'Your account has been invalid or banned by system admin!',
    'user_reset_password' => 'Your password reset mail had been sent to your registered mail.',
    'user_not_found' => 'Can\'t found your email in Our system !',
    'unverified_user' => 'Unverified User',
    'invalid_credentials' => 'Invalid Credentials',
    'invalid_credentials_message' => 'These credentials do not match our records.',
    'profile_update_error' => 'Can not save your profile !',
    'unable_to_assign_order' => 'Unable to assign the order ! Please check supplier availability and order ref!',
    'unexpected_error' => 'Unexpected Error',
    'cannot_change_available' => 'Delivery Supplier service cannot change.',
    'order_route_not_complete' => ':route delivering now. You can\'t assign in this order ref.',
    'order_does_not_assigned' => 'Your Assign Request does not successfully success! Please try Again!',
    'order_route_all_complete' => ':ref_no \'s order routes are all completed!',
    'mismatch_delivery_supplier' => 'Delivery Supplier Mismatch!',
    'order_route_completed' => 'Order Route already Completed'
];