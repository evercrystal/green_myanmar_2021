<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed' => 'The user was successfully confirmed.',
            'created' => 'The user was successfully created.',
            'deleted' => 'The user was successfully deleted.',
            'not_deleted' => 'The user was successfully not deleted.It is used in merchant table.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored' => 'The user was successfully restored.',
            'session_cleared' => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated' => 'The user was successfully updated.',
            'updated_password' => "The user's password was successfully updated.",
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
        'order' => [
            'invalid_route' => 'There is no route for your given data.Please Try Again!',
            'invalid_pickup_schedule' => 'There is no access route for your pickup time.Please Try Again!',
            'valid_route' => 'Success Route',
            'saved_order' => 'Your order has been successfully saved .Choose Payment Method!',
            'order_does_not_created' => 'Your Order does not successfully success! Please Try Again!',
            'order_payment_error' => 'Order Payment does not successfully scuuess! Please Try Again!',
            'no_order_found' => 'No Order Found!',
            'no_order_with_ref' => 'There is no order for reference number :ref_no.',
        ],
        'page' => [
            'no_page_content_with_accept_language' => 'There is no page content with accept language'
        ]
    ],

    'successfully_update_profile' => 'Successfully Updated Your Profile!',
    'successfully_changed_password' => 'Successfully Changed Your Password!',
    'successfully_changed_order_route' => ' :route transportation status successfully changed!',
];
