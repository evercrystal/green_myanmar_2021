<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'All',
        'yes' => 'Yes',
        'no' => 'No',
        'copyright' => 'Copyright',
        'custom' => 'Custom',
        'actions' => 'Actions',
        'active' => 'Active',
        'buttons' => [
            'save' => 'Save',
            'update' => 'Update',
        ],
        'hide' => 'Hide',
        'inactive' => 'Inactive',
        'none' => 'None',
        'show' => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
        'create_new' => 'Create New',
        'toolbar_btn_groups' => 'Toolbar with button groups',
        'more' => 'More',
    ],
    'pickup' => [
        'today' => 'Today',
        'tomorrow_morning'   => 'Tomorrow Morning',
        'tomorrow_afternoon' => 'Tomorrow Afternoon',
        'other' => 'Other'
    ],
    'pickup_schedule' => [
        '9-5'  => 'Anytime (9am to 5pm)',
        '9-12' => 'Morning (9am to 12 noon)',
        '1-5'  => 'Evening (1pm to 5pm)',
    ],
    'payment_mode' => [
        'sender_will_paid_all_route' => 'Sender Will Pay All Route',
        'sender_will_paid_to_gate'   => 'Sender Will Pay To Gate',
        'sender_will_paid_to_receiver_gate' => 'Sender Will Pay To Receiver Gate',
        'receiver_will_paid_all_route' => 'Receiver Will Pay All Route'
    ],
    'payment_method' => [
        'cod' => 'Cash On Delivery',
        'bank_transfer'   => 'Bank Transfer',
        'bnf_topup' => 'Bnf Topup',
        'mpu' => 'MPU',
        'pay_by_merchant' => 'Pay By Merchant'
    ],
    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions' => 'Permissions',
                    'role' => 'Role',
                    'sort' => 'Sort',
                    'total' => 'role total|roles total',
                ],
            ],

            'users' => [
                'active' => 'Active Users',
                'all_permissions' => 'All Permissions',
                'change_password' => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'management' => 'User Management',
                'no_permissions' => 'No Permissions',
                'no_roles' => 'No Roles to set.',
                'permissions' => 'Permissions',
                'user_actions' => 'User Actions',

                'table' => [
                    'confirmed' => 'Confirmed',
                    'created' => 'Created',
                    'email' => 'E-mail',
                    'mobile' => 'Mobile',
                    'id' => 'ID',
                    'last_updated' => 'Last Updated',
                    'name' => 'Name',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted' => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions' => 'Permissions',
                    'abilities' => 'Abilities',
                    'roles' => 'Roles',
                    'social' => 'Social',
                    'total' => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history' => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmed',
                            'created_at' => 'Created At',
                            'deleted_at' => 'Deleted At',
                            'email' => 'E-mail',
                            'mobile' => 'Mobile Number',
                            'last_login_at' => 'Last Login At',
                            'last_login_ip' => 'Last Login IP',
                            'last_updated' => 'Last Updated',
                            'name' => 'Name',
                            'first_name' => 'First Name',
                            'last_name' => 'Last Name',
                            'status' => 'Status',
                            'timezone' => 'Timezone',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
    ],

    'frontend' => [
        'read_more' => 'Read More',
        'auth' => [
            'login_box_title' => 'Login',
            'login_button' => 'Login',
            'login_with' => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button' => 'Register',
            'remember_me' => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Send Information',
        ],
        'order' => [
            'track_order' => 'Track Order',
            'pickup' => 'Pickup',
            'deliver' => 'Deliver',
            'information' => 'Information Detail',
            'sender_name' => 'Sender Name',
            'sender_mobile' => 'Sender Mobile',
            'receiver_name' => 'Receiver Name',
            'receiver_mobile' => 'Receiver Mobile',
            'status' => 'Order Status',
            'no_of_item' => 'Number Of Item',
            'weight' => 'Weight',
            'pickup_date' => 'Pickup Date',
            'pickup_schedule' => 'Pickup Schedule',
            'product_type' => 'Product Type',
            'box_size' => 'Box Size',
            'total_amount' => 'Total Amount',
            'to_cash_on_delivery_amount' => 'To Cash On Delivery Amount',
            'remark' => 'Remark',
            'route' => 'Route',
            'price' => 'Price',
            'location_status' => 'Location Status',
            'order_detail' => 'Order Detail',
            'address' => 'Address',
            'detail' => 'Details',
            'order' => 'Order',
            'order_list' => 'Track Order List',
            'pickup_time' => 'Pickup Time',
            'delivery_time' => 'Delivery Time',
            'qr' => 'QR',
            'scan_here' => 'Scan Here'
        ],

        'order_form' => [
            'box_title'                     => app_name().' System',
            'calculate_price_button'        => 'Calculate Price',
            'from'                          => 'From',
            'to'                            => 'To',
            'product_type'                  => 'Product Type',
            'no_of_item'                    => 'Number Of Item',
            'select_box'                    => 'Select Box Size',
            'weight'                        => 'Weight(kg)',
            'pickup_time'                   => 'Pickup Time',
            'description'                   => 'We do not accept any illegal goods at all',
            'payment_mode'                  => 'Payment Mode',
            'calculate_price'               => 'Calculate Price',
            'order_info'                    => 'I understand that '.app_name().' do not accept illegal item in any form. Please visit T&Cs page for more details',
            'country'                       => 'Country',
            'city'                          =>'City',
            'city_not_found'                => 'city not found in this country',
            'township'                      =>'Township',
            'township_not_found'            => 'township not found in this city',
            'township_and_collection_not_found' => 'township and collection not found in this city',
            'address'                       => 'Address',
            'close'                         => 'Close',
            'set'                           => 'Set',
            'save_order_form'               => 'Save Order Form',
            'delivery_fee'                  => 'Delivery Fee',
            'receiver_name'                 => 'Receiver Name',
            'receiver_phone'                => 'Receiver Phone',
            'pickup_name'                   => 'Pickup Name',
            'pickup_phone'                  => 'Pickup Phone',
            'payment_mode'                  => 'Payment Mode',
            'proceed_to_next_step'          => 'Proceed To Next Step',
            'proceed_to_order'              => 'Proceed To Make Order',
            'choose_box'                    => 'Choose Box',
            'choose_payment_method'         => 'Choose Payment Method',
            'collection_point'              => 'Collection Point',
            'remark'                        => 'Remark',
            'same_pickup_delivery'          => 'Same Pickup Delivery',
            'required_cash_on_delivery'     => 'Required Cash on Delivery (COD) for the product?',
            'max_order_price'               => 'Maximum Advance Order Price is :max_order_price . If exceed,you must pay from bank.',
            'customer_order_number'         => 'Customer Order Number',
            'enter_township_or_collection'  => 'Enter your township or collection point',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Your password has expired.',
            'forgot_password' => 'Forgot Your Password?',
            'reset_password_box_title' => 'Reset Password',
            'reset_password_button' => 'Reset Password',
            'update_password_button' => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Created At',
                'edit_information' => 'Edit Information',
                'email' => 'E-mail',
                'last_updated' => 'Last Updated',
                'name' => 'Name',
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],

        'payment' => [
            'order_payment_confirmation' => 'Order Payment Confirmation',
            'receiver_order_complete' => 'Order Confirmation',
            'payment_confirmation_info' => '<p>Once you click on "Buy Now" buttom, you will be taken to our payment company page to make the payment.</p><p>In order to complete the payment, please do not forget to go to the end and reach to the final stage which you will be taken back to our '.app_name().' page.</p><p>Please be award that,closing the browser in the half way may cause pending process in our system or unsuccessful payment.</p>',
            'cod_payment_confirmation_info' => '<p>Your Order Payment has been saved successfully.You Must Pay Order Amount when Delivery Person collect Items.</p>',
            'receiver_order_complete_info' => '<p>Your Order has been saved successfully.Receiver Must Pay Order Amount when Delivery Person collect Items.</p>',
            'review_order_and_responsed' => 'If full payment is made, our operator will review the order and respond within 24 hours.',
            'confirm_to_pay' => 'is confirmed to pay now.', 
            'thank_info' => 'For more information,please contact '.config('appsetting.basic.phone').' (or) '.config('appsetting.basic.email').' .<br>Thank You.',
            'your_ref_number_is' => 'Your Order reference no. is',
            'back_to_home' => 'Back To Home',
            'transfer_info' => '<p>Your order amount need to transfer to '.app_name().' with following information</p>Amount - :amount MMK<br>
                            Payment Description - :payment_description <br><br><p>Please choose one of the following banks.(Payment can be made over the counter or with mobile-banking or ibanking.)<br>',
            'remark' => 'Remark',
            'bank_transfer' => 'Transfer Bank',
            'transfer_detail' => '<p>Booking REF : :ref</p><p>Amount : :amount MMK</p><p>Bank : :bankname</p><p>After payment is done,please let us know via viber '.config('appsetting.basic.email').' or email '.config('appsetting.basic.email').'  along with your bank receipt slip or screenshoot.</p>',
            'order_payment_successful' => 'Your order payment has been made successfully .',
            'order_payment_status_was' => 'Sorry, your booking payment status was',
            'payment_manually_later' => 'You can also pay this payment manually later with above Reference Number.',
            'back' => 'Back',

        ],
    ],
];
