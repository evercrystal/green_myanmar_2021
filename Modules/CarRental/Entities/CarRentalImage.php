<?php

namespace Modules\CarRental\Entities;

use Illuminate\Database\Eloquent\Model;

class CarRentalImage extends Model
{
    protected $fillable = ['carrental_id', 'image'];
    
    protected $table = 'car_rental_images';
}
