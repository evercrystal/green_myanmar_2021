<?php

namespace Modules\CarRental\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Enums\Table;
use Modules\Destination\Entities\Destination;
use Modules\CarRental\Entities\CarRentalImage;

class CarRental extends Model
{
    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = Table::CARRENTAL;

    protected $fillable = ["id","destination_id","title","content","price_table","service_include","service_exclude","remark"];

    
    public function destination()
    {
        return $this->hasOne(Destination::class,'id','destination_id');
    }

    public function carrental_images()
    {
        return $this->hasMany(CarRentalImage::class, 'carrental_id');
    }

    public function getShowButtonAttribute()
    {
        $has_image = count($this->carrental_images);

    	if(auth()->user()->can('view carrental')){
            if ($has_image > 0) 
        	return '<a href="'.route('admin.carrental.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-success"><i class="fas fa-upload"></i></a>';
            else
            return '<a href="'.route('admin.carrental.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-danger"><i class="fas fa-upload"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit carrental')){
        	return '<a href="'.route('admin.carrental.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
       	return '';
    }

     /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete carrental')) {
            return '<a href="'.route('admin.carrental.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
            return $this->getShowButtonAttribute().$this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
