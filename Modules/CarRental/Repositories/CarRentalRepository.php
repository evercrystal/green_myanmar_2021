<?php

namespace Modules\CarRental\Repositories;

use Modules\CarRental\Entities\CarRental;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use Modules\CarRental\Entities\CarRentalImage;
/**
 * Class CarRentalRepository.
 */
class CarRentalRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(CarRental $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }

    public function upload_image($id, array $input)
    {   
        $name = uniqid('carrental - ').'.'.$input['file']->extension();
        $carrental_photo = new CarRentalImage;
        $carrental_photo->carrental_id = $id;
        $carrental_photo->image = \Storage::disk('uploads')->putFileAs('carrental', $input['file'],$name);
        $carrental_photo->save();
        \Log::info('CarRental Image Upload : ' . auth()->user()->name);
        return true;
    }

    public function delete_uploaded_image($id, array $input)
    {
        \Log::info('CarRental Image Deleted : ' . auth()->user()->name);
        $carrental_image = CarRentalImage::where('carrental_id',$id)->where('id',$input['id'])->first();
        \File::delete('uploads/'. $carrental_image->image);
        return $carrental_image->delete();
    }

    public function get_uploaded_image($id)
    {
        return CarRentalImage::where('carrental_id',$id)->get();
    }
}
