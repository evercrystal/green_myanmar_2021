<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\CarRental\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('carrental/get', 'CarRentalTableController')->name('carrental.get');
            /*
             * User CRUD
             */
            Route::resource('carrental', 'CarRentalController');

            Route::any('carrental_image/{id}','CarRentalController@carrental_image')->name('carrental.carrental_image');
});