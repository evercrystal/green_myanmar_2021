<?php

namespace Modules\CarRental\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\CarRental\Repositories\CarRentalRepository;
use Modules\CarRental\Http\Requests\ManageCarRentalRequest;

class CarRentalTableController extends Controller
{
    /**
     * @var CarRentalRepository
     */
    protected $carrental;

    /**
     * @param CarRentalRepository $carrental
     */
    public function __construct(CarRentalRepository $carrental)
    {
        $this->carrental = $carrental;
    }

    /**
     * @param ManageCarRentalRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCarRentalRequest $request)
    {
        return DataTables::of($this->carrental->getForDataTable())
            ->addColumn('actions', function ($carrental) {
                return $carrental->action_buttons;
            })
            ->editColumn('destination',function($carrental){
                return '<a href="'. route('admin.destination.show', $carrental->destination_id) .'" target="_blank">'.$carrental->destination->name .'</a>';

                return ;
            })
            ->rawColumns(['actions','destination'])
            ->make(true);
    }
}
