<?php

namespace Modules\CarRental\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\CarRental\Entities\CarRental;
use Modules\CarRental\Http\Requests\ManageCarRentalRequest;
use Modules\CarRental\Http\Requests\CreateCarRentalRequest;
use Modules\CarRental\Http\Requests\UpdateCarRentalRequest;
use Modules\CarRental\Http\Requests\ShowCarRentalRequest;
use Modules\CarRental\Repositories\CarRentalRepository;
use Modules\Destination\Repositories\DestinationRepository;

class CarRentalController extends Controller
{
 /**
     * @var CarRentalRepository
     * @var CategoryRepository
     */
    protected $carrental;

    /**
     * @param CarRentalRepository $carrental
     */
    public function __construct(CarRentalRepository $carrental,DestinationRepository $destination)
    {
        $this->carrental = $carrental;
        $this->destination = $destination;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('carrental::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $destinations = $this->destination->where('is_active',1)->get();
        return view('carrental::create',compact('destinations'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateCarRentalRequest $request)
    {
        $input = $request->except('_token','_method');
        $this->carrental->create($input);
        return redirect()->route('admin.carrental.index')->withFlashSuccess(trans('carrental::alerts.backend.carrental.created'));
    }

    /**
     * @param CarRental              $carrental
     * @param ManageCarRentalRequest $request
     *
     * @return mixed
     */
    public function edit(CarRental $carrental, ManageCarRentalRequest $request)
    {
        $destinations = $this->destination->where('is_active',1)->get();
        return view('carrental::edit',compact('destinations','carrental'));
    }

    /**
     * @param CarRental              $carrental
     * @param UpdateCarRentalRequest $request
     *
     * @return mixed
     */
    public function update(CarRental $carrental, UpdateCarRentalRequest $request)
    {
        $this->carrental->updateById($carrental->id,$request->except('_token','_method'));

        return redirect()->route('admin.carrental.index')->withFlashSuccess(trans('carrental::alerts.backend.carrental.updated'));
    }

    /**
     * @param CarRental              $carrental
     * @param ManageCarRentalRequest $request
     *
     * @return mixed
     */
    public function show(CarRental $carrental, ShowCarRentalRequest $request)
    {
        return view('carrental::show',compact('carrental'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(CarRental $carrental)
    {
        $this->carrental->deleteById($carrental->id);

        return redirect()->route('admin.carrental.index')->withFlashSuccess(trans('carrental::alerts.backend.carrental.deleted'));
    }

    public function carrental_image($id, Request $request)
    {
        
        switch ($request->method()){
            case 'POST' :
                $this->carrental->upload_image($id,$request->all());
                break;

            case 'DELETE' :
                $this->carrental->delete_uploaded_image($id,$request->all());
                break;

            default:
                $images = $this->carrental->get_uploaded_image($id);
                // dd($images);
                $count = 0 ;
                $obj = array();
                foreach ($images as $image) {
                    $obj[$count]['id'] = $image->id;
                    $obj[$count]['name'] = 'Image - '.$image->id;
                    $obj[$count]['image'] = url('uploads/'.$image->image);
                    $obj[$count]['size'] = \Storage::disk('uploads')->size($image->image);
                    $count++;
                }

                return response()->json($obj);
                break;

        }
    }
}
