<?php

Breadcrumbs::for('admin.carrental.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('carrental::labels.backend.carrental.management'), route('admin.carrental.index'));
});

Breadcrumbs::for('admin.carrental.create', function ($trail) {
    $trail->parent('admin.carrental.index');
    $trail->push(__('carrental::labels.backend.carrental.create'), route('admin.carrental.create'));
});

Breadcrumbs::for('admin.carrental.show', function ($trail, $id) {
    $trail->parent('admin.carrental.index');
    $trail->push(__('carrental::labels.backend.carrental.show'), route('admin.carrental.show', $id));
});

Breadcrumbs::for('admin.carrental.edit', function ($trail, $id) {
    $trail->parent('admin.carrental.index');
    $trail->push(__('carrental::labels.backend.carrental.edit'), route('admin.carrental.edit', $id));
});
