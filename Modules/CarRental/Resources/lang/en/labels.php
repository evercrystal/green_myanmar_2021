<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'carrental' => [
                    'create'     => 'Create CarRental',
                    'edit'       => 'Edit CarRental',
                    'management' => 'CarRental Management',
                    'list'       => 'CarRental List',
                    'show'       => 'CarRental Detail',

                    'table' => [
                        'number_of_users' => 'Number of CarRentals',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'carrental total|carrental total',
                        'destination'      => 'Destination',
                        'title'            => 'Title',
                        'content'          => 'Content',
                        'price_table'      => 'Price Lists Table',
                        'service_include'  => 'Service Include',
                        'service_exclude'  => 'Service Exclude',
                        'remark'           => 'Remark',
                    ]
                ]
            ]

];