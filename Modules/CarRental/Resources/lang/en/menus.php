<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'carrental' => 'CarRental',
                'carrental_bin' => 'CarRental Bin',
            ],
            'carrental' => [
                'all'        => 'All CarRental',
                'create'     => 'Create CarRental',
                'edit'       => 'Edit CarRental',
                'show'       => 'Show CarRental',
                'management' => 'CarRental Management',
                'main'       => 'CarRental',
            ]
        ]
];