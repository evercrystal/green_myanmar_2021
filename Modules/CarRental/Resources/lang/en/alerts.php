<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'carrental' => [
            'created' => 'The carrental was successfully created.',
            'deleted' => 'The carrental was successfully deleted.',
            'updated' => 'The carrental was successfully updated.'
        ]
    ]
];