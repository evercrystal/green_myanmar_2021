@extends ('backend.layouts.app')

@section ('title', __('carrental::labels.backend.carrental.management'))

@section('breadcrumb-links')
    @include('carrental::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('js/plugin/dropzone/dist/min/basic.min.css') }}
{{ style('js/plugin/dropzone/dist/min/dropzone.min.css') }}

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('carrental::labels.backend.carrental.management') }}
                    <small class="text-muted">{{ __('carrental::labels.backend.carrental.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                {{ html()->form('POST', route('admin.carrental.carrental_image', $carrental->id))->class('dropzone dropzone-file-area')->id('my-awesome-dropzone')->attributes(['files'=> true, 'role' => 'form'])->open() }}
                <h3 class="sbold">Drop files here or click to upload photo(640px,1136px)</h3>

                {{ html()->closeModelForm() }}
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.destination') }}</th>
                            <td><a href="route('admin.destination.show', $carrental->destination_id)" target="_blank">{{$carrental->destination->name}}</a></td>
                        </tr>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.title') }}</th>
                            <td>{{ $carrental->title }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.content') }}</th>
                            <td>{{ $carrental->content }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.price_table') }}</th>
                            <td>{{ $carrental->price_table }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.service_include') }}</th>
                            <td>{{ $carrental->service_include }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.service_exclude') }}</th>
                            <td>{{ $carrental->service_exclude }}</td>
                        </tr>

                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.remark') }}</th>
                            <td>{{ $carrental->remark }}</td>
                        </tr>
                    </tbody>
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('carrental::labels.backend.carrental.table.created') }}:</strong> {{ $carrental->updated_at->timezone(get_user_timezone()) }} ({{ $carrental->created_at->diffForHumans() }}),
                    <strong>{{ __('carrental::labels.backend.carrental.table.last_updated') }}:</strong> {{ $carrental->created_at->timezone(get_user_timezone()) }} ({{ $carrental->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')
{{ script("js/plugin/dropzone/dist/min/dropzone.min.js") }}

<script>
    $(document).ready(function() {
    });

        Dropzone.options.myAwesomeDropzone = {
            init: function () {
                thisDropzone = this;
                this.on('success', function(){
                    if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                        location.reload();
                    }
                });
                $.get('{{ route('admin.carrental.carrental_image' , $carrental->id ) }}', function(data) {
                    $.each(data, function (key, value) {

                        var mockFile = {name: value.name , size : value.size , id : value.id };

                        thisDropzone.options.addedfile.call(thisDropzone, mockFile);

                        thisDropzone.options.thumbnail.call(thisDropzone, mockFile, value.image );
                        thisDropzone.emit("complete", mockFile);
                    });
                });

            },
            dictRemoveFileConfirmation: 'Are you sure!',

            addRemoveLinks: true,

            removedfile : function (file) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var id = file.id;
                $.ajax({
                    type: 'DELETE',
                    url: '{{ route('admin.carrental.carrental_image' , $carrental->id ) }}',
                    data: {
                        'id': id
                    },
                    dataType: 'json'
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            }
        };

</script>
@endpush