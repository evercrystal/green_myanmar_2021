@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('carrental::labels.backend.carrental.management'))

@section('breadcrumb-links')
    @include('carrental::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('carrental::labels.backend.carrental.management') }} <small class="text-muted">{{ __('carrental::labels.backend.carrental.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('carrental::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="carrental-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('carrental::labels.backend.carrental.table.id') }}</th>
                            <th>{{ __('carrental::labels.backend.carrental.table.destination') }}</th>
                            <th>{{ __('carrental::labels.backend.carrental.table.title') }}</th>
                            <th>{{ __('carrental::labels.backend.carrental.table.content') }}</th>
                            <th>{{ __('carrental::labels.backend.carrental.table.service_include') }}</th>
                            <th>{{ __('carrental::labels.backend.carrental.table.service_exclude') }}</th>
                            <th>{{ __('carrental::labels.backend.carrental.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#carrental-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.carrental.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'destination', name: 'destination'},
                    {data: 'title', name: 'title'},
                    {data: 'content', name: 'content'},
                    {data: 'service_include', name: 'service_include'},
                    {data: 'service_exclude', name: 'service_exclude'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush