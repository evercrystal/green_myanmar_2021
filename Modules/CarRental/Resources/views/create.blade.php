@extends ('backend.layouts.app')

@section ('title', __('carrental::labels.backend.carrental.management') . ' | ' . __('carrental::labels.backend.carrental.create'))

@section('breadcrumb-links')
    @include('carrental::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
<link rel="stylesheet" type="text/css" href="/css/plugin/bootstrap-summernote/summernote.css">
@endpush

@section('content')
{{ html()->form('POST', route('admin.carrental.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('carrental::labels.backend.carrental.management') }}
                        <small class="text-muted">{{ __('carrental::labels.backend.carrental.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">   

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.destination'))->class('col-md-2 form-control-label')->for('destination') }}

                        <div class="col-md-10">
                            <select name="destination_id" class="form-control select2">
                                @foreach($destinations as $destination)
                                    <option value="{{$destination->id}}"> {{$destination->name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.title'))->class('col-md-2 form-control-label')->for('title') }}

                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->placeholder(__('carrental::labels.backend.carrental.table.title'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.content'))->class('col-md-2 form-control-label')->for('content') }}

                        <div class="col-md-10">
                            {{ html()->text('content')
                                ->class('form-control')
                                ->placeholder(__('carrental::labels.backend.carrental.table.content'))
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.price_table'))->class('col-md-2 form-control-label')->for('price_table') }}

                        <div class="col-md-10">
                            {{ html()->textarea('price_table')
                                ->class('form-control editor400')
                                ->placeholder(__('carrental::labels.backend.carrental.table.price_table'))
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.service_include'))->class('col-md-2 form-control-label')->for('service_include') }}

                        <div class="col-md-10">
                            {{ html()->textarea('service_include')
                                ->class('form-control editor200')
                                ->placeholder(__('carrental::labels.backend.carrental.table.service_include'))
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.service_exclude'))->class('col-md-2 form-control-label')->for('service_exclude') }}

                        <div class="col-md-10">
                            {{ html()->textarea('service_exclude')
                                ->class('form-control editor200')
                                ->placeholder(__('carrental::labels.backend.carrental.table.service_exclude'))
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('carrental::labels.backend.carrental.table.remark'))->class('col-md-2 form-control-label')->for('remark') }}

                        <div class="col-md-10">
                            {{ html()->textarea('remark')
                                ->class('form-control editor200')
                                ->placeholder(__('carrental::labels.backend.carrental.table.remark'))
                                }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.carrental.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')
{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script src="/js/plugin/bootstrap-summernote/summernote.min.js"></script>

<script type="text/javascript">
$(function(){
    $(".select").select2({
        placeholder: 'Choose Destination',
        width: '100%'
    });
})

$(document).ready(function(){
    $('.editor400').summernote({
      height: 400
    });

    $('.editor200').summernote({
      height: 200
    });
});

</script>
@endpush