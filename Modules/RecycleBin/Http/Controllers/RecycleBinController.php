<?php

namespace Modules\RecycleBin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\RecycleBin\Entities\RecycleBin;
use Modules\RecycleBin\Http\Requests\ManageRecycleBinRequest;
use Modules\RecycleBin\Http\Requests\CreateRecycleBinRequest;
use Modules\RecycleBin\Http\Requests\UpdateRecycleBinRequest;
use Modules\RecycleBin\Http\Requests\ShowRecycleBinRequest;
use Modules\RecycleBin\Repositories\RecycleBinRepository;
use Illuminate\Support\Facades\DB;
use App\Repositories\Backend\Auth\UserRepository;

class RecycleBinController extends Controller
{
 /**
     * @var RecycleBinRepository
     * @var CategoryRepository
     */
    protected $recyclebin;

        /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @param RecycleBinRepository $recyclebin
     */
    public function __construct(RecycleBinRepository $recyclebin,UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->recyclebin = $recyclebin;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $submodule = $request->get('submodule');
        return view('recyclebin::index',compact('submodule'));
    }

    /**
     * @param RecycleBin              $recyclebin
     * @param ManageRecycleBinRequest $request
     *
     * @return mixed
     */
    public function show(RecycleBin $recyclebin, ShowRecycleBinRequest $request)
    {
        $name = strtolower($recyclebin->module);
        $NamespacedModel = $recyclebin->eloquent;
        $data = $NamespacedModel::onlyTrashed()->find($recyclebin->related_row_id);
        return view($name.'::show',[$name => $data]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(RecycleBin $recyclebin)
    {
        $name = strtolower($recyclebin->module);
        $NamespacedModel = $recyclebin->eloquent;
        $module = $NamespacedModel::onlyTrashed()->find($recyclebin->related_row_id);

        $module->restore();

        if($name == 'merchant'){
            $this->userRepository->mark($module->user, 1);
        }

        $this->recyclebin->deleteById($recyclebin->id);
        return redirect()->route('admin.'.$name.'.index')->withFlashSuccess(trans('recyclebin::alerts.backend.recyclebin.restored'));
    }
}
