<?php

return [
    'name' => 'RecycleBin',
    'icon' => 'nav-icon fas fa-trash',
];
