<?php

namespace Modules\Country\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Country\Repositories\CountryRepository;
use Modules\Country\Http\Requests\ManageCountryRequest;

class CountryTableController extends Controller
{
    /**
     * @var CountryRepository
     */
    protected $country;

    /**
     * @param CountryRepository $country
     */
    public function __construct(CountryRepository $country)
    {
        $this->country = $country;
    }

    /**
     * @param ManageCountryRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCountryRequest $request)
    {
        return DataTables::of($this->country->getForDataTable())
            ->addColumn('actions', function ($country) {
                return $country->action_buttons;
            })
            ->editColumn('country_image',function($country){
                return '<img style="width:100px;height:100px;" src="../uploads/'.$country->country_image.'">';
            })
             ->addColumn('status', function ($country) {
                return $country->status_badge;
            })
            ->rawColumns(['actions','status','country_image'])
            ->make(true);
    }
}
