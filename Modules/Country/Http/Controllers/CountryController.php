<?php

namespace Modules\Country\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Country\Entities\Country;
use Modules\Country\Http\Requests\ManageCountryRequest;
use Modules\Country\Http\Requests\CreateCountryRequest;
use Modules\Country\Http\Requests\UpdateCountryRequest;
use Modules\Country\Http\Requests\ShowCountryRequest;
use Modules\Country\Repositories\CountryRepository;
use Illuminate\Http\UploadedFile;
use Storage;

class CountryController extends Controller
{
 /**
     * @var CountryRepository
     * @var CategoryRepository
     */
    protected $country;

    /**
     * @param CountryRepository $country
     */
    public function __construct(CountryRepository $country)
    {
        $this->country = $country;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('country::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('country::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateCountryRequest $request)
    {
        // $request['is_active'] = $request->is_active ? $request->is_active : 0;
        $input = $request->except('_token','_method');
        $image = uniqid('country_image-').'.'.$request->country_image->extension();

        $input['country_image'] = \Storage::disk('uploads')->putFileAs('country_image', $request->file('country_image'),$image);

        $this->country->create($input);
        return redirect()->route('admin.country.index')->withFlashSuccess(trans('country::alerts.backend.country.created'));
    }

    /**
     * @param Country              $country
     * @param ManageCountryRequest $request
     *
     * @return mixed
     */
    public function edit(Country $country, ManageCountryRequest $request)
    {
        return view('country::edit')
            ->withCountry($country);
    }

    /**
     * @param Country              $country
     * @param UpdateCountryRequest $request
     *
     * @return mixed
     */
    public function update(Country $country, UpdateCountryRequest $request)
    {
        $input = $request->except('_token','_method');
        if($request->country_image) {
            \File::delete('uploads/'.$country->country_image);
            $image = uniqid('country_image-').'.'.$request->country_image->extension();
            $input['country_image'] = \Storage::disk('uploads')->putFileAs('country_image', $request->country_image,$image);
        }

        $this->country->updateById($country->id,$input);

        return redirect()->route('admin.country.index')->withFlashSuccess(trans('country::alerts.backend.country.updated'));
    }

    /**
     * @param Country              $country
     * @param ManageCountryRequest $request
     *
     * @return mixed
     */
    public function show(Country $country, ShowCountryRequest $request)
    {
        return view('country::show')->withCountry($country);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Country $country)
    {
        \File::delete('uploads/'.$country->country_image);
        $this->country->deleteById($country->id);

        return redirect()->route('admin.country.index')->withFlashSuccess(trans('country::alerts.backend.country.deleted'));
    }
}
