<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Country\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('country/get', 'CountryTableController')->name('country.get');
            /*
             * User CRUD
             */
            Route::resource('country', 'CountryController');
});