<?php

Breadcrumbs::for('admin.country.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('country::labels.backend.country.management'), route('admin.country.index'));
});

Breadcrumbs::for('admin.country.create', function ($trail) {
    $trail->parent('admin.country.index');
    $trail->push(__('country::labels.backend.country.create'), route('admin.country.create'));
});

Breadcrumbs::for('admin.country.show', function ($trail, $id) {
    $trail->parent('admin.country.index');
    $trail->push(__('country::labels.backend.country.show'), route('admin.country.show', $id));
});

Breadcrumbs::for('admin.country.edit', function ($trail, $id) {
    $trail->parent('admin.country.index');
    $trail->push(__('country::labels.backend.country.edit'), route('admin.country.edit', $id));
});
