<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'country' => 'Country',
                'country_bin' => 'Country Bin',
            ],
            'country' => [
                'all'        => 'All Country',
                'create'     => 'Create Country',
                'edit'       => 'Edit Country',
                'show'       => 'Show Country',
                'management' => 'Country Management',
                'main'       => 'Country',
            ]
        ]
];