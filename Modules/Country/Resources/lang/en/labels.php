<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'country' => [
                    'create'     => 'Create Country',
                    'edit'       => 'Edit Country',
                    'management' => 'Country Management',
                    'list'       => 'Country List',
                    'show'       => 'Country Detail',

                    'table' => [
                        'number_of_users' => 'Number of Countrys',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'name_mm'          => 'Myanmar Name',
                        'country_code'     => 'Country Code',
                        'video_url'        => 'Video URL',
                        'country_image'    => 'Country Image',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'active'           => 'Active',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'country total|country total',
                    ]
                ]
            ]

];