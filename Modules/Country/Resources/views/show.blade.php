@extends ('backend.layouts.app')

@section ('title', __('country::labels.backend.country.management'))

@section('breadcrumb-links')
    @include('country::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('country::labels.backend.country.management') }}
                    <small class="text-muted">{{ __('country::labels.backend.country.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.name') }}</th>
                            <td>{{ $country->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.name_mm') }}</th>
                            <td>{{ $country->name_mm }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.country_code') }}</th>
                            <td>{{ $country->country_code }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.video_url') }}</th>
                            <td>{{ $country->video_url }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.country_image') }}</th>
                            <td><img src="{{asset('uploads/'.$country->country_image) }}" alt="" class="img-thumbnail" width="200px"></td>
                        </tr>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.active') }}</th>
                            <td>
                                {!! $country->status_badge !!}
                              
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('country::labels.backend.country.table.created') }}:</strong> {{ $country->updated_at->timezone(get_user_timezone()) }} ({{ $country->created_at->diffForHumans() }}),
                    <strong>{{ __('country::labels.backend.country.table.last_updated') }}:</strong> {{ $country->created_at->timezone(get_user_timezone()) }} ({{ $country->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush