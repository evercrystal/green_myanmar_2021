<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'appsetting' => [
                    'create'     => 'Create AppSetting',
                    'edit'       => 'Edit AppSetting',
                    'management' => 'AppSetting Management',
                    'list'       => 'AppSetting List',
                    'show'       => 'AppSetting Detail',

                    'table' => [
                        'number_of_users' => 'Number of AppSettings',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'appsetting total|appsetting total',
                    ],
                    'basic' => [
                        'main_logo' => 'App Main Logo',
                        'fav_icon' => 'App Fav Icon',
                        'app_name' => 'App Name',
                        'facebook_link' => 'Facebook Link',
                        'app_email' => 'Email Address',
                        'qr_prefix' => 'QR Prefix',
                        'phone_number' => 'Phone Number',
                        'address' => 'Address',
                        'doller_rate' => 'Doller Exchange Rate',
                        'google_map_api' => 'Google Map Api',
                        'meta_keywords' => 'Meta Keywords',
                        'meta_description' => 'Meta Description',
                        'appstore' => 'App Store',
                        'playstore' => 'Play Store',
                        'intro_content' => 'Intro Content',
                        'youtubedemo' => 'Youtube Demo',
                        'basic_setting' => 'Basic Setting',
                        'email' => 'Email Setting',
                        'date_format' => 'Date Format',
                        'time_format' => 'Time Format',
                        'datetime_format' => 'Date Time Format',
                    ],
                ]
            ]

];