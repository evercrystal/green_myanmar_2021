<?php

namespace Modules\Destination\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Destination\Repositories\DestinationRepository;
use Modules\Destination\Http\Requests\ManageDestinationRequest;

class DestinationTableController extends Controller
{
    /**
     * @var DestinationRepository
     */
    protected $destination;

    /**
     * @param DestinationRepository $destination
     */
    public function __construct(DestinationRepository $destination)
    {
        $this->destination = $destination;
    }

    /**
     * @param ManageDestinationRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageDestinationRequest $request)
    {
        return DataTables::of($this->destination->getForDataTable())
            ->editColumn('destination_image', function ($destination){
                return '<img style="width:100px;height:100px;" src="../uploads/'.$destination->destination_image.'">';
            })
            ->editColumn('country_id', function($destination)
            {
                return $destination->country->name;
            })
            ->addColumn('status', function ($route) {
                return $route->status_badge;
            })
            ->addColumn('actions', function ($destination) {
                return $destination->action_buttons;
            })
            ->rawColumns(['country_id','destination_image','status','actions'])
            ->make(true);
    }
}
