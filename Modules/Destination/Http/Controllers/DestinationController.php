<?php

namespace Modules\Destination\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Destination\Entities\Destination;
use Modules\Destination\Http\Requests\ManageDestinationRequest;
use Modules\Destination\Http\Requests\CreateDestinationRequest;
use Modules\Destination\Http\Requests\UpdateDestinationRequest;
use Modules\Destination\Http\Requests\ShowDestinationRequest;
use Modules\Destination\Repositories\DestinationRepository;
use Modules\Country\Repositories\CountryRepository;

class DestinationController extends Controller
{
 /**
     * @var DestinationRepository
     * @var CategoryRepository
     */
    protected $destination;

    /**
     * @param DestinationRepository $destination
     */
    public function __construct(DestinationRepository $destination , CountryRepository $country)
    {
        $this->destination = $destination;
        $this->country = $country;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('destination::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $countries = $this->country->where('is_active',1)->get();
        return view('destination::create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateDestinationRequest $request)
    {
        $input = $request->except('_token','_method');
        $image = uniqid('destination_image-').'.'.$request->destination_image->extension();

        $input['destination_image'] = \Storage::disk('uploads')->putFileAs('destination_image', $request->file('destination_image'),$image);
        $this->destination->create($input);
        return redirect()->route('admin.destination.index')->withFlashSuccess(trans('destination::alerts.backend.destination.created'));
    }

    /**
     * @param Destination              $destination
     * @param ManageDestinationRequest $request
     *
     * @return mixed
     */
    public function edit(Destination $destination, ManageDestinationRequest $request)
    {
        $countries = $this->country->where('is_active',1)->get();
        return view('destination::edit',compact('countries'))
            ->withDestination($destination);
    }

    /**
     * @param Destination              $destination
     * @param UpdateDestinationRequest $request
     *
     * @return mixed
     */
    public function update(Destination $destination, UpdateDestinationRequest $request)
    {
        $input = $request->except('_token','_method');
        if($request->destination_image) {
            \File::delete('uploads/'.$destination->destination_image);
            $image = uniqid('destination_image-').'.'.$request->destination_image->extension();
            $input['destination_image'] = \Storage::disk('uploads')->putFileAs('destination_image', $request->destination_image,$image);
        }

        $this->destination->updateById($destination->id,$input);

        return redirect()->route('admin.destination.index')->withFlashSuccess(trans('destination::alerts.backend.destination.updated'));
    }

    /**
     * @param Destination              $destination
     * @param ManageDestinationRequest $request
     *
     * @return mixed
     */
    public function show(Destination $destination, ShowDestinationRequest $request)
    {
        return view('destination::show')->withDestination($destination);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Destination $destination)
    {
        $this->destination->deleteById($destination->id);

        return redirect()->route('admin.destination.index')->withFlashSuccess(trans('destination::alerts.backend.destination.deleted'));
    }
}
