<?php

Breadcrumbs::for('admin.destination.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('destination::labels.backend.destination.management'), route('admin.destination.index'));
});

Breadcrumbs::for('admin.destination.create', function ($trail) {
    $trail->parent('admin.destination.index');
    $trail->push(__('destination::labels.backend.destination.create'), route('admin.destination.create'));
});

Breadcrumbs::for('admin.destination.show', function ($trail, $id) {
    $trail->parent('admin.destination.index');
    $trail->push(__('destination::labels.backend.destination.show'), route('admin.destination.show', $id));
});

Breadcrumbs::for('admin.destination.edit', function ($trail, $id) {
    $trail->parent('admin.destination.index');
    $trail->push(__('destination::labels.backend.destination.edit'), route('admin.destination.edit', $id));
});
