<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Destination\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('destination/get', 'DestinationTableController')->name('destination.get');
            /*
             * User CRUD
             */
            Route::resource('destination', 'DestinationController');
});