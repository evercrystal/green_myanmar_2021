<?php

namespace Modules\Destination\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

class CreateDestinationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191|unique:'.Table::DESTINATION,
            'name_mm' => 'required|max:191|unique:'.Table::DESTINATION,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create destination');
    }
}
