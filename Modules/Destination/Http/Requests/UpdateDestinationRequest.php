<?php

namespace Modules\Destination\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

class UpdateDestinationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name' => 'required|unique:'.Table::DESTINATION.',name,'.$id,
            'name_mm' => 'required|unique:'.Table::DESTINATION.',name_mm,'.$id
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit destination');
    }
}
