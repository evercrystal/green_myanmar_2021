<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'destination' => [
                    'create'     => 'Create Destination',
                    'edit'       => 'Edit Destination',
                    'management' => 'Destination Management',
                    'list'       => 'Destination List',
                    'show'       => 'Destination Detail',

                    'table' => [
                        'number_of_users' => 'Number of Destinations',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'country'          => 'Country',
                        'name'             => 'Name',
                        'name_mm'          => 'Myanmar Name',
                        'country_code'     => 'Country Code',
                        'video_url'        => 'Video URL',
                        'destination_image'=> 'Destination Image',
                        'active'           => 'Active',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'destination total|destination total',
                    ]
                ]
            ]

];