<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'destination' => [
            'created' => 'The destination was successfully created.',
            'deleted' => 'The destination was successfully deleted.',
            'updated' => 'The destination was successfully updated.'
        ]
    ]
];