@extends ('backend.layouts.app')

@section ('title', __('destination::labels.backend.destination.management') . ' | ' . __('destination::labels.backend.destination.edit'))

@section('breadcrumb-links')
    @include('destination::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
@endpush

@section('content')
{{ html()->modelForm($destination, 'PATCH', route('admin.destination.update', $destination->id))->attribute('enctype', 'multipart/form-data')->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('destination::labels.backend.destination.management') }}
                        <small class="text-muted">{{ __('destination::labels.backend.destination.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('destination::labels.backend.destination.table.country').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('country_id') }}

                        <div class="col-md-10">
                            <select name="country_id" class="form-control select2">
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if($country->id == $destination->country_id) selected @endif> {{$country->name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('destination::labels.backend.destination.table.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('destination::labels.backend.destination.table.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('destination::labels.backend.destination.table.name_mm'))->class('col-md-2 form-control-label')->for('name_mm') }}

                        <div class="col-md-10">
                            {{ html()->text('name_mm')
                                ->class('form-control')
                                ->placeholder(__('destination::labels.backend.destination.table.name_mm'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('destination::labels.backend.destination.table.video_url').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('video_url') }}

                        <div class="col-md-10">
                            {{ html()->text('video_url')
                                ->class('form-control')
                                ->placeholder(__('destination::labels.backend.destination.table.video_url'))}}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('destination::labels.backend.destination.table.destination_image').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('destination_image') }}
                        <div class="col-md-10">
                            @if($destination->destination_image)
                                <img src="{{asset('uploads/'.$destination->destination_image) }}" alt="" class="img-thumbnail" width="200px">
                            @endif
                            {{ html()->file('destination_image')
                                ->class('form-control-file')}}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('destination::labels.backend.destination.table.active'))->class('col-md-2 form-control-label')->for('active') }}

                        <div class="col-md-10">
                            <label class="switch switch-label switch-pill switch-primary">
                                @if($destination->is_active == 1)
                                    {{ html()->checkbox('is_active', true)->class('switch-input') }}
                                @else
                                    {{ html()->checkbox('is_active', false)->class('switch-input') }}
                                @endif
                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                            </label>
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.destination.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')
{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script>
$(function(){
    $(".select").select2({
        placeholder: 'Choose Country',
        width: '100%'
    });
})

</script>
@endpush