@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('destination::labels.backend.destination.management'))

@section('breadcrumb-links')
    @include('destination::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('destination::labels.backend.destination.management') }} <small class="text-muted">{{ __('destination::labels.backend.destination.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('destination::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="destination-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.id') }}</th>
                            <th>{{ __('destination::labels.backend.destination.table.country') }}</th>
                            <th>{{ __('destination::labels.backend.destination.table.name') }}</th>
                            <th>{{ __('destination::labels.backend.destination.table.name_mm') }}</th>
                            <th>{{ __('destination::labels.backend.destination.table.destination_image') }}</th>
                            <th>{{ __('destination::labels.backend.destination.table.active') }}</th>
                            <th>{{ __('destination::labels.backend.destination.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#destination-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.destination.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'country_id', name: 'country_id'},
                    {data: 'name', name: 'name'},
                    {data: 'name_mm', name: 'name_mm'},
                    {data: 'destination_image', name: 'destination_image'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush