@extends ('backend.layouts.app')

@section ('title', __('destination::labels.backend.destination.management'))

@section('breadcrumb-links')
    @include('destination::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('destination::labels.backend.destination.management') }}
                    <small class="text-muted">{{ __('destination::labels.backend.destination.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.country') }}</th>
                            <td>{{ $destination->country->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.name') }}</th>
                            <td>{{ $destination->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.name_mm') }}</th>
                            <td>{{ $destination->name_mm }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.video_url') }}</th>
                            <td>{{ $destination->video_url }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.destination_image') }}</th>
                            <td><img src="{{asset('uploads/'.$destination->destination_image) }}" alt="" class="img-thumbnail" width="200px"></td>
                        </tr>
                        <tr>
                            <th>{{ __('destination::labels.backend.destination.table.active') }}</th>
                            <td>
                                {!! $destination->status_badge !!}
                              
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('destination::labels.backend.destination.table.created') }}:</strong> {{ $destination->updated_at->timezone(get_user_timezone()) }} ({{ $destination->created_at->diffForHumans() }}),
                    <strong>{{ __('destination::labels.backend.destination.table.last_updated') }}:</strong> {{ $destination->created_at->timezone(get_user_timezone()) }} ({{ $destination->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush