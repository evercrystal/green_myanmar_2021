<?php

return [
    'name' => 'Destination',
    'icon' => 'nav-icon fas fa-map-marked-alt',
];
