<ul class="dropdown">
    <li><a href="index.php" class="nav-open">HOME</a></li>
    <li><a href="car-rental.php" class="nav-border">CAR RENTAL</a>
        <ul class="sub_menu">
            <li><a href="yangon-car-rental.php" title="Yangon Sightseeing car rental">Yangon Car Rental</a></li>
            <li><a href="mandalay-car-rental.php" title="Mandalay Sightseeing car rental">Mandalay Car Rental</a></li>
            <li><a href="bagan-car-rental.php" title="Bagan Sightseeing Car Rental">Bagan Car Rental</a></li>
            <li><a href="inle-lake-car-rental.php" title="Inle Lake Sightseeing Car rental">Inle Lake Car rental</a></li>
            <li><a href="myanmar-shared-taxi-services.php" title="Myanmar Shared Taxi">Myanmar Shared Taxi services</a></li>

        </ul>
    </li>
    <!-- <li><a href="car-rental.php" class="nav-border">Flights</a>
        <ul class="sub_menu">
            <li><a href="yangon-car-rental.php" title="Yangon Sightseeing car rental">Yangon Car Rental</a></li>
            <li><a href="mandalay-car-rental.php" title="Mandalay Sightseeing car rental">Mandalay Car Rental</a></li>
            <li><a href="bagan-car-rental.php" title="Bagan Sightseeing Car Rental">Bagan Car Rental</a></li>
            <li><a href="inle-lake-car-rental.php" title="Inle Lake Sightseeing Car rental">Inle Lake Car rental</a></li>
        </ul>
    </li> -->
    <li><a href="itinerary.php" class="nav-border">MYANMAR TOUR PACKAGES</a>
        <ul class="sub_menu" >
            <li><a href="4days.php" title="Myanmar Culture package Tour">4 Days Tour</a></li>
            <li><a href="5days.php" title="Myanmar Culture package Tour">5 Days Tour</a></li>
            <li><a href="6days.php" title="Myanmar Culture package Tour">6 Days Tour</a></li>
            <li><a href="7days.php" title="Myanmar Culture package Tour">7 Days Tour</a></li>
            <li><a href="8days.php" title="Myanmar Adventure package Tour">8 Days Tour</a></li>
            <li><a href="14days.php" title="Myanmar Trekking package Tour">14 Days Tour</a></li>
            <li><a href="myanmar-one-day-tour.php" title="Myanmar City package Tour">Myanmar One Day Tour</a></li>
            <li><a href="http://myanmardaytrips.com" title="Myanmar Attractions">Attractions</a></li>
        </ul>
    </li>
    <!-- <li><a href="photos.php" class="nav-border">PHOTOS</a></li> -->
    <li><a href="myanmarhotels.php" class="nav-border">MYANMAR HOTELS</a></li>
    <li><a href="http://myanmarflightticket.com" class="nav-border">FLIGHT TICKETS</a></li>
    <li><a href="reservation.php" style="color:yellow;font-size: 15px;margin-top: -5px" class="nav-border">Do Inquiry</a></li>
    <!-- <li><a href="about.php" class="nav-border">ABOUT US</a>
        <ul class="sub_menu">
            <li><a href="sitemap.xml" title="sitemap">Site Map</a></li>
        </ul>
    </li> -->
</ul>
