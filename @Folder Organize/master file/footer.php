  <style>
      .noshow { display:none; }
  </style>

<!-- Footer Section -->
  <div id="footer">
    <div id="footer-bg">
      <div id="footer-content">
        <div id="footer-contact">
          <h2 id="fcontact">Contact us</h2>
          <div id="contact-wrapper">
            <form class="contactform" id="commentForm" method="post" action="gmbksd.php">

                <input type="hidden" name="link" value="<?php echo  $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']; ?>" />
                <p>
                <input id="name" name="name" class="required" value="Name" />
              </p>
              <p>
                <input id="email" name="email" class="required email" value="Email" />
              </p>
              <p>
                <textarea cols="20" rows="4" id="message" name="message" class="required"> Message </textarea>
              </p>

                <p>
                    <img src="captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg' >
                    <label for='message'>Enter the code above here :</label>
                    <input id="6_letters_code" name="6_letters_code" type="text" class="required"><br>
                    <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
                </p>

                <p>
                <input class="submit" style="cursor: pointer" type="submit" value="Submit"/>
              </p>
            </form>
          </div>
        </div>

        <div id="footer-panel1" class="footer-nev">
          <h2 id="panel1">Quick Links</h2>
          <ul>
              <li>
                  <div style="margin-left:25px;margin-top: 15px">
                      <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                          <input type="hidden" name="cmd" value="_xclick">
                          <input type="hidden" name="business" value="gmbfgroup@gmail.com">
                          <input type="hidden" name="lc" value="US">
                          <input type="hidden" name="button_subtype" value="services">
                          <input type="hidden" name="no_note" value="0">
                          <input type="hidden" name="currency_code" value="USD">
                          <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest">
                          <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                          <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                      </form>
                  </div>
              </li>
            <li><a href="destination.php">Destination</a></li>
            <li><a href="itinerary.php">Itinerary</a></li>
            <li><a href="photos.php">Photos &amp; Gallery</a></li>
            <!-- <li><a href="about.php">About Us</a></li>
              <li><a href="myanmar-food.php">Myanmar Food</a></li> -->
              <li><a href="termandcondition.php">Terms and Conditions</a></li>

              <li><a href="myanmar-unesco-tour.php">Myanmar Unesco Tour</a></li>


              <li><a href="hotels.php">Our Partner Hotels </a></li>

              <li><a href="http://myanmartravelmap.com/map_main.php?text1=4">Myanmar Travel Map</a> </li>
              <li><a href="myanmar_festival.php" class="nav-border">Myanmar Festivals </a></li>
              <li><a href="link.php">Our Partner Links</a></li>
              <li><a href="jobs.php">Jobs</a></li>
              <li><a href="faq.php">FAQ</a></li>
              <li><a href="myanmar-travel-tips.php">Myanmar Travel Tips</a></li>

          </ul>
        </div>
        <div id="footer-panel2" class="footer-nev editable">
          <h2 id="panel2">Contact</h2>
          <ul>
          <ul>
                      <li><a><b><i>Location:</i></b><br/>391, WayZaYanTar Road, Za/South Ward, Thingangyun Township, Yangon , Myanmar</a></li>
                      <li><a><b><i>Phone No:</i></b><br/>+95(9)420077655,<br/>+95(9)428016234.</a></li>
                      <li><a><b><i>Fax:</i></b><br/>+95 (1) 8569936.</a></li>
                      <li><a><b><i>Email:</i></b><br/>hello@green-myanmar.com</a></li>
         </ul>

              <p>Copyright © 2017 <a href="http://www.evercrystal.com/">Evercrystal  IT solutions</a> All rights reserved.</p>

        </div>
        <div class="ft-coppy">


        </div>


      </div>
      <!--end footer content--> 
    </div>
    <!--end footer background--> 
  </div>
  <!--end footer--> 

<!--end wrapper-->

  <script language='JavaScript' type='text/javascript'>
      function refreshCaptcha()
      {
          var img = document.images['captchaimg'];
          img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
      }
  </script>

</body>
</html>


