<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
<head>
    <meta name="msvalidate.01" content="708F18953982636A9F9E60EF8D4AF4C3" />
    <title>Green Myanmar Online Travel Agent</title>
    <meta name="robots" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Reliable Myanmar Tour Operators and Myanmar Travel Agent Providing Myanmar Tour Packages , Yangon City Tour , Bagan City Tours, Mandalay Tour For Myanmar Budget Travel and Independent Travellers" />
    <meta name="keyword" content=" Myanmar Travels Tours,Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Best Myanmar Travel Agent, Myanmar Budget Tour" />
    <meta content="en" http-equiv="Content-Language" />
    <meta name="generator" content="" />
    <meta name="viewport" />
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif] -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->




    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="yangon car rental " width="73" height="78" />

    <?php include 'menu.php'; ?>
    <!--end class dropdown-->

</div>
<!--end Header-->

<!--end  Header Section-->

  <!-- Content Section -->

  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form"  style="background: none;padding-top: 15px" action="" method="post" >

          <p><a href="blog.php"><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar Travel Blog" /> Myanmar Travel Blog </a>
          <a href="buy-myanmar-bus-ticket.php"><img src="images/leaflogo.png" height="14" width="20" alt="Book Myanmar Bus Ticket Here" />Myanmar Bus </a></p>
          <p><a href="http://myanmardaytrips.com"><img src="images/leaflogo.png" height="14" width="20" alt="Book online Myanmar Day Tours and Attractions here " />Book Day Tours and Attractions here </a></p>

      </form>

      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>

      </ul>

      <br class="clear"/>
    </div>


    <!--end content_header-->

    <div id="featured" class="editable">
      <div id="block_inside">
        <div id="featured-image"> <img src="images/featured-imagecopy.jpg" alt="featured-imagecopy" width="250" height="260" /> </div>
        <!--end featured-image-->
        <div id="featured-text">
          <h4 style="color:#0FA811;margin-top:10px;"> Feel Free to contact Us - hello@green-myanmar.com </h4>
          <p>Green Myanmar travel agent is providing best quality and service to give best travel experiences for world wide travellers. Please contact us for more Myanmar Travel packages promotion</p>
          <h5><div id="main" style="margin-right:40px;text-align: center"><a href="#">OUR SERVICES</a></div></h5>
            <table border="0">
                <tr>
                    <td>
                        <div id="main1" style="font-size:12px;margin-left:10px;">
                                <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar Tour Packages" /> <a href="itinerary.php">Myanmar Tour packages</a></span><br />
                                <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar visa on Arrival" /> <a href="buy-myanmar-bus-ticket.php">Myanmar Bus Tickets</a></span><br />
                                <span><img src="images/leaflogo.png" height="14" width="20" alt="Yangon Mandalay Car Rental" /> <a href="car-rental.php">Myanmar car rental service</a></span><br />
                                <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar Domestic Flight" /> <a href="myanmar-domestic-flights.php">Myanmar Domestic Flight</a></span><br />
                                <span><img src="images/leaflogo.png" height="14" width="20" alt="Mandalay City Tour" /> <a href="mandalay-city-sightseeing-day-tour.php">Mandalay Sightseeing Tour</a></span><br />

                        </div>
                    </td>
                    <td>
                        <div id="main2" style="font-size:12px;margin-left:60px;">
                            <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar Tour guide" /> <i><a href="myanmar-budget-tour.php">Myanmar budget tour</a></i></span><br />
                            <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar hotel booking" /> <i><a href="myanmarhotels.php">Myanmar hotels</a></i></span><br />
                            <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar Budget Tour" /> <i><a href="kalaw-to-inle-trekking-tour.php"> Myanmar Trekking Tour</a></i></span><br />
                            <span><img src="images/leaflogo.png" height="14" width="20" alt="Myanmar Sightseeing Tour" /> <i><a href="myanmar-sightseeing-tours.php">Myanmar Sightseeing Tour</a></i></span><br />
                            <span><img src="images/leaflogo.png" height="14" width="20" alt="Bagan Sightseeing" /> <a href="bagan-sightseeing-day-tour.php">Bagan Sightseeing DayTour</a></span><br />

                        </div>
                    </td>
                </tr>
            </table>
      <!-- For Google Translation -->
                <div style="margin-left: 0px;">

                                    <div id="google_translate_element" style="margin-top: 5px;margin-left: 50px"></div><script type="text/javascript">
                                    function googleTranslateElementInit() {
                                        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                                    }
                                </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


                </div><!-- End Of Google Translation -->
            <br class="clear"/>
        </div>
        <!--featured text-->
      </div>
      <!-- end block inside-->
    </div>
    <!--end featured-->

    <!--begin main column-->
    <div id="main-column" class="editable" >
      <div id="latest-location">
        <h2 id="last-adventure"></h2>
        <ul>
          <li> <a href="yangon-thanlyin-tour.php"><img src="images/yangon.jpg" width="260" height="112" alt="yangon" title="yangon"/></a>
            <div class="text">
              <h4><a href="yangon-bagan-mandalay-inle-yangon.php">Yangon-Bagan-Mandalay-Inle Lake-Yangon (7Days/6Nights)</a></h4>
              <p>Free and Easy Myanmar Budget Tour to Yangon , Bagan , Mandalay , Inle Lake , Yangon .... Book Now <a href="yangon-bagan-mandalay-inle-yangon.php">more »</a></p>
            </div>
          </li>
          <li class="last"> <a href="yangon-city-tour.php"><img src="images/shwedagon.jpg" width="260" height="112" alt="shwedagon" title="shwedagon"/></a>
            <div class="text">
              <h4><a href="yangon-city-tour.php">Yangon Daily City Tour - Yangon Sightseeing </a></h4>
                <h4>(Yangon city sightseeing tour)</h4>
              <p>Experience the city sights with bus tour,to explore Yangon's famous places Shwedagon pagoda,Sule pagoda,Kandawgyi lake,Bogyoke Aung San Museum.... <a href="yangon-city-tour.php">more »</a></p>
            </div>
          </li>
          <li> <a href="yangon-river-cruise-tour.php"><img src="newimages/cruise.jpg" width="260" height="112" alt="yangon river" title="yangon river"/></a>
            <div class="text">
              <h4> <a href="yangon-river-cruise-tour.php">Sunset Cruise in Yangon River</a></h4>
                <h4>(2-3 Hours Trip)</h4>
              <p>Enjoy Yangon's beautiful sunset with a river cruise,Botataung jetty-Yangon river-Botataung jetty.... <a href="yangon-river-cruise-tour.php">more »</a></p>
            </div>
          </li>
          <li class="last"> <a href="#"><img src="images/bago.jpg" width="260" height="112" alt="bago" title="bago"/></a>
            <div class="text">
              <h4><a href="mandalay-bagan-tour.php">Myanmar Bagan Tour only 288 USD per person </a> </h4>
              <p>Yangon - Bagan – Yangon ( By Coach 3 Nights / 4 Days ).... <a href="mandalay-bagan-tour.php">more »</a></p>
            </div>
          </li>
          <li> <a href="#"><img src="images/inlay.jpg" width="260" height="112" alt="inle lake tour" title="inle lake tour"/></a>
            <div class="text">
              <h4><a href="inle-tour.php">Myanmar Special Highlights Tour </a></h4>
              <p>Yangon - Bagan – Mandalay - Pindaya – Kalaw - Inle Lake –Ngapali Beach-Yangon (14 Days/13 Nights).....<a href="yangon-bagan–mandalay-pindaya–kalaw-inlelake–ngapalibeach-yangon.php">more »</a></p>
            </div>
          </li>
          <li class="last"> <a href="yangon-mandalay-mingun-bagan-pindaya-kalaw-inle-kyaikhtiyo-bago-yangon-14.php"><img src="images/mandalay.jpg" width="260" height="112" alt="wonders of myanmar" title="wonders of Myanmar"/></a>
            <div class="text">
              <h4><a href="yangon-mandalay-mingun-bagan-pindaya-kalaw-inle-kyaikhtiyo-bago-yangon-14.php">Wonders of Myanmar 14 Days Budget Tour</a> </h4>
              <p>Myanmar Land , Myanmar Beach , Myanmar Culture , if you want all those in one package ..This is best Myanmar Tour Package for you.....<a href="yangon-mandalay-mingun-bagan-pindaya-kalaw-inle-kyaikhtiyo-bago-yangon-14.php">more »</a></p>
            </div>
          </li>

        </ul>
      </div>
      <!--end latest-location-->
      <div class="clear"></div>
    </div>
    <!--end main column-->

      <!--side bar-->
      <?php include 'myanmar-travel-promotion-sidebar.php'; ?>



      <br class="clear"/>
      <br class="clear"/>
      <br class="clear"/>
      <br class="clear"/>
      <br class="clear"/>



      <br class="clear"/>
  </div>






  <!--end content-wrapper-->
   <div class="clear"></div>
  <!--end content section-->


<?php include 'footer.php'; ?>
