<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
      xml:lang="en-gb" lang="en-gb">
 
<head>
    <title>Myanmar Visa On Arrival Service</title>
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Myanmar Visa on Arrival Service " />
    <meta name="keyword" content="Myanmar Visa on Arrival">    <meta content="en" http-equiv="Content-Language">
    <meta name="generator" content="">
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->




    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

<!-- Content Section -->

<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="full-column" >
        <div class="full-column-header"></div>
        <!--end full column header-->
        <div class="full-column-body editable" id="untitled-region-7"><h2>Welcome To Myanmar (The Golden Land)</h2>

            <!-- <p><img alt="inle lake tour" class="image-right editable" src="images/images/inlay.jpg" title=
                    "Shwedagon pagoda" /></p> -->

            <h3>Myanmar Visa on Arrival Service</h3>


            <p><a href="reservation.php"><img alt="booking button" src=
                    "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                      "book now" /></a></p>

            <p>Our Green Myanmar Travel Provides Myanmar Visa On arrival Service with 30 USD per person  and 2 working weeks .</p>

            <p>Please feel free to contact us hello@green-myanmar.com</p>

            <p>We do provide speical Inle late boat rental service . please contact hello@green-myanmar.com</p>

            <p>Pre-arranged on-arrival Myanmar Visa </p>
            <p>Now “Pre-arranged on-arrival visa” is come to available again to all visitors to Myanmar. There have been a lot of changes on the on-arrival visa –at first pre-arranged, than fully functional on arrival visa, and finally back to “pre-arranged visa”. Since the immigration policies are changing very swiftly, you should always check with us for the latest developments before making decision on taking an “on-arrival” visa. Please note that this service is available only at two of international airports – Yangon (airport code RGN) and Mandalay (airport code MDL). Occasionally, authorities set up an on-arrival visa booth for those arriving by cruise to Yangon jetty. This on-arrival service is available to any nationality holding a valid passport with at least 6-month’s validity. The whole on-arrival visa process will take approximately two weeks and thus guests are suggested to send the required particulars as early as possible.However, it is highly recommended to obtain the normal Myanmar Visa at your home country before coming to Myanmar if you are not sure with your real background or profession as Myanmar authorities will be very serious and not reluctant to deport any visitors that might cause troubles in terms of national security. </p>

            </p >Myanmar on-arrival visa available port of entry, types, and requirements </p>
            <h4>1. Available Ports of Entry</h4>

            <p>a. Yangon International Airport (Airport code: RGN)</p>
            <p>b. Mandalay International Airport (Airport code: MDY)</p>
            <p>c. Yangon International Port (by sea) </p>

            <h4>2. Types of On-Arrival Visa</h4>

            <p>Types of on-arrival visa	Stay Validity	Visa Fees	Visa extension	Visa Exemption</p>
            <p>Tourist	28-day	US$30	Not Extendable	Nil </p>
            <p>Business	71-day	US$40	Extendable </p>
            <p>Social Visit	28-day	US$40	Extendable </p>
            <p>Transit	24-hour	US$18	Not Extendable </p>

            <h4>3. On-Arrival Visa requirements *Note that all 18 points are required.</h4>

            <p>Full Name </p>
            <p>Sex </p>
            <p>Nationality </p>
            <p>Date of birth </p>
            <p>Place of birth </p>
            <p>Passport number </p>
            <p>Passport date of issue </p>
            <p>Passport expiry date </p>
            <p>Father's Name </p>
            <p>Full valid and permanent address</p>
            <p>Occupation</p>
            <p>Religion</p>
            <p>Skin color</p>
            <p>Eye color</p>
            <p>Height (in centimeters)</p>
            <p>Race </p>
            <p>Flight details with PNR number (passenger name registration number) to/from Myanmar </p>
            <p>Passport size color photo (300px x 360px) or (2.54cm x 3.05cm) </p>

            <h4>4. How does it work? </h4>

            <p>On-arrival visa applicant sends the required particulars at least two weeks prior to arrival date. We will send the applications to the different ministries in new capital, Nay Pyi Taw. After receiving the permission, we will go to the airline to inform that the on-arrival visa has been approved so that the airline will allow you to board the flight bound to Myanmar. We will send a copy of on-arrival visa approval to you as the airline authorities may check with the documents they already received from us. Once you arrive to Yangon airport, go to the on-arrival visa counter before the immigration booths. The immigration officer will check the documents they have from us and the visa will be printed on your passport. Finally, you will go to the airport immigration booths for visa check (and queue if necessary like others). You’ll be welcomed by a company staff to take you to the hotel.. </p>



            <p><a href="reservation.php"><img alt="booking button" src=
                    "images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
                                                      "book now" /></a></p></div>
        <!--end column body-->
        <div class="full-column-footer"></div>
        <!--end full column footer-->

    </div>
    <!--end full column -->
    <div class="clear"></div>
</div>
<!--end content-wrapper-->
<div class="clear"></div>
<!--end content section-->

<?php include 'footer.php'; ?>
