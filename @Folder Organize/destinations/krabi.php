<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travels & tours is providing best yangon city tours, yangon sightseeing tours ,river cruise tours, myanmar tours, hotel reservation , ticketing in yangon, mandalay , myanmar and world wide">
    <meta name="keyword" content="Green, Myanmar , Green Myanmar, yangon city tour, Yangon sightseeing, Myanmar , myanmar travels tours, Rangoon city tour, ticketing, hotel booking yangon, mandalay, bagan">
    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <title>Green Myanmar Travel Agent</title>

    <style>
        .noshow { display:none; }
    </style>


    <!-- including Style sheets -->
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

<!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

<!-- END BuySellAds.com Ad Code -->
<!--[if IE 6]>
	<script src="js/DD_belatedPNG_0.0.8a.js"></script>
	<script type="text/javascript">
	DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
	</script>
	<![endif]-->
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
</head>

<body>
<div id="wrapper" > 
  
  <!--Begin Header Section-->
  
  <div id="header" >
    <h1 id="logo"><a href="index.html" title="Green Myanmar Travel & Tour"></a></h1>
    <div id="coin-slider">
      <ul>
          <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
          <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
          <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
          <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
          <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
      </ul>
    </div>
    <!-- end Slider --> 
    
    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
      <?php include 'menu.php'; ?>


      </li>
      <li><div style="margin-left:25px;">
      <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
      <input type="hidden" name="cmd" value="_xclick">
      <input type="hidden" name="business" value="mgphyozaw@gmail.com">
      <input type="hidden" name="lc" value="US">
      <input type="hidden" name="item_name" value="paypal">
      <input type="hidden" name="button_subtype" value="services">
      <input type="hidden" name="no_note" value="0">
      <input type="hidden" name="currency_code" value="USD">
      <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest">
      <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
      <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
      </form>
      </div>
      </li>
    </ul>
    <!--end class dropdown--> 
    
  </div>
  <!--end Header--> 
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Krabi</h2>
	<p>Krabi is a town on the west coast of southern Thailand at the mouth of the Krabi River where it empties in Phangnga Bay.The town is the capital of Krabi Province and Krabi district.</p>
	<p>Much of the province has been the seat of several national parks.The topmost destinations are Hat Noppharat Thara,Ao Nang,Railay,Ko Phi Phi National Park.Over 80 smaller islands such as Lanta islands,or Koh Lanta,Phi Phi islands - made famous as the ideal location for adventurers,yachtsmen,scuba-divers,snorkelers and day-trippers from Phuket.</p>
	<p>The Ko Lanta National Park,also in Krabi province,includes several coral-fringed islands with well-known diving sites.The largest island,Ko Lanta Yai,is the site of park headquarters,and is also home to Chao Le,or sea gypsies who sustain themselves largely through fishing.The islands are best visited during the non-monsoon months of October through April.
	Kayaking,sailing,birdwatching,snorkeling are also among top activities.In the interior,two predominantly mainland national parks,Khao Phanom Bencha and Than Bokk-horani,offer inland scenic attractions including waterfalls and caves,and opportunities for trekking,birdwatching and eco-tours.
	The rock faces at Railay Beach near Aonang have attracted climbers from all over the world and each year are the venue for the Rock and Fire Festival.There are several Rock Climbing schools at Railay beach.</p>
	<img src="imagesnew/krabi1.jpg" title="nature beauty of krabi beach" alt="nature beauty of krabi beach" class="image-right"  />
	<img src="imagesnew/krabi2.jpg" title="scuba diving at ko phi phi national park" alt="scuba diving at ko phi phi national park" class="image-right"  />
	<img src="imagesnew/krabi3.jpg" title="sunset-krabi beach" alt="sunset-krabi beach" class="image-right"  />
    <img src="imagesnew/krabi4.jpg" title="Tiger Cave Temple" alt="Tiger Cave Temple" class="image-right"  />
	<img src="imagesnew/krabi5.jpg" title="krabi-town" alt="krabi-town" class="image-right"  />
	<img src="imagesnew/krabi6.jpg" title="krabi-wat" alt="krabi-wat" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
  <div id="footer">
    <div id="footer-bg">
      <div id="footer-content">
        <div id="footer-contact">

        </div>
        <div id="footer-panel1" class="footer-nev">
          <h2 id="panel1">Quick Links</h2>
          <ul>
	    <li><a href="index.html">Home</a></li>
            <li><a href="destination.html">Destination</a></li>
            <li><a href="itnerary.html">Itnerary</a></li>
            <li><a href="photos.html">Photos &amp; Gallery</a></li>
            <li><a href="about.html">About Us</a></li>
          </ul>
        </div>
        <div id="footer-panel2" class="footer-nev">
          <h2 id="panel2">Poppula Activity</h2>
          <ul>
                      <li><a><b><i>Location:</i></b><br/>(69)Room,(102)Conner of Kannar Street and Botataung Zay Street,Botataung,Yangon,Myanmar.</a></li>
                      <li><a><b><i>Phone No:</i></b><br/>+95(1)420077655,+95(9)73128596.</a></li>
                      <li><a><b><i>Fax:</i></b><br/>+95 (1) 291887.</a></li>
                      <li><a><b><i>Email:</i></b><br/>hello@green-myanmar.com</a></li>
         </ul>
        </div>
        <div class="ft-coppy">
          <p>Copyright � 2013 <a href="http://www.evercrystal.com/">Evercrystal</a> All rights reserved.</p>
        </div>
      </div>
      <!--end footer content--> 
    </div>
    <!--end footer background--> 
  </div>
  <!--end footer--> 
  
</div>
<!--end wrapper-->

</body>
</html>
