<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Singapore Flyer</h2>
        <p>At a height of 165m,Singapore Flyer is the world's largest Giant Observation Wheel and also one of Asia's biggest tourist attractions.It promises more than just a view,but a panorama that captures Marina Bay's skyline with a glimpse of neighbouring Malaysia and Indonesia.</p>
	<p>Views from the Top of the flyer,you can see-
	
	    Collyer Quay,
	    Raffles Place,
	    The Fullerton Singapore,
	    Esplanade - Theatres On The Bay,
	    Merlion Park,
	    Empress Place,
	    Singapore River,
	    Padang,
	    Fort Canning Park,
	    The Float @ Marina Bay,
	    F1 Circuit,
	    The Helix Bridge,
	    Youth Olympic Park,
	    Marina Barrage,
	    Marina Bay Sands Integrated Resort,
	
	</p>
	<img src="imagesnew/singaporeflyer1.jpg" title="Night view of singapore flyer" alt="Night view of singapore flyer" class="image-right"  />
	<img src="imagesnew/singaporeflyer2.jpg" title="Singapore Flyer" alt="Singapore Flyer" class="image-right"  />
	<img src="imagesnew/singaporeflyer3.jpg" title="sunrise-Singapore Flyer" alt="sunrise-Singapore Flyer" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
