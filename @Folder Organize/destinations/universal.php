<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Universal studio</h2>
        <p>Universal Studios Singapore is 20 hectares (49 acres) in size,occupying the easternmost part of the 49-hectare (120-acre) Resorts World Sentosa.There are a total of 24 attractions,of which 18 are original or specially adapted for the park.The park consists of seven themed zones which surround a lagoon.Each zone is based on a blockbuster movie or a television show,featuring their own unique attractions,character appearances,dining and shopping areas.The park features the world's tallest pair of dueling roller coasters that are based on the popular television series,Battlestar Galactica;a castle from the world of Shrek and Monster Rock,a live musical show featuring the Universal Monsters.Universal Studios Singapore has over 30 restaurants and food carts,together with 20 unique retail stores and carts located around the park.Attractions premiering are marked "Premiere" and dining outlets that are certified Halal are marked with "Halal".</p>
	<p><b>Hollywood:</b>Themed after the real Hollywood Boulevard of the 1970s,the Hollywood zone is framed by dynamic architecture,palm trees and the famous Hollywood 'Walk of Fame'.<p>
	<p><b>New York</b>Themed after post-modern New York City that features sidewalks and classic landmarks that mimic its cityscape.The area is enhanced by neon lights and flanked by street facades that set the scenes for big city fun and also real-time movie production for most Hollywood blockbuster films.</p>
	<p><b>Sci-Fi City:</b>is themed after what cities and metropolis may look like in the future,equipped with space-age technologies and the ultimate modern conveniences.</p>
	<p><b>Ancient Egypt:</b>As the name suggests,the Ancient Egypt zone is themed after the rich Egyptian architecture and artifacts found during the Golden Age of Egyptian Exploration in the 1930s.</p>
	<p><b>The Lost World zone:</b>features two smaller themed regions:Jurassic Park and Waterworld.</p>
	<p><b>Far Far Away:</b>is based on the kingdom of the same name featured in DreamWorks Animations' Shrek.</p>
	<p><b>The Madagascar zone:</b>features a displaced cargo ship that houses the Madagascar:A Crate Adventure ride.It also features all of the characters from DreamWorks Animations' Madagascar.</p>
	<img src="imagesnew/universal1.jpg" title="Universal Studio" alt="Universal Studio" class="image-right"  />
	<img src="imagesnew/universal2.jpg" title="New York Library-Universal Studio" alt="New York Library-Universal Studio" class="image-right"  />
	<img src="imagesnew/universal3.jpg" title="Hollywood Boulevard-Universal Studio" alt="Hollywood Boulevard-Universal Studio" class="image-right"  />
	<img src="imagesnew/universal4.jpg" title="Battlestar Galactica dueling roller coasters" alt="Battlestar Galactica dueling roller coasters" class="image-right"  />
	<img src="imagesnew/universal5.jpg" title="Universal Globe" alt="Universal Globe" class="image-right"  />
	<img src="imagesnew/universal6.jpg" title="Ancient Egypt Statue" alt="Ancient Egypt Statue" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
