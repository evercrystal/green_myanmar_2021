<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
       <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="main-column" >
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body">
        <h2 id="column-header">Welcome To <span class="green">Singapore</span></h2>
        <p>Singapore is a city-state of Southeast Asia and one of the most enjoyable cities in Southeast Asia,too.Founded as a British trading colony in 1819,since independence it has become one of the world's most prosperous countries and boasts the world's busiest port.Combining the skyscrapers and subways of a modern,affluent city with a medley of Chinese,Malay and Indian influences and a tropical climate,with tasty food,good shopping and a vibrant night-life scene, this Garden City makes a great stopover or springboard into the region. 
        </p>
      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      
      <a href="singapore.html"><h4 id="singapore">Singapore</h4></a>
      <div class="destination-bg">
        <div class="tri">
          <ul>
            <li><a href="botanicgarden.html">Botanic Garden</a></li>
            <li><a href="jurong.html">Jurong Bird Park</a></li>
            <li><a href="nightsafari.html">Night Safari</a></li>
          </ul>
        </div>
        <!--end tri div-->
        <div class="tri">
          <ul>
            <li><a href="merlion.html">Merlion statue</a></li>
            <li><a href="orchardroad.html">Orchard Road</a></li>
            <li><a href="pulauubin.html">Pulau Ubin</a></li>
          </ul>
        </div>
        <!--end tri div-->
        
        <div class="tri-last">
          <ul>
            <li><a href="sentosa.html">Sentosa Island</a></li>
            <li><a href="singaporeflyer.html">Singapore Flyer</a></li>
            <li><a href="universal.html">Universal Studios</a></li>
          </ul>
        </div>
        <!--end tri-last div--> 
	</div>
	
	<a href="myanmar.html"><h4 id="myanmar">Myanmar</h4></a>
	      <div class="destination-bg">
	        <div class="tri">
	          <ul>
	            <li><a href="bagan.html">Bagan</a></li>
		        <li><a href="chaungtha.html">Chaungtha</a></li>
		        <li><a href="inle.html">Inle</a></li>
	          </ul>
	        </div>
	        <!--end tri div-->
	        <div class="tri">
	          <ul>
	            <li><a href="ngwesaung.html">Ngwesaung</a></li>
		        <li><a href="naypyitaw.html">Naypyidaw</a></li>
		        <li><a href="ngapali.html">Ngapali</a></li>
	          </ul>
	        </div>
	        <!--end tri div-->
	        
	        <div class="tri-last">
	          <ul>
		        <li><a href="mandalay.html">Mandalay</a></li>
		        <li><a href="taunggyi.html">Taunggyi</a></li>
		        <li><a href="yangon.html">Yangon</a></li>
	          </ul>
	        </div>
	        <!--end tri-last div--> 
	      </div>
	      
	<a href="thailand.html"><h4 id="thailand">Thailand</h4></a>
	      <div class="destination-bg">
	        <div class="tri">
	          <ul>
		        <li><a href="ayutthaya.html">Ayutthaya</a></li>
	            <li><a href="bangkok.html">Bangkok</a></li>
	            <li><a href="chiangmai.html">Chiang Mai</a></li>
	          </ul>
	        </div>
	        <!--end tri div-->
	        <div class="tri">
	          <ul>
	            <li><a href="huahin.html">Hua Hin</a></li>
	            <li><a href="kanchanaburi.html">Kanchanaburi</a></li>
	            <li><a href="kohsamui.html">Koh Samui</a></li>
	          </ul>
	        </div>
	        <!--end tri div-->
	        
	        <div class="tri-last">
	          <ul>
	            <li><a href="krabi.html">Krabi</a></li>
	            <li><a href="phuket.html">Phuket</a></li>
		        <li><a href="pattaya.html">Pattaya</a></li>
	          </ul>
	        </div>
	        <!--end tri-last div--> 
	      </div>
	      
      <div class="clear"></div>
    </div>
    <!--end main column --> 
    
    <!--begin  sliderbar-->
    <div id="sidebar" >
      <div id="newsletter">
        <h2>Sign up for our Newsletter</h2>
        <p>Be the first to know about events and special offers! </p>
        <form action="notify.php" method="post" id="notifyForm">
          <div id="letter">
            <input type="text" name="email" class="required email" value="Email"/>
            <button type="submit" name="signup">Sign Up</button>
          </div>
        </form>
      </div>
      <!--end newsletter -->
      
      <div class="tabs">
        <ul class="tabNavigation">
          <li><a href="#first">Tour Highlights</a></li>
          <li><a href="#second">Best Deal</a></li>
        </ul>
        <div id="first">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p style="color:yellow">Yangon-Thanlyin<br/>(One Day Tour)<p>
              The best tour program to explore Yangon's famous places,National Races Village,Midstream Pagoda(Kyauk Hmaw Wun,Thanlyin)....
              <p><a href="yangon_thanlyin_tour.html">read more &#187;</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" alt="tab-thumb" />
              <p> Aene aisun veneghfnatis risus ac libfg gfhero sagitti consectds detur de elit.<br />
                <a href="#">read more &#187;</a></p>
            </li>
            <li  class="last"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p>Consectds detur adipiscing elit.Aene aisun venend atisd ds ac libero <br />
                <a href="#"> read more &#187;</a></p>
            </li>
          </ul>
        </div>
        <div id="second">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p> Bisun ven gg  genatis risus ac libero sagi consectds detur adipiscing deradse. <br />
                <a href="#">read more &#187;</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" alt="tab-thumb" />
              <p> Aene aisun veneghfnatis risus ac libfg gfhero sagitti consectds detur de elit.<br />
                <a href="#">read more &#187;</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p> Reaisun venen co nsectds detur adipiscing. risus acs ded libero sagittis <br />
                <a href="#">read more &#187;</a></p>
            </li>
            <li  class="last"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p>Consectds detur adipiscing elit.Aene aisun venend atisd ds ac libero <br />
                <a href="#"> read more &#187;</a></p>
            </li>
          </ul>
        </div>
      </div>
      <div class="box"></div>
    </div>
    <!--end sidebar --> 
    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
