<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Ayutthaya</h2>
	<p>Ayutthaya(full name Phra Nakhon Si Ayutthaya,also spelled "Ayudhya") city is the capital of Ayutthaya province in Thailand.Located in the valley of the Chao Phraya River.The city was founded in 1350 by King U Thong,who went there to escape a smallpox outbreak in Lop Buri and proclaimed it the capital of his kingdom,often referred to as the Ayutthaya kingdom or Siam.Ayutthaya became the second Siamese capital after Sukhothai.Its remains, characterized by the prang (reliquary towers) and gigantic monasteries,give an idea of its past splendour.It is estimated that Ayutthaya by the year 1600 CE had a population of about 300,000,with the population perhaps reaching 1,000,000 around 1700 CE,making it one of the world's largest cities at that time.In 1767, the city was destroyed by the Burmese army,resulting in the collapse of the kingdom.The Ayutthaya historical park is the ruins of the former capital of the Kingdom of Siam.It is the site of mass murder,rape and enslavement of Siamese people and destruction of the Ayutthaya city,its art and buildings by the Burmese in 1767, which is recognized internationally as a UNESCO World Heritage Site.The city was refounded a few kilometers to the east.The city is sometimes called "Venice of the East".
	</p>
	<img src="imagesnew/ayutthaya3.jpg" alt="Ayutthaya Ancient Temple" title="Ayutthaya Ancient Temple" class="image-right"  />
	<img src="imagesnew/ayutthaya2.jpg" alt="Night view of Ayutthaya" title="Night view of Ayutthaya" class="image-right"  />
	<img src="imagesnew/ayutthaya1.jpg" alt="Ayutthaya Ancient Temple" title="Ayutthaya Ancient Temple" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
