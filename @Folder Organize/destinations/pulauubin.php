<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Pulau Ubin</h2>
        <p>Pulau Ubin is a small island situated in the north east of Singapore,to the west of Pulau Tekong.Granite quarrying supported a few thousand settlers on Pulau Ubin in the 1960s,but only about a hundred villagers live there today.It is one of the last rural areas to be found in Singapore,with an abundance of natural flora and fauna.</p>
	<p>Legend has it that Pulau Ubin was formed when three animals from Singapore (a frog,a pig and an elephant) challenged each other to a race to reach the shores of Johor.The animals that failed would turn to stone.All three came across many difficulties and were unable to reach the shores of Johor.Therefore,the elephant and pig together turned into Pulau Ubin whilst the frog became Pulau Sekudu or Frog Island.</p>
	<img src="imagesnew/pulauubin1.jpg" title="sunrise at pulau ubin" alt="sunrise at pulau ubin" class="image-right"  />
	<img src="imagesnew/pulauubin2.jpg" title="landscape view of pulau ubin" alt="landscape view of pulau ubin" class="image-right"  />
	<img src="imagesnew/pulauubin3.jpg" title="beautiful pulau ubin" alt="beautiful pulau ubin" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
