<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Taunggyi</h2>
	<p>Taunggyi is the capital of Shan State, Myanmar.the fifth largest city in Myanmar (behind Mawlamyine),and is at an elevation of 4,712 feet (1,436 m) above sea level.The name Taunggyi means "huge mountain" in the Burmese language,and is named after the ridge on the east of the city,part of the Shan Hills system,whose prominent high point is called Taung-chun or "The Spur".Locally this spur is popularly known as Phaya Taung.The ridge has a more prominent and more popular feature known as Chauk Talone,meaning the Craigs.Although within the Shan State,the Shans are not the predominant population of this city.The Inthas and the Pa-Ohs,who are also the original inhabitants of the Shan Plateau,form the most visible population.They however are culturally and linguistically different from the Shan.Recently there has been a flood of Chinese immigrants.Taunggyi lies within the Myelat region of Shan State.</p>
	<p>Taunggyi is best known for the annual Fire Balloon festival held for one week near the beginning of November each year.Other famous place of Taunggyi are Kakku pagoda,Phaungdaw Oo Pagoda,Inle(30 km to the south of Taunggyi),Pindaya and Sular Muni Pagoda.</p>
	<img src="imagesnew/taunggyi3.jpg" title="Kakku Pagoda" alt="Kakku Pagoda" class="image-right"  />
	<img src="imagesnew/taunggyi2.jpg" title="Taunggyi Lighting Festival" alt="Taunggyi Lighting Festival" class="image-right"  />
	<img src="imagesnew/taunggyi1.jpg" title="Sutaungpye Pagoda" alt="Sutaungpye Pagoda" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
