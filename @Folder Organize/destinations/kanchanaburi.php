<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Kanchanaburi</h2>
        <p>Kanchanaburi is a city located at the confluence of the rivers Kwai Noi and Kwai Yai.For most visitors the main sight of interest is the Bridge over the River Kwai,as the start of the infamous World War II Death Railway to Burma (now Myanmar),as well as the many associated museums.There is an increasingly thriving backpacker scene taking advantage of the chilled-out riverside vibe for those that need to get away from Bangkok.Kanchanaburi is also the gateway to the surrounding province of the same name.More foreign visitors are discovering why Thais know it as one of the most beautiful provinces in the country with its easily accessible waterfalls and national parks.</p>
	<p>Pretty much all the sights in Kanchanaburi itself are directly related to World War II.The museums are dusty and generally not worth it,except for the Thailand-Burma Railway Centre,which gives a good introduction of the Death Railway and its history.There are also two war cemeteries,the most moving of which is the Kanchanaburi War Cemetery.</p>
	<p>There are a lot of war cementery such as-Chongkai War Cemetery,Kanchanaburi War Cemetery,Thailand-Burma Railway Centre,World War II Museum and Art Gallery,JEATH War Museum.And you should visit the Tiger Temple,too.</p>
	<img src="imagesnew/kanchanaburi1.jpg" title="Erawan waterfall" alt="Erawan waterfall" class="image-right"  />
	<img src="imagesnew/kanchanaburi2.jpg" title="River Kwai bridge" alt="River Kwai bridge"class="image-right"  />
	<img src="imagesnew/kanchanaburi3.jpg" title="float-house-resort" alt="float-house-resort" class="image-right"  />
	<img src="imagesnew/kanchanaburi4.jpg" title="Kanchanaburi war cementery" alt="Kanchanaburi war cementery" class="image-right"  />
	<img src="imagesnew/kanchanaburi5.jpg" title="Erawan national park" alt="Erawan national park" class="image-right"  />
	<img src="imagesnew/kanchanaburi6.jpg" title="riding an elephant" alt="riding an elephant" class="image-right"  />
	
        </div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
