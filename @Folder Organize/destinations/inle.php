<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Inle Lake Tour</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel Agent provides travel packages and tours  to Inle Lake tour including Budget Inle Lake Tour, Inle Lake Hotel booking , Flight to Inle Lake, Inle Lake Coach. ">
    <meta name="keyword" content="Inle Lake Tour, Travel Package to Inle Lake">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Inle Lake</h2>
	<p>Inle Lake is a freshwater lake located in the Nyaungshwe Township of Taunggyi District of Shan State,part of Shan Hills in Myanmar(Burma).The watershed area for the lake lies to a large extent to the north and west of the lake.The lake drains through the Nam Pilu or Balu Chaung on its southern end.There is a hot spring on its northwestern shore.</p>
	<p>Although the lake is not large,it contains a number of endemic species.Over twenty species of snails and nine species of fish are found nowhere else in the world.Some of these,like the silver-blue scaleless Sawbwa barb,the crossbanded dwarf danio,and the Lake Inle danio,are of minor commercial importance for the aquarium trade.It hosts approximately 20,000 brown and black head migratory seagulls in November,December and January.</p>
	<p>The people of Inle Lake(called Intha),some 70,000 of them,live in four cities bordering the lake,in numerous small villages along the lake's shores,and on the lake itself.Most transportation on the lake is traditionally by small boats,or by somewhat larger boats fitted with single cylinder inboard diesel engines.Local fishermen are known for practicing a distinctive rowing style which involves standing at the stern on one leg and wrapping the other leg around the oar.This unique style evolved for the reason that the lake is covered by reeds and floating plants making it difficult to see above them while sitting. Standing provides the rower with a view beyond the reeds.However,the leg rowing style is only practiced by the men.Women row in the customary style,using the oar with their hands,sitting cross legged at the stern.</p>
	<p>A local market serves most common shopping needs and is held daily but the location of the event rotates through five different sites around the lake area,thus each of them hosting an itinerant market every fifth day.When held on the lake itself,trading is conducted from small boats.This 'floating-market' event tends to emphasize tourist trade much more than the other four.</p>
	<img src="imagesnew/inle3.jpg" title="floating market" alt="floating market" class="image-right"  />
	<img src="imagesnew/inle1.jpg" title="floating garden" alt="floating garden" class="image-right"  />
	<img src="imagesnew/inle2.jpg" title="Phaung Daw Oo Pagoda" alt="Phaung Daw Oo Pagoda" class="image-right"  />
        </div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
<?php include 'footer.php'; ?>
