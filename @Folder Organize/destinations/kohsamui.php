<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href= "https://plus.google.com/108886326770633962604/posts"id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Koshamui</h2>
	<p>Ko Samui or often simply Samui as it is referred to by locals,is an island of the east coast of the Kra Isthmus in Thailand.It is located close to the mainland town of Surat Thani in Surat Thani Province.It is Thailand's second largest island after Phuket,with an area of 228.7 km2 and a population of over 50,000 (2008) attracting 1.5 million tourists per year.Ko Samui has abundant natural resources, white sandy beaches, coral reefs and coconut trees.
	</p>
	<p>In ko samui,you can eat fresh and delicious sea food.If you are interested in best attractions and activities you should visit Bophut Beach.In Bophut Beach:</p>
	<p>Scuba Diving: is one of the best activities in Bophut Beach(Bophut is a beach village located on the northern coast of Ko Samui),as the ocean waters here are amazingly clear and are inhabited by colorful,bright and breathtaking various underwater creatures such as tropical coral fishes,hiding in wonderful anemones,and even small reef sharks attracting amazing and colorful underwater flora and fauna.</p>
	<p>Snorkeling: is another popular water activity in Bophut Beach.The ocean waters are so warm and clear that you do not even need to wear any extra equipment besides swimming suit,diving mask,swim fins and a snorkel.</p>
	<p>Kayaking: is another popular activity in Bophut Beach as being one of the most exciting ways of exploring astonishing nature,picturesque rocks and abundant tropical greenery surrounding this beautiful pristine beach.</p>
	<img src="imagesnew/kosamui1.jpg" title="sunrise at the ko samui" alt="sunrise at the ko samui" class="image-right"  />
	<img src="imagesnew/kosamui2.jpg" title="Landscape view of ko samui" alt="Landscape view of ko samui" class="image-right"  />
	<img src="imagesnew/kosamui3.jpg" title="blue water of ko samui" alt="blue water of ko samui" class="image-right"  />
    <img src="imagesnew/kosamui4.jpg" title="beach resort of ko samui" alt="beach resort of ko samui" class="image-right"  />
	<img src="imagesnew/kosamui5.jpg" title="snorkeling at bophut beach" alt="snorkeling at bophut beach" class="image-right"  />
	<img src="imagesnew/kosamui6.jpg" title="scuba diving at bophut beach" alt="scuba diving at bophut beach" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
