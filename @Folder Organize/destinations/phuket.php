<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Krabi</h2>
	<p>Phuket is Thailand's largest island.It is 48 km in length,21 km at its widest,and is in Southern Thailand,on the west-facing Andaman Sea coastline,suspended from the southern tip of Phang Nga Province by a pair of short but substantial road bridges.</p>
	<p>In phuket,you should visit AC's Phuket Fishing Park,Chalong Temple,Crocodile Farm,Phuket Aquarium,Phuket Butterfly Garden and Insect World,Phuket FantaSea(this is referred to as the Disneyland of South East Asia),Prom Thep Cape,Waterfalls.</p>
	<p>Swimming,snorkelling,diving,yachting,jet-skiing and parasailing are the most popular activities on the island.Other activities include Freediving,windsurfing,kite surfing,wakeboarding and deep sea fishing.Popular kite surfing locations include Nai Yang in summer and Chalong Bay in winter season.Sea Canoeing is a popular activity in the Phang Nga Bay,as many grottoes are only accessible by canoe.</p>
	<img src="imagesnew/phuket1.jpg" title="phuket-beach" alt="phuket-beach" class="image-right"  />
	<img src="imagesnew/phuket2.jpg" title="sunset-phuket" alt="sunset-phuket" class="image-right"  />
	<img src="imagesnew/phuket3.jpg" title="elephant show of phuket fantasea" alt="elephant show of phuket fantasea" class="image-right"  />
    <img src="imagesnew/phuket4.jpg" title="phuket aquarium" alt="phuket aquarium" class="image-right"  />
	<img src="imagesnew/phuket5.jpg" title="phuket butterfly garden and insect world" alt="phuket butterfly garden and insect world" class="image-right"  />
	<img src="imagesnew/phuket6.jpg" title="Chalong Temple" alt="Chalong Temple" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
<?php include 'footer.php'; ?>
