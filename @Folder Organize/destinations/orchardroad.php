<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Orchard Road</h2>
        <p>Orchard Road is a 2.2 kilometre-long street that is the retail and entertainment hub of Singapore.It is a major tourist attraction,in addition to being the most popular shopping enclave in the city-state.Often,the surrounding area is known simply as Orchard,partly because the MRT station that serves the vicinity is named Orchard.The Orchard Planning Area is one of 55 urban planning areas as specified by the Urban Redevelopment Authority and is a commercial district.It is part of the Central Region and Singapore's central business district,the Central Area.</p>
	<p>Orchard Road is the main shopping street of Singapore.Large trees still shade the road,providing a modicum of relief from the heat.Orchard Road has an extensive network of underpasses that connect many of the malls providing even more shelter from the blistering equatorial heat and,on occasion,rain.Weekends in the area are often packed with locals and visitors alike out to consume en masse.Only at the eastern Bras Basah end do the shopping malls peter out,with some fine colonial architecture and a few of Singapore's top museums to be found instead.The Christmas decorations along Orchard are mildly famous and entirely over the top,with reindeers cavorting through palm trees and gingerbread houses topped with fake snow.</p>
	<img src="imagesnew/orchardroad1.jpg" title="Landscape view of orchard road" alt="Landscape view of orchard road" class="image-right"  />
	<img src="imagesnew/orchardroad2.jpg" title="orchard road with merlion statue" alt="orchard road with merlion statue" class="image-right"  />
	<img src="imagesnew/orchardroad3.jpg" title="night view of orchard road" alt="night view of orchard road" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
<?php include 'footer.php'; ?>
