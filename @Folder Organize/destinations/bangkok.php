<?php include 'header.php'; ?>
<style>
    .noshow { display:none; }
</style>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Bangkok</h2>
	<p>Bangkok is the capital of Thailand and,with a population of over eleven million inhabitants,by far its largest city.Its high-rise buildings,heavy traffic congestion,intense heat and naughty nightlife may not immediately give you the best impression but don't let that mislead you.It is one of Asia's most cosmopolitan cities with magnificent temples and palaces,authentic canals,busy markets and a vibrant nightlife that has something for everyone. </p>
	<p>For years,it was only a small trading post at the banks of the Chao Phraya River, until King Rama I,the first monarch of the present Chakri dynasty,turned it into the capital of Siam in 1782,after the burning of Ayutthaya by Burmese invaders but they did not take over Ayutthaya.Since then,Bangkok has turned into a national treasure house and functions as Thailand's spiritual,cultural,political,commercial,educational and diplomatic centre.</p>
	<img src="imagesnew/bangkok3.jpg" title="bangkok at night" alt="bangkok at night" class="image-right"  />
	<img src="imagesnew/bangkok2.jpg" title="Grand Palace" alt="Grand Palace" class="image-right"  />
	<img src="imagesnew/bangkok1.jpg" title="Grand Palace at night" alt="Grand Palace at night" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
  <div id="footer">
    <div id="footer-bg">
      <div id="footer-content">
        <div id="footer-contact">

        </div>
        <div id="footer-panel1" class="footer-nev">
          <h2 id="panel1">Quick Links</h2>
          <ul>
	    <li><a href="index.html">Home</a></li>
            <li><a href="destination.html">Destination</a></li>
            <li><a href="itnerary.html">Itinerary</a></li>
            <li><a href="photos.html">Photos &amp; Gallery</a></li>
            <li><a href="about.html">About Us</a></li>
          </ul>
        </div>
        <div id="footer-panel2" class="footer-nev">
          <h2 id="panel2">Poppula Activity</h2>
            <ul>
          <ul>
                      <li><a><b><i>Location:</i></b><br/>(69)Room,(102)Conner of Kannar Street and Botataung Zay Street,Botataung,Yangon,Myanmar.</a></li>
                      <li><a><b><i>Phone No:</i></b><br/>+95(9)420077655,+95(9)73128596,.</a></li>
                      <li><a><b><i>Fax:</i></b><br/>+95 (1) 291887.</a></li>
                      <li><a><b><i>Email:</i></b><br/>hello@green-myanmar.com</a></li>
         </ul>
        </div>
        <div class="ft-coppy">
          <p>Copyright � 2017 <a href="http://www.evercrystal.com/">Evercrystal</a> All rights reserved.</p>
        </div>
      </div>
      <!--end footer content--> 
    </div>
    <!--end footer background--> 
  </div>
  <!--end footer--> 
  
</div>
<!--end wrapper-->

</body>
</html>
		
