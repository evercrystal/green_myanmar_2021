<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Ngwesaung</h2>
        <p>Ngwesaung is a beach resort located 48 km west of Pathein,Ayeyarwady Region,Myanmar.The beach is 5 hours' drive away from the principal city of Yangon,and an airport is in the works.Opened in March 2000,Ngwe Saung is newer than nearby and more popular Chaungtha Beach,and is designed to attract people with larger holiday budgets.</p>        
        <p>An unspoilt 15-km stretch of silvery sand and modern amenities have made Ngwesaung a popular destination for less budget conscious tourists from Lower Myanmar.Still Ngwesaung has much to develop.Its choices for nightlife activities remain paltry,even by local standards.Chaungtha and Ngapali beaches have greater choices of nighttime activities.At this point,a nearby elephant training camp is a main daytime attraction at Ngwesaung.</p>	
        <img src="imagesnew/ngwesaung3.jpg" title="sunset-ngwesaung beach" alt="sunset-ngwesaung beach" class="image-right"  />
	    <img src="imagesnew/ngwesaung1.jpg" title="pagoda at ngwesaung beach" alt="pagoda at ngwesaung beach" class="image-right"  />
	    <img src="imagesnew/ngwesaung2.jpg" title="swimming pool at ngwesaung beach" alt="swimming pool at ngwesaung beach" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
