<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Huahin</h2>
        <p>Hua Hin is a seaside resort city in Thailand.It is popular with Thais,having become quite fashionable as a weekend getaway spot for Bangkok residents,as well with foreigners and as an ex-pat retirement or holiday home location.Although developing rapidly,there is a commitment by the local and provincial authorities to avoid the kind of overheated blight that has affected other beach resorts in Thailand.</p> 
        <p>There is only one beach,Hua Hin Beach which runs from one side of Hua Hin to the other.Hua Hin Beach extends three kilometres southwards from a rocky headland until a southern headland where a Buddhist temple cling to the cliffs.Fine white sand,resort hotels and many water sports opportunities lend the resort its distinctive ambiance.At the south end of town,the hill of Khao Takiab interrupts the beach.On the other side (technically not Hua Hin anymore) is a lovely swimming beach with a view of Khao Takiab's South face.Just a little further on,Suan Son beach is owned by the Thai army,but open to the public.Notable for the tropical sea pines lining the shore.A little south of the Marriott,watch for tiny sand crabs digging industriously leaving odd patterns of tiny sand clods around their beachfront property.</p>
	<img src="imagesnew/huahin1.jpg" title="Landscape view of Huahin beach" alt="Landscape view of Huahin beach" class="image-right"  />
	<img src="imagesnew/huahin2.jpg" title="Blue water,white sand and green trees" alt="Blue water,white sand and green trees" class="image-right"  />
	<img src="imagesnew/huahin3.jpg" title="Huahin railway station"  alt="Huahin railway station"class="image-right"  />
	
	
        </div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
<?php include 'footer.php'; ?>
