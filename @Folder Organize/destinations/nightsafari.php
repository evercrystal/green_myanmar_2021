<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Night Safari</h2>
        <p>The Singapore Zoo is operated by Wildlife Reserves Singapore,who also manages the neighboring Night Safari and the Jurong Bird Park.There are about 315 species of animal in the zoo,of which some 16% are considered threatened species.The zoo attracts about 1.6 million visitors a year.Singapore Zoo is in the north of the island at Mandai,by the serene Seletar Reservoir,and is immensely proud of its orangutan-breeding program.By focusing on these magnificent creatures,whose plight symbolizes the disappearance of tropical rainforests around the world,the zoo hopes to nurture a respect and deep appreciation of nature in all who visit the zoo.It is not all dark at the world�s first nighttime zoo safari,with subtle moon glow lighting highlighting the raw beauty of 40 hectares of secondary jungle next to the Singapore Zoo.Thrills are guaranteed as you ride the open tram through the tropical jungle,with all its mysteries laid bare.The journey passes a large reservoir and snakes through habitats that are as identical to the natural environment as much as possible,from the Himalayan foothills to the Southeast Asian rainforest and Indian subcontinent.</p>
	<img src="imagesnew/nightsafari1.jpg" title="Entrance" alt="Entrance" class="image-right"  />
	<img src="imagesnew/nightsafari2.jpg" title="white tigers" alt="white tigers" class="image-right"  />
	<img src="imagesnew/nightsafari3.jpg" title="entertainment of night safari" alt="entertainment of night safari" class="image-right"  />
	<img src="imagesnew/nightsafari4.jpg" title="halloween horrors at night safari" alt="halloween horrors at night safari" class="image-right"  />
	<img src="imagesnew/nightsafari5.jpg" title="Leopard" alt="Leopard" class="image-right"  />
	<img src="imagesnew/nightsafari6.jpg" title="beautiful fishing cat" alt="beautiful fishing cat" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
