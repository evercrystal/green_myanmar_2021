<?php include 'header.php'; ?>

<!--Begin Header Section-->
  
  <div id="header" >
    <h1 id="logo"><a href="index.html" title="Green Myanmar Travel & Tour"></a></h1>
    <div id="coin-slider">
      <ul>
          <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
          <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
          <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
          <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
          <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
      </ul>
    </div>
    <!-- end Slider --> 
    
    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>



      </li>
      <li><div style="margin-left:25px;">
      <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
      <input type="hidden" name="cmd" value="_xclick">
      <input type="hidden" name="business" value="mgphyozaw@gmail.com">
      <input type="hidden" name="lc" value="US">
      <input type="hidden" name="item_name" value="paypal">
      <input type="hidden" name="button_subtype" value="services">
      <input type="hidden" name="no_note" value="0">
      <input type="hidden" name="currency_code" value="USD">
      <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest">
      <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
      <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
      </form>
      </div>
      </li>
    </ul>
    <!--end class dropdown--> 
    
  </div>
  <!--end Header--> 
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Botanic Garden</h2>
        <p>The Singapore Botanic Gardens is a 74-hectare(183-acre) botanical garden in Singapore.It is the only botanic garden in the world that opens from 5 a.m. to 12 midnight every single day of the year,and does not charge an admission fee,except for the National Orchid Garden.The garden is bordered by Holland Road and Napier Road to the south,Cluny Road to the east,Tyersall Avenue and Cluny Park Road to the west and Bukit Timah Road to the North.The linear distance between the northern and southern ends is around 2.5 km (1.6 mi).</p>
	<p>The National Orchid Garden is the main attraction within the Botanic Gardens.Located on the mid-western side of the Garden,the hilly three-hectare site has a collection of more than 1,000 species and 2,000 hybrids of orchids.Within the Orchid Garden there are a number of attractions such as the following:</p>
	<p>Burkill Hall and the VIP Orchid Garden: Burkill Hall is a colonial plantation bungalow built in 1886.It used to be the director's house and was named in honour of the only father and son pair to hold the post of Director of Singapore Botanic Gardens,Isaac and Humphrey Burkill.The ground level serves as an exhibition area,showcasing information on the different hybrids named after VIPs who have visited the garden.</p>
	<p>Orchidarium: A haven for serious orchids enthusiasts,the Orchidarium houses natural species in a tropical setting.</p>
	<p>Tan Hoon Siang Misthouse: Tan Hoon Siang was a descendant of Tan Tock Seng,who was a philanthropist and founder of the Tan Tock Seng Hospital.The misthouse contains a colourful collection of different hybrids.It also has a small collection of fragrant orchids like Vanda Mimi Plamer.</p>
	<p>Lady Yuen-Peng McNeice Bromeliad House: Named in honour of its sponsor,the Bromeliad House showcases plants from the Bromeliaceae family,which includes the pineapple.The unique collection of bromeliads on display was acquired from Shelldance Nursery in the United States in 1994.</p>
	<p>Coolhouse: The Coolhouse tries to recreate the environment of a tropical highland forest and showcases orchids that are normally only found in the tropical highland areas.</p>
	<p>Botnic garden consists of Rainforest,Evolution Garden,Ginger Garden,Botany Centre and Tanglin Gate and Jacob Ballas Children's Garden.</p>
	<img src="imagesnew/botanicgarden1.jpg" title="botanic-garden" alt="botanic-garden" class="image-right"  />
	<img src="imagesnew/botanicgarden2.jpg" title="saraca stream" alt="saraca stream" class="image-right"  />
	<img src="imagesnew/botanicgarden3.jpg" title="Evolution Garden" alt="Evolution Garden" class="image-right"  />
	<img src="imagesnew/botanicgarden4.jpg" title="orchid" alt="orchid" class="image-right"  />
	<img src="imagesnew/botanicgarden5.jpg" title="sun garden" alt="sun garden" class="image-right"  />
	<img src="imagesnew/botanicgarden6.jpg" title="symphony lake" alt="symphony lake" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
