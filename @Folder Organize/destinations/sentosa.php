<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Sentosa</h2>
        <p>Sentosa is a popular island resort in Singapore,visited by some five million people a year.Attractions include a 2 km (1.2 mi) long sheltered beach,Fort Siloso,two golf courses,two five-star hotels,and the Resorts World Sentosa,featuring the theme park Universal Studios Singapore.</p>
	<p>Sentosa offers a variety of attractions,museums and other facilities to provide a variety of experiences,recreation and entertainment to visitors.</p>
	<p><b>Tiger Sky Tower(also known as the Carlsberg Sky Tower):</b>is a free-standing observation tower.At a height of 110 m (360 ft) above ground and 131 m (430 ft) above sea level,it offers visitors a panoramic view of Sentosa,Singapore,and the Southern Islands.On a clear day,the view extends to parts of Malaysia and Indonesia.</p>
	<p><b>Butterfly Park and Insect Kingdom:</b>a landscape garden with over 15,000 live butterflies,representing more than fifty species.Housed in a cool outdoor conservatory,these butterflies range from the 25 millimetre (1 in) Eurema sari to the 150 mm (6 in) Papilio iswara.The Insect Kingdom houses some 3,000 species of rare insects from around the world,including a 160 mm Dynastes Hercules beetle.</p>
	<p><b>Merlion Statue:</b>A gigantic 37-metre tall replica of the Merlion,it houses two viewing galleries and a souvenir shop.The Merlion Statue played several parts in several performances at the famous Sentosa Musical Fountain,which included the Spirits Of Sentosa show,and the Magical Sentosa show.</p>
	<p><b>Underwater World and Dolphin Lagoon:</b>Underwater World is an oceanarium located on the western part of Sentosa. Opened in 1991, the living museum has more than 2,500 marine and fresh-water animals of 250 species from different regions of the world.The oceanarium is underground and has an 83 metre long travelator that moves visitors along a submerged glass-windowed tunnel from which they can look at an array of marine life including a coral reef,stringrays,moray eels,turtles,sharks,and other fishes.In the 'Dive-with-the-Sharks' program visitors can scuba dive in the large oceanarium,even if they are not scuba qualified.Qualified scuba divers can also "Dive-with-the-Dugong".The Underwater World also includes a Dolphin Lagoon which is home to some Indo-Pacific humpback dolphins,also known as the pink dolphins.</p>
	<p><b>Songs of the Sea:</b>designed by Yves Pepin,the Songs of the Sea show started on 26 March 2007,replacing the world-famous Magical Sentosa show at the 25-year old Sentosa Musical Fountain.The Malay Kampung by the Sea or more commonly known as a Kelong,is 120 m (390 ft) long while the rest of the equipment (water jets,water screens,lasers and projectors) is hidden at the back of the kelong.It features pyrotechnics displays,water jets,laser show and flame bursts a live cast and an open-air viewing gallery which can comfortably accommodate 2,500 visitors.The show runs twice nightly every evening.</p>
	<p><b>Sentosa Luge & Skyride:</b>features a self-steering,gravity-driven three-wheel cart.Originally from New Zealand,the non-motorised cart allows rider to speed down a hill over a course of 650 m ending at the Siloso Beach.At the end of the luge,there is the Skyride that can allow rider to see from a high view.It also can be boarded at the start of the Luge.</p>
	<p><b>Fort Siloso:</b>located in the west of the island,the guns of this preserved fort still stand.Fort Siloso was built by the British in 1880s to guard the narrow western entrance to Keppel Harbour.It was later modernised.The fort guarded the western approaches to Singapore during World War II.By 1939 it was armed with two 6-inch (150 mm) Mark2 guns and two rapid firing 12-pounder guns.Fort Siloso is now the only surviving coastal gun battery from the twelve such batteries that made up Fortress Singapore at the start of the war.The ammunition bunkers,barracks,tunnels,and gun emplacements of the fort are now open to visitors,as a military-themed attraction.Also on display is a collection of artillery guns dating from the 17th century to World War II.Life-sized replicas of British soldiers and other people were on display to depict lives at the fort in the past.There is also an exhibition with a large collection of photographs,documents and film clips.The fort served as the place of internment of the Singaporean political prisoner Chia Thye Poh in the period from 1989 to 1993.This attraction was once served by the Fort Siloso Monorail Station of the discontinued Sentosa Monorail,after the monorail closed in 2005,the station was repurposed to add on to this attraction.</p>
	<img src="imagesnew/sentosa1.jpg" title="Singapore Under Water World" alt="Singapore Under Water World" class="image-right"  />
	<img src="imagesnew/sentosa2.jpg" title="Fort siloso" alt="Fort siloso" class="image-right"  />
	<img src="imagesnew/sentosa3.jpg" title="Tiger Sky Tower" alt="Tiger Sky Tower" class="image-right"  />
	<img src="imagesnew/sentosa4.jpg" title="Merlion statue" alt="Merlion statue" class="image-right"  />
	<img src="imagesnew/sentosa5.jpg" title="Cable Car" alt="Cable Car" class="image-right"  />
	<img src="imagesnew/sentosa6.jpg" title="Sentosa beach" alt="Sentosa beach" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
