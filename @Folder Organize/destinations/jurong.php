<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Jurong Bird Park</h2>
        <p>Jurong Bird Park is a tourist attraction in Singapore managed by Wildlife Reserves Singapore.It is a landscaped park,built on the western slope of Jurong Hill.It is located within the Boon Lay Planning Area of the Jurong district and has an area of 202,000 square metres(50 acres).</p>
	<p><b>High Flyers Show:</b> Formerly called the "Birds n Buddies Show",this birdshow showcases the world's largest number of performing birds in a single act.Besides highlighting the antics of talented birds like the mimicking cockatoos,this show is also a window for visitors to the natural behaviour of birds like pelicans,flamingos and hornbills.</p>
	<p><b>Birds of Prey Show:</b>Visitors can watch birds of prey such as eagles,hawks and falcons,who will fly in aerial loops and soar above the treetops.Visitors will also learn about falconry as these birds are put through their actions in a simulated hunt.</p>
	<p><b>Wetlands:</b>The new exhibit will give visitors a more balanced eco-system display and hopefully will be able to provide a better understanding of how nature,the birds and men co-exist in this one world we call our home.Species here include shoebill,saddle-billed stork,and a few species of African fish.</p>
	<p><b>African Waterfall Aviary:</b>The African Waterfall Aviary is the world's largest walk-in aviary with more than 1,500 free-flying birds from over 50 species.Visitors may hop aboard the Panorail,the world's only monorail that runs through an aviary.Jurong Falls,which is located within the African Waterfall Aviary,is the world's tallest man-made waterfall in an aviary at 30 metres high.Species include Golden-breasted Starling,turacos,and the hoopoe.</p>
	<p><b>Flightless Birds:</b>in one corner of the zoo there is a section full of flightless birds.Ostriches,emus,rheas,and cassowaries are the residents of this exhibit.</p>
	<p><b>Southeast Asian Birds Aviary:</b>Visitors can view the largest collection of Southeast Asian birds,which has over 200 species.There are large,central walk-in aviary and peripheral aviaries that house the more delicate or territorial birds.A daily simulated mid-day thunderstorm is followed by a cool,light drizzle.Territorial species are kept in large cages,while species that can coexist with each other (fruit doves and pigeons being two examples) are left to fly free in the aviary.</p>
	<p><b>Lory Loft</b> covers 3,000 square metres (32,000 sq ft),is about 9 storeys high,and is the world's largest walk-in flight aviary for lories and lorikeets,with over 1,000 free-flying lories.The ambience is similar to that of a rainforest valley in tropical Northern Australia.Visitors can offer the lories a special nectar mix and the birds will flock to them.</p>
	<p><b>Penguin Coast houses</b> six species of penguins within 1,600 square metres (17,000 sq ft).Featuring a 21-metre (69 ft) tall Portuguese galleon facade designed to resemble a ship,the interior of Penguin Coast is constructed with timber beams and wooden flooring.Penguin Coast is home to the Humboldt,Rockhopper,Macaroni,Little and King Penguins which live in an indoor,climate-controlled den as well as an outdoor penguin enclosure showcasing Jackass penguins,one of the few species that are adapted to the tropics.Joining them are the Cape Shelducks and gulls.</p>
	<p><b>World of Darkness:</b> Asia's first nocturnal bird house features a system of reverse lighting,converting day to night and vice versa.On display are 60 birds from 17 species,like the Night Herons,Fish Owls,boobook owls and Snowy Owls.It is akin to a quiet nocturnal walk along a starlit jungle path,watching birds in their nocturnal surroundings and hearing them beckon.</p>
	<p><b>Pelican Cove:</b>Visitors can catch a glimpse of all 7 species of pelicans,including the endangered Dalmatian Pelican.There is a boardwalk,where visitors can stroll along and observe these birds.Visitors can also see the pelicans at the world's first underwater viewing gallery for pelicans,where the birds scoop for fish at feeding time.</p>
	<p><b>Lunch with the Birds:</b>Visitors can enjoy a beautiful view of the Flamingo Lake while they enjoy breakfast.</p>
	<img src="imagesnew/jurong1.jpg" title="flamingos at jurong bird park" alt="flamingos at jurong bird park" class="image-right"  />
	<img src="imagesnew/jurong2.jpg" title="wetland waterfall" alt="wetland waterfall" class="image-right"  />
	<img src="imagesnew/jurong3.jpg" title="beautiful yellow bird" alt="beautiful yellow bird" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
