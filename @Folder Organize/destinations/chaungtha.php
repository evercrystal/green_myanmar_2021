<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Chaungtha</h2>
        <p>Chaungtha is a village and beach resort located in Ayeyarwady Region,Myanmar.It is about 5 hours' drive away from Yangon.Chaungtha received its name from the small stream (chaung) which flowed at the western part of the village.In the Burmese language,Chaungtha means Pleasant Stream.</p> 
	<p>Chaungtha is renowned for its fresh and reasonably priced seafood.A major attraction is a small pagoda built on a limestone boulder at the southern end of the beach.Nearby fishing villages and tidal mangrove forests are also popular amongst the tourists.</p>
	<img src="imagesnew/chaungtha1.jpg" title="pagoda at chaungtha beach" alt="pagoda at chaungtha beach" class="image-right"  />
	<img src="imagesnew/chaungtha2.jpg" title="chaungtha sandy beach" alt="chaungtha sandy beach"  class="image-right"  />
	<img src="imagesnew/chaungtha3.jpg" title="swimming pool at chaungtha beach" alt="swimming pool at chaungtha beach" class="image-right"  />
	
	
        </div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
