<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
      <h2>Merlion Statue</h2>
        <p>The Merlion is a mythical creature with the head of a lion and the body of a fish,used as a mascot and national personification of Singapore.Its name combines "mer" meaning the sea and "lion".The fish body represents Singapore's origin as a fishing village when it was called Temasek,which means "sea town" in Javanese.The lion head represents Singapore's original name Singapura-meaning "lion city" or "kota singa".The symbol was designed by Alec Fraser-Brunner,a member of the Souvenir Committee and curator of the Van Kleef Aquarium,for the logo of the Singapore Tourism Board(STB) in use from 26 March 1964 to 1997and has been its trademarked symbol since 20 July 1966.Although the STB changed their logo in 1997,the STB Act continues to protect the Merlion symbol.Approval must be received from STB before it can be used.The Merlion appears frequently on STB-approved souvenirs.The merlion a mythical creature with the body of a fish and the head of a lion occurs in a number of different artistic traditions.Lions with fishtails can be found on Indian murals at Ajanta and Mathura,and on Etruscan coins of the Hellenistic period.Merlions, or "heraldic sea-lions",are an established element of Western heraldry,and have been used on the coat of arms of the cities of Portsmouth and Great Yarmouth in the United Kingdom;the City of Manila;and the East India Company.</p>
	<img src="imagesnew/merlion1.jpg" title="merlion statue at night" alt="merlion statue at night" class="image-right"  />
	<img src="imagesnew/merlion2.jpg" title="merlion statue" alt="merlion statue"class="image-right"  />
	<img src="imagesnew/merlion3.jpg" title="merlion 'the icon of singapore'" alt="merlion 'the icon of singapore'" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
<?php include 'footer.php'; ?>
