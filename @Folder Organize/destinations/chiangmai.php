<?php include 'header.php'; ?>


<!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Chiangmai</h2>
	<p>Chiang Mai is the hub of Northern Thailand.With a population of over 170,000 in the city proper (but more than 1 million in the metropolitan area),it is Thailand's fifth-largest city.Located on a plain at an elevation of 316 m,surrounded by mountains and lush countryside,it is much greener and quieter than the capital,and has a cosmopolitan air and a significant expat population,factors which have led many from Bangkok to settle permanently in this "Rose of the North".
        </p>
	<p>In chiangmai you should visit-Wat Chiang Mai,Chiang Mai City Arts and Cultural Center,Chiang Mai National Museum,Tribal Museum,Elephant Nature Park,Wat Phrathat Doi Suthep,Wat Umong,Wat Phra Singh,Wat Suan Dok,Chiang Mai Night Safari,Chiang Mai Zoo....etc.</p>
	<img src="imagesnew/chiangmai3.jpg" title="Chiangmai Aquarium Zoo" alt="Chiangmai Aquarium Zoo" class="image-right"  />
	<img src="imagesnew/chiangmai2.jpg" title="Nature beauty of Chiangmai" alt="Nature beauty of Chiangmai" class="image-right"  />
	<img src="imagesnew/chiangmai1.jpg" title="Chiangmai night safari" alt="Chiangmai night safari" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
