<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body">
        <h2>Pattaya</h2>
	<p>Pattaya is a seaside resort on the Eastern Gulf Coast of Thailand,about 150 km southeast of Bangkok.</p>
	<p>The three kilometres long Pattaya Beach runs along the city centre.The beach is full of life with hotels,restaurants,shopping malls all along the road facing the beach.The street south of Beach Road,Walking Street,comes alive with rock music as the night sets in.People throng the street to experience the night life,which is unique in all respects.There are hundreds of beer bars,go-go bars,discotheques around the area.Muay Thai,the Thai Boxing,an open arena in some of the beer bars are exciting to the hilt.</p>
	<p>Another beach in Pattaya,Jomtien Beach,more popular among family vacationers,is on the southern part of the city,and separated from the main Pattaya Bay by Buddha hill.Jomtien is more calm and serene compared to the crowded Pattaya Beach and a paradise for water sports enthusiasts.Jomtien is also popular for its amusement park and tower,to keep children entertained.The one km long,Wong Phrachan Beach,situated on the Northern part of the Pattaya Beach is a favourite among swimmers.</p>
	<p>You should visit Wong Phra Chan Beach,Wong Amat Beach,Naklua Beach,Ban Sukhawadee(Bodhisattva Kwan Yin),Buddha Hill,Chaloemphrakiat Park,Sanctuary of Truth(huge ancient-style wooden temple),Wat Yanasangwararam Woramahawihan,Mini Siam( one of the most famous model villages in the world),Nong Nooch Tropical Garden,Siriphon Orchid Farm,Three Kingdoms Theme Park(Chinese-style garden filled with pagodas),The Million-Year Stone Park and Crocodile Farm,Pattaya Elephant Village and Underwater World.</p>
	<p>Pattaya's main beaches are popular and busy places for activities.Some of these include banana-boat rides,jet-skiing,water-skiing,parasailing,windsurfing,and the like;anyone who just wants to enjoy a simple swim or a good splash around with a frisbee or rented inner tube will always find themselves with plenty of company as well.</p>
	<img src="imagesnew/pattaya1.jpg" title="pattaya beach" alt="pattaya beach" class="image-right"  />
	<img src="imagesnew/pattaya2.jpg" title="Sanctuary of Truth" alt="Sanctuary of Truth" class="image-right"  />
	<img src="imagesnew/pattaya3.jpg" title="night view of pattaya" alt="night view of pattaya" class="image-right"  />
    <img src="imagesnew/pattaya4.jpg" title="Mini Siam" alt="Mini Siam" class="image-right"  />
	<img src="imagesnew/pattaya5.jpg" title="pattaya underwater world" alt="pattaya underwater world" class="image-right"  />
	<img src="imagesnew/pattaya6.jpg" title="pattaya crocodile farm" alt="pattaya crocodile farm" class="image-right"  />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
<?php include 'footer.php'; ?>
