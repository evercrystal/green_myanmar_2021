<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon City Tour - Yangon Sightseeing - Yangon Highlights Full Day Yangon Sightseeing </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Experience the city sights with bus tour,to explore Yangon's famous places Shwedagon pagoda, Sule pagoda, Botahtaung Pagoda & Jetty, Kandawgyi lake, Bogyoke Aung San Museum, 6 storey Budda , Sule landmark , Yangon heritage">
    <meta name="keyword" content="Myanmar Travel Agent, Yangon Highlights, Yangon city tour, Yangon Sightseeing, Yangon explorer ,Shwedagon pagoda, Sule pagoda, Botahtaung Pagoda & Jetty, Kandawgyi lake, Bogyoke Aung San Museum, 6 storey Budda , Sule landmark , Yangon heritage">    <meta content="en" http-equiv="Content-Language">

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="https://www.facebook.com/pages/Green-Myanmar-CoLtd/493986973980789" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable">
         <a style="text-align: center" href="yangon-city-tour-reservation.php">Book Yangon one day city tour now</a>

          <h2>Yangon</h2>
	<p>Yangon(also known as Rangoon) is a former capital of Burma(Myanmar) and the capital of Yangon Region.Yangon is a combination of the two words (yan) and (koun),which mean "enemies" and "run out of" respectively.It is also translated as "End of Strife".Yangon,with a population of over four million,continues to be the country's largest city and the most important commercial centre.Although Yangon's infrastructure is undeveloped compared to those of other major cities in Southeast Asia,it has the largest number of colonial buildings in the region today.While many high-rise residential and commercial buildings have been constructed or renovated throughout downtown and Greater Yangon in the past two decades,most satellite towns that ring the city continue to be deeply impoverished.</p>
	<p>Interesting places in yangon are-Shwedagon Pagoda,Sule Pagoda,Botahtaung Pagoda,Kabar Aye Pagoda,Chauk Htat Gyi,Karaweik Hall,Bogyoke Aung San Market,National Museum,Myanmar Gems Museum,Bogyoke Aung San Museum,Koe Htat Gyi,Ngar Htat Gyi,Maelamu Pagoda,Kyauktawgyi,Kyaikkalo and Kyaikkalei,Allied War Cemetery,National Races Village,Kandawgyi Garden,Zoological Garden,China Town,Ah Lain Nga Sint Temple,Colonial Buildings in Yangon,Kyaikkasan Pagoda,Maha Wizaya Pagoda,People's Park and Square,Immanuel Baptist Church,St.Mary's Cathedral,Swe Taw Myat Pagoda and many other places.
	</p>
          <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
          <h2>Helpful information about Yangon</h2>
          <ul style="list-style: none">
              <li><a href="yangon-nightlife.php">Yangon Nightlife</a> </li>
              <li><a href="resturants-in-yangon.php">Resturants in Yangon</a> </li>
              <li><a href="attractions-in-yangon.php">Attractions in Yangon</a> </li>
              <li><a href="transportations-in-yangon.php">Transportation in Yangon</a> </li>

          </ul>


          <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
            <h2>Highlight Attractions in Yangon</h2>
          <img src="newimages/botataung.jpg" class="editable" alt="botataung" title="Botataung pagoda">
          <h5>1.Botahtaung Pagoda & Jetty</h5>
          <p>This is 2500-year old pagoda was originally called Kyaik-de-att,and was later named after the 1000 military generals who brought the Buddha hair relic from India to Myanmar.</p>
          <img src="newimages/sule.jpg" alt="sule" class="editable" title="Sule pagoda">
          <h5>2.Sule Pagoda</h5>
          <p>The pagoda is located right in the center of the city.In fact the pagoda itself is in the middle of a traffic circle.It is said to be over 2000 years old and to contain a hair relic of Buddha.</p>
          <img src="newimages/bogyokemarket.jpg" alt="bogyoke" class="editable" title="Bogyoke market">
          <h5>3.Bogyoke Market</h5>
          <p>Formerly known as Scott Market,this is the most famous local market in Yangon.The large colonial building was built in 1926.There are 1641 shops in the market and almost anything you can imagine is on sale.</p>
          <img src="newimages/nationalmuseum.jpg" alt="national" class="editable" title="National museum">
          <h5>4.National Museum</h5>
          <p>The five-story museum house a priceless collection of artifacts and items of historical significance.The highlight of the collection is the Sihasana(Lion Throne)which belonged to King Thibaw.</p>
          <img src="newimages/gabaraye.jpg" alt="gabaraye" class="editable" title="Gabaraye pagoda">
          <h5>5.Gabar Aye Pagoda</h5>
          <p>This is a new pagoda constructed in 1952,and was built for the sixth Buddhist synod,held from 1954 to 1956.The name means 'world peace' in English,and is known as the 'World Peace Pagoda'.</p>
          <img src="newimages/gem.jpg" alt="gems" class="editable" title="Gems museum">
          <h5>6.Gems Museum</h5>
          <p>It display very famous gem stones of Myanmar such as rubies,jades,sapphires an Pearls.It also houses the Gems Mart.</p>
          <img src="newimages/pagoda.jpg" alt="pagoda" class="editable" title="Chaukhtat Gyi pagoda">
          <h5>7.Chaukhtat Gyi Pagoda</h5>
          <p>Originally constructed in 1907,the reclining Buddha is 72 meters long and is the biggest reclining Buddha image in Myanmar.However,it suffered damage over the years,and reconstruction was started in 1957 and completed 1966.</p>
          <img src="newimages/bogyokemuseum.jpg" alt="bogyokemuseum" class="editable" title="Bogyoke museum">
          <h5>8.<a href="http://en.wikipedia.org/wiki/Bogyoke_Aung_San_Museum"> Bogyoke Museum </a></h5>
          <p>This was the house where General Aung San lived,with his wife,Daw Khin Kyi,and their three children.The 1920s house is still in original condition,and has many interesting items on display including Aung San's car,his library,photos and his famous suit.</p>
          <img src="newimages/kandawgyi.jpg" alt="kandawgyi" class="editable" title="Kandawgyi lake and Karaweik">
          <h5>9.<a href="http://karaweikpalace.com/">Karawait Palace</a>  & <a href="http://en.wikipedia.org/wiki/Kandawgyi_Lake">Kandawgyi Lake</a></h5>
          <p>This large lake is just north of downtown,and around it is a park and recreation area.It's a busy roads around it.The lake is best known for the karaweik,a huge concrete replica of a royal Burmese Barge.</p>
          <img src="newimages/shwedagon.jpg" alt="shwedagon" class="editable" title="Shwedagon pagoda">
          <h5>10. <a href="http://www.shwedagonpagoda.com/"> Shwedagon Pagoda </a></h5>
          <p>The largest and most important pagoda in Myanmar is the Shwe Dagon Pagoda.It rises almost 100 meters above the greer cityscape of Yangon.</p>

          <img src="newimages/thestrand.jpg" alt="shwedagon" class="editable" title="the strand hotel" width="250px" height="150px">
          <h5>11. <a href="http://www.hotelthestrand.com/" >The Strand Hotel </a></h5>
          <p>The Strand Hotel in Yangon is one of the most iconic hotels in Myanmar. Built in 1901, it remains as awe inspiring as it was in the early 20th Century.</p>

            <div style="clear:both"></div>
	<img src="imagesnew/yangon1.jpg" alt="Kandawgyi garden view with Shwedagon pagoda" class="image-right editable" title="Kandawgyi garden view with Shwedagon pagoda" />
	<img src="imagesnew/yangon4.jpg" alt="Sule pagoda" class="image-right editable" title="Sule pagoda" />
	<img src="imagesnew/yangon5.jpg" alt="Shwedagon pagoda" class="image-right editable" title="Shwedagon pagoda" />
	<img src="imagesnew/yangon6.jpg" alt="Yangon city hall" class="image-right editable" title="Yangon city hall" />
	<img src="imagesnew/yangon3.jpg" alt="Taukkyan War Cemetery" class="image-right editable" title="Taukkyan War Cemetery" />
	<img src="imagesnew/yangon2.jpg" alt="Karawake restaurants" class="image-right editable" title="Karawake restaurants" />
	</div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
