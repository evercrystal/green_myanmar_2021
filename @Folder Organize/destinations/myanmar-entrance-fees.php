<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Myanmar Entrance Fees - Myanmar Tourist Entrance Fees - Myanmar Entrance Zone Fees </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Myanmar Entrance Fees is collection of Myanmar Entrance Zone Fees, including Yangon Entrance Fees , Bagan Entrance Zone fees ,Mandalay Entrance Zone fees, , Inle entrances Zone fees and others">
    <meta name="keyword" content="Myanmar Entrance Fees , Myanmar Entrance Zone Fees, Yangon Entrance Fees , ShweDagon Pagoda Entrance fees, Botatung Pagoda Entrance Fees, Mandalay Entrance Fees , Bagan Entrances Fees , Inle Lake Entrance Fees ">    <meta content="en" http-equiv="Content-Language">

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>
<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>

    <!--end class dropdown-->

</div>
<!--end Header-->






<!--end Header-->

<!-- Content Section -->

<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="full-column" >
        <div class="full-column-header"></div>
        <!--end full column header-->
        <div class="full-column-body editable">
            <div id="main">
                <h2>Myanmar Entrance fees or zone feesfor the pagodas, museum and monasteries. </h2><br>

                <h5 style="color:white;font-size: 12px"><b>Update : August 2014</h5>


                <br />
                <br />
            </div>
            <div>
                <img src="newimages/botataung.jpg" class="editable" alt="botataung" title="Botataung pagoda" style="float: left;padding-right: 15px;padding-left: 20px">

                <img src="newimages/gabaraye.jpg" alt="gabaraye" class="editable" title="Gabaraye pagoda" style="float: left;padding-right: 15px">

                <img src="newimages/thestrand.jpg" alt="shwedagon" class="editable" title="the strand hotel" width="270px" height="165px">

            </div>
            <br />
            <br />
            <p style="text-align: center"><a href="yangon-city-tour-reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;" /></a>
                <!-- <a style="color:orange; float:right" href="yangon-one-day-city-tour.pdf">Download Yangon City Tour Itinerary</a> -->
            </p>
            <br />






            <div style="margin-left: 200px">
            <h4 style="color: orange">Yangon Entrance Zone fees </h4>
            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>8000 Kyats (EST 8 USD)</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>


                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Botatung Pagoda </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>3 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Chaukhtutgyi Pagoda ( 6 storage budda) </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Free entrance</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Chaukhtutgyi Pagoda ( 6 storage budda) </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Free entrance</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Sule Pagoda </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>2 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Chaukhtutgyi Pagoda ( 6 storage budda) </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Free entrance</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>


                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>National Museum </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Gems Museum </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>
                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Bogyoke Museum Museum </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>300 kyats</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>
            <br />
            <br />
            <br />



            <h4 style="color: orange">Syriam (Thanlyin) Entrance Zone fees </h4>
            <br />

            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Kyauktan Yele Pagoda (Midstream Pagoda) </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>2 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>
            <br />
            <br />
            <br />


            <h4 style="color: orange">Bago Entrance Zone fees </h4>
            <br />

            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Bago Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>10 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />


            <h4 style="color: orange">Bago Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Bago Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>10 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Pyay (Prome) Entrance Zone fees </h4>
            <br />

            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Pyay (Prome) Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>10 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>
            <br />
            <br />
            <br />

            <h4 style="color: orange">Pyay (Prome) Entrance Zone fees </h4>
            <br />

            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Pyay (Prome) Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>10 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Kayin State Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Kyaikhtiyo Pagoda (Golden Rock)</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>6000 kyats</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Bagan Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Bagan Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>15 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Archeological Museum </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>


            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Mandalay Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Mandalay Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>10 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Sagaing + Mingun  </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>3 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>


            </table>

            <br />
            <br />
            <br />


            <h4 style="color: orange">Monywa Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Moe Hnin Thanbuddhay Pagoda </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>3 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Mount Pho Win  </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>2 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b> Shwe Ba Mountain  </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>1 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>


            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Pyin Oo Lwin Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Paikchinmyaung Pagoda  </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>2 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Botanical Garden   </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>4 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Inle Lake Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Inle Lake Zone fees for the whole area </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>10 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Saghar</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Pindaya Cave</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />

            <h4 style="color: orange">Taunggyi Entrance Zone fees </h4>

            <br/>
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Kakku Pagodas</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>8 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Shan Museum </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>3 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />
            <h4 style="color: orange">Mrauk U Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Mrauk U Zone fees for the whole area</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Mahamuni Pagoda  </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Mrauk U Archeological Museum  </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>5 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>

            </table>

            <br />
            <br />
            <br />
            <h4 style="color: orange">Sittwe  Entrance Zone fees </h4>

            <br />
            <table border="1" style="width: 600px">

                <tr style="font-size: 14px;color: green;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Place </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Entrance fee / pax</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>Additional Information </b></td>

                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="100px" style="text-align: center;"><b>Buddhist Museum </b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b>3 USD</b></td>
                    <td height="15px" width="100px" style="text-align: center;"><b> </b></td>
                </tr>



            </table>

            <br />
            <br />
            <br />

            </div>
        <!--end column body-->
        <div class="full-column-footer"></div>
        <!--end full column footer-->

    </div>
    <!--end full column -->
    <div class="clear"></div>
</div>
<!--end content-wrapper-->
<div class="clear"></div>
<!--end content section-->

<?php include 'footer.php'; ?>

<a href="http://casino.us.org/review/coolcat-casino/">casino.us.org/review/coolcat-casino/</a>
