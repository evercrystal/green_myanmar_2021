<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Myanmar Unesco Tour - Myanmar Tour packages </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Best Myanmar Travel Agent provides  Myanmar Unesco tour to ease at arranging their tour in myanmar  .">
    <meta name="keyword" content="Myanmar Unesco Tour ,  Myanmar Travel, Myanmar Tour package, Myanmar Budget Tour, Bagan, Mandalay ,Tour for independent travellers >    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7"><h2>Myanmar Unesco Tour - Pyu Ancient Cities </h2>

<br />
                <br />

                <div>
                    <img src="newimages/unesco1.jpg" class="editable" alt="Myanmar Unesco Tour" title="Myanmar Unesco Tour" style="float: left;padding-right: 15px;padding-left: 20px;width: 270px;height: 165px">

                    <img src="newimages/unesco2.jpg" alt="gabaraye" class="editable" title="Myanmar Unesco Tour" style="float: left;padding-right: 15px" width="270px" height="165px">

                    <img src="newimages/unesco3.jpg" alt="shwedagon" class="editable" title="Myanmar Unesco Tour" width="270px" height="165px">

                </div>
                <br />

                <h2>Pyu Ancient Cities</h2>
                <br />
                <p>Pyu Ancient Cities includes the remains of three brick, walled and moated cities of Halin, Beikthano and Sri Ksetra located in vast irrigated landscapes in the dry zone of the Ayeyarwady (Irrawaddy) River basin. They reflect the Pyu Kingdoms that flourished  for over 1,000 years between 200 B.C and 900 A.D. The three cities are partly excavated archaeological sites. Remains include excavated palace citadels, burial grounds and early industrial production sites, as well as monumental brick Buddhist stupas, partly standing walls and water management features – some still in use -  that underpinned the organized intensive agriculture. </p>


                <h3 style="color: orange"> If you want to inquire more about Myanmar World heritage tour , please contact hello@green-myanmar.com</h3>
                ref : <a href="http://whc.unesco.org/en/list/1444/">Myanmar World Heritage</a>




            <p><a href="reservation.php"><img alt="booking button" src=
                    "images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
                                                      "book now" /></a></p></div>
        <!--end column body-->
        <div class="full-column-footer"></div>
        <!--end full column footer-->

    </div>
    <!--end full column -->
    <div class="clear"></div>
</div>
<!--end content-wrapper-->
<div class="clear"></div>
<!--end content section-->

<?php include 'footer.php'; ?>
