<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon- Mandalay- Amarapura- Sagaing- Mingun- Yangon</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon-Bagan-Mandalay-Inle Lake-Yangon (7Days/6Nights) Packages is complete exploring Myanmar with amazing experience .">
    <meta name="keyword" content="Yangon,Bagan,Mandalay, Inle Lake,Yangon,Myanmar Tour Packages,(7Days/6Nights)">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7"><h3>Yangon-Mandalay-Amarapura-Sagaing-Mingun-Yangon(10Days/9Nights)
            </h3>



                <br />
                <br />

<h5 style="color: orange">Day 01 Arrive to Yangon                                                                                                         ( - /- / - )<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Arrive Yangon by morning/afternoon flight. You will be welcomed and transfer to the hotel. Have a short rest at the hotel. If time permits, start sightseeing with highlights to;</p>
<p>•Botahtaung Pagoda</p>
<p>•Chauk Htat Gyi Pagoda </p>
<p>•Sule landmark and colonial buildings</p>
<p>•Shwedagon Pagoda</p>

<h5>Overnight in Yangon.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/shwedagon pagoda.jpg" style="width:300px;margin-right:10px" class="editable" alt="shwedagon pagoda" titlx="shwedagon pagoda">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 02 Fly to Mandalay, Sagaing + Amarapura full day sightseeing<h5>
<small><p>After breakfast at the hotel, Transfer to the airport to take the flight to Mandalay. Arrive Mandalay and then start sightseeings to Sagaing and Amarapura with highlights to;</p>
</small>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>•Mahagandayone Monastery</p>
<p>•KaungHmuDaw Pagoda</p>
<p>•Sagaing Hill</p>
<p>•Uminthonese’ Pagoda</p>
<p>•SoonU Ponya shin</p>
<p>•Evening, enjoys sunset at U Bein’s bridge </p>

<h5>Overnight in Mandaly.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/at-Sagaing.jpg" style="width:300px;margin-right:10px" class="editable" alt="at-Sagaing" titlx="at-Sagaing">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange"> Day 03 Mingun- Mandalay full day sightseeing<h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>After Breakfast at Hotel, start excursion to Mingun by boat and visit; </p>
<p>•   Mingun Pagoda</p>
<p>•   Mingun bell</p>
<p>•   Myatheintan Pagoda</p>
<p>Then drive back to Mandalay. Highlight of Mandalay include;</p>
<p>•   Zay Cho Market</p>
<p>•   Mahamuni Pagoda</p>
<p>•   Golden Palace Monastery</p>
<p>•   Kuthodaw Pagoda</p>
<p>•   Enjoys sunset on Mandalay Hill</p> 
 
<h5>Overnight in Mandalay.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/mingun-2.jpg" style="width:300px;margin-right:10px" class="editable" alt="mingun-2" titlx="mingun-2">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 04 Mandalay-Bagan(by minibus) , Bagan Sightseeing <h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at hotel. Drive to Bagan(about 4 hours). Arrive to Bagan then transfer to hotel for check in. Take a rest at Hotel. 
Afternoon, sightseeing around Bagan by e-bike or Horsecart by enjoying Bagan amazing sunset.</p> 

<h5>Overnight in Bagan.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/bagan-horsecart.jpg" style="width:300px;margin-right:10px" class="editable" alt="bagan-horsecart" titlx="bagan-horsecart">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 05 Bagan fullday sightseeing <h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel, continue sightseeing around Bagan by private car. Highlights include;</p>

<p>•   Nyaung Oo Market</p>
<p>•   Shwezigon Pagoda</p>
<p>•   Ananda temple</p>
<p>•   Sulamini temple</p>
<p>•   Htilominlo Pagoda; </p>
<p>•   Dhamayangyi</p>
<p>•   late evening enjoy the sunset on top of the Shwesandaw pagoda or Bu pagoda.</p>


<h5>Overnight in Bagan.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/sunrise-in-bagan1.jpg" style="width:300px;margin-right:10px" class="editable" alt="sunrise-in-bagan1" titlx="sunrise-in-bagan1">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 06 Fly to Heho.  Pindaya-Kalaw sightseeing <h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel. Transfer to the airport to take the flight to Heho. Upon arrival at Heho, proceed to Pindaya, famous Botoloke Lake. Then drive to Kalaw. Highlights include</p>
<p>•Shwe U Min Pagoda</p>
<p>•Hnee Buddha Image</p>

<h5>Overnight in Kalaw.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/pindaya-caves.jpg" style="width:300px;margin-right:10px" class="editable" alt="pindaya-caves" titlx="pindaya-caves">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 07 Kalaw- Inle, Inle sightseeing <h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>After breakfast visit Kalaw Market then proceed to the town of Nyaungshwe, a town on the edge of the lake Inle. Then transfer to hotel and start sightseeing in Inle Lake by boat. Highlights include</p>

<p>•Phaung Daw Oo Pagoda </p>
<p>•Nga-phe Chaung Monastery </p><br>
<p>Then, head up to the Red Mountain Estate, vineyard in the hills to the east of Inle Lake for some wine tasting. One of only two vineyards in Myanmar, this definitely breaks the mould of your day-to-day Myanmar experience, making it suddenly feel as if you have been transplanted to the south of France. You can try the tasting menu for 2000 kyats (featuring wines from four different types of grape) whilst taking in the stunning sunset views over the lake.</p>

<h5>Overnight in Inle.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/Inle Lake-2.jpg" style="width:300px;margin-right:10px" class="editable" alt="Inle Lake-2" titlx="Inle Lake-2">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day  08  Indein - Inle Lake sightseeing, Back to Yangon (by VIP Express)<h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel. Continue sightseeing in Inle Lake , Highlight sightseeing of Inle Lake includes </p>
<p>•   Indein</p>
<p>•   Ywama village</p>

<h5>Overnight on Coach to Yangon. </h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/inle-3.jpg" style="width:300px;margin-right:10px" class="editable" alt="inle-3" titlx="inle-3">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day  9 Yangon sightseeing<h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Early arrive to Yangon around 5:30am and transfer to the hotel. Take a rest at Hotel.</p>
<p>Then take Yangon circular Train to experience daily life of Yangon . Then, visit to Bogyoke Market for shopping and China town and Colonial buildings for strolling. </p>

<h5>Overnight in Yangon.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/circular train.jpg" style="width:300px;margin-right:10px" class="editable" alt="circular train" titlx="circular train">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: orange">Day 10 Yangon Airport transfer<h5>
<small>
<p>Free time until transfer to Yangon International Airport for Departure. </p>


<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: red">Service Include : </h5><p>  Transfers and Sightseeing by private air-conditioned vehicles </p>
<p>   Accommodation 8 Nights with breakfast from hotel</p>
<p>   Mingun boat</p>
<p>   Horse cart in Inwa</p>
<p>   E-bike or horse cart  in Bagan</p>
<p>   Circular train ticket in Yangon</p>
<p>   VIP bus tickets as per program(Mandalay – Bagan, Inle-Yangon,)</p>
<p>   Domestic Air ticket as per program(Yangon- Mandalay, Nyaung U- Heho)</p>
<p>   English Speaking Station Guide service </p>
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: red">Services Exclude : </h5><p> Meals ( Lunch / Dinner )</p>
<p>   The entry fees for sightseeing places</p>
<p>   Drinks, Beverages, Tips,  personal expenses</p>
<p>   International Airport Tax ( 10 US$ per person in Yangon ) </p>
<p>   Any other services not mentioned in the “Services Include:” section as above</p>

<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h3>Cost</h3> 
<h4>For 2 pax, US$1403 per person</h4>
<h4>For 10pax, US$980 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>    

                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                          "book now" /></a></p>

            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>


