<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Inle Lake Day Tour</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel Agent provides travel to Inle Lake tour including Budget Inle Lake Tour, Inle Lake Hotel booking , Flight to Inle Lake, Inle Lake Coach. ">
    <meta name="keyword" content="Inle Lake Tour, Travel Package to Inle Lake">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7"><h2>Welcome To Myanmar (The Golden Land)</h2>

<p><img alt="inle lake tour" class="image-right editable" src="images/images/inlay.jpg" title=
"Shwedagon pagoda" /></p>

<h3>Inle Lake Tour</h3>

<h3>Inle Lake Festival will be held from 5th to 22nd October</h3>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>

<p>Our Green Myanmar Travel Provides Special offers tour packages , hotel reservation , inle lake boat rental service for Inle lake festival .</p>
<p> <b>8:00am- 12:00am</b></p>

<p>After breakfast at hotel, start boat trip to Indein- a small village located at the south western bank of the lake and famous for its ruined clusters of pagodas dated back to 16th century. The tranquility, peaceful ambience and ruins overgrown with bushes would bring you to centuries back. One can also have the chance to chat with villagers during the sightseeing time.</p>


          <p> <b>12:00am- 1:00pm</b></p>

<p>-Have a lunch at local restaurant.</p>
          <p> <b>1:00pm-6:00pm</b></p>
            <p>-Then proceed to Phaung Daw Oo pagoda</p>
          <p> -Visit to Ngaphechaung Monastery (formerly also known as jumping cat monastery for trained jumping cats).</p>
          <p> -Visit Ywa-Ma village which is well known for its gold and silver ware.</p>
          <p> -Visit Paw Khon village to see silk weaving, lotus petal robe weaving</p>
          <p>-Visit floating garden and back to your Hotel.

<p>Please feel free to contact us hello@green-myanmar.com</p>

<p>We do provide special Inle late boat rental service . please contact hello@green-myanmar.com</p>


          <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
              <h5 style="color:red">Services Include:</h5><br>
          <p>•	Transfer and Sightseeing by boat</p>
          <p>•	English Speaking  Guide</p>
          <p>•	Entry fees for sightseeing places</p>
          <p>•	Drinking water 1 bottle</p>
          <p>•	Boat Ticket</p>
          <p>•	Lunch</p>
          <h5 style="color:red">Services Exclude:</h5><br>
          <p>•	Drinks, Beverages, Tips, Personal Expenses</p>
          <p>•	Inle Zone Fee</p>
          <p>•	Any other services not mentioned in the “Services Include” section a above</p>

          <h5 style="color:red">Remarks: Minimum travelers for a departure – 2 persons</h5><br>


          <p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
"book now" /></a></p></div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
