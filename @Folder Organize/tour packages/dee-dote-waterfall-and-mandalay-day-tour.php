<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Dee Dote Waterfall and Mandalay Day Tour</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel agent provides Budget Myanmar Bagan (Pagan)  Mandalay tour package. ">
    <meta name="keyword" content="Budget Bagan Mandalay , Mandalay Tour package ,Bagan Tour Package">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />


    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

<style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />

        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">

                     

                    <h1 style="text-align: center;"><strong>Dee Dote Waterfall and Mandalay Day Tour</strong></h1>
<h1 style="text-align: center;"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.MD1001</h1>

</br>
</br>
                  

<div>
                    <img src="newimages/maldalay-1.jpg" class="editable" alt="botataung" title="Bagan Sightseeing Tour" style="float: left;padding-right: 15px;padding-left: 20px" width="270px" height="165px">

                    <img src="newimages/maldalay-2.jpg" alt="gabaraye" class="editable" title="bagan day tour" style="float: left;padding-right: 15px" width="270px" height="165px">

                    <img src="newimages/mandalay-shwe-kyaung.jpg" alt="shwedagon" class="editable" title="bagan Sightseeing tour" width="270px" height="165px">

                </div>

                    <br />
                     <p>Pick up at your Hotel in Mandalay and start sightseeing to;</p>
                     <h5 style="color: orange">Mahamyatmuni Pagoda</h5>
                     <h5 style="color: orange">Kuthodaw Pagoda</h5>
                     <h5 style="color: orange">Mandalay Shwe Kyaung</h5>


<p>This monastery is famous for its beautiful wood-carvings. This wooden monastery is carved all over with motifs and mythical creatures. It was once entirely covered in thick gold, but only the interior gold remains due to harshness of the tropical weather. Some of the outer carvings have been prised off and removed being damaged by the weather. Inside the monastery, there are 10 jataka scenes taken from the Buddha's life. These are still in a very good condition
</p>

                    <p>Then drive to Dee Dote layered waterfall –</p>
                
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: orange">Dee Dote layered waterfall</h5>
<div><p>Dee Dote layered waterfall with crystal clear, bluish water (one hour drive from Mandalay). Dee Doke waterfall is located at the outskirts of Mandalay city. It takes about 1 hours driving distance (Car/motor) from Mandalay city. From Mandalay to Pyin Oo Lwin highway, near Ohn Chaw town use the road which leads to Ye Ywar Hydro Power Dam. There is only one highway to go there. Keep driving until you see the Sign “To Dee Doke Waterfall”.
    </p>
<br />
<p>-After that drive back to Mandalay and visit Mandalay Hill where you can enjoy amazing sunset on there.Transfer to your Hotel in Mandalay and end of your service. Enjoy!
</p>

</div>
 <h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: red">Service Include : </h5>
<p>Transfer and Sightseeing by private air-conditioned vehicles</p>
<p>English Speaking  Guide</p>  
<p>Drinking water 1 bottle</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: red">Services Exclude : </h5>
<p>Meals(Lunch/Dinner)</p>
<p>Drinks, Beverages, Tips, Personal Expenses<p/>
<p>Mandalay Zone fees</p>
<p>Camera fees, any other services not mentioned in the “Services Include” section a above</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>


<h3>Cost</h3> 
<h4>For 2 pax, US$84 per person</h4>
<h4>For 10pax, US$32 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>


                    <a href="reservation.php"><img src="images/booking-button.jpg" class="editable" alt="booking button" title="book now" style="margin: 5px 0px 5px 5px;"/></a>
            </div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

<?php include 'footer.php'; ?>
