<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Myanmar Golden Rock Day Return Tour - Golden Rock Bago Day Return Tour </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Myanamr Golden Rock Day Return Tour - Golden Rock Bago Day Return Tour ">
    <meta name="keyword" content="Myanamr Golden Rock Day Return Tour ">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="full-column" >
        <div class="full-column-header"></div>
        <!--end full column header-->
        <div class="full-column-body editable">
            <div id="main">
                <h2></h2><br>

                <h5 style="color:white"><b>Myanmar Golden Rock Day Return Tour - Golden Rock Bago Day Return Tour - contact hello@green-myanmar.com for more information</h5>

                <br />
                <br />
            </div>
            <div>
                <img src="imagesnew/goldenrock1.jpeg" class="editable" alt="botataung" title="Myanmar Golden Rock Day Return Tour" style="float: left;padding-right: 15px;padding-left: 20px">

                <img src="imagesnew/goldenrock2.jpeg" alt="gabaraye" class="editable" title="Myanmar Golden Rock Day Return Tour" style="float: left;padding-right: 15px">

                <img src="imagesnew/goldenrock3.jpg" alt="shwedagon" class="editable" title="Myanmar Golden Rock Day Return Tour" width="270px" height="185px">

            </div>
            <br />
            <br />
            <p style="text-align: center"><a href="reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;" /></a>
            </p>
            <br />
            <h4 style="color: orange">Morning Tour</h4>
            <br />
            <p>04:00 am   >>Drive to the camp Kimpun (about 5 hours), from where the uphill motor road to hermit’s hill starts.  </p>
        </p>
            <p>Arrive Kimpun camp and take the uphill pick up truck about an hour to reach to the top of the mountain where you can see the Golden Rock(Kyaikhtiyo)- a large
                boulder positioned well balanced on the cliff of that mountain - seems to be defying the force of gravity from the spur of the cliff. Enjoy sunset view over deep forested mountain.
            </p>

            <p>En-route visit Bago; Shwethalyaung reclining Buddha Image – originally built in 994 AD and is a masterpiece of Myanmar stucco and symmetry. Arrive Yangon. </p>


            <p>19:00 pm   >> End of Tour & back to Hotel,Yangon. Transfer back to Hotel.</p>

            <br />
            <br />




            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
            <div>
                <h5 style="color:red">Services Include:</h5><br>
                <p style="color:white"> -  Transfers and Sightseeing by private air-conditioned vehicles </p>
                <p style="color:white"> -  Local English Speaking  Guide </p>
                <p style="color:white"> -  The entry fees for sightseeing places </p>
                <p style="color:white"> -  Truck from camp Kimpun camp to Golden rock , return</p>
                <p style="color:white"> -  Drinking water 1 bottle </p>
                <br>

                <h5 style="color:red">Services Exclude:</h5><br>


                <p style="color:white"> - Drinks, Beverages, Tips,  personal expenses </p>
                <p style="color:white"> - Meal </p>

                <p></p>

                <p style="color:white"> -  Any other services not mentioned in the “Services Include:” section as above</p>
            </div>


            <h4 style="color: orange">Cost per person: </h4>
            <p style="color:white">  1 pax  - USD $ 350 </p>
            <p style="color:white">  2 pax - USD $ 182</p>
            <p style="color:white">  3 pax - contact hello@green-myanmar.com</p>
            <p style="color:white">  15 & above (1 FOC) -  contact hello@green-myanmar.com <br/>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>


            <img src="imagesnew/goldenrock4.jpg" class="editable" alt="botataung" title="Botataung pagoda">
            <h5>1.Golden Rock History</h5>
            <p>The Golden Rock (Kyaik-htiyo or Kyaiktiyo), perched atop a cliff near Yangon, is one of the most sacred sites in Burma. The great boulder precariously balances on the edge of a cliff and is topped by a small stupa. An endless stream of pilgrims come to admire the sight and add squares of gold leaf to its surface.</p>

            <a href="reservation.php"><img src="images/booking-button.jpg" class="editable" alt="booking button" title="book now" style="margin: 5px 0px 5px 5px;"/></a>

        </div>
        <!--end column body-->
        <div class="full-column-footer"></div>
        <!--end full column footer-->

    </div>
    <!--end full column -->
    <div class="clear"></div>
</div>
<!--end content-wrapper-->
<div class="clear"></div>
<!--end content section-->

<?php include 'footer.php'; ?>
