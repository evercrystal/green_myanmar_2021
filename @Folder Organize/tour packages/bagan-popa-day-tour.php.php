<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel Agent provides budget and best Mandalay City Tour for Mandalay Sightseeing. It includes Ancient cities of Upper Myanmar: Innwa, Amarapura, Sagaing.">
    <meta name="keyword" content="Green Myanmar Travel Agent, Mandalay City Tour , Mandalay Sightseeing , Innwa, Amarapura, Sagaing">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <title>Bagan + Popa Day Tour</title>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

<style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>
    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">
                <h2>Bagan + Popa Day Tour</h2><br>

                <h5 style="color:green"><b>Lowest Price Starts from 27 USD for 10 pax - contact hello@green-myanmar.com for more information</h5>

<h4 style="color: orange">8:00 am >></h4>
<p>Pick up at your hotel in Bagan, visit to Nyaung U morning market to see the lifestyle of local people.</p>

<h4 style="color:orange">>> Pahtodawgyi Pagoda which has not finish purposely </h4>
<div class="row">
                    <div class="left">
                        <img src="newimages/mingun-2.jpg" style="width:300px;" class="editable" alt="mingun-2" title="mingun-2">  
                    </div>
                    <div class="right">
                        
                        <p>The Mingun Pahtodawgyi is one of the famous monuments in the world. It is also known as the world's largest unfinished pagoda. It lies in 10 kilometers northwest of Mandalay in Sagaing Region in central Myanmar. It was one of the big fours built by King Bodawpaya around the place. The building of Mingun Pahtodawgyi started in 1791 which was intentionally left unfinished. It was stopped at 49 meters height. If the constrution had been completed, it would have been the largest in the world at 150 meters . There are all together 174 steps to climb to the top. The scenery is marvelous from the top of the Pagoda. You can view natural scenic beauty of Ayeyarwaddy river and green and pleasant Minwun Hill. This pagoda was badly damaged with cracks by the earthquake of 1839.</p>
                    </div>

</div>
<br>
<div class="row">
                    
                    <div style="float:left;width:590px">
<h4 style="color:orange">>> World 2nd largest Mingun Bell</h4>
<p>The weight of the bell is 55,555 viss (90 tons) and about 13 feet tall. This number is conveniently remembered by many people in Myanmar as "Min Hpyu Hman Hman Pyaw", with the consonants representing the number 5 in Burmese astronomy and numerology.</p>
<p>The Mingun Bell was knocked off its supports by the earthquake of 1839. It was resuspended by the Irrawaddy Flotilla Company in 1896.</p>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/mingun-bell.jpg" style="width:300px;" class="editable" alt="mingun-bell" title="mingun-bell">  
                    </div>
</br>
<br>
<h4 style="color: orange">>> Mya Theintan Pagoda</h4>

<h4 style="color: orange">> Mingun aged home</h4>
</br>

<p>Then drive back to Mandalay and visit;</p>
<div class="row">
                    <div class="left">
                        <img src="newimages/mandalay-shwe-kyaung.jpg" style="width:300px;" class="editable" alt="mandalay-shwe-kyaung" title="mandalay-shwe-kyaung">  
                    </div>
                    <div class="right">
                        
        <h4 style="color: orange">>> Shwenandaw Monastery</h4>
<br>
<p>This monastery is famous for its beautiful wood-carvings. This wooden monastery is carved all over with motifs and mythical creatures. It was once entirely covered in thick gold, but only the interior gold remains due to harshness of the tropical weather. Some of the outer carvings have been prised off and removed being damaged by the weather. Inside the monastery, there are 10 jataka scenes taken from the Buddha's life. These are still in a very good condition</p>
</br>
</div>

</div>
<h4 style="color: black">                                                -</h4>
<br>
<div class="row">
                    
    <div style="float:left;width:580px">
                        
        <h4 style="color: orange">>> Mahamyatmuni Pagoda</h4>
<br>
<p>The Height of the Maha Muni Buddha Image is 8 Cubits and 1 Maik ( 3.83m ). The altar is 2.13m high. There, 2 bronze Siamese images, 3 bronze lion images and 1 bronze three headed Ayeyawun elephants are housed and displayed in the precinct on the left side of the northern exit passage.. The Buddha image is covered with gold and the coating of gold thickness is 5.9 inch.</p></br>
</div>

<div style="float:right;width:300px">
                        <img src="newimages/mahamyatmuni1.jpg" style="width:300px;" class="editable" alt="mahamyatmuni1" title="mahamyatmuni1">  
                    </div>

</div>
</br>
<h4 style="color: black">                                                                                                                                          -</h4>
<div class="row">
                    <div class="left">
                        <img src="imagesnew/kuthodaw-pagoda1.jpg" style="width:300px;" class="editable" alt="kuthodaw-pagoda1" title="kuthodaw-pagoda1">  
                    </div>

                    <div class="right">
                        
        <h4 style="color: orange">>>Kuthodaw Pagoda</h4>
<br>
<p>It is figuratively called the World's Biggest Book. With the public donated money, an ornamental umbrella of stone was offered to shelter each standing inscription slab so as to preserve the inscriptions through time and changes. Between one cave-shrine housing the inscription slab and another were systematically grown star-flower trees and Madhuca longifolia at equal distances under the supervision of the Moe Bye Sit-ke ( Second-in-command of a military unit ) in M.E 1254. Today visitors to the pagoda can pay obeisance to the Buddha Image, and enjoy sweet recreation beneath the sweet-smelling, cool, shady trees.</p>
</br>
</div>
</div>
<h4 style="color: orange">18:00 >>End of tour and transfer back to the hotel.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<br>
<h4 style="color: red">Service Include :</h4>
<p>Transfer and Sightseeing by private air-conditioned vehicles</p>
<p>English Speaking  Guide</p>  
<p>Mingun boat</p>
<p>Drinking water 1 bottle</p>

<h4 style="color:red">Services Exclude :</h4> 

<p>Meals(Lunch/Dinner)</p>
<p>Drinks, Beverages, Tips, Personal Expenses</p>
<p>Mandalay Zone fees</p>
<p>Camera fees, any other services not mentioned in the “Services Include” section a above</p>


            <div>
                <a href="reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;"/></a>

            </div></div></br>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

<?php include 'footer.php'; ?>
