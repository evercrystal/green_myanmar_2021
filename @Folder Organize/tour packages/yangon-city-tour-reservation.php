<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
 
<head>
    <title>Green Myanmar Travel Agent</title>
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon City Tour Reservation " />
    <meta name="keyword" content="Yangon City Tour,Yangon Sightseeing, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="generator" content="">
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->




    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!-- Le styles -->
<link href="assets/css/bootstrap.css" rel="stylesheet">

<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="assets/js/html5shiv.js"></script>
<![endif]-->

<style>

    .reserve-label{
        color: #00B238;
    }
</style>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = '47889';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable">
          <form class="form-horizontal contactform"  id="commentForm"style="margin-top: 50px" action="make-yangon-city-tour-reservation.php" method="post">

              <div class="control-group">
                  <label class="control-label reserve-label" for="inputEmail">Tour Program or Where would you like to go ?</label>
                  <div class="controls">
                      <input type="text" name="tour" class="required" id="inputName" placeholder="Tour" value="Yangon City Tour">

                  </div>
              </div>

              <div class="control-group">
                  <label class="control-label reserve-label" for="inputEmail">Name</label>
                  <div class="controls">
                      <input type="text" name="name" class="required" id="inputName" >
                  </div>
              </div>
              <div class="control-group">
                  <label class="control-label reserve-label" for="inputEmail">Email</label>
                  <div class="controls">
                      <input type="text" name="email" class="required email" id="inputEmail" >
                  </div>
              </div>




              <div class="control-group">
                  <label class="control-label reserve-label" for="inputEmail">Arrival date</label>
                  <div class="controls">
                      <input type="text" name="arrive" id="arrive" placeholder="DD-MM-YYYY">

                  </div>
              </div>


              <div class="control-group">
                  <label class="control-label reserve-label" for="inputEmail">Message</label>
                  <div class="controls">
                      <textarea cols="30" rows="7" id="message" name="message" class="required">  </textarea>

                  </div>
              </div>

              <div class="control-group">
                  <label class="control-label reserve-label" for="inputEmail">What is 2+1 =? <br />(human verification)</label>
                  <div class="controls">
                      <input type="text" name="verify" class="required" id="verify" placeholder="">
                  </div>
              </div>


              <div class="control-group">
                  <div class="controls">
                      <button type="submit" class="btn">Do Inquiry</button>
                  </div>
              </div>


          </form>

          <div class="control-group">
              <label class="control-label reserve-label" for="inputEmail">Payment : Good News !!! We accept online payment .</label>
              <div class="controls">
                  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                      <input type="hidden" name="cmd" value="_xclick">
                      <input type="hidden" name="business" value="mgphyozaw@gmail.com">
                      <input type="hidden" name="lc" value="US">
                      <input type="hidden" name="button_subtype" value="services">
                      <input type="hidden" name="no_note" value="0">
                      <input type="hidden" name="currency_code" value="USD">
                      <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest">
                      <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                      <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                  </form>


              </div>
          </div>
      </div>

      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer-->
      
      <div class="clear"></div>
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/bootstrap-transition.js"></script>
<script src="assets/js/bootstrap-alert.js"></script>
<script src="assets/js/bootstrap-modal.js"></script>
<script src="assets/js/bootstrap-dropdown.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>
<script src="assets/js/bootstrap-tab.js"></script>
<script src="assets/js/bootstrap-tooltip.js"></script>
<script src="assets/js/bootstrap-popover.js"></script>
<script src="assets/js/bootstrap-button.js"></script>
<script src="assets/js/bootstrap-collapse.js"></script>
<script src="assets/js/bootstrap-carousel.js"></script>
<script src="assets/js/bootstrap-typeahead.js"></script>

<?php include 'footer.php'; ?>

