<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel Agent provides budget and best Mandalay City Tour for Mandalay Sightseeing. It includes Ancient cities of Upper Myanmar: Innwa, Amarapura, Sagaing.">
    <meta name="keyword" content="Green Myanmar Travel Agent, Mandalay City Tour , Mandalay Sightseeing , Innwa, Amarapura, Sagaing">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <title>Mandalay City Group Tour , Mandalay Day Group Tour ( Mandalay Group Sightseeing )</title>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">
            
                <h1 style="text-align: center;"><strong>Mandalay City Group Sightseeing Tour</strong></h1>
<h1 style="text-align: center;"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.MD1016</h1>

                    <h1 style="text-align: center;">If you are looking best way to go around Mandalay in best way . This is best selling all inclusive  Group Tour but it costs only 23000 MMK (19.99 USD ) only per person.</h1>


                    </br>
</br>

               
               <div>
                    <img src="newimages/mandalay-shwe-kyaung.jpg" class="editable" alt="botataung" title="mandalay2" style="float: left;padding-right: 15px;padding-left: 20px" width="270px" height="165px">

                    <img src="newimages/mahamyatmuni1.jpg" alt="gabaraye" class="editable" title="mandalay3" style="float: left;padding-right: 15px" width="270px" height="165px">

                    <img src="newimages/maldalay-1.jpg" alt="shwedagon" class="editable" title="maldalay-1" width="270px" height="165px">

                </div>

<br>
<h5 style="color : orange">08:00 – 11:59<h5></br>
<small><p>>>Pick you up from Hotel in Mandalay. Visit to Mahamuni Pagoda  which houses one of Myanmar’s most sacred Buddha Images covered with thick layers of gold leave.</p>
<p>>>Visit to local artisan’s workshops of gold leaf making, woodcarving and kalaga tapestries.</p>
<p>>>Visit to  Zaycho Market, the oldest, largest and the most important market in upper Myanmar.</p></small>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color : orange">12:00 – 13:00<h5>
<small><p>>>Lunch at Local Restaurant</p></small>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color : orange">13:00 – 18:00<h5>
<small><p>>>Visit to magnificent complex of Royal Palace with impressive crowning wooden pavilion and surrounding moat. </p>
<p>>>Visit to Shwenandaw Monastery, noted for its exquisite wood carvings and consists mostly of teak wood; the renovated Atumashi Monastery which is referred as incomparable monastery and the 'world’s largest book' at Kuthodaw Pagoda, being the Buddhist Scriptures inscribed on marble slabs. </p>    
<p>>>Go up the top of Mandalay Hill, a vantage point for panoramic view of the city and spectacular sunset view.</p>
<p>>>End of Tours & transfer back to Hotel.</p><small>


<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                    <h5 style="color : red">Services Include:<h5><br><small><p> Transfers and Sightseeing by private air-conditioned vehicles </p>
                    <p> English Speaking station Guide </p>
                    <p> Transfer and Sightseeing with private Air-condition Vehicle </p>
                    <p> Drinking water 1 bottle </p></small>
                    <p> Lunch </p>
                    <br>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                    <h5 style="color : red">Services Exclude:<h5><br><small>
                    <p> Drinks, Beverages, Tips,  personal expenses </p>
                    <p>Entrance Fees</p>
                    <p> Any other services not mentioned in the “Services Include:” section as above</p></small>

                </div>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3>

                <a style="font-size: 30px" href="http://myanmardaytrips.com/dailytour_detail/38">Find Mandalay City Group Tour price here</a>

<h4>for more details,please contact hello@green-myanmar.com  </h4>

                <h5 style="color : red">Remark:<h5><br><small>
                            <p> Zaycho Market is closed during Sabbath Days and Gazetted Holidays. </p>
                           </small>

            </div>
            <h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

            <a href="reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;"/></a>

            </div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

<?php include 'footer.php'; ?>
