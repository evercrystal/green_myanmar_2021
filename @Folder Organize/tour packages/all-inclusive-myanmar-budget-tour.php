<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>All inclusive Myanmar Budget Tour - Myanmar Budget tour with river cruise </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="All inclusive Myanmar Budget Tour - Myanamr Budget tour with river cruise">
    <meta name="keyword" content="All inclusive Myanmar Budget Tour , Myanamr Budget tour with river cruise,river cruise">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="full-column" >
        <div class="full-column-header"></div>
        <!--end full column header-->
        <div class="full-column-body editable">
            <div id="main">
                <h2></h2><br>

                <h5 style="color:white"><b>All inclusive Myanmar Budget Tour - Myanamr Budget tour with river cruise - </h5><h5> contact hello@green-myanmar.com for more information</h5>

                <br />
                <br />
            </div>

            <div>
                <img src="imagesnew/goldenrock1.jpeg" class="editable" alt="botataung" title="Myanmar Golden Rock Day Return Tour" style="float: left;padding-right: 15px;padding-left: 20px">

                <img src="imagesnew/myanmarcruise.png" alt="gabaraye" class="editable" title="Myanmar Cruise Tour" style="float: left;padding-right: 15px">

                <img src="newimages/mandalay2.jpg" alt="shwedagon" class="editable" title="Myanmar Golden Rock Day Return Tour" width="270px" height="165px">

            </div>
            <br />
            <br />
            <p style="text-align: center"><a href="reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;" /></a>
            </p>
            <br />
            <img src="imagesnew/myanmartourmap.png" class="editable" alt="Myanmar Tour map" title="Botataung pagoda">
<br />
            <h4 style="color: orange">Day 01 Arrival Yangon (by morning/ midday flight )</h4>
            <br />
            <p>Arrive Yangon by morning or midday flight. You will be welcome by tour representative and transfer to the hotel. Have a rest at the hotel until the late afternoon sightseeing starts.</p>
            <p>Late afternoon visit the Botataung Pagoda and the Shwedagon Pagoda and sunset viewing from there. </p>
            <p>Overnight Yangon.</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 02 Golden Rock</h4>
            <br />
            <p>In early morning (est : 4am ),  Drive to the camp Kimpun (about 5 hours), from where the uphill motor road to hermit’s hill starts.

                Arrive Kimpun camp and take the uphill pick up truck about an hour to reach to the top of the mountain where you can see the Golden Rock(Kyaikhtiyo)- a large
                boulder positioned well balanced on the cliff of that mountain - seems to be defying the force of gravity from the spur of the cliff. Enjoy sunset view over deep forested mountain.
            </p>
            <p>En-route visit Bago; Shwethalyaung reclining Buddha Image – originally built in 994 AD and is a masterpiece of Myanmar stucco and symmetry. Arrive Yangon. </p>
            <p>
                Culture Show at Karawait Palace
            </p>
            <p>Overnight Yangon.</p>
            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 03 Yangon Sightseeing(Coach to Bagan )    </h4>
            <br />
            <p>Early breakfast at hotel and Sightseeing in yangon. Highlights of Yangon Sightseeing </p>
            <ul>
                <li>Sule Landmark</li>
                <li>Chaukhtat Gyi Pagoda</li>
                <li>Karawait Palace & Kandawgyi Lake</li>
                <li>Gaba Aye Pagoda</li>
                <li>Mid-stream Pagoda</li>
                <li>Yangon Colonial Building</li>

            </ul>
            <p>Overnight on Coach.</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 04 Yangon-Nyaung U (Coach )/ Bagan full day sightseeing   </h4>
            <br />
            <p>Arrive to Bagan in early morning and welcomed by Agent. The best time to go and look Bagan amazing sunrise and have breakfast. Check in to hotel , Short break and Start bagan sighseeing. Some highlights for bagan sightseeing. </p>
            <ul>
                <li>Nyaung Oo Market</li>
                <li>Shwezigon Pagoda</li>
                <li>Gubyaukgyi Temple</li>
                <li>Anada Temple</li>
                <li>Lacquer ware factory followed by a visit to local village</li>

            </ul>
            <p>Overnight at Bagan.</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 05 Bagan-Mandalay with cruise , Sightseeing along the river   </h4>
            <br />
            <p>Breakfast at the hotel , Check in to cruise. On the way along road to Mandalay, You will see breathtaking view of Myanmar. Have lunch on cruise and arrive to Mandalay evening. </p>

            <p>Overnight at Mandalay.</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 06 Mandalay Amarapura & Mandalay Sightseeing        </h4>
            <br />
            <p>Breakfast at the hotel. Drive to Amarapura, former Royal capital of Myanmar but the suburb of Mandalay nowadays. Highlights include</p>

            <ul>
                <li>Mahargandarone monastery</li>
                <li>U Bein Bridge</li>
                <li>Mandalay Palace</li>
                <li>Kuthodaw Pagoda</li>
                <li>Mandalay Hill </li>

            </ul>
            <p>Overnight at Mandalay.</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 07 Mandalay-Kalaw (Coach) / visit Pindaya and drive to Inle Lake        </h4>
            <br />
            <p>Breakfast at the hotel. Transfer coach station to get to Inle . Visit Kalaw , Pindaya Natural Cave and continue driving to Inle Lake</p>


            <p>Overnight at Nyaung Shwe(Inle Lake).</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 08 Visit Indein and Inle Lake    </h4>
            <br />
            <p>Breakfast at the hotel. Visit the Inle Lake , Highlight sightseeing of Inle Lake includes </p>

            <ul>
                <li>Indein</li>
                <li>Nga Hpe Kyaung (jumping cat) monastery </li>
                <li>The Phaung Daw U Pagoda</li>
                <li>Ywama village</li>

            </ul>
            <p>Overnight at Nyaung Shwe(Inle Lake).</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 09 Nyaung Shwe-Yangon ( Night Coach ) </h4>
            <br />
            <p>For an entirely different type of experience, head up to the Red Mountain Estate vineyard in the hills to the east of Inle Lake for some wine tasting. One of only two vineyards in Myanmar, this definitely breaks the mould of your day-to-day Myanmar experience, making it suddenly feel as if you have been transplanted to the south of France. You can try the tasting menu for K2000 (featuring wines from four different types of grape) whilst taking in the stunning sunset views over the lake. The vineyard can be reached by taxi or bicycle from Nyaung Shwe.</p>


            <p>Overnight on Coach.</p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4 style="color: orange">Day 10 Day Free / Departure Yangon </h4>
            <br />

            <p>Pick up from hotel, Have Breakfast. Departure transfer</p>

            <p> ------- End of journey with happiness ------ </p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <h4>Highlights in Tour</h4>
                <h5>•	Culture Show at Karawait Palace</h5>
                    <h5>•	River Cruise from Bagan to Mandalay</h5>
                        <h5>•	Day Return to Golden Rock</h5>
                            <h5>•	Midstream Pagoda</h5>

            <div>
                <h5 style="color:red">Services Include:</h5><br>
                <p style="color:white"> -  Transfers and Sightseeing by private air-conditioned vehicles </p>
                <p style="color:white"> -  Local English Speaking  Guide </p>
                <p style="color:white"> -  The entry fees for sightseeing places </p>
                <p style="color:white"> -  Truck from camp Kimpun camp to Golden rock , return</p>
                <p style="color:white"> -  Drinking water 1 bottle </p>
                <br>

                <h5 style="color:red">Services Exclude:</h5><br>


                <p style="color:white"> - Drinks, Beverages, Tips,  personal expenses </p>
                <p style="color:white"> - Meal </p>

                <p></p>

                <p style="color:white"> -  Any other services not mentioned in the “Services Include:” section as above</p>
            </div>


            <h4 style="color: orange">Cost per person: </h4>
            <p style="color:white">  Lowest price starts from 499 USD per person for more details contact hello@green-myanmar.com </p>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>


            <img src="imagesnew/myanmardancinglady.jpg" class="editable" alt="botataung" title="Botataung pagoda">
            <h5>1.Golden Rock History</h5>
            <p>The Golden Rock (Kyaik-htiyo or Kyaiktiyo), perched atop a cliff near Yangon, is one of the most sacred sites in Burma. The great boulder precariously balances on the edge of a cliff and is topped by a small stupa. An endless stream of pilgrims come to admire the sight and add squares of gold leaf to its surface.</p>

            <a href="reservation.php"><img src="images/booking-button.jpg" class="editable" alt="booking button" title="book now" style="margin: 5px 0px 5px 5px;"/></a>

        </div>
        <!--end column body-->
        <div class="full-column-footer"></div>
        <!--end full column footer-->

    </div>
    <!--end full column -->
    <div class="clear"></div>
</div>
<!--end content-wrapper-->
<div class="clear"></div>
<!--end content section-->

<?php include 'footer.php'; ?>
