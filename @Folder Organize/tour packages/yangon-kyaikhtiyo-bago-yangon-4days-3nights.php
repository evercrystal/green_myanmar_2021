<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


    <title>Yangon- Kyaikhtiyo-Bago-Yangon 4 Days 3 Nights</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travel agent provides yangon city tours, yangon sightseeing tours , Myanmar tours, Myanmar Tour Packages , Mandalay Bagan Inle Lake Tour packages, tours to Myanmar with best rates.">
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->
  
  <!-- Content Section -->
  
     <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7">
       <h1 style="text-align: center;"><strong>Yangon-Kyaikhtiyo-Bago-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>(4Days-3Nights)</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.D4003K</h1>
       



<h5 style="color: orange">Day 1 Yangon Full Day sightseeing<h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Arrive Yangon by morning or midday flight. You will be welcomed by your tour representative and transfer to hotel for hotel check in. Take a short rest. If time permit, start sightseeing to-</p>
<p>•   Botahtaung Pagoda</p>
<p>•   ChaukHtetgyi Pagoda</p>
<p>•   Kandawgyi Nature park</p>

<p>View Amazing Sunset at ShweDagon Pagoda.</p>
<h5>Overnight in Yangon.</h5>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/2012-12-02-2104.jpg" style="width:300px;" class="editable" alt="2012-12-02-2104" title="2012-12-02-2104">  
                    </div>

</div>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 2 Yangon- Kyaikhtiyo (by private car)<h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Breakfast at the hotel. Then drive to Golden Rock around 5:00 am. Arrive Kimpun Camp around 10 am, from where the uphill motor road to hermit’s hill starts. The uphill pick up truck takes about an hour to reach to the top of the mountain where you can see the Golden Rock(Kyaikhtiyo)- a large boulder positioned well balanced on the cliff of that mountain - seems to be defying the force of gravity from the spur of the cliff.  </p>
<h5>Overnight in Kyaikhtiyo.</h5>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/Kyaikhteeyo.jpg" style="width:300px;" class="editable" alt="Kyaikhteeyo" title="Kyaikhteeyo">  
                    </div>

</div>        

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 3 Kyaikhtiyo- Bago – Yangon Full day sightseeing </h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Breakfast at hotel. Then descend back from Kyaikhtiyo mountain and drive back to Yangon back. En-route visit and sightseeing in Bago includes-</p>
<p>•   Shwe Mawdaw Pagoda</p>
<p>•   Shwethahlyaung Pagoda</p>
<p>•   Kyaik Pon Pagoda</p>
<p>•   Koethein Koethan Pagoda</p>

<h5>Overnight in Yangon.</h5>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/Shwe-thar-lyaung-Pagoda-Myanmar.jpg" style="width:300px;" class="editable" alt="Shwe-thar-lyaung-Pagoda-Myanmar" title="Shwe-thar-lyaung-Pagoda-Myanmar">  
                    </div>

</div>        

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 4 Yangon Airport transfer</h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Breakfast at the hotel. Visit to Bogyoke Market. Free time until transfer to Yangon International Airport for Departure. </p>




                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/bogyokemarket.jpg" style="width:300px;" class="editable" alt="bogyokemarket" title="bogyokemarket">  
                    </div>

</div>        

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Service Include : </h4><p>  Transfers and Sightseeing by private air-conditioned vehicles</p> 
<p>   Accommodation 2 Nights with breakfast from hotel</p>
<p>   Truck to and from Golden Rock mountain</p>
<p>   Station Guide service </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Services Exclude : </h4><p> Meals ( Lunch / Dinner )</p>
<p>   The entry fees for sightseeing places</p>
<p>   Drinks, Beverages, Tips,  personal expenses</p>
<p>   International Airport Tax ( 10 US$ per person in Yangon )</p> 
<p>   Any other services not mentioned in the “Services Include:” section as above</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3>
<h5>For 2 pax, US$507 per person</h5>
<h5>For 10pax, US$282 per person</h5>
<h3>for more details,please contact hello@green-myanmar.com </h3>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>
                </div>

      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
