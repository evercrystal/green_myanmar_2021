<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Wonders of Myanmar 12 Days packages</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Wonders of Myanmar 12 Days Packages is complete exploring Myanmar with amazing experience . It includes travelling to Yangon , Bago , Mandalay , Bagan , Mount popa, Inle Lake , Kalaw , Taunggyi . It is nearly all round Myanmar.">
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7"><h2>Wonders of Myanmar 12 Days</h2>

<p><img alt="inle lake tour" class="image-right editable" src="images/images/inlay.jpg" title=
"Shwedagon pagoda" /></p>

<h3>The best tour package to explore Myanmar</h3>

          <h3>Yangon - Bago - Bagan - Mandalay - Inle Lake - Yangon</h3>
<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>

<h4> Day - 1 - Yangon </h4>
<p>Arrive Yangon airport by Morning flight. Welcomed at the airport by our experienced guide. Proceeded to your hotel and check in, take a short rest and start highlight sightseeing in Yangon. Visit Mahabandoola Park, Colonial buildings, the Strand Hotel (built over 100 years ago) and study & sightseeing, and Yangon downtown area. To Karaweik Hall, a 'hamsa-bird-shaped' floating barge on Kandawgyi (Royal) Lake. Then great Shwe Dagon Pagoda built over 2500 years ago, its golden dome rises 96 meters above its base and is covered with 60 tons of pure gold leaves. Also to see sunset from the pagoda. And then visit to Bronze image pagoda. In the evening, have dinner at restaurant and overnight at the hotel in Yangon. </p>
          <hr style="">

          <h4> Day - 2  - Yangon - Bago </h4>
          <p><img alt="inle lake tour" class="image-right editable" src="images/images/bago.jpg" title=
                  "Bago Myanmar" /></p>

        <p> After breakfast drive to Bago which is 80 km far from Yangon. Arrive in Bago in the morning. Visit Kyaik Pun (Four Sisters) Pagoda, Shwe Thar Hlaung, Ma Ha Zedi Pagoda, Kan Baw Za Tha Di Old Palace, now excavated and Shwe Maw Daw Pagoda. Then proceed to Kim Pun Camp, 106 km from Bago, the basic foot hill camp of Kyaik Hti Yoe mountain range. On the way to base camp lunch at local restaurant near the town of Kyaik Hto. Then climb up to Ya-The-Taung, a sub-camp by light truck. Continue climbing from Ya-The-Taung to hill top by foot which is about 1.5km far from and about one hour walk on the hill top, transfer to hotel. And then visit Kyaik Hti Yoe (Golden Rock) Pagoda within 300 meters walking distance from the hotel, Kyikan Bazat (Crow's mouth) Cave and take photographs, a market place for shopping and strolling. Dinner and overnight at the hotel in Bago. </p>
          <hr style="">

          <h4> Day - 3  - Bago - Yangon </h4>
          <p><img alt="inle lake tour" class="image-right editable" src="images/images/mandalay.jpg" title=
                  "Bago Myanmar" /></p>

          <p>After breakfast, morning short trekking to the pagoda and its vicinity and small hills. Then climb down to Ya-The-Taung sub-base camp by foot and from Ya-The-Taung to Kinpun base camp at the foot hill by light truck. Drive back to Yangon. Lunch on the way. On the way to Yangon, visit Shwe Nyaung Pin Nat (Worship centre of spiritual being live in banyan tree) and Htauk Kyant War Cemetery. Arrive in Yangon in the late evening. Dinner at a good and famous restaurant and overnight at the hotel in Yangon. </p>
          <hr style="">

          <h4> Day - 4  - Yangon- Bagan </h4>

          <p><img alt="Bagan Package tour" class="image-right editable" src="images/images/shwedagon.jpg" title=
                  "Bagan package Tour" /></p>

          <p>Morning, breakfast at the hotel. Transfer to the Airport for (Morning flight) flying to Bagan. Upon arrival, transfer and check in at the hotel. Visit hundreds of colorful Nyaung Oo local markets; Shwezigon Pagoda, the prototype of later Myanmar stupas; Ananda temple, an architectural masterpiece resembling a Greek Cross; Sulamini temple, the paintings inside believed to be the original and oldest remained in Bagan; Dhamayangyi, a massive temple with a finest brickwork and lacquer ware workshop. To see Manuha Pagoda, Mya Zaydi, Gupyaukgyi Pagoda, late evening enjoy the sunset on top of the Shwesandaw pagoda or Pyatthatgyi temple, visit Nghatpyit taung in Nyaung U. There are cave existed for 900 years. Then proceed to Lawkananda tooth relic pagoda and then go to U Ba Nyein lacquer ware business. Dinner at one of the good and famous restaurants and overnight at the hotel in Bagan. </p>
          <hr style="">

          <h4> Day - 5  - Bagan - Mt. Popa - Bagan </h4>

          <p> After breakfast morning drive to Mt. Popa, the core of an extinct volcano last active 25,000 years ago, known as the Mt. Olympus of Myanmar, the abode of Myanmar's most powerful Nats (Spiritual beings), the most important nat worship centre. Lunch at the local restaurant. Then return to Bagan (Nyuang Oo), visit Phwar Saw Village, Phayar Thone Su & Viewing Tower, Tharapa Old Gate, Damayangyi Temple, the biggest volume in Bangan, SularMuni Temple and AlodawPyae Pagoda and monastery. Myinkaba villages to see village lives, Dinner with culture show at famous restaurant and overnight at the hotel in Bagan. </p>
          <hr style="">

          <h4> Day - 6  - Bagan - Mandalay - Amarapura </h4>
          <p>After breakfast, transfer to Bagan Airport for (Morning flight) flying to Mandalay. Arrival Mandalay, check in to hotel, and then visit Amarapura, start detail sightseeing, This trip will take you to Mahagandayon Monastery where more than a thousand monks live and study, to see their last meal of the day in total silence, continue to U Pein Wooden Bridge built in 1782 using only wood even nail. And observe Myanmar traditional hand-woven silk workshop. And then visit to Mandalay high light sightseeing include Mahamuni Image covered with very thick layer of pure gold leaves weighing between 3-5 tons on the body except face, Maha Aung Mye Bonzan Oak Kyaung, a brick and stucco monastery, Golden palace (ex palace of former Myanmar kings newly built as the original style and design), Lunch at local restaurant and then visit Atumashi (the unrivalled) Monastery, Kuthodaw Pagoda know as the world biggest book with the collection of 729 stone slabs on which are inscribed the whole of Buddhist scripture at the foot of Mandalay Hill, then climb up to the Mandalay Hill Top to see sunset on the Ayeyarwaddy River and Mandalay city view. Dinner at very nice local restaurant and overnight at the hotel in Amarapura. </p>
          <hr style="">

          <h4> Day - 7  - Amarapura - Mandalay - Mingun - Mandalay </h4>
          <p>After breakfast morning drive by Car to Mandalay, and then rest at hotel, after that take Special Boat trip to Mingun, just 12km on the west bank upstream from Mandalay. Upon arrival, visit Pondaw Paya, a five meter high working model for gigantic structure; Settawya Paya, the vaulted shrine with a footprint of the Buddha; Mingun Pahtotagyi Paya, one of the world's largest chides built by Bodaw Paya; the World renowned ringing, Mingun bell weighing 90 tons; Hsinbyume Paya, a representation of the Sulamani Paya which stands atop Mt. Meru. Return to Mandalay. Lunch at local restaurant. Afternoon sightseeing includes Shwe Inbin Monastery, one of the rare specimens of typical Myanmar architecture; Shwe Kyaung Gyi, a superb traditional wooden building. Also see and study in lacquer and gold leaf work shop, then dinner and overnight at the hotel in Mandalay. </p>
          <hr style="">

          <h4> Day - 8  - Mandalay - Heho - Pindaya - Kalaw </h4>
          <p>After breakfast, transfer to airport for (Morning flight) fly to Heho. Arrive in Heho in the late morning then proceed to Pindaya Market and the amazing Shan Paper making and Bamboo umbrealla and bamboo hat making workshop. Then proceed to Kalaw, a hill resort town on the Shan Plateau which is 64 km far. Arrive in Kalaw at noon. Transfer to hotel. Lunch at local restaurant. Afternoon sightseeing around Kalaw. Dinner and overnight at the hotel in Kalaw. </p>
          <hr style="">

          <h4> Day - 9  - Kalaw - Nyaung Shwe </h4>
          <p>After breakfast visit Kalaw Market then proceed to the town of Nyaungshwe, a town on the edge of the lake Inle. Then transfer to hotel. After lunch, afternoon start by boat to Phaung Daw Oo Pagoda, where sacred and almost shapeless Buddha Image are housed, to one village which turn for 5 day rotation market, floating village of Heya Ywama and weaving village of Inpawkon. Lunch at a floating village. After lunch visit Nga-phe Chaung Monastery better known as jumping cat monastery for its wonder of trained cats, other floating gardens and canals. To see native's method of rowing by leg, method of fishing, Shan shoulders bag weaving workshop in Nan Pan village on the lake. Dinner and overnight at the hotel in Nyaungshwe。 </p>
          <hr style="">

          <h4> Day - 10  - Nyaungshwe - Indein - Inle Lake</h4>
          <p>After Breakfast at the hotel, visit by boat to Indein village. Make the sightseeing around the Indain famous areas. Lunch there. After lunch, drive back to the lake by boat from one floating village to another, one floating garden to another and one floating canal to another. Dinner at restaurant and overnight at the hotel in Inle Lake. </p>
          <hr style="">

          <h4> Day - 11  -     Inle Lake - Nyaung Shwe - Yangon</h4>
          <p>After breakfast at hotel and transfer to Nyaungshwe and then drive to Heho Airport for (Morning flight) fly to Yangon, check in at hotel; take a short rest, and then start to visit and sightseeing to Botahtaung Pagoda (in which Hair relics are enshrined) and Gem Museum. Visit Yangon's famous places detail highlights sightseeing and Yangon's famous Bogyoke Aung San (Scott) Market for shopping, and then a short visit of China town for strolling. Then Dinner at very famous restaurant and overnight at the hotel in Yangon.
          </p>
          <hr style="">

          <h4> Day - 12  -     Yangon</h4>
          <p>After breakfast, morning free time and until transfer by car to Yangon Airport for departure to your country. End of service.
              back to topTour ends
          </p>
          <hr style="">

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
"book now" /></a></p></div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
