<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
 
<head>
    <title> Myanmar Travel Agent</title>
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel agent is Myanmar inbound Tour operator especially culture and classic tours in Yangon, Bagan, Mandalay, Inle Lake and all around Myanmar. Myanmar Travel Information " />
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="generator" content="">
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->




    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>

    <!--end class dropdown-->

</div>
<!--end Header-->

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="main-column" >
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body">
        <h2 id="column-header">Welcome To <span class="green">Myanmar</span></h2>
        <p>Myanmar or Burma,officially the Republic of the Union of Myanmar is a country in Southeast Asia.It lies on the Bay of Bengal and Andaman Sea coast with Bangladesh and India to the west,China to the north,and Laos and Thailand to the east.</p>
	<p>Myanmar's attractions lie largely in the area of the spiritual.Temples,pagodas and historical sites abound with some areas such as Bagan boasting so many attractions that it would be impossible to take them in during a single visit.With landscapes,a tropical climate,beaches,cheap transportation and truly awesome sights,Myanmar is a fascinating and bewitching destination.</p>
      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      
      <a href="myanmar.php"><h4 id="myanmar">Myanmar</h4></a>
      <div class="destination-bg">
        <div class="tri">
          <ul>
            <li><a href="bagan.php">Bagan</a></li>
	        <li><a href="chaungtha.php">Chaungtha</a></li>
	        <li><a href="inle.php">Inle</a></li>
          </ul>
        </div>
        <!--end tri div-->
        <div class="tri">
          <ul>
            <li><a href="ngwesaung.php">Ngwesaung</a></li>
	        <li><a href="naypyitaw.php">Naypyidaw</a></li>
	        <li><a href="ngapali.php">Ngapali</a></li>
          </ul>
        </div>
        <!--end tri div-->
        
        <div class="tri-last">
          <ul>
	        <li><a href="mandalay.php">Mandalay</a></li>
	        <li><a href="taunggyi.php">Taunggyi</a></li>
	        <li><a href="yangon.php">Yangon</a></li>
          </ul>
        </div>
        <!--end tri-last div--> 
      </div>
      <a href="thailand.php"><h4 id="thailand">Thailand</h4></a>
      <div class="destination-bg">
        <div class="tri">
          <ul>
	        <li><a href="ayutthaya.php">Ayutthaya</a></li>
            <li><a href="bangkok.php">Bangkok</a></li>
            <li><a href="chiangmai.php">Chiang Mai</a></li>
          </ul>
        </div>
        <!--end tri div-->
        <div class="tri">
          <ul>
            <li><a href="huahin.php">Hua Hin</a></li>
            <li><a href="kanchanaburi.php>Kanchanaburi</a></li>
            <li><a href="kohsamui.php>Koh Samui</a></li>
          </ul>
        </div>
        <!--end tri div-->
        
        <div class="tri-last">
          <ul>
            <li><a href="krabi.php">Krabi</a></li>
            <li><a href="phuket.php">Phuket</a></li>
	        s<li><a href="pattaya.php">Pattaya</a></li>
          </ul>
        </div>
        <!--end tri-last div--> 
      </div>
      <a href="singapore.php"><h4 id="singapore">Singapore</h4></a>
      <div class="destination-bg">
        <div class="tri">
          <ul>
            <li><a href="botanicgarden.php">Botanic Garden</a></li>
            <li><a href="jurong.php">Jurong Bird Park</a></li>
            <li><a href="nightsafari.php">Night Safari</a></li>
          </ul>
        </div>
        <!--end tri div-->
        <div class="tri">
          <ul>
            <li><a href="merlion.php">Merlion statue</a></li>
            <li><a href="orchardroad.php">Orchard Road</a></li>
            <li><a href="pulauubin.php">Pulau Ubin</a></li>
          </ul>
        </div>
        <!--end tri div-->
        
        <div class="tri-last">
          <ul>
            <li><a href="sentosa.php">Sentosa Island</a></li>
            <li><a href="singaporeflyer.php">Singapore Flyer</a></li>
            <li><a href="universal.php">Universal Studios</a></li>
          </ul>
        </div>
        <!--end tri-last div--> 
      </div>
      <div class="clear"></div>
    </div>
    <!--end main column -->

      <!----- Side Bar --->
      <?php include 'myanmar-travel-promotion-sidebar.php'; ?>
    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
