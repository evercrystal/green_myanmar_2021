<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Myanmar package tour - Myanmar Budget Tours -Yangon- Bagan-Popa-Yangon- Myanmar travel packages</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon-Bagan-Mandalay-Inle Lake-Yangon (7Days/6Nights) Packages is complete exploring Myanmar with amazing experience .">
    <meta name="keyword" content="Yangon,Bagan,Mandalay, Inle Lake,Yangon,Myanmar Tour Packages,(7Days/6Nights)">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7">
            <h1 style="text-align: center;"><strong>Yangon-Bagan-Popa-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>(4Days-3Nights)</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.D4001B</h1>



                <br />
                <br />

<h5 style="color: orange">Day 1 Yangon Full Day sightseeing, Yangon-Bagan(VIP express)<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Arrive Yangon  and you will be welcomed by our tour representative. If time permits, start sightseeing to-</p>
<p>•   Botahtaung Pagoda</p>
<p>•   ChaukHtetgyi Pagoda</p>
<p>•   Kandawgyi Nature park</p>

<p>View Sunset at World famous Shwedagon Pagoda .</p>
<p>Then transfer to Aung Mingalar bus station to take the bus to Bagan.</p></small>
<h5>Overnight on Coach.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/2012-12-02-2104.jpg" style="width:300px;margin-right:10px" class="editable" alt="2012-12-02-2104" titlx="2012-12-02-2104">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
                
<h5 style="color: orange">Day 02 Bagan Sightseeings                                                                             ( -/ - / - )<h5>
<small><p>Early arrive to Bagan around 5:30am, welcomed by Agent. The best time to go and look Bagan amazing sunrise and have breakfast. Check in to hotel , Short break and Start Bagan sighseeing. Some highlights for Bagan sightseeings include</p></small>              
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>•     Shwezigon Pagoda</p>
<p>•    Ananda temple</p>
<p>•    Sulamini temple</p>
<p>•    Htilominlo Pagoda; </p>
<p>•    Dhamayangyi</p>
<p>-late evening enjoy amazing sunset on top of the Shwesandaw pagoda or Bu pagoda.</p></small>
<h5>Overnight in Bagan.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/sunrise-in-bagan1.jpg" style="width:300px;margin-right:10px" class="editable" alt="sunrise-in-bagan1" titlx="sunrise-in-bagan1">  
                    
                      </div>  
</div> 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 03 Bagan & Popa fullday sightseeing,  Bagan-Yangon(VIP Express)   (B/-/-)<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel, drive to MOUNT POPA (45 km - 1.5 h), an extinct volcano with spectacular views and the birth place of the Myanmar 'nats' or spirits.</p> 
<p>Return to Bagan. Highlights in Bagan include</p>

<p>•Nyaung Oo Market</p>
<p>•Thatbaynyu Pagoda,</p> 
<p>•Lawkananda,</p> 
<p>•Gupyaukgyi Pagoda</p>
<p>In the evening transfer Nyaung U bus station to take bus to Yangon.</p></small>
<h5>Overnight on Coach to Yangon(ETD-20:00, ETA-5:30).<h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/mountpopa.jpg" style="width:300px;margin-right:10px" class="editable" alt="mountpopa" titlx="mountpopa">  
                    
                      </div>  
</div> 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 4 Yangon Airport transfer<h5>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Early arrive to Yangon. Depending on your flight time, relax at Hotel and then visit Bogyoke Market. Transfer to Yangon International Airport for Departure. </p></small>
</div>                  
                    <div style="float:right">
                        <img src="newimages/bogyoke-market1.jpg" style="width:300px;margin-right:10px" class="editable" alt="bogyoke-market1" titlx="bogyoke-market1">  
                    
                      </div>  
</div>
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: red">Service Include : </h5><p>  Transfers and Sightseeing by private air-conditioned vehicles </p>
<p>    VIP Bus tickets as per program (Yangon- Bagan, Bagan- Yangon)</p>
<p>    Accommodation 1 Night with breakfast from hotel </p>
<p>    English Speaking Station Guide service </p> 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: red">Services Exclude : </h5><p> Meals ( Lunch / Dinner )</p>
<p>    The entry fees for sightseeing places</p>
<p>   Drinks, Beverages, Tips,  personal expenses</p>
<p>   International Airport Tax ( 10 US$ per person in Yangon )</p> 
<p>   Any other services not mentioned in the “Services Include:” section as above</p>

<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h3>Cost<h3/>
<small>
<p>For 2 pax, US$382 per person</p>
<p>For 10pax, US$239 per person</p><small/>
<h3>for more details,please contact hello@green-myanmar.com <h3/>

                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                          "book now" /></a></p>

            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>


