<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon-Thanlyin-Yangon</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travel agent provides yangon city tours, yangon sightseeing tours , Myanmar tours, Myanmar Tour Packages , Mandalay Bagan Inle Lake Tour packages, tours to Myanmar with best rates.">
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->
  <!-- Content Section -->
  
     <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7">

<h1 style="text-align: center;"><strong>Yangon-Thanlyin-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.YD1003</h1>

</br>
</br>
 <div>
                    <img src="newimages/kyaik-khauk-pagoda.jpg" class="editable" alt="botataung" title="Bagan Sightseeing Tour" style="float: left;padding-right: 15px;padding-left: 20px" width="270px" height="165px">

                    <img src="newimages/yele-pagoda1.jpg" alt="gabaraye" class="editable" title="bagan day tour" style="float: left;padding-right: 15px" width="270px" height="165px">

                    <img src="newimages/shwedagon pagoda.jpg" alt="shwedagon" class="editable" title="bagan Sightseeing tour" width="270px" height="165px">

                </div>
<h4>Itinerary 8:00am to 5:00pm </h4>

<p>&nbsp;</p>


<h5 style="color: orange">8:00>> </h5><p> Pick up at your hotel in Yangon and drive to Thanlyin and Kyauktan .</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
                        <h5 style="color: orange">9:00am</h5>
                        <h5>Kyaikkhauk Pagoda</h5>
                        <p>>>Kyaikkhauk Pagoda
Arrive Thanlyin and visit to Kyaikkhauk Pagoda. It’s 4 kilometres outside of the town centre and it was built same as Shwedagon pagoda. In the compound of the pagoda ,there are two cemeteries. One of them is ex-king of Taungoo. Another one is the cemetery of minister Padaytha Yarzar, well know poet in konbaung Era.
</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                <h5 style="color: orange">10:00</h5>
                       <h5>Kyauk Tan (or) Yele Pagoda</h5>
 
<p>>>Then continue to Kyauk Tan to vitsit Yele Pagoda, a stupa on a small island. You have to take a boat to get to Yele Pagoda . Moreover, there are many giant fishes which you can feed.

</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
       
<h5 style="color: orange">11:00</h5><p>>>visit to Ancient Portuguese Church.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: orange">11:30</h5><p>>>On the way return to Yangon, our next break point is Parda Gyi Pagoda in Parda Gyi village which is accepted as a ancient palace and small city in history.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: orange">12:00</h5><p>>>Lunch break at local restaurant</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: orange">13:00</h5><p>>>After Lunch  retrace to Yangon</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: orange">14:00</h5><p> >>Arrive Yangon and  Visit Botahtaung Pagoda and Ahmagyi Mya Nan Nwe.. The Botahtaung Pagoda is located on the Botahtaung Pagoda Road. Situated beside the Yangon Pagoda is hollow inside and you can walk through it. Ahmagyi Mya Nan Nwe, a devotee to the Botahtaung Pagoda. She was known to be a dragon guarding the pagoda. Hundreds of people come to this place to donate offertories and also to ask for blessing of the sister.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                        <h5 style="color: orange">15:00>></h5>
                        <h5>Shwedagon Pagoda</h5>
                        <p> >>Shwedagon Pagoda
Continue to Shwedagon Pagoda . it’s the most admirable Buddhist Pagoda in Myanmar. Most of Myanmar people believed that it was built in more than 2000 years ago but it can be proved that it was about 11 century then renovated several times until taking its current shape in the 15th century. The 8-sided central stupa is 99 meters tall and is covered with 60 tons of pure gold  leaf .

</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>


<h5 style="color: red">Service Include : </h5><p>•  Transfer and Sightseeing by private air-conditioned vehicles</p>
<p>The entry fees for sightseeing places</p>
<p>Local Station English speaking Guide </p>
<p>Drinking water 1 bottle</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: red">Services Exclude : </h5><p>•   Meals(Lunch/Dinner)</p>
<p>The entry fees for sightseeing places</p>
<p>Drinks, Beverages, Tips, Personal Expenses</p>
<p>Camera fees, any other usages and any other services not mentioned in the “Services Include” section a above.</p>
                
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3> 
<h4>For 2 pax, US$72 per person</h4>
<h4>For 10pax, US$24 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>
                     </div>

      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
