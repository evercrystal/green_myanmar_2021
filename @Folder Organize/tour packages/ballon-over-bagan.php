<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Ballons Over Bagan</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Ballon Over Bagan">
    <meta name="keyword" content="Ballon Over Bagan, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7"><h2> Full Day Electric Biking in Bagan</h2>

                <p><img alt="inle lake tour" class="image-right editable" src="images/images/inlay.jpg" title=
                        "Shwedagon pagoda" /></p>

                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                          "book now" /></a></p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>



                <h4 style="color:orange">About Ballons Over Bagan</h4>

                <h4 style="color:red">Check in at 5:30 am</h4>

                <p style="color:white">•	The minibus will pick you up at your hotel </p>
                <p style="color:white">•	Flying over the thousand of pagodas and the legendary Ayeyarwaddy River,</p>
                <p style="color:white">•	Breathing taking panorama view of thousand years old pagodas is the unique opportunity to see this ancient kingdom behind distance mountain. </p>
                <p style="color:white">•	The legend of Bagan has attracted the heart of visitor to Myanmar for years. One of the ancient temples land, magnificent view of land, mountain and river together will support the Balloons over bagan to be unique experience of the life.</p>


                <h4 style="color: orange">About of Hot Balloon Over Bagan </h4>

                <p>Our Guide will pick you up at your hotel.</p>

               <p>The balloon is guided by gentle winds not exceeding 15 mph, allowing passengers a serene and peaceful bird's-eye view of ancient temples drifting by. Using his skill the pilot is able to guide the balloon to a gentle landing where our friendly crew and celebratory glass of champagne will be on hand. </p>

                <p>Our Guide will pick you up at your hotel. </p>

                <p>The balloon is guided by gentle winds not exceeding 15 mph, allowing passengers a serene and peaceful bird's-eye view of ancient temples drifting by. Using his skill the pilot is able to guide the balloon to a gentle landing where our friendly crew and celebratory glass of champagne will be on hand. </p>

                <p>Commercial passenger ballooning has the best safety record of all forms of air transport. "Balloons over Bagan's" pilots and technicians are qualified and registered in the UK. Our balloons, which are built to the highest standards of the British aerospace industry, are provided by Cameron Balloons Limited, the world's largest manufacturer of high quality balloons. </p>

                <p>Duration:5hrs </p>

                <p>Availability:Daily upon request, start from October</p>

                <p>Cancellation Policy:No refund policy </p>

                <p>To Know:Minimum 2 persons </p>

                <p>What to Bring:Towel, waterproof sunscreen, hat, sunglasses, water shoes, water bottle </p>

                <p>What to Wear:Please dress for the weather conditions on that given day. Long pants or skirts are suitable as it is a unique place for Buddhists.
                Address:Bagan </p>

                <p> Price = 415 $  per person</p>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>



                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
                                                          "book now" /></a></p></div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>
