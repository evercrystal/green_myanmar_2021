<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Kalaw to Inle Lake Trekking Tour , Myanmar Trekking tour ">
    <meta name="keyword" content="Kalaw to Inle Trekking Tour, Myanmar Trekking">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <title>Kalaw To Inle Trekking Tour</title>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->






    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">

                    <h2 style="color:orange">KALAW TO INLE TREKKING</h2>
                    <h2 style="color:orange">KALAW TREKKING COSTS </h2>



                    <table border="0">
                        <tr>
                            <td style="width: 120px">cost per pax</td>
                            <td style="width: 120px">Half Day Trek</td>
                            <td style="width: 120px">1 Day Trek</td>
                            <td style="width: 120px">2 Days Trek</td>
                            <td style="width: 120px">3 Days Trek</td>
                        </tr>
                        <tr>
                            <td style="width: 120px">1 pax</td>
                            <td style="width: 120px">20$</td>
                            <td style="width: 120px">30$</td>
                            <td style="width: 120px">75$</td>
                            <td style="width: 120px">105$</td>
                        </tr>
                        <tr>
                            <td style="width: 120px">2 to 5 pax</td>
                            <td style="width: 120px">12$</td>
                            <td style="width: 120px">20$</td>
                            <td style="width: 120px">55$</td>
                            <td style="width: 120px">80$</td>
                        </tr>

                        <tr>
                            <td style="width: 120px">6pax and above</td>
                            <td style="width: 120px">10$</td>
                            <td style="width: 120px">15$</td>
                            <td style="width: 120px">45$</td>
                            <td style="width: 120px">65$</td>
                        </tr>

                    </table>


                    <p>All the trek are including,</p>
                    <p>1- food</p>
                    <p>2- drinking water,</p>
                    <p>3- seasonal fruits,</p>
                    <p>4- snow tower,</p>
                    <p>5- accommodation,</p>
                    <p>6- helper ( if will need)</p>
                    <p>7- bag transport to your hotel in Nyaung Shwe (or) on your boat.</p>
                    <p>Extra</p>
                    <p>jetty to nyaung shwe 20000/- kyats for 1 boat for 5 people.</p>
                    <p>under 5 people is also one boat. </p>


                    <p style="text-align: center"><a href="yangon-city-tour-reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;" /></a>


                    <p> --------------------------------------------------------------------------------------------------------

                    <h2 style="color:orange">Kalaw Itinerary </h2>

                    <h2 style="color:orange">KALAW 1/2 DAY TREK </h2>
                    <p>we need vehicle or car to starting point.</p>
                    <p>(around about 7 or 10 minutes)</p>
                    <p>1. Starting point - Pinne pin and Yeya .</p>
                    <p>Yeya village - it must be wait the car .</p>
                    <p>(around about 4 hours)</p>

                    <br>
                    <br>
                    <p>2. Starting point - Pinne pin - Myinka</p>
                    <p>(around about 4 hours)v
                    <p>kalaw 1 day trek</p>
                    <br>
                    <br>
                    <p>1.Kalaw - Tar yaw -Ywar thit - View point(lunch)</p>
                    <p>Kalaw</p>
                    <p>total -around about 7 hours walking</p>
                    <br>
                    <br>
                    <p>2. Kalaw - Pinne pin - Myin ka - Kalaw(lunch)</p>
                    <p>total -around about 7 hours walking.</p>
                    <p>3. Kalaw - lutpyin - Taung gyi chae -Myinmahti cave        (lunch) - Kalaw</p>
                    <p>total -around about 8 hours (that trip good for under 40 age)</p>
                    vKalaw 1 night 2 days trek around kalaw</p>
                    <p>Day (1)</p>
                    <p>Kalaw - Ywa thit - Tar yaw- View point (lunch) - Hinn kha gone -Myink taik (sleep)</p>
                    <p>varound about 6 1/2 or 7 hours</p>
                    <br>
                    <br>
                    <h2 style="color:orange">Day (2)-  Have 2 different ways</h2>
                    <p><1> Myin taik - station - Thit Hla - Ywar pu (lunch)- lut pyin - kalaw</p>
                    <p>around about 7 hours</p>
                        <br>
                        <br>
                    <p><2> Myin taik - station - ywar pu - lut pyin - Taung gyi chae - Myin ma hti - kalaw</p>
                    <p>around about 8 hours</p>
                        <br>
                        <br>
                    <p>Note * some of the tour , if they want a short we must try (and) change our programme.</p>
                    <p>Kalaw 1 night 2 days to Inlay lake.</p>
                    <p>Day (1)</p>
                    <p>We need vehicle or car to starting point .(40 minutes)</p>
                    <p>Lamine - Pinnwe - paw kae - kone hla (lunch) - pattu pauk (sleep)</p>
                    <p>around about 7 hours.</p>
                        <br>
                        <br>
                    <p>Day (2)</p>
                    <p>Pattu pauk - kyauk su - nan yoke - Jetty.</p>
                    <p>We have (3) different jetty . we can choice.</p>
                    <p>around about 4 or 4.5 hours.</p>
                    <p>Kalaw 2 nights 3 days trek to Inlay lake</p>
                    <p>Day (1)</p>
                    <p>Kalaw - view point - Hin kha gone _ Myin taik(lunch) - station - ywar pu (sleep)</p>
                    <p>around about 8 hours</p>
                        <br>
                        <br>
                    <p>Day (2)</p>
                    <p>Ywar pu - lamine - pinnwe - paw ke - kone hla(lunch) - pattu pauk(sleep)</p>
                    <p>around about 8 hours</p>
                        <br>
                        <br>
                    <p>Day (3)</p>
                    <p>Pattu pauk - kyauk su - nan yoke - Jetty.</p>
                    <p>around about 4 or 4.5 hours.</p>
                        <br>
                        <br>
                </div>
                <!--end column body-->
                <div class="full-column-footer"></div>
                <!--end full column footer-->

            </div>
            <!--end full column -->
            <div class="clear"></div>
        </div>
        <!--end content-wrapper-->
        <div class="clear"></div>
        <!--end content section-->

        <?php include 'footer.php'; ?>
