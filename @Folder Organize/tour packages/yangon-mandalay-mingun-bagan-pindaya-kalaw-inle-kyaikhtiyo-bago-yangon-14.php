<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon-Mandalay- Mingun- Bagan- Popa- Pindaya- Kalaw- Inle- Ngapali- Yangon</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon-Bagan-Mandalay-Inle Lake-Yangon (7Days/6Nights) Packages is complete exploring Myanmar with amazing experience .">
    <meta name="keyword" content="Yangon,Bagan,Mandalay, Inle Lake,Yangon,Myanmar Tour Packages,(7Days/6Nights)">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7">
             <h1 style="text-align: center;"><strong>Yangon-Golden Rock-Mandalay-Bagan-Inle-Ngapali-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>(14Days-13Nights)</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.D8001</h1>

</br>
</br>

<h5 style="color: orange">Day 01 Arrive to Yangon<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Arrive Yangon by morning/afternoon flight. You will be welcomed and transfer to the hotel. Have a rest at the hotel and then if time permits, sightseeing starts. Highlights include</p>
 <p>•Botahtaung Pagoda</p>
 <p>•Chauk Htat Gyi Pagoda </p>
 <p>•Sule landmark and colonial buildings</p>
 <p>•Shwedagon Pagoda </p></small>

<h5>Overnight in Yangon.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/shwedagon pagoda.jpg" style="width:300px;margin-right:10px" class="editable" alt="shwedagon pagoda" titlx="shwedagon pagoda">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: orange">Day 2 Golden Rock Day Return (by private car)<h5>
<small>
<p>Let’s get up early and starts the tour around 5am. Drive to the camp Kimpun (about 5 hours), from where the uphill motor road to hermit’s hill starts. 
Arrive Kimpun camp and take the uphill pick up truck about an hour to reach to the top of the mountain where you can see the Golden Rock(Kyaikhtiyo)- a large boulder positioned well balanced on the cliff of that mountain - seems to be defying the force of gravity from the spur of the cliff. At evening, descend the mountain to reach back to Kimpum camp. Drive back to Yangon.
En-route, visit Bago if time permit.</p></small>

<h5>Overnight in Yangon.</h5>

<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: orange">Day 3 Fly to Mandalay, Sagaing+Amarapura  full day sightseeing<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>After breakfast at the hotel, Transfer to the airport to take the flight to Mandalay. Arrived Mandalay and then start sightseeings to Sagaing and Amarapura with highlights to;</p>

<p>•Mahagandayone Monastery</p>
<p>•KaungHmuDaw Pagoda</p>
<p>•Sagaing Hill</p>
<p>•Uminthonese’ Pagoda</p>
<p>•SoonU Ponya shin</p>
<p>•Evening, enjoys sunset at U Bein’s bridge </p></small>

<h5>Overnight in Mandaly.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/at-Sagaing.jpg" style="width:300px;margin-right:10px" class="editable" alt="at-Sagaing" titlx="at-Sagaing">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange"> Day 4 Mingun- Mandalay full day sightseeing<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>After Breakfast at Hotel, start excursion  to Mingun by boat and visit; </p>
<p>•   Mingun Pagoda</p>
<p>•   Mingun bell</p>
<p>•   Myatheintan Pagoda</p>
<p>Then drive back to Mandalay. Highlight of Mandalay include;</p>
<p>•   Zay Cho Market</p>
<p>•   Mahamuni Pagoda</p>
<p>•   Golden Palace Monastery</p>
<p>•   Kuthodaw Pagoda</p>
<p>•   Enjoys sunset on Mandalay Hill </p>
 
<h5>Overnight in Mandalay.</h5>

</div>                  
                    <div style="float:right">
                        <img src="imagesnew/mandalay2.jpg" style="width:300px;margin-right:10px" class="editable" alt="mandalay2" titlx="mandalay2">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: orange">Day 05 Mandalay-Bagan(Whole Day cruise) along Ayeyarwady river<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at the hotel , Check in (6:00am) to cruise. On the way along road to Bagan. You will see breathtaking view of Myanmar. Have lunch on cruise and arrive to Bagan evening around 4pm. </p></small>

<h5>Overnight in Bagan.</h5>

</div>                  
                    <div style="float:right">
                        <img src="imagesnew/myanmarcruise.jpg" style="width:300px;margin-right:10px" class="editable" alt="myanmarcruise" titlx="myanmarcruise">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: orange">Day 06 Popa+Bagan Sightseeing <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at the hotel, drive to MOUNT POPA (45 km - 1.5 h), an extinct volcano with spectacular views and the birth place of the Myanmar 'nats' or spirits. 
Return to Bagan and sightseeing around Bagan by e-bike or Horsecart by enjoying Bagan amazing sunset. </p></small>

<h5>Overnight in Bagan.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/bagan-horsecart.jpg" style="width:300px;margin-right:10px" class="editable" alt="bagan-horsecart" titlx="bagan-horsecart">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 07 Bagan fullday sightseeing <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at the hotel, continue sightseeing around Bagan by private car. Highlights include;</p>

<p>•   Nyaung Oo Market</p>
<p>•   Shwezigon Pagoda</p>
<p>•   Ananda temple</p>
<p>•   Sulamini temple</p>
<p>•   Htilominlo Pagoda; </p>
<p>•   Dhamayangyi</p>
<p>•   late evening enjoy the sunset on top of the Shwesandaw pagoda or Bu pagoda.</p></small>


<h5>Overnight in Bagan.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/bagan-myanmar.jpg" style="width:300px;margin-right:10px" class="editable" alt="bagan-myanmar" titlx="bagan-myanmar">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 08 Bagan/Heho (Flight), Pindaya-Red Mountain full day sightseeing <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at the hotel. Then transfer to the airport to fly to Heho. Arrived Heho and proceed to Pindaya, famous Shwe U Min Pagoda (Pindaya Cave) and Botoloke Lake Then drive Back to Inle.</p>
<p>Then, head up to the Red Mountain Estate, vineyard in the hills to the east of Inle Lake for some wine tasting. One of only two vineyards in Myanmar, this definitely breaks the mould of your day-to-day Myanmar experience, making it suddenly feel as if you have been transplanted to the south of France. You can try the tasting menu for 2000 kyats (featuring wines from four different types of grape) whilst taking in the stunning sunset views over the lake.</p></small>

<h5>Overnight In Inle.</h5>

</div>                  
                    <div style="float:right">
                        <img src="imagesnew/kosamui1.jpg" style="width:300px;margin-right:10px" class="editable" alt="kosamui1" titlx="kosamui1">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day-9 Indein & Inle Full Day Sightseeing, Inle – Yangon (VIP bus) <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at the hotel. Continue sightseeing in  Inle Lake to</p>
<p>•   Indein</p>
<p>•   Ywama village</p>
<p>•   Phaungdawoo Pagoda</p>
<p>•   Ngaphe’ Chaung Monastery </p></small>

<h5>Overnight in Inle.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/inle-3.jpg" style="width:300px;margin-right:10px" class="editable" alt="inle-3" titlx="inle-3">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 10 Inle- Ngapali <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at Hotel. Transfer to airport for Thandwe flight. Upon arrival, transfer to Hotel. Free time at Ngapali Beach.</p></small>

<h5>Overnight in Ngapali.</h5>

</div>                  
                    <div style="float:right">
                        <img src="imagesnew/ngapali3.jpg" style="width:300px;margin-right:10px" class="editable" alt="ngapali3" titlx="ngapali3">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 11 Ngapali Beach <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Free time at Ngapali Beach.</p></small>

</div>  
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 12 Ngapali Beach <h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Free time at Ngapali Beach</p></small>

</div>  
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 13 Ngapali Beach - Yangon<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>After breakfast at Hotel, transfer to airport for Yangon flight. Upon arrive to Yangon, transfer to Hotel. </p>
<p>Then take Yangon circular Train to experience daily life of Yangon . Then, visit to Bogyoke Market for shopping and China town and Colonial buildings for strolling. </p></small>

<h5>Overnight in Yangon.</h5>

</div>                  
                    <div style="float:right">
                        <img src="newimages/circular train.jpg" style="width:300px;margin-right:10px" class="editable" alt="circular train" titlx="circular train">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day 14 Yangon Airport transfer<h5>

<div class="row">
             <div style="float:left;width:580px">
<small><p>Breakfast at the hotel .Free time until transfer to Yangon International Airport for Departure. </p></small>

</div>  
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h4 style=" color: red">Services Include:</h4>  
<small>
<p>   Transfers and Sightseeing by Boat and private air-conditioned vehicles </p>
<p>   Domestic Air ticket as per program</p>
<p>   Mingun Boat</p>
<p>   Truck to and from Golden Rock Mountain</p>
<p>   Circular train ticket in Yangon</p>
<p>   Accommodation 13 Nights with breakfast from hotel</p>
<p>   English Speaking Station Guide service </p></small>
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h4 style=" color: red">Services Exclude: </h4>
<small>
<p>   Meals ( Lunch / Dinner )</p>
<p>   The entry fees for sightseeing places</p>
<p>   Drinks, Beverages, Tips,  personal expenses</p>
<p>   International Airport Tax ( 10 US$ per person in Yangon ) </p>
<p>   Any other services not mentioned in the “Services Include:” section as above</p></small>
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

Cost<br>
For 2 pax, US$2390 per person<br>
For 10pax, US$1912 per person<br>
for more details, please contact hello@green-myanmar.com 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h4 style=" color: red">Remark:</h4>
<small>
<p>Itinerary may be a bit changed according to your flight time.</p>
<p>Additional Extra single charges will be add  to total cost if number of travelers is odd such as 5,7,9</p></small>

 

  
                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                          "book now" /></a></p>

            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>


