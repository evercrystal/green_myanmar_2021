<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon Bago Yangon Day Tour Myanmar</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travel agent provides yangon city tours, yangon sightseeing tours , Myanmar tours, Myanmar Tour Packages , Mandalay Bagan Inle Lake Tour packages, tours to Myanmar with best rates.">
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->
  
  <!-- Content Section -->
  
     <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7">


      <h1 style="text-align: center;"><strong>Yangon-Bago-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.YD1001</h1>

</br>
</br>

<br>
 <div>
                    <img src="newimages/kandawgyi.jpg" class="editable" alt="botataung" title="kandawgyi" style="float: left;padding-right: 15px;padding-left: 20px" width="270px" height="165px">

                    <img src="newimages/kandawgyi-lake-yangon.jpg" alt="gabaraye" class="editable" title="kandawgyi-lake-yangon" style="float: left;padding-right: 15px" width="270px" height="165px">

                    <img src="newimages/botataung.jpg" alt="shwedagon" class="editable" title="bagan Sightseeing tour" width="270px" height="165px">

                </div>

<br>
<h4>Itinerary 7:00am to 6:00pm </h4></br>

<p>&nbsp;</p>


<h4 style="color: orange">7:00 </h4><p>>> Pick up at your hotel in Yangon and drive to Bago which a city and the capital of Bago Region in Myanmar. It is located 80 km (about 50 miles) from Yangon. It has a population of 220,000.</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                        <h4 style="color: orange">8:30 am </h4>
                        <h5>Shwe Thahlaung Pagoda </h5>
                        <p>>>Shwe Thahlaung Pagoda 
Visit to Shwe Thalhyaung Pagoda. The image has a length of 55 meter and a height of 16 meter, is the second largest Reclining Buddha Image in the world.
</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                <h4 style="color: orange">9:30 am </h4>
                       <h5>Shwe Mawdaw Pagoda</h5>
 
<p>>>Visit to Shwe Mawdaw Pagoda . The Shwe Mawdaw Pagoda or 'Great Golden God Pagoda' was built around 1000 years ago and located in Bago, Myanmar. It was originally built to a height of 23 meters and was rebuilt higher several times until it finally reached its present 114 meters in 1954.

</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                        <h4 style="color: orange">10:30 am </h4>
                        <h5>Kyaikpun Pagoda </h5>
                        <p>>>Visit to Kyaikpun Pagoda. Kyaikpun Pagoda has four gigantic Buddha images all in sitting back to back against a brick pillar. This pagoda was buit by King Dhammazedi in A.D. 1476. Impressive images are 27 meters height and keep in a fair state of preservation. 
</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h4 style="color: orange">11:20 am </h4><p>>>Visit to Myatharlyaung Buddha Image. Myatharlyaung Buddha Image is located nearby the famous Shwetharlyaung Buddha Image. It is another huge reclining Buddha image. It is out in the open without any cover of any sort.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: orange">12:00 am </h4><p>>>Lunch break at Myanmar traditional restaurant.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: orange">13:00  </h4><p>>>Visit to Mahazedi Pagoda. It is one of the revered pagodas in Bago. The Pagoda was built by King Bayinnaung in 1560 A.D. It is known that the pagoda enshrines a tooth-relic brought from Sri Lanka.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: orange">14:00</h4><p>>>Visit to Hinthagone Pagoda.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: orange">15:00 </h4><p> >>Visit to Shwegugale Pagoda. Extremely mouldy and old-looking but beautiful is the Shwegugale Pagoda. Unlike any temples, the stupa is a circular tunnel where 64 Buddha statues are kept. The pagoda dates to 1494 and the reign of Myanmar King Byinnya Yan.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: orange">16:30 </h4><p>>>Back to Yangon.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: orange">18:00 </h4><p>>>Arrive Yangon and Transfer to the Hotel.</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Service Include : </h4><p>Transfer and Sightseeing by private air-conditioned vehicle as per program</p>
<p>English Speaking  Guide  </p>
<p>Drinking water 1 bottle</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Services Exclude : </h4><p>Meals (lunch/dinner)</p>
<p>Entry fees for sightseeing places / Zone fees</p>
<p>Camera fees</p>
<p>Drinks, Beverages, Tips, Personal Expenses</p>
<p>Any other services not mentioned in the “Services Include” section above</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3> 
<h4>For 2 pax, US$74 per person</h4>
<h4>For 10pax, US$22 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>
                </div></div>

</div>

      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
