<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon City Tour - Yangon Sightseeing - Yangon Highlights Full Day Yangon Sightseeing </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Experience the city sights with bus tour,to explore Yangon's famous places Shwedagon pagoda, Sule pagoda, Botahtaung Pagoda & Jetty, Kandawgyi lake, Bogyoke Aung San Museum, 6 storey Budda , Sule landmark , Yangon heritage">
    <meta name="keyword" content="Myanmar Travel Agent, Yangon Highlights, Yangon city tour, Yangon Sightseeing, Yangon explorer ,Shwedagon pagoda, Sule pagoda, Botahtaung Pagoda & Jetty, Kandawgyi lake, Bogyoke Aung San Museum, 6 storey Budda , Sule landmark , Yangon heritage">    <meta content="en" http-equiv="Content-Language">

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>

    <!--end class dropdown-->

</div>
<!--end Header-->






<!--end Header-->

<!-- Content Section -->

<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="full-column" >
        <div class="full-column-header"></div>
        <!--end full column header-->
        <div class="full-column-body editable">
            <div id="main">
                <h2>Yangon City Sightseeing Tour - Yangon Sightseeing One day with Yangon Highlights</h2><br>

                <h5 style="color:white"><b>Lowest Price Starts from 79 USD per person for all inclusive - contact hello@green-myanmar.com for more information</h5>

                <br />
                <br />
            </div>
            <div>
                <img src="newimages/botataung.jpg" class="editable" alt="botataung" title="Botataung pagoda" style="float: left;padding-right: 15px;padding-left: 20px">

                <img src="newimages/gabaraye.jpg" alt="gabaraye" class="editable" title="Gabaraye pagoda" style="float: left;padding-right: 15px">

                <img src="newimages/thestrand.jpg" alt="shwedagon" class="editable" title="the strand hotel" width="270px" height="165px">

            </div>
            <br />
            <br />
            <p style="text-align: center"><a href="yangon-city-tour-reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;" /></a>
               <!-- <a style="color:orange; float:right" href="yangon-one-day-city-tour.pdf">Download Yangon City Tour Itinerary</a> -->
            </p>
            <br />
            <h4 style="color: orange">Morning Tour</h4>
            <br />
            <p>08:00 am   >> Pick up from Hotel or client's location. Visit to Botataung Pagoda and explore the daily lives of people
                around jetty and Yangon </p> <p style="margin-left: 80px">River view for photoshoot(1 hrs).
        </p>
            <p>09:00 am   >> Visit to City center landmark Sule Pagoda, Mahabandoola Park  &
                downtown of Yangon with remarkable sites of Yangon Colonial Buildings </p> <p style="margin-left: 80px">and City Hall. </p>

            <p>10:00 am   >> Visit to Royal Palace(Karaweik)& Kandawgyi Lake for sunset view
                with glory of famous Shwedagon Pagoda from the Park.</p>

            <p>11:30 am   >> Visit to Chaukhtat Gyi,the biggest colossal reclining image in
                Myanmar.</p>

            <p>12:30 am   >> Visit to Kabar Aye Pagoda(world peace pagoda)</p>

            <p style="margin-left: 80px">  - - - - -            Lunch at local Restaurant       - - - - - - -</p>

            <h4 style="color: orange">Afternoon Tour</h4>

            <br />

            <p>14:30 pm   >> Visit to Bogyoke Market,the most famous for shopping with full of Myanmar arts,Handicrafts,Lacquerware & Jewelleries.</p>

            <p>16:00 pm   >> Visit to World famous Great Golden Shwedagon Pagoda.</p>

            <p>18:00 pm   >> End of Tour & back to Hotel,Yangon. Transfer back to Hotel.</p>

            <br />
            <br />

            <h4 style="color: orange">You can include following optional with tour </h4>

            <br />

            <br />

            <p>Option   ( 1 )  if you would like to include Afternoon Tea at Strand Hotel  - 25 USD per person </p>


            <p>Remark: Gem Museum, National Museum close on Monday, Tuesday & Gazette Holidays.
                Bogyoke Market closes on Monday.
            </p>

            <p><a href="yangon-attrations-tours.php">More options for Yangon City Tour - Yangon Attractions</a></p>


            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
            <div>
                <h5 style="color:red">Services Include:</h5><br>
                <p style="color:white"> -  Transfers and Sightseeing by private air-conditioned vehicles </p>
                <p style="color:white"> -  Local English Speaking  Guide </p>
                <p style="color:white"> Nice Myanmar Traditional Lunch </p
                <p style="color:white"> -  The entry fees for sightseeing places </p>
                <p style="color:white"> -  Drinking water 1 bottle </p>
                <br>

                <h5 style="color:red">Services Exclude:</h5><br>


                <p style="color:white"> - Drinks, Beverages, Tips,  personal expenses </p>

                <p style="color:white"> -  Any other services not mentioned in the “Services Include:” section as above</p>
            </div>


            <h4 style="color: orange">Cost per person: </h4>
            <p style="color:white">  1 pax  - from USD $ 125 </p>
            <p style="color:white">  2 pax - from USD $ 78</p>
            <p style="color:white">  3 pax - from USD $ 55</p>
            <p style="color:white">  15 & above (1 FOC) - from  USD $ 50 <br/>

            <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

            <img src="newimages/botataung.jpg" class="editable" alt="botataung" title="Botataung pagoda">
            <h5>1.Botahtaung Pagoda & Jetty</h5>
            <p>This is 2500-year old pagoda was originally called Kyaik-de-att,and was later named after the 1000 military generals who brought the Buddha hair relic from India to Myanmar.</p>
            <img src="newimages/sule.jpg" alt="sule" class="editable" title="Sule pagoda">
            <h5>2.Sule Pagoda</h5>
            <p>The pagoda is located right in the center of the city.In fact the pagoda itself is in the middle of a traffic circle.It is said to be over 2000 years old and to contain a hair relic of Buddha.</p>
            <img src="newimages/bogyokemarket.jpg" alt="bogyoke" class="editable" title="Bogyoke market">
            <h5>3.Bogyoke Market</h5>
            <p>Formerly known as Scott Market,this is the most famous local market in Yangon.The large colonial building was built in 1926.There are 1641 shops in the market and almost anything you can imagine is on sale.</p>
            <img src="newimages/nationalmuseum.jpg" alt="national" class="editable" title="National museum">
            <h5>4.National Museum</h5>
            <p>The five-story museum house a priceless collection of artifacts and items of historical significance.The highlight of the collection is the Sihasana(Lion Throne)which belonged to King Thibaw.</p>
            <img src="newimages/gabaraye.jpg" alt="gabaraye" class="editable" title="Gabaraye pagoda">
            <h5>5.Gabar Aye Pagoda</h5>
            <p>This is a new pagoda constructed in 1952,and was built for the sixth Buddhist synod,held from 1954 to 1956.The name means 'world peace' in English,and is known as the 'World Peace Pagoda'.</p>
            <img src="newimages/gem.jpg" alt="gems" class="editable" title="Gems museum">
            <h5>6.Gems Museum</h5>
            <p>It display very famous gem stones of Myanmar such as rubies,jades,sapphires an Pearls.It also houses the Gems Mart.</p>
            <img src="newimages/pagoda.jpg" alt="pagoda" class="editable" title="Chaukhtat Gyi pagoda">
            <h5>7.Chaukhtat Gyi Pagoda</h5>
            <p>Originally constructed in 1907,the reclining Buddha is 72 meters long and is the biggest reclining Buddha image in Myanmar.However,it suffered damage over the years,and reconstruction was started in 1957 and completed 1966.</p>
            <img src="newimages/bogyokemuseum.jpg" alt="bogyokemuseum" class="editable" title="Bogyoke museum">
            <h5>8.<a href="http://en.wikipedia.org/wiki/Bogyoke_Aung_San_Museum"> Bogyoke Museum </a></h5>
            <p>This was the house where General Aung San lived,with his wife,Daw Khin Kyi,and their three children.The 1920s house is still in original condition,and has many interesting items on display including Aung San's car,his library,photos and his famous suit.</p>
            <img src="newimages/kandawgyi.jpg" alt="kandawgyi" class="editable" title="Kandawgyi lake and Karaweik">
            <h5>9.<a href="http://karaweikpalace.com/">Karawait Palace</a>  & <a href="http://en.wikipedia.org/wiki/Kandawgyi_Lake">Kandawgyi Lake</a></h5>
            <p>This large lake is just north of downtown,and around it is a park and recreation area.It's a busy roads around it.The lake is best known for the karaweik,a huge concrete replica of a royal Burmese Barge.</p>
            <img src="newimages/shwedagon.jpg" alt="shwedagon" class="editable" title="Shwedagon pagoda">
            <h5>10. <a href="http://www.shwedagonpagoda.com/"> Shwedagon Pagoda </a></h5>
            <p>The largest and most important pagoda in Myanmar is the Shwe Dagon Pagoda.It rises almost 100 meters above the greer cityscape of Yangon.</p>

            <img src="newimages/thestrand.jpg" alt="shwedagon" class="editable" title="the strand hotel" width="250px" height="150px">
            <h5>11. <a href="http://www.hotelthestrand.com/" >The Strand Hotel </a></h5>
            <p>The Strand Hotel in Yangon is one of the most iconic hotels in Myanmar. Built in 1901, it remains as awe inspiring as it was in the early 20th Century.</p>
            <a href="yangon-city-tour-reservation.php"><img src="images/booking-button.jpg" class="editable" alt="booking button" title="book now" style="margin: 5px 0px 5px 5px;"/></a>

        </div>
        <!--end column body-->
        <div class="full-column-footer"></div>
        <!--end full column footer-->

    </div>
    <!--end full column -->
    <div class="clear"></div>
</div>
<!--end content-wrapper-->
<div class="clear"></div>
<!--end content section-->

<?php include 'footer.php'; ?>

<a href="http://casino.us.org/review/coolcat-casino/">casino.us.org/review/coolcat-casino/</a>
