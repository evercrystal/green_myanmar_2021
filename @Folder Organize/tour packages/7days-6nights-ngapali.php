<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


    <title>Yangon-Bagan-Ngapali Beach-Yangon(7Days-6Nights)</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travel agent provides yangon city tours, yangon sightseeing tours , Myanmar tours, Myanmar Tour Packages , Mandalay Bagan Inle Lake Tour packages, tours to Myanmar with best rates.">
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->
  
  <!-- Content Section -->
  
     <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7">
      <h1 style="text-align: center;"><strong>Yangon-Bagan-Ngapali Beach-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>(7Days-6Nights)</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.D7002BN</h1>

</br>
</br>
   
<h5 style="color: orange">Day- 1 Arrive Yangon  <h5>
<div class="row">
                    <div style="float:left;width:480px"><small>
<p>Arrive Yangon and you will be welcomed by our tour guide and transfer to hotel for a short rest. If time permit,  start sightseeing with highlights to</p>

<p>•   Botahtaung Pagoda</p>
<p>•   ChaukHtetgyi Pagoda</p>
<p>•   Kandawgyi Nature park</p>
<p>•   World famous Shwedagon Pagoda</p>

<h5>Overnight in Yangon.</h5></small>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="imagesnew/yangon5.jpg" style="width:300px;" class="editable" alt="yangon5" title="yangon5">  
                    </div>

</div>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 02 Fly to Bagan, Bagan full day Sightseeing by private car   <h5>
<div class="row">
                    <div style="float:left;width:480px"><small>
<p>Breakfast at the hotel. Then start sightseeing in Bagan. Highlight include;</p>

<p>•    Ananda Pagoda</p>
<p>•   Thabanyu Temple</p>
<p>•   Dammayangyi Pagoda</p>
<p>•   Htilominlo Pagoda</p>
<p>•   Shwezigon Pagoda</p>
<p>•   Bu Pagoda</p>
<p>•   Gawdotplin Temple</p>
<p>•   Manuha Temple</p>
<p>Enjoying amazing sunset of Bagan, take E-bike around Bagan.</p>

<h5>Overnight in Bagan.</h5></small>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="imagesnew/bagan3.jpg" style="width:300px;" class="editable" alt="bagan3" title="bagan3">  
                    </div>

</div>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day-3 Bagan full day sightseeing  <h5>
<div class="row">
                    <div style="float:left;width:480px"><small>
<p>Breakfast at the hotel. Then continue sightseeing around Bagan. Highlight places include-</p>
<p>•   Sulamuni Temple</p>
<p>•   Bu Pagoda</p>
<p>•   Gubyauk kyi Temple</p>
<p>•   Nann Pagoda</p>
<p>•   Pyathetgyi Pagoda</p>
<p>•   Lawka Nandar Pagoda</p>


<h5>Overnight in Bagan.</h5></small>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="imagesnew/wine-taste.jpg" style="width:300px;" class="editable" alt="wine-taste" title="wine-taste">  
                    </div>

</div>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: orange">Day-4 Fly to Thandwe, free time in Ngapali<h5>
<small>
<p>After breakfast, transfer to airport to fly to Thandwe. Arrive Thandwe and transfer to hotel in Ngapali. Free time at Ngapali beach.</p>

<h5>Overnight in Ngapali.</h5></small>


<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day- 5 Ngapali beach<h5>
<small>
<p>Free time and relax at Ngapali Beach.</p>
</small>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day- 6 Ngapali <h5>
<small>
<p>Free time and relax at Ngapali Beach.</p>
</small>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day-7 Ngapali-Yangon (flight)<h5>
<small>
<p>Breakfast at the hotel. Transfer to airport and fly back to Yangon. </p>
</small>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Service Include : </h4>
<p>    Transfers and Sightseeing private air-conditioned vehicles </p>
<p>   Accommodation 6 Nights with breakfast from hotel</p>
<p>   Domestic Air ticket as per program </p>
<p>   Horse cart or E-Bike in Bagan</p>
<p>   English Speaking Station Guide service in Bagan</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Services Exclude : </h4>
<p>    Meals ( Lunch / Dinner )</p>
<p>   The entry fees for sightseeing places</p>
<p>   Drinks, Beverages, Tips,  personal expenses</p>
<p>   International Airport Tax ( 10 US$ per person in Yangon ) </p>
<p>   Any other services not mentioned in the “Services Include:” section as above</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3> 
<h4>For 2 pax, US$1177 per person</h4>
<h4>For 10pax, US$1064 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>
                </div>

      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
