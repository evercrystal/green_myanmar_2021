<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon - Phoe Kyar Elephant Camp – Bagan – Heho – Kalaw (Trekking) – Inle Lake – Yangon ( 10 Days/ 9 Nights)</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon - Phoe Kyar Elephant Camp – Bagan – Heho – Kalaw (Trekking) – Inle Lake – Yangon( 10 Days/ 9 Nights)">
    <meta name="keyword" content="Yangon , Phoe Kyar Elephant Camp , Bagan , Heho , Kalaw (Trekking) , Inle Lake , Yangon , Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7"><h2>Yangon - Phoe Kyar Elephant Camp – Bagan – Heho – Kalaw (Trekking) – Inle Lake – Yangon( 10 Days/ 9 Nights)
            </h2>

                <p><img alt="inle lake tour" class="image-right editable" src="images/images/inlay.jpg" title=
                        "Shwedagon pagoda" /></p>

                <h3 style="color: orange">Lowest Price starts from 999 USD per person</h3>

                <h4 style="color: orange">Yangon - Phoe Kyar Elephant Camp – Bagan – Heho – Kalaw (Trekking) – Inle Lake – Yangon ( 10 Days/ 9 Nights) </h4>
                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                          "book now" /></a></p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange"> Day 01: Arrive Yangon , Yangon full day sightseeing  (- / - / - ) </h4>
                <p>Arrive Yangon by morning flight. Welcomed by your tour guide and start the full day tour of Yangon commences with a visit to Botataung Pagoda and Jetty, proceed to city center and Sule Pagoda, the landmark of downtown Yangon, followed by Chaukhtatgyi - the colossal reclining Buddha, Kandawgyi (Royal) Lake for photographing the panoramic view of the lake overlooking the Shwedagon Pagoda. </p>
                <p>Afternoon sightseeing to Shwedagon Pagoda, an inspiring golden stupa which is one of the best landmarks in Myanmar. </p>
                <p>Overnight at Yangon. </p>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange"> Day 02: Yangon to Phoe Kyar Elephant Camp (B / L /D ) </h4>
                <p>Breakfast at the hotel. Drive to Phoe Khyar Elephant Camp about 5 hrs. We can  see there, natural forest plants including different species of  hardwood, large forests teak wood, other valuable forest plants different life span (middle age – young age)   bamboos, cane and different flora. </p>
                <p>- Elephant riding </p>
                <p>- Elephant Bathing </p>
                <p>- Bringing up of the young elephants.</p>
                <p>- Studying the nature of forest.</p>
                <p>- Flora & Fauna Survey</p>
                <p>Overnight at Phoe Kyar Forest Resort.</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange"> Day 03: Back to Yangon (B / L /- ) </h4>
                <p>Breakfast at the Resort. Visit in the forest by elephant riding. After luch back to Yangon </p>
                <p>Overnight at Yangon. </p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 04: Yangon to Bagan by morning flight, Bagan Sightseeing  (B / - / - ) </h4>
                <p>Early morning flight to Bagan (YH-917, 06:10-07:25). Sightseeing at the Asia’s most popular archaeological destinations and one out of two most pre-eminent religious old cities in South East Asia. Visit to Nyaung Oo market – a typical local market; followed by visits to most distinctive pagodas such as Shwezigon Pagoda – the holiest one believed to be enshrined with Buddha tooth and hair; Gubyaukgyi Temple (Wetkyiinn village) – the 13th century ‘cave temple’ with fine frescoes of scenes from Jakatas; Anada Temple – one of the masterpieces surviving and remaining in Bagan and considered to be in the Mon architecture style. </p>
                <p>Afternoon, we will visit a lacquer ware factory, followed by a visit to local villages. </p>
                <p>Overnight at Bagan. </p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 05: Bagan (B / - / - ) </h4>
                <p>Breakfast at the hotel.  Free & easy in Bagan by cycling among the pagodas.. </p>
                <p>Overnight at Bagan. </p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                <h4 style="color: orange">Day 06: Bagan-Heho by morning flight, drive to Kalaw  (B / - / - ) </h4>
                <p>After breakfast we will take the morning flight to Heho (YH-918, 08:30-09:00). Drive to Kalaw; Kalaw is an old British hill resort, stands at the altitude of 1,315 meters, is also known as Pine Land because of its abundant in pine trees. The sightseeing includes: Nee Paya (Bamboo strip lacquer Buddha image) and Shweumin natural caves; taking along the up-down meandering roads of Kalaw to have the panoramic view of Kalaw; old colonial style buildings from a hillock opposite of Kalaw. </p>
                <p>Overnight at Kalaw </p>
                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                <h4>Day 07 : Trekking to Villages (B / L /D )</h4>
                <p>Breakfast at the hotel. Visit to Kalaw Market and drive to Baw Hnin Kon (Danu Village) on the Pin Laung - Loikaw Road to start the trekking. Let your Car & Driver proceed to Inle Lake with the baggage. </p>
                <p>1 ½ hr trekking to Tun Kong (Pa O Village) & Have a Lunch there. 1 hr to Pattu Pauk (Pa O Village), ½ hr to Hti Thein (Pa O Village). </p>
                <p>Dinner & Overnight at the Village Monastery . </p>
                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 08 : Trekking to Indein Village, Sightseeing at Inle Lake (B / L /- ) </h4>
                <p>After breakfast depart from Hti Thein village. After 3 hr trekking arrive to Indein Village, This small village located on the southwestern bank of the lake is famous for its ruined clusters of pagodas dated back to 16th century. The tranquil peaceful ambience and the ruins overgrown with bushes will bring you centuries back. </p>
                <p>Meet your driver & take a boat trip to your hotel. </p>
                <p>Overnight at Inle Lake. </p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 09: Inle Sightseeing  (B / - / - )</h4>
                <p>Breakfast at the hotel. Sightseeing at Inle Lake; one of the magical lakes in Myanmar located 900 meters above sea level surrounded by beautiful mountains of Shan plateau, is the home land of the Inthar tribe. We will visit Phaungdawoo Pagoda – famous for its richly gilded five small Buddha images; Ngaphechaung monastery (also known as jumping cat monastery) was built more than one and a half century ago and features a number of interesting old Buddha Images. Other attractions include: unique leg-rowing style and fishing method of the Inthar, floating gardens used as vegetable plantations, silk and cotton weaving cottages and black smith.</p>
                <p>Overnight at Inle Lake </p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 10: Inle-Yangon by Morning Flight  (B / - / - )</h4>
                <p>Breakfast  at the hotel. Transfer to Heho Airport to take the morning flight back to Yangon. Free shopping at Bogyoke Market and transfer back to International Airport for your Evening Departure flight. </p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Cost per person in twin sharing basic: </h4>
                <p>Pax	Rate </p>
                <p>1	USD $2,640</p>
                <p>2	USD $ 1,668</p>
                <p>3-5	USD $ 1,506</p>
                <p>6-9	USD $ 1,320</p>
                <p>10-14	USD $ 1,254</p>
                <p>15 & above (1 FOC)	USD $ 1,272</p>
                <h4 style="color: orange">Cities	Hotels (3 Stars) </h4>
                <p> <a href="yangon.php">Yangon</a> 	<a href="http://hotel7mile.com/"> Hotel 7 Mile </a> (Superior) or similar</p>
                <p><a href="bagan.php">Bagan</a> 	<a href="http://www.baganthandehotel.net/"> Bagan Thande Hotel </a> (Superior) or similar</p>
                <p>Kalaw	Dream Villa Resort (Superior) or similar</p>
                <p>Inle Lake	<a href="http://www.gichotelgroup.com/"> Inle Resort (Garden Cottage) </a> or similar</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <p> For the package rate , Please contact hello@green-myanmar.com</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>


                <h4 style="color:red">Services Include: </h4>
                <p style="color:white"> - Transfers and Sightseeing by Boat and private air-conditioned vehicles  </p>
                <p style="color:white"> - Accommodation 9 Nights with daily breakfast at 3 stars hotel </p>
                <p style="color:white"> - Domestic Air tickets as per program(Rgn-Bgn-Inle-Rgn) </p>
                <p style="color:white"> - Domestic Airport Tax </p>
                <p style="color:white"> - Station English speaking Guide (each city –different guide)</p>
                <p style="color:white"> - The entry fees for sightseeing places </p>
                <p style="color:white"> - Drinking water 1 bottle per day </p>
                <p style="color:white"> - Postage at the airport and boat stand </p>
                <h3>Services Exclude: </h3>
                <p style="color:white"> - Meals ( Lunch / Dinner ) </p>
                <p style="color:white"> - Drinks, Beverages, Tips,  personal expenses </p>
                <p style="color:white"> - International Airport Tax ( 10 US$ per person in Yangon ) </p>
                <p style="color:white"> - Any other services not mentioned in the “Services Include:” section as above </p>
                <h5>Proposed date:	 1st October 2014 </h5>
                <h5>Validity: 1st Oct 2014 to 31 Mar 2015 (Except Christmas & New Year period)</h5>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>



                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
                                                          "book now" /></a></p></div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>
