<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



	<title>Yangon One Day City Tour - Yangon Sightseeing - Yangon Highlights Full Day Yangon Sightseeing </title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Experience the city sights with bus tour,to explore Yangon's famous places Shwedagon pagoda, Sule pagoda, Botahtaung Pagoda & Jetty, Kandawgyi lake, Bogyoke Aung San Museum, 6 storey Budda , Sule landmark , Yangon heritage">
	<meta name="keyword" content="Myanmar Travel Agent, Yangon Highlights, Yangon city tour, Yangon Sightseeing, Yangon explorer ,Shwedagon pagoda, Sule pagoda, Botahtaung Pagoda & Jetty, Kandawgyi lake, Bogyoke Aung San Museum, 6 storey Budda , Sule landmark , Yangon heritage">    <meta content="en" http-equiv="Content-Language">

	<!-- including Style sheets -->
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
	<link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

	<!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
	<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/coin-slider.min.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

	<meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

	<!-- END BuySellAds.com Ad Code -->
	<!--[if IE 6]>
	<script src="js/DD_belatedPNG_0.0.8a.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
	</script>
	<![endif]-->
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

	<!-- google analyzer -->
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-38877742-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<!-- End of google analyzer  -->


	<!-- BEGIN JIVOSITE CODE {literal} -->
	<!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
	<script type='text/javascript'>
		(function(){ var widget_id = '47889';
			var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
	<!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

	<!--Begin Header Section-->

	<div id="header" >
		<h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
		<div id="coin-slider">
			<ul>
				<li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
				<li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
				<li><img src="images/3.jpg" alt="inle" title="inle"/></li>
				<li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
				<li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
			</ul>
		</div>
		<!-- end Slider -->

		<img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
		<?php include 'menu.php'; ?>

		<!--end class dropdown-->

	</div>
	<!--end Header-->






	<!--end Header-->

	<!-- Content Section -->

	<div id="content-wrapper" >
		<div id="content-header" >
			<h2 id="welcome"></h2>
			<form id="search-form" action="" method="post">
				<input id="searchInput" name="searchInput" type="text" />
				<input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
			</form>
			<ul id="button">
				<li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
				<li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
				<li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
			</ul>
			<br class="clear"/>
		</div>
		<!--end content_header-->

		<!--begin main column-->
		<div id="full-column" >
			<div class="full-column-header"></div>
			<!--end full column header-->
			<div class="full-column-body editable">
				<div id="main">
				
                <h1 style="text-align: center;font-size: 23px"><strong>Yangon One Day City Tour</strong></h1>
<h1 style="text-align: center;font-size: 23px"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;font-size: 23px">Tour code: GMBF.YD1004</h1>
                    <p style="margin-left:100px">If you would like to make online booking or find out more about price details,  please click <a href="https://myanmardaytrips.com"> here ( Yangon city tour details ) </a></p>

</br>
</br>

				</div>
				<div>
					<img src="newimages/botataung.jpg" class="editable" alt="botataung" title="Botataung pagoda" style="float: left;padding-right: 15px;padding-left: 20px">

					<img src="newimages/gabaraye.jpg" alt="gabaraye" class="editable" title="Gabaraye pagoda" style="float: left;padding-right: 15px">

					<img src="newimages/thestrand.jpg" alt="shwedagon" class="editable" title="the strand hotel" width="270px" height="165px">

				</div>
				<br />
				<br />
				
			
				<h5 style="color: orange">08:00 – 11:59</h5> <p>>> Visit to Botataung Pagoda and explore the daily lives of people around jetty and Yangon River view for photoshoot(1 hrs).</p>
				                      <p>>> Visit to City center landmark Sule Pagoda, Mahabandoola Park and downtown of Yangon with remarkable sites of Colonial Buildings & City Hall.</p>
				                      <p>>> Visit to Chaukhtat Gyi, the biggest colossal reclining image in Myanmar.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

				<h5 style="color: orange">12:00 – 13:00</h5> <p>>> Lunch at Local Restaurant </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

				<h5 style="color: orange">13:00 – 18:00</h5> <p>>> Visit to Bogyoke Market,the most famous for shopping with full of Myanmar arts,Handicrafts,Lacquerware & Jewelleries.</p>
				                      <p>>> Visit to Bogyoke Museum</p>
				                      <p>>> Visit to Royal Palace(Karaweik) & Kandawgyi Lake</p>
				                      <p>>> Visit to World famous Great Golden Shwedagon Pagoda.</p>
				                      <p>>> End of Tour & back to Hotel,Yangon….</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

				<h5 style="color: orange">You can include following optional with tour </h5>


				<p>Option   ( 1 )  if you would like to include Afternoon Tea at Strand Hotel  - 25 USD per person </p>


				<p>Remark: Gem Museum, National Museum close on Monday, Tuesday & Gazette Holidays.
					Bogyoke Market closes on Monday.
				</p>

			   <p><a href="yangon-attrations-tours.php">More options for Yangon City Tour - Yangon Attractions</a></p>


<h4 style="color: black"> --------------------------------------------------------------------------------------------------------------------------------------- </h4>
				<div>
					<h5 style="color:red">Services Include:</h5><br>
					<p style="color:white"> -  Transfers and Sightseeing by private air-conditioned vehicles </p>
					<p style="color:white"> -  Local English Speaking  Guide </p>
					<p style="color:white"> -  Drinking water 1 bottle </p>
					<br>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

					<h5 style="color:red">Services Exclude:</h5><br>


					<p style="color:white"> - Meals(lunch/ dinner) </p>
					<p style="color:white"> - Drinks, Beverages, Tips,  personal expenses </p>
					<p style="color:white"> - The entry fees for sightseeing places </p>
					<p style="color:white">   Camera fees, other usages and any other services not mentioned in the “Services Include:” section as above </p>
					<p style="color:white"> -  Any other services not mentioned in the ÒServices Include:Ó section as above</p>
				</div>


				<h4 style="color: black"> --------------------------------------------------------------------------------------------------------------------------------------- </h4>

<h3>Cost</h3>
                <a style="font-size: 30px" href="http://myanmardaytrips.com/dailytour_detail/2">Find Yangon City Tour price here</a>

<h4>for more details,please contact hello@green-myanmar.com  </h4>

				<a href="yangon-city-tour-reservation.php"><img src="images/booking-button.jpg" class="editable" alt="booking button" title="book now" style="margin: 5px 0px 5px 5px;"/></a>

			</div>
			<!--end column body-->
			<div class="full-column-footer"></div>
			<!--end full column footer-->

		</div>
		<!--end full column -->
		<div class="clear"></div>
	</div>
	<!--end content-wrapper-->
	<div class="clear"></div>
	<!--end content section-->

<?php include 'footer.php'; ?>

