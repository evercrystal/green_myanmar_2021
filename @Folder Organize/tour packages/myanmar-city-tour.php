<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Myanmar Tour Packages ,Myanmar Travel Packages, Myanmar Packages Tour, Yangon Package Tour, Mandalay Package Tour, Bagan Package Tour</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Best Myanmar Travel Agent Providing all range of Myanmar packages tours from Budget to Luxury inclusive of Myanmar Classic and Culture Tours and Myanmar Adventure Tour" />
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Package Tour, Myanamr Luxury Tour, Myanmar Adventure Tour, Myanmar Classic Tour , Myanmar Culture Tour">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>
    <!--end class dropdown-->

</div>
<!--end Header-->


<!--end Header-->

<!-- Content Section -->

<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="main-column" >
        <div class="column-header"></div>
        <!--end column header-->
        <div class="column-body editable">
            <h2 id="column-header editable">Welcome To <span class="green">Green Myanmar Travel</span></h2>
            <p>Green Myanmar Travel is providing variety of Travel Service in World Wide and All destination in Myanmar. </p>
            <p>Please find best deal packages. </p>
        </div>
        <!--end column body-->
        <div class="column-footer"></div>
        <div class="editable">
            <a href="yangon.php"><h4>Wonderful Myanmar</h4></a>
            <div class="destination-bg">
                <div class="tri" style="width:100%">
                    <ul>
                        <li><a href="wonders-of-myanmar-12days.php">Wonders of Myanmar 12 Days </a></li>
                    </ul>

                    <ul>
                        <li><a href="myanmar-trekking-tour.php">Taste of Myanmar with trekking 10 days  </a></li>
                    </ul>

                    <ul>
                        <li><a href="yangon-bagan-mandalay-inle-yangon.php">Yangon-Bagan-Mandalay-Inle Lake-Yangon (7Days/6Nights)
                        </a></li>
                    </ul>
                    <ul>
                        <li><a href="phoekyarelephantcamp–bagan.php">Yangon - Phoe Kyar Elephant Camp – Bagan – Heho – Kalaw (Trekking) – Inle Lake – Yangon( 10 Days/ 9 Nights)

                        </a></li>
                    </ul>

                    <ul>
                        <li><a href="yangon-bagan–mandalay-pindaya–kalaw-inlelake–ngapalibeach-yangon.php">Yangon - Bagan – Mandalay - Pindaya – Kalaw - Inle Lake –Ngapali Beach-Yangon

                            (14 Days/13 Nights)


                        </a></li>
                    </ul>

                    <ul>
                        <li><a href="myanmar-budget-tour.php">Wonders of Myanmar 10 Days Budget tour with Coach </a> (749 USD per person for 2 pax )</li>
                    </ul>

                </div>
                <!--end tri div-->
                <!-- <div class="tri">
                    <ul>
                        <li><a href="ngwesaung.php">Ngwesaung</a></li>
                        <li><a href="naypyitaw.php">Naypyidaw</a></li>
                        <li><a href="ngapali.php">Ngapali</a></li>
                    </ul>
                </div> -->
                <!--end tri div-->

                <!-- <div class="tri-last">
                    <ul>
                        <li><a href="mandalay.php">Mandalay</a></li>
                        <li><a href="taunggyi.php">Taunggyi</a></li>
                        <li><a href="yangon.php">Yangon</a></li>
                    </ul>
                </div> -->
                <!--end tri-last div-->
            </div>
        <!--end column header-->
        <div class="editable">
        <a href="yangon.php"><h4>Yangon</h4></a>
        <div class="destination-bg">
            <div class="tri" style="width:300px">
                <ul>
                    <li><a href="yangon-city-tour.php">Yangon City Tour</a></li>
                    <li><a href="yangon-river-cruise-tour.php">Yangon River Cruise Tour</a></li>
                    <li><a href="yangon-thanlyin-tour.php">Yangon - Thanlyin Tour</a></li>
                </ul>
            </div>
            <!--end tri div-->
            <!-- <div class="tri">
                <ul>
                    <li><a href="ngwesaung.php">Ngwesaung</a></li>
                    <li><a href="naypyitaw.php">Naypyidaw</a></li>
                    <li><a href="ngapali.php">Ngapali</a></li>
                </ul>
            </div> -->
            <!--end tri div-->

            <!-- <div class="tri-last">
                <ul>
                    <li><a href="mandalay.php">Mandalay</a></li>
                    <li><a href="taunggyi.php">Taunggyi</a></li>
                    <li><a href="yangon.php">Yangon</a></li>
                </ul>
            </div> -->
            <!--end tri-last div-->
        </div>
       <a href="thailand.php"><h4>Mandalay</h4></a>
        <div class="destination-bg">
            <div class="tri" style="width:300px">
                <ul>
                    <li><a href="mandalay-city-sightseeing-day-tour.php">Mandalay City tour</a></li>
                    <li><a href="reservation.php">Mandalay Saging Tour</a></li>
                    <!-- li><a href="chiangmai.php">Chiang Mai</a></li>-->
                </ul>
            </div>
            <!--end tri div-->
            <!-- <div class="tri">
                <ul>
                    <li><a href="huahin.php">Hua Hin</a></li>
                    <li><a href="kanchanaburi.php">Kanchanaburi</a></li>
                    <li><a href="kohsamui.php">Koh Samui</a></li>
                </ul>
            </div> -->
            <!--end tri div-->

            <!--<div class="tri-last">
                <ul>
                    <li><a href="krabi.php">Krabi</a></li>
                    <li><a href="phuket.php">Phuket</a></li>
                    <li><a href="pattaya.php">Pattaya</a></li>
                </ul>
            </div>-->
            <!--end tri-last div-->
        </div>
        <a href="bagan.php"><h4>Bagan</h4></a>
        <div class="destination-bg">
            <div class="tri" style="width:300px">
                <ul>
                    <li><a href="bagan-sightseeing-day-tour.php">Bagan Sightseeing Tour</a></li>
                    <!--<li><a href="jurong.php">Jurong Bird Park</a></li>
                    <li><a href="nightsafari.php">Night Safari</a></li> -->
                </ul>
            </div>

        </div>

        <a href="bagan.php"><h4>Inle</h4></a>
          <div class="destination-bg">
                <div class="tri" style="width:300px">
                    <ul>
                        <li><a href="reservation.php">Inle Tour</a></li>
                        <!--<li><a href="jurong.php">Jurong Bird Park</a></li>
                        <li><a href="nightsafari.php">Night Safari</a></li> -->
                    </ul>
                </div>
            </div>
            <!--end tri div-->
            <!-- <div class="tri">
                <ul>
                    <li><a href="merlion.php">Merlion statue</a></li>
                    <li><a href="orchardroad.php">Orchard Road</a></li>
                    <li><a href="pulauubin.php">Pulau Ubin</a></li>
                </ul>
            </div> -->
            <!--end tri div-->

            <!-- <div class="tri-last">
                <ul>
                    <li><a href="sentosa.php">Sentosa Island</a></li>
                    <li><a href="singaporeflyer.php">Singapore Flyer</a></li>
                    <li><a href="universal.php">Universal Studios</a></li>
                </ul>
            </div> -->
            <!--end tri-last div-->


            <a href="hotels.php"><h4>Myanmar Hotels</h4></a>
        <div class="clear"></div>
    </div>
    <!--end main column -->

<div class="clear"></div>
<!--end content section-->

            <?php include 'footer.php'; ?>
