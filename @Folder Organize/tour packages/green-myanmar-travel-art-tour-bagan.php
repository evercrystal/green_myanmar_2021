<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="main-column" >
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body editable" id="untitled-region-1">
        <h2 id="column-header">The best <span class="green">Itineraries From Green Myanmar Travel</span></h2>
        <p> Green Myanmar Travels is providing best and valuable itineraries for world wide customer as follow</p>
        
        
        <h1>        Yangon ~ Mandalay ~ Sagaing ~ Yangon Trip ( Daily ) ( 3Nights / 4 Days ) </h1>
        <p>

Day 01 ~ Depart from Yangon , Aung Min ga lar Bus Station to Mandalay by VIP air con
express bus around 8 pm . Overnight at a coach.</p>

<p>Day 02 ~ Arrive at Mandalay bus station. Pick you up and send to you Hotel for a while . </p>
<p>After that drive to Sagaing Sightseeing . At evening, drive back to Amarapura Sightseeing including U Bein Bridge Sunset. And then send to you hotel and Overnight at Hotel in Mandalay.</p>

<p>Day 03 ~ Breakfast at Hotel . After breakfast , start to Mandalay Sightseeing. At noon check out from hotel and sightseeing in Mandalay. AT evening , transfer to Mandalay bus station. Depart from Mandalay to Yangon by VIP air con express bus around 8 am . Overnight at a coach.</p>

<p>Day 04 ~ At monring,around 6 pm arrive to Yangon safe and sound. </p>

<p>***********End of Program with Happiness.***********</p>

<p>Package rate inclusive :</p>

• 1 Night stay at Hotel in Mandalay ( Twin sharing with Breakfast ) 
• VIP Bus tickets ( Yangon ~ Mandalay ~ Yangon ) 
• Bus Station Pick up and drop at Mandalay
• Sightseeing car in Mandalay & Sagaing

Package rate exclusive:

• Meals ( Lunch & Dinner )
• Minibar, Laundry at Hotel and other accidental charges
• Tour Guide, Entrance and Zone fee
• Other than those items mentioned in inclusive

Validity : up to end of March,2013.

*Formore detail please feel free to contact us*

Thet Hein
Sales & Marketing Manager ( Green Myanmar T&T Co.,Ltd. )
09-420077655
        
        </p>
        
      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      
      <div class="clear"></div>
    </div>
    <!--end main column --> 
    
    <!--begin  sliderbar-->
    <div id="sidebar" >
      <div id="newsletter">
        <h2>Sign up for our Newsletter</h2>
        <p>Be the first to know about events and special offers! </p>
        <form action="notify.php" method="post" id="notifyForm">
          <div id="letter">
            <input type="text" name="email" class="required email" value="Email"/>
            <button type="submit" name="signup">Sign Up</button>
          </div>
        </form>
      </div>
      <!--end newsletter -->
      
      <div class="tabs">
        <ul class="tabNavigation">
          <li><a href="#first">Tour Highlights</a></li>
          <li><a href="#second">Best Deal</a></li>
        </ul>
        <div id="first">
          <ul>
            <li class="first"> <img class="fr" src="images/tourist.jpg" width="75" height="75" alt="tab-thumb" />
              <p style="color:yellow">Yangon-Thanlyin<p>
	      The best tour program to explore Yangon's famous places,National Races Village,Midstream Pagoda(Kyauk Hmaw Wun,Thanlyin).... &nbsp;
               <a href="yangon_thanlyin_tour.html">read more »</a>
            </li>
            <li><img class="fr" src="images/cruise.jpg" width="75" alt="tab-thumb" />
              <p style="color:yellow">Yangon-River-Cruise-Tour</p>
              Enjoy Yangon's beautiful sunset with a river cruise,Botataung jetty-Yangon river-Botataung jetty.... &nbsp;
              <a href="yangon_river_tour.html">read more »</a>
            </li>
            <li  class="last"><img class="fr" src="images/bus.jpg" width="75" height="75" alt="tab-thumb" />
              <p style="color:yellow">Yangon-Sightseeing-Tour</p>
                Experience the city sights with bus tour,to explore Yangon's most famous places.... &nbsp;
                <a href="yangon_city_tour.html">read more »</a>
            </li>
          </ul>
        </div>
        <div id="second">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb"/>
              <p> Only 60 USD for Thanlyin Yangon city Tour Program <br />
                <a href="#">read more »</a></p>
            </li>
           
          </ul>
        </div>
      </div>
      <div class="box"></div>

    </div>
    <!--end sidebar --> 
    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>

		
