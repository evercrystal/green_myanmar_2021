<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>




    <title>Myanmar Budget Package Tour - Myanmar budget tour for independent travellers</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Best Myanmar Travel Agent provides Myanmar Budget Tour for budget and independent travellers in ordor to ease at arraning their tour in myanmar  .">
    <meta name="keyword" content="Myanmar Travels Tours ,  Myanmar Travel, Myanmar Tour package, Myanmar Budget Tour, Bagan, Mandalay ,Tour for independent travellers >    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>
    <!--end class dropdown-->

</div>
<!--end Header-->


<!--end Header-->

<!-- Content Section -->

<div id="content-wrapper" >
    <div id="content-header" >
        <h2 id="welcome"></h2>
        <form id="search-form" action="" method="post">
            <input id="searchInput" name="searchInput" type="text" />
            <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
        </form>
        <ul id="button">
            <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
            <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
            <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
        </ul>
        <br class="clear"/>
    </div>
    <!--end content_header-->

    <!--begin main column-->
    <div id="main-column" >
        <div class="column-header"></div>
        <!--end column header-->
        <div class="column-body editable">
            <h2 id="column-header editable">Green Myanmar's Budget Tours Packages</span></h2>
            <p>Of course There are so many ways travelling in myanmar with budget price and best experiences.  </p>
            <p>We are here to arrange our best for budget travellers packages and arranging hotels , transportation and sightseeing independents budget travellers </p>
            <p>If you still need further information about budget travelling in myanmar or more information please feel free to email us hello@green-myanmar.com and we are always there to share all information we have !!! </p>
        </div>
        <!--end column body-->
        <div class="column-footer"></div>
        <div class="editable">
            <a href="myanmar-budget-tours.php"><h4>Myanmar Budget Tours</h4></a>
            <div class="destination-bg">
                <div class="tri" style="width:100%">

                    <ul>
                        <li><a href="yangon-city-tour.php">Yangon City Tour ( Lowest price starts from 50 USD per person for inclusive one day tour)</a></li>

                    </ul>

                    <ul>
                        <li><a href="myanmar-budget-tour.php">Yangon - Bago - Mandalay - Bagan - Inle Lake - Kalaw - 9 Nights 10 Days Myanmar Budget Tour </a> (Lowest Price starts from 840 USD per person  )</li>
                    </ul>

                </div>

            </div>

            <div class="column-body editable">
                <p>Travelling with Bus in Myanmar is best to save your budget on your Myanmar tour and There are many more tips about budget tour . Please feel free to email to us hello@green-myanmar.com . We are always there to suggest how to make your Myanmar budget tour.</p>
            </div>




            <a href="myanmar-budget-hotels.php"><h4>Myanmar Budget Hotels</h4></a>
        <div class="clear"></div>
    </div>
    <!--end main column -->

<div class="clear"></div>
<!--end content section-->
            <?php include 'footer.php'; ?>
