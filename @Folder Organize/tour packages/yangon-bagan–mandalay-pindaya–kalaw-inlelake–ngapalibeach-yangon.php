<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Yangon - Bagan – Mandalay - Pindaya – Kalaw - Inle Lake – Ngapali Beach - Yangon (14 Days/13 Nights) - Myanmar Budget tour - Myanmar Beach Tour
    </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon - Phoe Kyar Elephant Camp – Bagan – Heho – Kalaw (Trekking) – Inle Lake – Yangon( 10 Days/ 9 Nights)- Myanmar Budget Tour , Myanmar Beach Tour , Myanmar Tour packages">
    <meta name="keyword" content="Yangon , Ngapali Beach, Bagan , Heho , Kalaw (Trekking) , Inle Lake , Yangon , Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7"><h2> Yangon - Bagan – Mandalay - Pindaya – Kalaw - Inle Lake –Ngapali Beach-Yangon (14 Days/13 Nights)

            </h2>

                <p><img alt="inle lake tour" class="image-right editable" src="images/images/inlay.jpg" title=
                        "Shwedagon pagoda" /></p>

                <h3 style="color: orange">Myanmar Budget tour , Lowest price starts from 1400 USD per person including staying at beautiful myanmar beach</h3>

                <h4 style="color: orange">Yangon - Bagan – Mandalay - Pindaya – Kalaw - Inle Lake –Ngapali Beach-Yangon(14 Days/13 Nights)
                </h4>
                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
                                                          "book now" /></a></p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 01 Arrival Yangon (by morning/ midday flight )					(  - /- / - ) </h4>
                <p>Arrive Yangon by morning/ midday flight. You will be welcome by your tour guide and transfer to the hotel. Have a rest at the hotel until the sightseeing starts. </p>
                <p>Visit to city center and Sule Pagoda, the landmark of downtown Yangon, followed by Chaukhtatgyi - the colossal reclining Buddha, Kandawgyi (Royal) Lake for photographing the panoramic view of the lake overlooking the Shwedagon Pagoda. Proceed to Shwedagon Pagoda, an inspiring golden stupa which is one of the best landmarks in Myanmar. </p>
                <p>Overnight at Yangon. </p>




                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Day 02 Trip to Golden Rock									( B/- / - ) </h4>
                <p>Breakfast at the hotel.Drive to the camp Kimpun (about 5 hours), from where the uphill motor road to hermit’s hill starts. On the way, we will visit gigantic Kyaikpun Buddha Images; Shwemawdaw Pagoda, towering 375 feet and taller than Shwedagon. </p>
                <p>Arrive Kimpun camp and take the uphill pick up truck to reach the hermit’s hill. A short trek, about an hour would take you to the top of the mountain where you can see the Golden Rock - a large boulder positioned well balanced on the cliff of that mountain - seems to be defying the force of gravity from the spur of the cliff. Enjoy sunset view over deep forested mountain. </p>
                <p>Overnight at Kyaikhtiyo. </p>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                <h4 style="color: orange">Day 03 Kyaikhtiyo – Bago -Yangon	 							( B/- / - ) </h4>
                <p>Early sunrise viewing before breakfast. Breakfast at the hotel and descend the mountain to reach back to Kimpun camp from where we start our return journey back to Yangon. </p>
                <p>En-route visit Bago; Shwethalyaung reclining Buddha Image – originally built in 994 AD and is a masterpiece of Myanmar stucco and symmetry. Arrive Yangon. </p>
                <p>Overnight at Yangon. </p>
                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                <h4 style="color: orange">Day 04 Yangon-Nyaung U (Flight)/ Bagan full day sightseeing 			 	( B / - / - )</h4>
                <p>Breakfast at the hotel. Transfer to the airport to take the flight to Bagan (ETD, 06:10-07:25). Arrive Bagan and check in at the hotel. Bagan, one of Asia’s most popular archaeological destinations and one out of two most pre-eminent religious old cities in South East Asia. The sightseeing starts with a visit to Nyaung Oo market – a typical local market; followed by visits to most distinctive pagodas such as Shwezigon Pagoda – the holiest one believed to be enshrined with Buddha tooth and hair; Gubyaukgyi Temple (Wetkyiinn village) – the 13th century ‘cave temple’ with fine frescoes of scenes from Jakatas; Anada Temple – one of the masterpieces surviving and remaining in Bagan and considered to be in the Mon architecture style.</p>
                <p>A visit to the lacquer ware factory followed by a visit to local village. We will enjoy the sunset from the top of one of the temples.</p>
                <p>Overnight Bagan.</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                    <h4 style="color: orange">Day 05 Bagan-Popa-Bagan sightseeing 			 				( B / - / - )</h4>
                <p>Breakfast at the hotel. Drive to Mt. Popa, an extinct volcano locates 65km south east of Bagan. It stands as one of the National parks with preserved forest in the hot, dry and plain region of middle Myanmar. The popularity of Mt. Popa today lies in the interest of religious and mystical beliefs as an abode of spirits, “Nats”. Climb the Taungkalat, believed to be the part of the main volcano blown apart and plugged at the current location, to see the panoramic view of the whole area. On the way back to Bagan we will visit the toddy palm producing home cottage.</p>
                <p>Afternoon visit to Myinkaba village Gubyaukgyi Temple– believed to be built for Rajakumar on the death of his father, King Kyansitta, and famous nowadays for its well preserved wall paintings; Manuha Temple – built by detained Mon King Manuha; Ngagayon Temple – similar to the plan of Anada Temple and the main attraction is that the twice life size Buddha Image shelters under the hood of the naga or serpent inside the temple. We will view the sunset from the top of one of the temples.</p>
                <p>Overnight at Bagan.


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                        <h4 style="color: orange">Day 06 Bagan-Mandalay (Flight) - Amarapura & Mandalay Sightseeing      		( B / - / - )</h4>
                <p>Breakfast at the hotel. Transfer to the airport to take the flight to Mandalay(ETD, 07:45-08:15). Upon arrival Mandalay, drive to Amarapura, former Royal capital of Myanmar but the suburb of Mandalay nowadays. Highlights include Mahargandaryone monastery - a typical monastic school; U Bein bridge – the world’s longest teak wood bridge across the Taungthaman lake – an ideal place to view the sunset also; and silk weaving factory.</p>
                <p>Drive to Mandalay and check in at the hotel. Afternoon sightseeing includes Shwenandaw monastery – still existing masterpiece of last Myanmar kingdom’s architecture in teak and wood carving; Kuthodaw Pagoda – renowned as the world’s largest book; and Mandalay Hill – regarded a natural watch tower in the plain region of Mandalay – to view sunset.</p>
                <p>Overnight Mandalay.

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                            <h4 style="color: orange">Day 07 Mandalay-Sagaing-Mingun Sightseeing      						( B / - / - )</h4>
                <p>Breakfast at the hotel. Drive to Sagaing, a former capital of Shan Kingdom and Myanmar Kingdom. Sagaing nowadays is renowned as the religious center of Myanmar because of over 400 monasteries spreading throughout the hillocks. After crossing the Inwa bridge (also called Sagaing Bridge), one can visit the Sagaing hill by trucks. The highlights include Soonooponyashin Pagoda, Uminthonese Pagoda, a thirty man made caves pagoda. Other highlights include silver smith and Kaunghmudaw pagoda – the gigantic stupa represent paddy heap.</p>
                <p>Afternoon from the Mandalay Gawwein jetty, we will take a short boat ride up river to reach Mingun, located at the other side of the Ayeyarwaddy River and north of Mandalay. The boat ride is always pleasant as one can learn the ways of local life along the river. Sightseeing highlights includes Pathodawgyi, believed to be one of the largest brick base in the world and it would be one of the wonders of the world in case if it was completed; Mingun bell, said to be world’s biggest un-cracked ringing bell and Myatheindan pagoda, represents for Buddhist cosmology. Before the sundown, we would take back a boat ride down along the Ayeyarwaddy River to enjoy the sunset and relax.</p>
                <p>Overnight at Mandalay
                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                            <h4 style="color: orange">Day 08 Mandalay-Heho (Flight) / visit Pindaya and Kalaw 				( B / - / - )</h4>
                <p>Breakfast at the hotel. Transfer to the airport to take the flight to Heho (ETD, 08:30-09:00). Upon arrival at Heho, proceed to Pindaya, a historic town situated about three hours drive from Inle Lake passing through PhaO and Danu villages en-route for photo stops. Pindaya caves – natural lime stone caves not only with numerous stalactites and stalagmites but also filled with about 8000 various sizes of Buddha images believed to be there since 11th century; famous Botoloke Lake. Ethnic groups like Shan, PaO and Danu are living environs of Pindaya and the place nowadays is also famous for traditional style Shan paper making and Shan umbrella making. After sightseeing in Pindaya drive back about an hour to Kalaw.</p>
                <p>Overnight at Kalaw.

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                             <h4 style="color: orange">Day 09 Kalaw Sightseeing, drive to Inle Lake 						( B / - / - )</h4>
                <p>Breakfast at the hotel. Sightseeing at Kalaw; Kalaw is an old British hill resort, stands at the altitude of 1,315 meters, is also known as Pine Land because of its abundant in pine trees. The sightseeing includes: Kalaw market, Nee Paya (Bamboo strip lacquer Buddha image) and Shweumin natural caves; taking along the up-down meandering roads of Kalaw to have the panoramic view of Kalaw; old colonial style buildings from a hillock opposite of Kalaw.</p>
                <p>Afternoon drive to Inle Lake. Transfer to your hotel by Boat.</p>
                <p>Overnight at Inle Lake.</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                            <h4 style="color: orange">Day 10 Visit Indein and Inle Lake                                                                				( B / - / - )</h4>
                <p>Breakfast at the hotel. Visit the regional once in every fifth day market takes place around the different area of the Inle Lake if today coincides with the market day. Taking the boat to the down south and into a small twist and turn creek running into the lake, we would reach the Indein village – a small village located at the south western bank of the lake and famous for its ruined clusters of pagodas dated back to 16th century. The tranquility, peaceful ambience and ruins overgrown with bushes would bring you to centuries back. We can also have the chance to chat with villagers during the sightseeing time.</p>
                <p>Visit Inle Lake exists 900 meters above the sea level and the home land of Innthar tribe. Phaungdawoo Pagoda – famous for its richly gilded five small Buddha images and its history; Ngaphechaung monastery (formerly also known as jumping cat monastery for trained jumping cats) – more than one and a half century old monastery with its collection of ancient Buddha Images. Other attractions include unique leg-rowing style and fishing method of the Innthar; floating gardens and vegetable plantations; and weaving factory and black smith.</p>
                <p>Overnight at Inle Lake.</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                            <h4 style="color: orange">Day 11 Heho-Thandwe (Ngapali Beach) by morning Flight				( B / - / - )</h4>
                <p>Breakfast  at the hotel. Transfer to Heho airport  & take the morning flight  to Ngapali Beach.</p>
                <p>Overnight at Ngapali Beach</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                            <h4 style="color: orange">Day 12 Ngapali Beach									( B / - / - )</h4>
                <p>Breakfast  at the hotel. Free at Leisure at the beach.</p>
                <p>Overnight at Ngapali Beach</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                            <h4 style="color: orange">Day 13 Ngapali Beach-Yangon by afternoon flight						( B / - / - )</h4>
                <p>Breakfast  at the hotel. Free at Leisure at the beach till the transfer to airport to take the flight back to Yangon</p>
                <p>Overnight at Yangon</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>
                            <h4 style="color: orange">Day 14 Departure Yangon          			       		 			( B / - / - )</h4>
                <p> Breakfast at the hotel. Shopping at famous shopping center Bogyoke Market (formerly called Scott Market), where you can see varieties of goods ranging from handicrafts to clothing and local food/ snack.</p>
                <p>(or) visit to National Museum, a unique place to learn more about Myanmar, its culture and people.</p>
                <p> Transfer to international airport for your onward departure flight.

                <p>Remark: National Museum closes on Monday,Tuesday & public holidays. Bogyoke Market closes on Monday.</p>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <h4 style="color: orange">Cost per person in twin sharing basic: </h4>
                <h4 style="color: orange">Pax	Rate (without guide)</h4>
                <p>contact hello@green-myanmar.com for price details</p>
                <h4 style="color: orange">Cities	Hotels (3 Stars)</h4>
                <p><a href="yangon.php">Yangon</a> 	<a href="http://hotel7mile.com/"> Hotel 7 Mile </a>(Superior) or similar</p>
                <p><a href="bagan.php">Bagan</a> 	<a href="http://www.baganthandehotel.net/"> Bagan Thande Hotel </a>(Superior) or similar</p>
                <p><a href="mandalay.php"> Mandalay </a>	<a href="http://www.mandalaycityhotel.com/">Mandalay City Hotel </a>(Superior) or similar</p>
                <p>Kalaw	<a href="http://www.tripadvisor.com/Hotel_Review-g1016308-d1980479-Reviews-Hill_Top_Villa_Resort_Kalaw-Kalaw_Shan_State.html"> Hill Top Villa Resort </a> (Superior) or similar</p>
                <p>Inle Lake <a href="http://www.gichotelgroup.com/">GIC  (Standard)</a>  or similar</p>
                <p><a href="ngapali.php"> Ngapali Beach </a><a href="http://www.amataresort.com/" accesskey="">	Amata Resort & Spa</a> (Superior) or similar</p>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>

                <p> For the package rate or more information , Please contact hello@green-myanmar.com</p>

                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>


                <h4 style="color:red">Services Include: </h4>


                <p style="color:white">•	Transfers and Sightseeing by Boat and private air-conditioned vehicles
                <p style="color:white">•	Accommodation 13 Nights with daily breakfast at 3 stars hotel
                <p style="color:white">•	Domestic Air tickets as per program(Rgn-Bgn-Mdy-Inle-Snw-Rgn)
                <p style="color:white">•	Domestic Airport Tax
                <p style="color:white">•	Mingun Boat service
                <p style="color:white">•	Drinking water 1 bottle per day
                <p style="color:white">•	Poterage at the airport and boat stand

                <h4 style="color:red">Services Exclude: </h4>
                <p style="color:white">•	Meals ( Lunch / Dinner )
                <p style="color:white">•	The entry fees for sightseeing places
                <p style="color:white">•	Guide Service
                <p style="color:white">•	Drinks, Beverages, Tips,  personal expenses
                <p style="color:white">•	International Airport Tax ( 10 US$ per person in Yangon )
                <p style="color:white">•	Any other services not mentioned in the “Services Include:” section as above


                <h5>Proposed date:	 1st October 2014 </h5>
                <h5>Validity: 1st Oct 2014 to 31 Mar 2015 (Except Christmas & New Year period)</h5>


                <p>  - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </p>



                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
                                                          "book now" /></a></p></div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>
