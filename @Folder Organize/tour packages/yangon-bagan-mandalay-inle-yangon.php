<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Myanmar package tour - Myanmar Budget Tours -Yangon-Bagan-Mandalay-Inle Lake tour packages - Myanmar travel packages</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Yangon-Bagan-Mandalay-Inle Lake-Yangon (7Days/6Nights) Packages is complete exploring Myanmar with amazing experience .">
    <meta name="keyword" content="Yangon,Bagan,Mandalay, Inle Lake,Yangon,Myanmar Tour Packages,(7Days/6Nights)">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" id="untitled-region-7">
              <h1 style="text-align: center;"><strong>Yangon-Bagan-Mandalay-Sagaing-Mingun-Inle-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>(8Days-7Nights)</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.D8001BMI</h1>

</br>
</br>

 <h5 style="color: orange">Day- 1 Yangon Full Day sightseeing , Yangon-Bagon (VIP Bus)<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Arrive Yangon and you will be welcomed by our tour representative. If time permit, then start sightseeing to-</p>
<p>•   Botahtaung Pagoda</p>
<p>•   ChaukHtetgyi Pagoda</p>
<p>•   Kandawgyi Nature park</p>
<p>•    World famous Shwedagon Pagoda</p>

<h5>Overnight on Coach to Bagan(ETD-20:00, ETA-5:30).</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/2012-12-02-2104.jpg" style="width:300px;margin-right:10px" class="editable" alt="2012-12-02-2104" titlx="2012-12-02-2104">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange">Day-2 Bagan Full Day Sightseeing<h5>
<small><p>Early arrive to Bagan around 5:30am. Best time to see amazing Bagan sunset and then transfer to the hotel to take a short rest . Then start sightseeing . Highlight include;</p>
</small>
<div class="row">
             <div style="float:left;width:580px">
<small>
<p>•   Ananda Pagoda</p>
<p>•   Thabanyu Temple</p>
<p>•   Dammayangyi Pagoda</p>
<p>•   Htilominlo Pagoda</p>
<p>•   Shwezigon Pagoda</p>
<p>•   Bu Pagoda</p>
<p>•   Gawdotplin Temple</p>
<p>•   Manuha Temple</p>
<p>View amazing sunset at the top of Shwesantaw Pagoda.</p></small>
<h5>Overnight in Bagan.</h5>
</div>                  
                    <div style="float:right">
                        <img src="newimages/shwesandaw-pagoda1.jpg" style="width:300px;margin-right:10px" class="editable" alt="shwesandaw-pagoda1" titlx="shwesandaw-pagoda1">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: orange; clear:both">Day 3 Bagan full day sightseeing, drive to Mandalay<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel. Then start sightseeing highlight places -</p>
<p>•   Sulamuni Temple</p>
<p>•   Bu Pagoda</p>
<p>•   Gawdotplin Pagoda</p>
<p>•   Gubyauk kyi Temple</p>
<p>•   Manuha Temple</p>
<p>•   Lawka Nandar Pagoda</p>
<p>Evening take minibus to Mandalay (about 4hours).</p>
<h5>Overnight in Mandalay.</h5>
</div>                  
                    <div style="float:right;width:300px">
                        <img src="newimages/sunrise-in-bagan1.jpg" style="width:300px;margin-right:40px" class="editable" alt="sunrise-in-bagan1" titlx="sunrise-in-bagan1">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange; clear:both">Day-4 Mandalay – Sagaing – Mingun Full Day Sightseeing<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>After breakfast at Hotel, drive to Amarapura, former Royal capital of Myanmar. Highlights include </p>
<p>•U Bein bridge </p>
<p>•Mahargandaryone monastery </p>

<p>Cross the river by ferry to Inwa (AVA), situated on the banks of the Irrawaddy River. Enjoy a leisurely Horsecart Ride around the peaceful countryside and briefly visit</p>
<p>•Bagaya Kyaung</p>
<p>•Maha AungMyay BonZan Kyaung </p>
<p>•Nan Myint Tower.</p>

Next head to Sagaing, the spiritual center of Myanmar. Highlights include  </p>
<p>•Sun U Ponya Shin Paya,</p>
<p>•U Myin Thonsei Paya </p>
<p>•Kaung Hmu Daw Paya </p>
<p>Sagaing Hill where you can enjou sun set</p>
<h5>Overnight in Mandalay.</h5>
</div>                  
                    <div style="float:right;width:300px">
                        <img src="newimages/maldalay-2.jpg" style="width:300px;margin-right:10px" class="editable" alt="maldalay-2" titlx="maldalay-2">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
                          
<h5 style="color: orange;clear: both">Day-5 Fly to Heho, Pindaya-Red Mountain sightseeing<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel. Then transfer to the airport to fly to Heho. Arrived Heho and proceed to Pindaya, famous Shwe U Min Pagoda (Pindaya Cave) and Botoloke Lake Then drive Back to Inle.</p>
<p>Then, head up to the Red Mountain Estate, vineyard in the hills to the east of Inle Lake for some wine tasting. One of only two vineyards in Myanmar, this definitely breaks the mould of your day-to-day Myanmar experience, making it suddenly feel as if you have been transplanted to the south of France. You can try the tasting menu for 2000 kyats (featuring wines from four different types of grape) whilst taking in the stunning sunset views over the lake.</p>
<p>Then, head up to the Red Mountain Estate, vineyard in the hills to the east of Inle Lake for some wine tasting. One of only two vineyards in Myanmar, this definitely breaks the mould of your day-to-day Myanmar experience, making it suddenly feel as if you have been transplanted to the south of France. You can try the tasting menu for 2000 kyats (featuring wines from four different types of grape) whilst taking in the stunning sunset views over the lake.</p>
<h5>Overnight In Inle.</h5>
</div>                  
                    <div style="float:right;width:300px">
                        <img src="newimages/Inle Lake-2.jpg" style="width:300px;margin-right:10px" class="editable" alt="Inle Lake-2" titlx="Inle Lake-2">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange; clear:both">Day-6 Inle Full Day Sightseeing, Inle - Yangon<h5>

<div class="row">
             <div style="float:left;width:580px">
<small>
<p>Breakfast at the hotel. Continue sightseeing in  Inle Lake to</p>
<p>•   Indein</p>
<p>•   Ywama village</p>
<p>•   Phaungdawoo Pagoda</p>
<p>•   Ngaphe’ Chaung Monastery </p>

<h5>Overnight in Inle.</h5>
</div>                  
                    <div style="float:right;width:300px">
                        <img src="newimages/inle-3.jpg" style="width:300px;margin-right:10px" class="editable" alt="inle-3" titlx="inle-3">  
                    
                      </div>  
</div> 
 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange;clear:both">Day -7 Fly to Yangon, Yangon Full Day Sightseeing<h5>
<small><p>After breakfast at Hotel, transfer to airport to fly to Yangon. Arrive Yangon and then take Yangon circular Train to experience daily life of Yangon . </p>
<p>Then, visit to Bogyoke Market for shopping and China town and Colonial buildings for strolling. </p>
<h5>Overnight in Yangon.</h5>
</small>

<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: orange; clear:both">Day -8 Yangon Airport Transfer<h5>
<small><p>Breakfast at the hotel. Free time until transfer to Yangon International Airport for departure.</p>
</small>

<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>
<h5 style="color: red">Service Include : </h5><p>  Transfers and Sightseeing by private air-conditioned vehicles </p>
<p>   Accommodation 5 Nights with breakfast from hotel</p>
<p>   Domestic Air ticket as per program(Mandalay -Heho)</p>
<p>   VIP bus ticket(Yangon – Bagan, Bagan – Mandalay, Inle-Yangon,)</p>
<p>   Mingun Boat</p>
<p>   Horse cart in Inwa</p>
<p>   English Speaking Station Guide service </p> 
<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h5 style="color: red">Services Exclude : </h5><p> Meals ( Lunch / Dinner )</p>
<p>   The entry fees for sightseeing places</p>
<p>   Drinks, Beverages, Tips,  personal expenses</p>
<p>   International Airport Tax ( 10 US$ per person in Yangon ) </p>
<p>   Any other services not mentioned in the “Services Include:” section as above</p>

<h4 style="color: black">-------------------------------------------------------------------------------------------------------------------------------------<h4>

<h3>Cost<h3>
<h5>For 2 pax, US$1157 per person</h5>
<h5>For 10pax, US$840 per person</h5>
<h3>for more details,please contact hello@green-myanmar.com    <h3>                                
                <p><a href="reservation.php"><img alt="booking button" src=
                        "images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
                                                          "book now" /></a></p></div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>


