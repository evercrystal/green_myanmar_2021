<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="The best yangon city tour program for mandalay sightseeing. It is budget mandalay city tour program but it is ajustable according to your requirment.">
    <meta name="keyword" content="Myanmar Travel Agent, Mandalay city tour, Mandalay Sightseeing, Mandalay explorer">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <title>Bagan Full Day City Tour ( Bagan Sightseeing Tour)</title>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">
                      
                      <h1 style="text-align: center;"><strong>Bagan City Sightseeing Day Tour</strong></h1>
<h1 style="text-align: center;"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.BD1003</h1>

                    <p style="margin-left:100px">If you would like to make online booking or find out more about price details,  please click <a href="http://myanmardaytrips.com/dailytour_detail/12"> here ( Bagan city tour details ) </a></p>


                    </br>
</br>
                   
                    <br />
                    <br />
                </div>
                <div>
                    <img src="imagesnew/bagan1.jpg" class="editable" alt="botataung" title="Bagan Sightseeing Tour" style="float: left;padding-right: 15px;padding-left: 20px" width="270px" height="165px">

                    <img src="imagesnew/bagan2.jpg" alt="gabaraye" class="editable" title="bagan day tour" style="float: left;padding-right: 15px" width="270px" height="165px">

                    <img src="imagesnew/bagan3.jpg" alt="shwedagon" class="editable" title="bagan Sightseeing tour" width="270px" height="165px">

                </div>
                <br />
                <br />
            
                </p>
                
                <h5 style="color: orange"> 8:00am-9:00am</h5> <p>>>Pick up at your hote and visit to Nyaung U morning market to see the lifestyle of local people.</p>
                                   
 <h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
               
                <h5 style="color: orange"> 9:00am-12:00am</h5> <p>>> And visit to the most significant pagodas and temples of Bagan including Shwezigon Pagoda built by King Anawrahta in the early 11 century as a religious shrine, Gubyaukgyi (Wet Kyi Inn) which  is a temple with superb murals of Jataka scenes, Anada Temples and Oakkyaung. </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                <h5 style="color: orange">12:00am- 1:00pm</h5> <p>>> Have a lunch at local restaurant. </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                <h5 style="color: orange">1:00pm-2:00pm</h5> <p>>> After that, visit one of Myanmar’s most treasured handicrafts, a lacquer ware home industry, nearby Myingabar Village. </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                <h5 style="color: orange">2:00pm-4:30pm</h5> <p>>> Visit Manuha Buddha Image, Nanphaya Temple and Abeyadana Temple. </p> 
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
 
                <h5 style="color: orange">4:30pm-6:00pm</h5> <p>>> View amazing sunset on the top of Shwe San Taw Pagoda or Bu Pagoda. </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                <h5 style="color: orange">Around 6:00pm</h5> <p>>> Drive back to Hotel and amazed by the most ancient capital of wonderful Myanmar. Enjoy the end of tour program.</p>                    
                <br />
                <br />

                <h5 style="color: orange">You can include following optional with tour </h5>

                <h4 style="color: black">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - </h4>
                
                    </b></p>
                    <h5 style="color:red">Services Include:</h5><br>
                    <p style="color:white"> -  Transfers and Sightseeing by private air-conditioned vehicles </p>
                    <p style="color:white"> -  English Speaking  Guide </p>
                    <p style="color:white"> -  Drinking water 1 bottle </p>
                    <br>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

                    <h5 style="color:red">Services Exclude:</h5><br>

                    <p style="color:white"> -  Meals(lunch/dinner) </p>

                    <p style="color:white"> -  Bagan Zone fee </p>

                    <p style="color:white"> -  Drinks, Beverages, Tips, Personal Expenses</p>
                    <p style="color:white"> -  Any other services not mentioned in the “Services Include” section a above </p>
                    <p style="color:white"> -  The entry fees for sightseeing places  </p>
   <h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>
        
<h3>Cost</h3>
                <a style="font-size: 30px" href="http://myanmardaytrips.com/dailytour_detail/12">Find Bagan City Tour price here</a>

<h4>for more details,please contact hello@green-myanmar.com  </h4>
            
                <a href="reservation.php"><img src="images/booking-button.jpg" class="editable" alt="booking button" title="book now" style="margin: 5px 0px 5px 5px;"/></a>
              
   
            </div></div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>

