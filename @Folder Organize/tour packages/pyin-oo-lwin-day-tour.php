<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green Myanmar Travel Agent provides budget and best Mandalay City Tour for Mandalay Sightseeing. It includes Ancient cities of Upper Myanmar: Innwa, Amarapura, Sagaing.">
    <meta name="keyword" content="Green Myanmar Travel Agent, Mandalay City Tour , Mandalay Sightseeing , Innwa, Amarapura, Sagaing">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <title>Pyin Oo lwin Day Tour</title>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

<style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>
    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">
               

               <h1 style="text-align: center;"><strong>Pyin Oo lwin Day Tour</strong></h1>
<h1 style="text-align: center;"><strong>1 Day Tour</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.MD1003</h1>

</br>
</br>
<div class="row">
                    <div style="float:left;width:280px">
                        <img src="newimages/10-ani_sa_khan_waterfall_pyin_oo_lwin.jpg" style="width:250px;" class="editable" alt="10-ani_sa_khan_waterfall_pyin_oo_lwin" title="10-ani_sa_khan_waterfall_pyin_oo_lwin">  
                    </div>
                    <div style="float:left;width:300px">
                        <img src="newimages/15252.jpg" style="width:250px;" class="editable" alt="15252" title="15252">
                    </div>
                    <div style="float:left;width:300px">
                        <img src="newimages/pywin_oo_lwin_-_aureum_dat_taw_gyaint_waterfall_resort_march_2011.jpg" style="width:250px;" class="editable" alt="pywin_oo_lwin_-_aureum_dat_taw_gyaint_waterfall_resort_march_2011" title="pywin_oo_lwin_-_aureum_dat_taw_gyaint_waterfall_resort_march_2011">
                    </div>
                </div>
<h5 style="color:orange">8:00 am>></h5>
<p>Pick up at your Hotel in Mandalay, visit to Pyin Oo lwin Market to see the life style of local people.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">9:00 am>></h5>  
<p>Visit to Mahar Ant Htoo Kanthar Pagoda.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">10:00 am>></h5> 
<p>Visit to Chan Tak Buddhist Temple.</p>
<p>It is a large and colorful Chinese temple built by the many Yunanese immigrants to the town. It is close to Thiri Myaing Hotel (originally called Candacraig).</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">11:00 >></h5> 
<p>Visit to Sutaung Pyae Hnee Buddha Image.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">12:00 >></h5> 
<p>Lunch at Local restaurant.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">13:00>></h5> 
<p>Visit to Peik Chin Myaung Cave or Maha Nandamu Cave. Peik Chin Myaung Cave (also known as Maha Nandamu Cave) is located on the Lashio road near Wetwun village, it is about 20 ft wide and 1,600 ft long,12 miles east of Pyin Oo Lwin town. It is a limestone cave estimated between 230 million and 310 million years old.</p>  
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">14:00>></h5> 
<p>Visit to Pwe Kauk or B.E Waterfalls . The Pwe Kauk Falls is a very pleasant picnic spot in Pyin Oo Lwin where many Myanmar families come for a picnic. Pwe Kauk or B.E waterfalls is also known as Hampshire Falls in British times.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">15:00 >></h5> 
<p>Visit to National Landmarks Garden Or National Kandawgyi Garden . The National Kandawgyi Gardens (formerly National Botanical Gardens) is located in the Alpine town about 1.5 km south of Pyin Oo Lwin (formerly Maymyo), Myanmar.</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color:orange">17:00 >></h5> 
<p>Drive back to Mandalay and transfer to your hotel</p>

<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: red">Service Include :</h5> 
<p>Transfer and Sightseeing by private air-conditioned vehicles</p>
<p>English Speaking  Guide</p> 
<p>Drinking water 1 bottle</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h5 style="color: red">Services Exclude :</h5> 

<p>Meals(Lunch/Dinner)</p>
<p>Drinks, Beverages, Tips, Personal Expenses</p>
<p>Mandalay Zone fees</p>
<p>Camera fees, any other services not mentioned in the “Services Include” section a above</p>

            <div>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3> 
<h4>For 2 pax, US$85 per person</h4>
<h4>For 10pax, US$35 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>

                <a href="reservation.php"><img src="images/booking-button.jpg" alt="booking button" title="book now" style="margin: 5px 0px 15px 5px;"/></a>

            </div></div></br>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

<?php include 'footer.php'; ?>
