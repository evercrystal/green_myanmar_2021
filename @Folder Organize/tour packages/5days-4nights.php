<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


    <title>Yangon–Kyaikhtiyoe-Mawlamyaing-Hpa An-Yangon(5Days-4Nights)</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travel agent provides yangon city tours, yangon sightseeing tours , Myanmar tours, Myanmar Tour Packages , Mandalay Bagan Inle Lake Tour packages, tours to Myanmar with best rates.">
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <style type="text/css">
        .row{
            width: 100%;
        }
        .left,.right{
            float: left;
        }
        .left{
            width: 35%;
        }
        .right{
            width: 65%;
        }
        .clear{
            clear: both;
        }
    </style>

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->


    <!--end Header-->
  
  <!-- Content Section -->
  
     <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7">
    <h1 style="text-align: center;"><strong>Yangon–Kyaikhtiyoe-Mawlamyaing-Hpa An-Yangon</strong></h1>
<h1 style="text-align: center;"><strong>(5Days-4Nights)</strong></h1>
<h1 style="text-align: center;">Tour code: GMBF.D5001MH</h1>

</br>
</br>
<h5 style="color: orange">Day 1: Arrive Yangon<h5>
<div class="row">
                    <div style="float:left;width:480px"><small>
<p>Arrive Yangon and you will be welcomed by our tour representative. Then transfer to Hotel and take a short rest. If time permit, starts Yangon full day sightseeing. Highlight Includes;</p>
<p>•Botahtaung Pagoda</p>
<p>•Chauk Htat Gyi Pagoda </p>
<p>•Sule landmark and colonial buildings</p>
<p>•Shwedagon Pagoda</p>

<h5>Overnight in Yangon.</h5></small>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/shwedagon pagoda.jpg" style="width:300px;" class="editable" alt="shwedagon pagoda" title="shwedagon pagoda">  
                    </div>

</div>

<h4 style="color: black;clear:both">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 2: Yangon-Golden Rock- Mawlamyine (private car)<h5>
<div class="row">
                    <div style="float:left;width:480px"><small>
<p>Breakfast at the hotel. Then drive to Golden Rock around 6:00 am. Arrive Kimpun camp around 10am and take the uphill pick up truck about an hour to reach to the top of the mountain where you can see the Golden Rock(Kyaikhtiyo)- a large boulder positioned well balanced on the cliff of that mountain - seems to be defying the force of gravity from the spur of the cliff.  Afternoon, descend back from Kyaikhtiyo mountain and drive to Mawlamyaing . <p>
<h5>Overnight in Mawlamyaing.</h5></small>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/Kyaikhteeyo.jpg" style="width:300px;" class="editable" alt="Kyaikhteeyo" title="Kyaikhteeyo">  
                    </div>

</div>        

<h4 style="color: black; clear:both">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 3: Mawlamyine sightseeing</h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Breakfast at the hotel. Then starts sightseeings in  Mawlamyaing with highlights to;</p>

<p>•Mahamuni Pagodda</p>
<p>•Kyaik Thanlan Pagoda</p>
<p>•Kyauk  Talone Pagoda</p>
<p>•Win Sein Tawya</p>

<h5>Overnight in Mawlamyine.</h5>

                    </div>
                    <div style="float:right;width:300px">
                        <img src="imagesnew/the-mawlamyine-highway-bridge.jpg" style="width:300px;" class="editable" alt="the-mawlamyine-highway-bridge" title="the-mawlamyine-highway-bridge">  
                    </div>

</div>        

<h4 style="color: black;clear:both">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 4: HpaAn sightseeing</h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Breakfast at hotel. Then Drive to PhaAn. Then start sightseeing Highlight places;</p>

<p>•Kawgun Cave</p>
<p>•Khayon Cave </p>
<p>•Bayin Nyi Cave</p>
<p>•Mt Zwekabin</p>
<p>•Pha An Market and Shwe yin Hmway Pagoda.</p>

<h5>Overnight in Hpa An.</h5>





                    </div>
                    <div style="float:right;width:300px">
                        <img src="imagesnew/hpaan2.jpg" style="width:300px;" class="editable" alt="hpaan2" title="hpaan2">  
                    </div>

</div>        

<h4 style="color: black;clear:both">----------------------------------------------------------------------------------------------------------------------------------------</h4>
<h5 style="color: orange">Day 5: HpaAn-Bago-Yangon</h5>
<div class="row">
                    <div style="float:left;width:480px">
<p>Breakfast in hotel. Then drive back to Yangon. En-route visit Bago. Then transfer to Yangon airport for departure to your country.</p>


                    </div>
                    <div style="float:right;width:300px">
                        <img src="newimages/kandawgyi.jpg" style="width:300px;" class="editable" alt="kandawgyi" title="kandawgyi">  
                    </div>

</div>        

<h4 style="color: black;clear:both">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Service Include : </h4>
<p>-Transfer and sightseeing by private air-conditioned vehicle</p>
<p>-Accommodation 4nights with breakfast from hotel</p>
<p>-English speaking guide </p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h4 style="color: red">Services Exclude : </h4>
<p>-Meals (lunch/dinner)</p>
<p>-Entrance fees for sightseeing places</p>
<p>-Drinks, beverages, personal expense, tips, camera fees</p>
<p>-International Airport Ta (10 US$ per person in Yangon)</p>
<p>-Any other services not mentioned in “Service include” section</p>
<h4 style="color: black">----------------------------------------------------------------------------------------------------------------------------------------</h4>

<h3>Cost</h3> 
<h4>For 2 pax, US$770 per person</h4>
<h4>For 10pax, US$360 per person</h4> 
<h4>for more details,please contact hello@green-myanmar.com  </h4>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>
                </div>

      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
