<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin  sliderbar-->
    <div id="sidebar-left" >
      <div id="newsletter">
        <h2>Sign up for our Newsletter</h2>
        <p>Be the first to know about events and special offers! </p>
        <form action="notify.php" method="post" id="notifyForm">
          <div id="letter">
            <input type="text" name="email" class="required email" value="Email"/>
            <button type="submit" name="signup">Sign Up</button>
          </div>
        </form>
      </div>
      <!--end newsletter -->
      
      <div class="tabs">
        <ul class="tabNavigation">
          <li><a href="#first">Tour Highlights</a></li>
          <li><a href="#second">Activity</a></li>
        </ul>
        <div id="first">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p> Bisun ven gg  genatis risus ac libero sagi consectd detur adipiscing fpv gtis. <br />
                <a href="#">read more »</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" alt="tab-thumb" />
              <p> Aene aisun veneghfnatis risus ac libfg gfhero sagitti consectds detur de elit.<br />
                <a href="#">read more »</a></p>
            </li>
            <li  class="last"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p>Consectds detur adipiscing elit.Aene aisun venend atisd ds ac libero <br />
                <a href="#"> read more »</a></p>
            </li>
          </ul>
        </div>
        <div id="second">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p> Bisun ven gg  genatis risus ac libero sagi consectds detur adipiscing deradse. <br />
                <a href="#">read more »</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" alt="tab-thumb" />
              <p> Aene aisun veneghfnatis risus ac libfg gfhero sagitti consectds detur de elit.<br />
                <a href="#">read more »</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p> Reaisun venen co nsectds detur adipiscing. risus acs ded libero sagittis <br />
                <a href="#">read more »</a></p>
            </li>
            <li  class="last"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="tab-thumb" />
              <p>Consectds detur adipiscing elit.Aene aisun venend atisd ds ac libero <br />
                <a href="#"> read more »</a></p>
            </li>
          </ul>
        </div>
      </div>
      <div class="box"></div>
      <div id="video-ads"> <img class="fr" src="images/video-thumb.jpg" width="100" height="74" alt="tab-thumb" />
        <h5>Last video</h5>
        <p> Lorem ipsum dolor sit amet, consectetur.<br />
          <a href="#">watch video »</a></p>
      </div>
    </div>
    <!--end sidebar --> 
    <!--begin main column-->
    <div id="main-column-right" >
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body">
        <h2 id="column-header">The best <span class="green">Activities</span></h2>
        <img src="images/1.jpg" alt="1" class="image-left"  />
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, hendrerit ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus.consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, hendrerit ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus. </p>
        <p>Dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit.Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus. </p>
        <p>Dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, hendrerit ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus. </p>
      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      
      <div class="clear"></div>
    </div>
    <!--end main column --> 
    
    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
