<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Green myanmar travels & tours is providing best yangon city tours, yangon sightseeing tours ,river cruise tours, myanamr tours, hotel reservation , ticketing in yangon, mandalay , myanmar and world wide">
    <meta name="keyword" content="Green, Myanmar , Green Myanmar, yangon city tour, Yangon sightseeing, Myanmar , myanmar travels tours, Rangoon city tour, ticketing, hotel booking yangon, mandalay, bagan">
    <meta content="en" http-equiv="Content-Language">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Green-Myanmar | simple page</title>

<!-- including Style sheets -->
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
<link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

<!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
<script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

<!-- END BuySellAds.com Ad Code -->
<!--[if IE 6]>
	<script src="js/DD_belatedPNG_0.0.8a.js"></script>
	<script type="text/javascript">
	DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
	</script>
	<![endif]-->
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
</head>

<body>
<div id="wrapper" > 
  
  <!--Begin Header Section-->
  
  <div id="header" >
    <h1 id="logo"><a href="index.html" title="Green Myanmar Travel & Tour"></a></h1>
    <div id="coin-slider">
      <ul>
          <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
          <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
          <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
          <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
          <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
      </ul>
    </div>
    <!-- end Slider --> 
    
    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <ul class="dropdown">
      <li><a href="index.html" class="nav-open">HOME</a></li>
      <li><a href="destination.html" class="nav-border">DESTINATIONS</a>
        <ul class="sub_menu">
          <li><a href="myanmar.html" title="Myanmar">Myanmar</a></li>
          <li><a href="thailand.html" title="Thailand">Thailand</a></li>
          <li><a href="singapore.html" title="Singapore">Singapore</a></li>
        </ul>
      </li>
      <li><a href="simple-page.html" class="nav-border">ACTIVITIES</a>
        <ul class="sub_menu">
          <li><a href="simple-page.html" title="Simple page">Simple page</a></li>
          <li><a href="sidebar_on_left.html" title="Sidebar on left">Sidebar on left</a></li>
        </ul>
      </li>
      <li><a href="photos.html" class="nav-border">PHOTOS</a></li>
      <li><a href="about.html" class="nav-border">ABOUT US</a>
      <ul class="sub_menu">
          <li><a href="sitemap.html" title="sitemap">Site Map</a></li>
      </ul>
      </li>
      <li><div style="margin-left:25px;">
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="mgphyozaw@gmail.com">
            <input type="hidden" name="lc" value="US">
            <input type="hidden" name="item_name" value="paypal">
            <input type="hidden" name="button_subtype" value="services">
            <input type="hidden" name="no_note" value="0">
            <input type="hidden" name="currency_code" value="USD">
            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest">
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
            </div>
            </li>
    </ul>
    <!--end class dropdown--> 
    
  </div>
  <!--end Header--> 
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
       <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="main-column" >
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body">
        <h2 id="column-header">The best <span class="green">Activities</span></h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, hendrerit ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus.consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, hendrerit ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus. </p>
        <h2>H2 Title Here</h2>
        <h3>H3 Title Here</h3>
        <h4>H4 Title Here</h4>
        <h5>H5 Title Here</h5>
        <br/>
        <p>Dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit.Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus. </p>
        <h4>Blockquote</h4>
        <br/>
        <blockquote>
          <p>Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus.consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu.</p>
        </blockquote>
        <br/>
        <p>Dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultricies tempor leo, tristique neque facilisis vitae. Pellentesque purus libero, laoreet sit amet tincidunt edyt, hendrerit ac arcu. Integer adipiscing tincidunt metus, at dapibus lectus vestibulu ardm accumsan. Vivamus hendrerit eros in ligula volutpat at fringilla diam blandit. Aene aisun venenatis risus ac libero sagit rhoncus. Nulla id augue tortor. in scelerisque risus. </p>
      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      
      <div class="clear"></div>
    </div>
    <!--end main column --> 
    
    <!--begin  sliderbar-->
    <div id="sidebar" >
      <div id="newsletter">
        <h2>Sign up for our Newsletter</h2>
        <p>Be the first to know about events and special offers! </p>
        <form action="notify.php" method="post" id="notifyForm">
          <div id="letter">
            <input type="text" name="email" class="required email" value="Email"/>
            <button type="submit" name="signup">Sign Up</button>
          </div>
        </form>
      </div>
      <!--end newsletter -->
      
      <div class="clear"> </div>
      <div class="tabs">
        <ul class="tabNavigation">
          <li><a href="#first">Tour Highlights</a></li>
          <li><a href="#second">Activity</a></li>
        </ul>
        <div id="first">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="" />
              <p> Bisun ven gg  genatis risus ac libero sagi consectd detur adipiscing fpv gtis. <br />
                <a href="#">read more »</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" alt="" />
              <p> Aene aisun veneghfnatis risus ac libfg gfhero sagitti consectds detur de elit.<br />
                <a href="#">read more »</a></p>
            </li>
            <li  class="last"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="" />
              <p>Consectds detur adipiscing elit.Aene aisun venend atisd ds ac libero <br />
                <a href="#"> read more »</a></p>
            </li>
          </ul>
        </div>
        <div id="second">
          <ul>
            <li class="first"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="" />
              <p> Bisun ven gg  genatis risus ac libero sagi consectds detur adipiscing deradse. <br />
                <a href="#">read more »</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" alt="" />
              <p> Aene aisun veneghfnatis risus ac libfg gfhero sagitti consectds detur de elit.<br />
                <a href="#">read more »</a></p>
            </li>
            <li> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="" />
              <p> Reaisun venen co nsectds detur adipiscing. risus acs ded libero sagittis <br />
                <a href="#">read more »</a></p>
            </li>
            <li  class="last"> <img class="fr" src="images/tab-thumb.jpg" width="75" height="75" alt="" />
              <p>Consectds detur adipiscing elit.Aene aisun venend atisd ds ac libero <br />
                <a href="#"> read more »</a></p>
            </li>
          </ul>
        </div>
      </div>
      <div class="box"></div>
      <div id="video-ads"> <img class="fr" src="images/video-thumb.jpg" width="100" height="74" alt="" />
        <h5>Last video</h5>
        <p> Lorem ipsum dolor sit amet, consectetur.<br />
          <a href="#">watch video »</a></p>
      </div>
    </div>
    <!--end sidebar --> 
    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section--> 
  
  <!-- Footer Section -->
  <div id="footer">
    <div id="footer-bg">
      <div id="footer-content">
        <div id="footer-contact">

        </div>
        <div id="footer-panel1" class="footer-nev">
          <h2 id="panel1">Quick Links</h2>
          <ul>
<li><a href="index.html">Home</a></li>
            <li><a href="destination.html">Destination</a></li>
            <li><a href="itnerary.html">Itnerary</a></li>
            <li><a href="photos.html">Photos &amp; Gallery</a></li>
            <li><a href="about.html">About Us</a></li>
          </ul>
        </div>
        <div id="footer-panel2" class="footer-nev">
          <h2 id="panel2">Poppula Activity</h2>
          <ul>
                      <li><a><b><i>Location:</i></b><br/>(69)Room,(102)Conner of Kannar Street and Botataung Zay Street,Botataung,Yangon,Myanmar.</a></li>
                      <li><a><b><i>Phone No:</i></b><br/>+95(1)291887,+95(9)73128596,<br/>+95(9)450055720.</a></li>
                      <li><a><b><i>Fax:</i></b><br/>+95 (1) 291887.</a></li>
                      <li><a><b><i>Email:</i></b><br/>hello@green-myanmar.com,<br/>sales@green-myanmar.com.</a></li>
         </ul>
        </div>
        <div class="ft-coppy">
          <p>Copyright © 2013 <a href="http://www.evercrystal.com/">Evercrystal</a> All rights reserved.</p>
        </div>
      </div>
      <!--end footer content--> 
    </div>
    <!--end footer background--> 
  </div>
  <!--end footer--> 
  
</div>
<!--end wrapper-->

</body>
</html>
