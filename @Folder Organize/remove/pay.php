<?php include 'header.php'; ?>

<!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body">
                <h2>Pay for the package now!</h2>
                <br>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_xclick">
                    <input type="hidden" name="business" value="mgphyozaw@gmail.com">
                    <input type="hidden" name="lc" value="US">
                    <input type="hidden" name="item_name" value="paypal">
                    <input type="hidden" name="button_subtype" value="services">
                    <input type="hidden" name="no_note" value="0">
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                <br>
                </form>

            </div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <!-- Footer Section -->
<?php include 'footer.php'; ?>
