<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Buy and Book Myanmar Train Ticket  - Yangon , Mandalay , Golden Rock , Mawlamyine</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Buy and Book Myanmar Train ticket - Tickets Available Yangon to Mandalay Train">
    <meta name="keyword" content="Myanmar Train , Yangon Train, Mandalay Train ,Bagan Train, Kalaw Train ,Mandalay to Bagan Train , Bagan to Inle Lake Train, Bagan to Mandalay Train, Yangon to Mandalay Train">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->






    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">
                    <h2 style="color:orange;font-size: 20px">Buy Myanmar Bus Ticket and find myanmar bus information</h2><br>

                    <h2>Green Myanmar Travel arranges Train tickets for our customers in Myanmar.</h2>
                    <br/>
                    <br/>
                    <a style="font-size: 16px" href="myanmar-bus-schedule.php">Check Myanmar Bus Schedule Here</a>

                    <p>Travelling by Train in Myanmar is one of another option and you can also experience breathtaking nature and daily life of Myanmar  .</p>

                    <p>However , arranging train tickets in Myanmar is not very easy Since you can get train ticket only 3 days in advance and have to present passport as well.</p>

                    <p>In the mean time, Green Myanmar Travel here to help and manage Myanmar train tickets for independent travellers as much as can.</p>

                    <p> For more information please contact hello@green-myanmar.com</p>

                    <p>Update : Mar 2016</p>


                    <h4 style="color: orange">Myanmar Train Schedule  </h4>

                    <h4 style="color: orange">Yangon Train Schedules  </h4>

                    <br />
                    <table border="1" style="width: 600px">

                        <tr style="font-size: 14px;color: green;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Train From </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Train To</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Departure Time </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Arrival Time </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Normal Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Special Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> + Bed Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> High Club </b></td>

                        </tr>

                        <!-- <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>4:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>3:30 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr> -->


                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>6:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>21:30 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Malawmyine</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>7:15 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>16:30 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>
                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Naypyitaw</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>8:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>17:00 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>
                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Bagan</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>16:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>8:30 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>
                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>17:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>7:45 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Napyitaw</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>20:30  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>5:00 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Malawmyine</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>21:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>6:00 </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>





                    </table>

                    <br />
                    <br />
                    <br />

                    <h4 style="color: orange">Yangon Train Schedules  </h4>

                    <br />
                    <table border="1" style="width: 600px">

                        <tr style="font-size: 14px;color: green;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Train From </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Train To</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Departure Time </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Arrival Time </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Normal Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Special Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> + Bed Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> High Club </b></td>

                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Naypyitaw</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>3:00,6:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>5:00,21:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>
                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Taungoo</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>3:00,6:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>5:00,21:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>3:00,6:00  </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>5:00,21:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Mandalay </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Myitkyina</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>4:30,13:00
                                16:20,19:45
                            </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>N/A</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>



                    </table>

                    <br />
                    <br />
                    <br />

                    <h4 style="color: orange">Others to Yangon Train Schedule  </h4>

                    <br />
                    <table border="1" style="width: 600px">

                        <tr style="font-size: 14px;color: green;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Train From </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Train To</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Departure Time </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Arrival Time </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Normal Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Special Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> + Bed Price </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> High Club </b></td>

                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Malawmyine </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>19:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>4:30</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>NayPyiTaw </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>20:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>00:35</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Bagan </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>16:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>10:30</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

                        <tr style="font-size: 14px;color: white;">
                            <td height="15px" width="100px" style="text-align: center;"><b>Malawmyin </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>Yangon</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>8:00</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>17:30</b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b>- </b></td>
                            <td height="15px" width="100px" style="text-align: center;"><b> - </b></td>
                        </tr>

        </table>

                </div>

                <div>
                    <b>Remark :</b>
                    <p>Please be aware All Myanmar train arrival time are estimated and train might be delayed for some resons.</p>
                </div>

                <!--end column body-->
                <div class="full-column-footer"></div>
                <!--end full column footer-->

            </div>
            <!--end full column -->
            <div class="clear"></div>
        </div>
        </div>
        <!--end content-wrapper-->
        <div class="clear"></div>
        <!--end content section-->

        <?php include 'footer.php'; ?>
