<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
 
<head>
    <title>Hotels in myanmar - Book Myanamr Hotel - hotel in bagan - hotel in yangon  </title>
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="WE provide Myanmar hotels with best rates " />
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Myanmar Hotels , Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="generator" content="">
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->



    <!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable" id="untitled-region-7"><h2>Our partner Hotels</h2>

<h3>We do provide Myanmar hotel booking for Yangon , Bagan , Inle , Mandalay , Golden rock and all around Myanmar.</h3>


<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 10px 5px;" title=
"book now" /></a></p>

          <div class="editable">
              <h4 style="color:orange"> <a href="yangon.php"> Yangon </a></h4>
              <ul>
                  <li>
                      <p><a href="http://www.kandawgyipalace-hotel.com/">Kandawgyi Palace</a></p>

                  </li>
                  <li>
                      <p><a href="http://www.parkroyalhotels.com/en/hotels/myanmar/yangon/parkroyal/">Park Royal</a></p>
                  </li>
                  <li>
                      <p><a href="http://www.shangri-la.com/yangon/traders/">Traders</a></p>
                  </li>
                  <li>
                      <p><a href="http://chatrium.com/chatrium_hotel_yangon/">Chatrium Hotel Royal Lake Yangon</a></p>
                  </li>
                  <li>
                      <p><a target="_blank" href="http://www.summitparkviewhotel.com">Summit Parkview</a></p>
                  </li>
                  <li>
                      <p>Thamada hotel</p>
                  </li>
              </ul>

              <br />
              <br />
              <h4 style="color:orange"><a href="mandalay.php">Mandalay</a></h4>
              <ul>
                  <li>
                      <p><a href="http://www.hotelredcanal.com/"> Red cannel</a></p>
                  </li>
                  <li>
                      <p>Ayayarwaddy river view hotel</p>
                  </li>
                  <li>
                      <p>Myitparayeyar hotel</p>
                  </li>
                  <li>
                      <p><a href="http://www.hotelshwepyithar.com/">Hotel Shwe pyi thar</a> </p>
                  </li>
              </ul>
              <br />
              <br />
              <h4 style="color:orange"> <a href="bagan.php">Bagan</a> </h4>
              <ul>
                  <li>
                      <p><a href="http://www.tharabargate.com/">Bagan Tharabargate Hotel</a> </p>
                  </li>
                  <li>
                      <p><a href="http://www.baganthandehotel.net/"> Bagan Thande Hotel</a></p>
                  </li>
                  <li>
                      <p><a href="http://www.thiripyitsaya-resort.com/">Bagan Thiripyitsaya Resort</a> </p>
                  </li>
                  <li>
                      <p>Ayeyar River View Hotel</p>
                  </li>
                  <li>
                      <p><a href="http://www.thazingarden.com/"> Tahzin garden hotel</a></p>
                  </li>
                  <li>
                      <p>Pagoda city hotel</p>
                  </li>
                  <li>
                      <p>Kaday Aung</p>
                  </li>
              </ul>
              <h4 style="color:orange"><a href="inle.php"> Inle Lake</a></h4>
              <ul>
                  <li>
                      <p><a href="http://inleprincessresort.net/">Inle princess</a></p>
                  </li>
                  <li>
                      <p><a href="Golden Island cottage">Golden Island cottage(I,II)</a></p>
                  </li>
                  <li>
                      <p>Naung shwe</p>
                  </li>
                  <li>
                      <p>Remember inn</p>
                  </li>
                  <li>
                      <p>Gold star</p>
                  </li>
                  <li>
                      <p><a href="http://www.paramountinleresort.com/"> Paramount Inle Resort</a></p>
                  </li>
              </ul>
              <br />
              <br />
              <h4 style="color: yellow">Golden Rock </h4>
              <ul>
                  <li>
                      <p>Golden Rock ( Kyaikthiyo)</p>
                  </li>
                  <li>
                      <a style="color:orange" href="http://www.tripadvisor.com/Hotel_Review-g612369-d2294089-Reviews-Mountain_View_Hotel-Kyaikto_Mon_State.html">Mountain View</a>
                  </li>
              </ul>
              <h4 style="color: yellow">Ngaplali</h4>
              <ul>
                  <li>
                      <p><a href="http://www.thandebeachhotelmyanmar.com">Thande Beach Hotel</a></p>
                  </li>
              </ul>
              <h4 style="color: yellow">Ngwesaung Beach </h4>
              <ul>
                  <li>
                      <p><a href="http://www.sunnyparadiseresort.net/">Sunny Paradise Resort </a> (<a target="_blank" href="http://www.sunnyparadiseresort.net/">www.sunnyparadiseresort.net/</a>)</p>
                  </li>
              </ul>

              <h4 style="color: yellow">Chaung Thar Beach </h4>
              <ul>
                  <li>
                      <p><a href="http://www.maxhotelsgroup.com/">Hotel Max</a>
                  </li>
              </ul>
          </div>

<p><a href="reservation.php"><img alt="booking button" src=
"images/booking-button.jpg" style="margin: 10px 0px 15px 5px;" title=
"book now" /></a></p></div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer--> 
      
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
    <a href="http://www.charleshotel.hu" target="_blank">Hotel Budapest - best prices in Charles</a> <br>

    <a href="http://www.budapesthotelseeker.com" target="_blank">Balassi Gellert Hill hostel - a cheap hotel in Budapest</a> <br>


    <a href="http://www.directoryworld.net/" target="_blank">Directory World</a>

<a href="http://www.athhotel.com/">athhotel</a>

    <A HREF="http://www.holidayrentalflorida.com"><IMG SRC="http://www.holidayrentalflorida.com/images/newlogo.jpg" ALT="Camsun Vacation - Your ultimate source of vacation homes near Disney World, Orlando Florida." border="0" width="120" height="60"></A>
