<?php include 'header.php'; ?>

<!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body">
                <h2>Booking Form</h2>
                <h4>Please fill the form below:</h4>
                <form action="send_booking.php" method="post" id="formID" onSubmit="return ResetForm(this)">
                    <br>
                    <p style="font-size:14px;">Name :<input id="Name" type="text" name="name" placeholder="Type name...." style="background-color:white;width:200px;height:25px;margin-left:98px;"></p>
                    <p style="font-size:14px;">Email :<input id="Email" type="text" name="email" placeholder="Type email...." style="background-color:white;width:200px;height:25px;margin-left:100px;"></p>
                    <p style="font-size:14px;">Phone :<input id="Phone" type="text" name="phone" placeholder="Type phone no...." style="background-color:white;width:200px;height:25px;margin-left:93px;"></p>
                    <p style="font-size:14px;">Residential address :<input id="Address" type="text" name="address" placeholder="Type residential address...." style="background-color:white;width:200px;height:25px;margin-left:9px;"></p>
                    <p style="font-size:14px;">Destination :
                        <select name="destination" style="background-color:white;width:205px;height:26px;margin-left:59px;">
                            <option selected style="margin:7px 3px 3px 3px;">Yangon_Thanlyin Daily Tour</option>
                            <option style="margin:7px 3px 3px 3px;">Yangon_River_Cruise Tour</option>
                            <option style="margin:7px 3px 3px 3px;">Yangon Sightseeing Tour</option>
                        </select>
                    </p>
                    <br/>
                    <input style="margin-left: 137px;margin-top: -10px;" src="images/submit.png" type="image" value="Submit">
                </form>

                <!-- Form validation -->
                <script  type="text/javascript">
                    var frmvalidator = new Validator("formID");
                    frmvalidator.addValidation("Name","req","Please enter your Name");
                    frmvalidator.addValidation("Name","maxlen=20",
                            "Max length for Name is 20");

                    frmvalidator.addValidation("Email","maxlen=50");
                    frmvalidator.addValidation("Email","req");
                    frmvalidator.addValidation("Email","email");

                    frmvalidator.addValidation("Phone","maxlen=50");
                    frmvalidator.addValidation("Phone","req");
                    frmvalidator.addValidation("Phone","numeric");

                    frmvalidator.addValidation("Address","req");
                    frmvalidator.addValidation("Address","varchar");
                    frmvalidator.addValidation("Address","maxlen=50");
                </script>
                <!-- End of validation -->


            </div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <!-- Footer Section -->
<?php include 'footer.php'; ?>
