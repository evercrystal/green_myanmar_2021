<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
      xml:lang="en-gb" lang="en-gb">
 
<head>
    <title>About Green Myanmar Travel Agent</title>
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="About Green Myanmar Travel Agent" />
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="generator" content="">
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->




    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />


        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper">
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="main-column">
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body">
        <h2 id="column-header">Terms & Conditions</h2>

          <h3>Booking procedure & Validity:</h3>
          <p>All clients must book directly with Green Myanmar Travels & Tours Co, Ltd. in Yangon, Myanmar via email: hello@green-myanmar in written.</p>
          <p>Rates are quoted on net basic in US Dollar and valid according to the date on the printed tariffs, quotation, invoice etc. We guarantee that the prices or rates quoted are correct at the time of issuance, but are subject to change in line with market, tax, or currency fluctuation. Green Myanmar Travels & Tours Co, Ltd. reserves the right to change with or without prior notice.</p>
          <br />

          <h3>Airline Ticket:</h3>
          <p>1. The name of the airline carrier is not shown until after payment has been made for the booking.</p>
          <p>2. Your preferred flight time will be adjusted to the closest available flight. You will be informed of that in advance.</p>
          <p>3. Airline paper tickets will be issued to the guest(s) either at our Yangon office or by sending E-ticket if available. In some case, we can arrange to deliver the ticket to client.</p>
          <br />

          <h3>Hotel Booking:</h3>
          <p>1. Hotel room assignments will be done at check-in and upgrades may not be available at that time.</p>
          <p>2. The guest under whose name the reservation is made must be present at check-in.</p>
          <p>3. Hotel booking vouchers will be provided to the guest(s) either at our Yangon office or by email.</p>
          <p>4. Bookings cannot be transferred to another party.</p>
          <br />


          <h3>Car Rental:</h3>
          <p>1. Driver and Fuel are included for all vehicle rental and airport transfer unless it is stated in different.</p>
          <p>2. All car rentals include unlimited mileage within agreed time frame unless otherwise stated and to be used only for the purpose that is agreed.</p>
          <p>3. Car rental and airport transfer confirmations will be sent by email or text message, and some amount of initial payment need to be paid at the time of booking confirmation in order to secure the reservation.</p>

          <br />


          <h3>Travel Packages:</h3>
          <p>Travel packages available through this Site will be subject to the published conditions of carriage and, as applicable, rules of the applicable airline, hotel or car rental company.</p>
          <p>1. You may cancel your hotel without affecting your other travel item(s).</p>
          <p>2. All services, tickets, attractions and dates of travel are not secured and reserved until a required payment is received and written confirmation is made by email from us.</p>
          <p>3. Required documents, vouchers and tickets will be issued to the guest(s) either at our Yangon office or by e-mail.</p>
          <p>4. All travellers must travel on the same agreed itinerary.</p>
          <p>5. Any other services beyond the scope of Service Included section in the itinerary will be charged as excluded service and client shall make the payment in advance.</p>
          <p>6. Any package including an airline ticket booking is subject to the restrictions listed in the Airline Ticket section (above).</p>
          <p>7. Any package including a hotel booking is subject to the restrictions listed in the Hotel Booking section (above).</p>
          <p>8. Any package including a car rental booking is subject to the restrictions listed in the Car Rental section (above).</p>
          <p>9. Prices for published/advertised packages are subject to change without notice, and reservations are based on availability.</p>
          <br />

          <h3>Payment:</h3>
          <p>Non-refundable 50% deposit of the total amount must be made upon confirmation of booking. Rest of outstanding balance must be paid prior to client’s arrival or on client's arrival if agreed previously. Payment to be made as per arrangement, and Green Myanmar Travels & Tours Co, Ltd. reserves the right to cancel reservations unless the remittance of payment is made prior to the client’s arrival or on client's arrival if agreed by us previously.</p>
          <p>Except tour package, 100% of the total amount must be made for all other services including flight, bus, river cruise and car rental upon confirmation and it is non refundable.</p>
          <br />

          <h3>Cancellation:</h3>
          <p>In case of cancellation by client side, the client or agent will have to pay the cancellation fees (as mentioned below)</p>
          <p>- 45 days prior to tour 40% of tour cost</p>
          <p>- 21 days prior to tour 75% of tour cost</p>
          <p>- Less than 10 days prior to tour 100% of tour cost</p>
          <p>In the case of changes in our suppliers’ policies, Green Myanmar Travels & Tours Co, Ltd reserves the right to alter the cancellation policy accordingly. The administrative fee, no more than $100, may apply in the event that all cancellation fees are waived.</p>
          <br />

          <h3>Refund Policy:</h3>
          <p>There shall be no refund after commencement of the service included in the tour program but not utilized by the clients. The refund of payments received by Green Myanmar Travels & Tours Co, Ltd. from tour participants shall release us from any additional liability.</p>
          <br />

          <h3>Group Policy:</h3>
          <p>Group reservations should be made at least three months ahead of the intended tour start date. Full details of clients’ together with rooming list should be provided within two weeks prior to the arrival.</p>
          <br />

          <h3>Responsibility and Liability:</h3>
          <p>Green Myanmar Travels & Tours Co, Ltd. acts as the mediator for the person in regard to travel whether by rail-road, motor-coach, boat, airplane or any other means of transport and assumes no liability for any losses, injury, or damage sustained by the tourists, including those occurring outside the touring programmes. We accept no responsibility for losses or additional expenses due to accident, delay, sickness, weather, political situation or other cause beyond its control. Green Myanmar Travels & Tours Co, Ltd. will do its best to conduct the tour as per agreement. It also reserves the right to modify or reverse the original program in the interest of the clients and may substitute transportation with any other means of transportation when necessary due to any circumstances beyond its control. Green Myanmar Travels & Tours Co, Ltd. will try our best to keep the original tour programmes and arrangements in tact as possible as we can and will make every reasonable effort to assist in case of any misadventure. Green Myanmar Travels & Tours Co, Ltd. advises all travellers and tour operators to ensure that appropriate medical coverage is in place, if medical care and evacuation are needed. In case of any such circumstances, additional costs shall be borne by the client (the traveller). Participation in the tour implies the person’s agreement to the above conditions.</p>
          <br />

          <h3>Claims:</h3>
          <p>All disputes, claims and litigation regarding the services shall first be settled by way of negotiation and mutual understanding. In the event that such efforts fail, disputes, claim or litigation shall be finally settled by arbitration in the Union of Myanmar. All claims must be submitted in writing to our management management@green-myanmar.com within 30 days after the end of the services.</p>
          <br />

          <h3>Ground Arrangements:</h3>
          <p>Accommodation and Ground Transportation are provided as indicated on the confirmation. The condition, services and the facilities of the hotels are as expressed by the hotel. If there should be any dissatisfaction during your stay, the hotel takes full responsibilities for the inconvenience. Daily breakfast, service charges and taxes at hotels are normally inclusive unless otherwise indicated.</p>
          <p>We utilize all vehicles equipped with air-conditioning. However, due to poor infrastructure and the poor quality of petrol (gasoline) the system may not function properly. We will provide the best available alternative where the facilities do not exist.
          <br />

      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      
      <div class="clear"></div>
    </div>
    <!--end main column -->

      <!-- Sidebar -->
      <?php include 'myanmar-travel-promotion-sidebar.php'; ?>

    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>
