<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" >
 
<head>
    <title> Myanmar Travel News and Information Blog</title>
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="One of Myanmar Best Myanmar Travel agent's Myanmar Travel News and Information Blog " />
    <meta name="keyword" content="Myanmar Information,Blog, Travel, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="generator" content="">
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->




    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>
    <!--end class dropdown-->

</div>
<!--end Header-->

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="main-column" >
      <div class="column-header"></div>
      <!--end column header-->
      <div class="column-body">
        <h2 id="column-header">Myanmar Travel News and Information Blog</h2>

          <p> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>
          <div>
              <h3 style="color:orange">Myanmar online E-visa will be available from September</h3>

              <p>will be started to use at the Yangon International Airport on coming September 1 according to Ministry of Immigration and Population.</p>

              <p>The E-visa will be applied for the tourists that will visit Myanmar and other visitors such as visitors for economic reasons and social reasons will not be allowed. Only after this E-visa is in steady regulation, other E-visas will be arranged.</p>
              <p>ref : <a href="myanmar-online-visa.php">Read More</a> </p>
          </div>




        <p> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>

        <p> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>
          <div>
              <h3 style="color:orange">Credit Cards , Visa , Masters in Burma ( Myanmar ) </h3>
              <p>    A year ago, Myanmar had no automated teller machines linked to international networks and not a single hotel or restaurant able to swipe credit cards. The throngs of foreigners arriving in the newly opened country had to bring crisp U.S. dollars to pay for everything. Today, Myanmar has 2,500 machines that process credit card payments, known as payment terminals, and 450 ATMs, including at least three at the gates of Yangon’s Shwedagon Pagoda, a popular tourist attraction, according to Kanbawza Bank, the largest privately owned bank in Myanmar. There’s a long way to go. While “the absolute need to carry bags of cash is declining, Myanmar remains a cash economy,” says Matt Davies, the International Monetary Fund’s mission chief to the country. “It takes time for practices to change.”
              </p>
              <p>
                  Visa (V) and MasterCard (MA) are working to speed the transition. Since September 2012, MasterCard has signed up nine banks to issue its cards. The company says those banks have installed payment terminals at 491 merchants, including the Strand and Governor’s Residence hotels in Yangon, the financial capital. About 210 ATMs also accept MasterCard for cash withdrawals. Visa says it has licensed eight banks to issue its cards, and they’ve signed up more than 600 merchants, including hotels, restaurants, airlines, and retailers, and installed more than 200 ATMs.              </p>
              <p>
                  As in most of Southeast Asia, Myanmar restaurants and stalls tend to specialize in a single dish or culinary style.
              </p>

              </div>

              <p>ref : <a href="http://www.businessweek.com/articles/2013-10-24/visa-mastercard-sign-up-myanmar-banks-to-expand-card-transactions">Read More</a> </p>
          </div>

          <p> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>


          <p> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>
          <div>
              <h3 style="color:orange">10 meals every Myanmar traveler should try</h3>
              <h5>    Rich, savory Burmese cuisine has been hidden away for half a century. That's about to change
              </h5>
              <p>
                  With an emphasis on rich, predominately savory/salty flavors, influences from South and Southeast Asia and a repertoire of ingredients not found in any other cuisine, there’s much to discover.
              </p>
              <p>
                  As in most of Southeast Asia, Myanmar restaurants and stalls tend to specialize in a single dish or culinary style.
              </p>
              <div>
              <ul>
                  <ol>
                      1 - Tea leaf salad
                  </ol>
                  <ol>
                      2 - Shan-style rice
                  </ol>
                  <ol>
                      3 - Burmese tea shop meal
                  </ol>
                  <ol>
                      4 - Burmese sweet snacks
                  </ol>
                  <ol>
                      5 -  Deep-fried stuff
                  </ol>
                  <ol>
                      6 -  Shan-style ‘tofu’ noodles
                  </ol>
                  <ol>
                      7 -  Nangyi thoke
                  </ol>

                  <ol>
                      8 -  Mohinga
                  </ol>
                  <ol>
                      6 -  Shan-style noodles
                  </ol>
                  <ol>
                      7 -  Nangyi thoke
                  </ol>


              </ul>
              </div>

              <p>ref : <a href="http://travel.cnn.com/myanmar-food-669416">Read More</a> </p>
          </div>

          <p> - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>

      </div>
      <!--end column body-->
      <div class="column-footer"></div>
      <!--end column header-->
      

        <!--end tri-last div--> 
      <div class="clear"></div>
    </div>
    <!--end main column -->

      <!----- Side Bar --->
      <!-- <?php include 'myanmar-travel-promotion-sidebar.php'; ?> -->
    <br class="clear"/>
  </div>
  <!--end content-wrapper--> 
  <div class="clear"></div>
  <!--end content section-->
<p><a href="http://myanmartourism.org/"> Myanmar Tourism Organization</a></p>
<p><a href="http://www.umtanet.org/"> Union of Myanmar Travel Association (UMTA)</a></p>
<?php include 'footer.php'; ?>
