<?php include 'header.php'; ?>

<!--end Header-->
  
  <!-- Content Section -->
  
  <div id="content-wrapper" >
    <div id="content-header" >
      <h2 id="welcome"></h2>
      <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
      </form>
      <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
      </ul>
      <br class="clear"/>
    </div>
    <!--end content_header--> 
    
    <!--begin main column-->
    <div id="full-column" >
      <div class="full-column-header"></div>
      <!--end full column header-->
      <div class="full-column-body editable">
        <h2 id="column-header">Let's Explore <span class="green">Green Myanmar's Photos Gallery</span></h2>
        <p> We built our best collection photo gallery to provide customers to experience online travelling value. Enjoy !!!  </p>
        <div id="photos-gallery" class="editable">
          <ul class="gallery clearfix">
            <li><a  rel="prettyPhoto[gallery2]" href="images/gallery/bagan1.jpg"><img class="editable" src="images/bagan1_thumb.jpg" alt="Bagan Bu Pagoda" /></a></li>
          </ul>
        </div>
        <!--end photos-gallery--> 
      </div>
      <!--end column body-->
      <div class="full-column-footer"></div>
      <!--end full column footer-->
      
      <div class="clear"></div>
    </div>
    <!--end full column -->
    <div class="clear"></div>
  </div>
  <!--end content-wrapper-->
  <div class="clear"></div>
  <!--end content section-->

<?php include 'footer.php'; ?>

