<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Myanmar Tour Packages</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Myanmar Tour Packages and itinerary for  Yangon, Bagan, Mandalay, Inle Lake and all around Myanmar " />
    <meta name="keyword" content="Green Myanmar, Myanmar Travel Agent, Myanmar Travel, Myanmar Tour package, Yangon city tour, Bagan, Mandalay ,tours to Myanmar, Myanmar Travel Information">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>
    <!--end class dropdown-->

</div>
<!--end Header-->


<!--end Header-->

<!-- Content Section -->

<div id="content-wrapper" >
<div id="content-header" >
    <h2 id="welcome"></h2>
    <form id="search-form" action="" method="post">
        <input id="searchInput" name="searchInput" type="text" />
        <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
    </form>
    <ul id="button">
        <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
        <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
        <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
    </ul>
    <br class="clear"/>
</div>
<!--end content_header-->

<!--begin main column-->
<div id="main-column" >
<div class="column-header"></div>
<!--end column header-->
<div class="column-body editable">
    <h2 id="column-header editable">Welcome To <span class="green">Green Myanmar Travel</span></h2>
    <p>Partnership Adding backlinks each other is warmly welcome , <br />Please request to admin@green-myanmar.com </p>
</div>
<!--end column body-->
<div class="column-footer"></div>
<div class="editable">
    <ul>
        <li>            <a href=http://greenmyanmartravel.adpost.com/ target=_BLANK><img width=468 height=60 src=http://greenmyanmartravel.adpost.com/images/adpost01.gif border=0></a>
        </li>
        <li> <a href= "http://www.willgoto.com">Willgoto, World travel directory and travel guide</a></li>
        <li>
            <a href='http://tripcook.com/countries--MM--Myanmar'>Find  Myanmar travel agency</a> - Tripcook provides listings of travel agents and companies in Myanmar

        </li>
        <li>
            <a href="http://www.seo-writer.com/freelance/ghost-writer.html">Travel writers</a> - A writing service offering writers with a variety of backgrounds and skills. Let's write your book, whether it be travel or biography or self-help.

            Read more: http://www.wv-travel-directory.com/directory/module-DP-submit-submit_type-free-cat_id-180-reg_id-114.html#ixzz33gbF0hWI
        </li>
        <li><a href="http://directory.myanmar.cm/">Myanmar Directory</a></li>

        <li><a href="http://www.myanmarlinkdirectory.com/" title="Myanmar Link Directory">Myanmar Link Directory</a></li>

        <li><A href="http://www.traveltourismdirectory.com/">Travel and Tourism Directory</A></li>

        <li><a href="http://www.wv-travel-directory.com">WV Travel Directory</a> - A world of travel packages and vacation guides located across the Internet. The WV Travel Directory is now auditioning for top talent to fill its pages.

            Read more: http://www.wv-travel-directory.com/directory/module-DP-submit-submit_type-free-cat_id-180-reg_id-114.html#ixzz35UeMS7iS</li>


        <li><a href="http://directory.myanmar.cm/">Myanmar Directory</a></li>

        <Li><A href="http://www.traveltourismdirectory.com/">Travel and Tourism Directory</A></Li>
        <li><script language="javascript" src="http://www.exactseek.com/remote-submit.js"></script></li>

        <li><!-- Start travelfromHere.com link-->
            <p align="center"><a href="http://www.travelfromhere.com">
                <img border=0 src="http://www.travelfromhere.com/travelfromherebutton.gif"
                     width="88" height="31" alt="Proud to be listed at travelfromHere.com">
            </a></p>
            <!-- End travelfromHere.com link--></li>

        <li><a href="http://www.touristsboard.com/italy/323-im-thinking-about-going-to-italy.html">Why Go To Italy</a></li>
        <li><a href="http://www.traveltourismdirectory.info/Accommodation___Lodging/Timeshare/">Visit Timeshares</a></li>

        <li><a target="_blank"
               href="http://www.leoccare.com"> Agriturismo Le Occare Guest-house Ferrara Italy</a> - A full comfort Guest House in the countryside of Ferrara</li>
    </ul>
</div>

        <a href="hotels.php"><h4>Myanmar Hotels</h4></a>
        <div class="clear"></div>
    </div>
    <!--end main column -->

    <div class="clear"></div>
    <!--end content section-->


            <?php include 'footer.php'; ?>
