<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Inle Lake Car Rental</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Inle Lake Car Rental rate">
    <meta name="keyword" content=" Inle Lake car rental , Kalaw Car rental , Taunggyi Car Rental , Myanmar Car Rental">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->

    <style>
        div.img {
            margin: 5px;
            padding: 5px;
            border: 1px solid #0000ff;
            height: auto;
            width: auto;
            float: left;
            text-align: center;
        }

        div.img img {
            display: inline;
            margin: 5px;
            border: 1px solid #ffffff;
        }

        div.img a:hover img {
            border:1px solid #0000ff;
        }

        div.desc {
            text-align: center;
            font-weight: normal;
            width: 120px;
            margin: 5px;
        }
    </style>
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>
        <!--end class dropdown-->

    </div>
    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" >
                <h4>Green Myanmar Travels & Tours Co.,Ltd.</h4>

                <p style="font-size: 14px;">Address: NO.350 , Waizayantar Road, Thingangyun Township , Yangon.<br/>
                    Phone:  +95 9 420077655 , +95 1 8569936<br/>
                    Fax: +95 1 291887<br/>
                    E-mail: hello@green-myanmar.com<br/>
                    Website: Green-Myanmar.com </p>
   <br/><h5>***All prices are in US$*** <h5>
                <p style="color: yellow;font-size: 15px;text-align: center"><b>Inle Lake Car Rental Rates</b></p>


               <table border="1">

                <tr style="font-size: 14px;color: white;">
                    <td height="15px" width="140px" style="text-align: center;"></td>
                    <td height="15px" width="140px" style="text-align: center;background-color: #8fbb29"><b>HH-NS / NS-HH transfer</b></td>
                    <td height="15px" width="140px" style="text-align: center;background-color: #8fbb29"><b>NS-TG Transfer</b></td>
                    <td height="15px" width="140px" style="text-align: center;background-color: #8fbb29"><b>NS-KLW Transfer</b></td>
                    <td height="15px" width="140px" style="text-align: center;background-color: #8fbb29"><b>TG-KK-TG / TG-HTSM-TG sightseeing</b></td>
                    <td height="15px" width="140px" style="text-align: center;background-color: #8fbb29"><b>NS/HH-PDY sightseeing</b></td>
                </tr>




                <tr style="font-size: 14px;color: white;">

                    <td height="20px" width="140px" style="text-align: center;background-color: #8fbb29">Saloon</br>(1-3 pax)</td>
                    <td height="20px" width="140px" style="text-align: center;">39</td>
                    <td height="20px" width="140px" style="text-align: center;">45</td>
                    <td height="20px" width="140px" style="text-align: center;">58</td>
                    <td height="20px" width="140px" style="text-align: center;">65</td>
                    <td height="20px" width="140px" style="text-align: center;">65</td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="20px" width="140px" style="text-align: center;background-color: #8fbb29">Super Custom</br>(3-4 pax)</td>
                    <td height="20px" width="140px" style="text-align: center;">63</td>
                    <td height="20px" width="140px" style="text-align: center;">65</td>
                    <td height="20px" width="140px" style="text-align: center;">79</td>
                    <td height="20px" width="140px" style="text-align: center;">91</td>
                    <td height="20px" width="140px" style="text-align: center;">91</td>
                </tr>

                <tr style="font-size: 14px;color: white;">
                    <td height="20px" width="140px" style="text-align: center;background-color: #8fbb29">Grand Cabin G(4-6 pax)</td>
                    <td height="20px" width="140px" style="text-align: center;">88</td>
                    <td height="20px" width="140px" style="text-align: center;">89</td>
                    <td height="20px" width="140px" style="text-align: center;">105</td>
                    <td height="20px" width="140px" style="text-align: center;">109</td>
                    <td height="20px" width="140px" style="text-align: center;">109</td>
                </tr>

                    <tr style="font-size: 14px;color: white;">
                        <td height="20px" width="140px" style="text-align: center;background-color: #8fbb29">19 </br> seaters</td>
                        <td height="20px" width="140px" style="text-align: center;">158</td>
                        <td height="20px" width="140px" style="text-align: center;">148</td>
                        <td height="20px" width="140px" style="text-align: center;">170</td>
                        <td height="20px" width="140px" style="text-align: center;">178</td>
                        <td height="20px" width="140px" style="text-align: center;">175</td>
                    </tr>

               
                </table>

                <br />
                <br />
                <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->

    <style>
        div.img {
            margin: 5px;
            padding: 5px;
            border: 1px solid #0000ff;
            height: auto;
            width: auto;
            float: left;
            text-align: center;
        }

        div.img img {
            display: inline;
            margin: 5px;
            border: 1px solid #ffffff;
        }

        div.img a:hover img {
            border:1px solid #0000ff;
        }

        div.desc {
            text-align: center;
            font-weight: normal;
            width: 120px;
            margin: 5px;
        }
    </style>
</head>
<body>



            <br /> <div class="img">
                <a target="_blank" href="carrentalphotos/toyota_crown_2011.jpg">
                    <img src="carrentalphotos/toyota_crown_2011.jpg" alt="Yangon Car Rental" width="270" height="180">
                </a>
            </div>
            <div class="img">
                <a target="_blank" href="carrentalphotos/2.Toyota Mark II.jpg">
                    <img src="carrentalphotos/2.Toyota Mark II.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>
            <div class="img">
                <a target="_blank" href="carrentalphotos/4 SuperCustom interior.jpg">
                    <img src="carrentalphotos/4 SuperCustom interior.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>
            <div class="img">
                <a target="_blank" href="carrentalphotos/5 toyota-alphard.jpg">
                    <img src="carrentalphotos/5 toyota-alphard.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>



            <div class="img">
                <a target="_blank" href="carrentalphotos/6 Alphard interior.jpg">
                    <img src="carrentalphotos/6 Alphard interior.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>

            <div class="img">
                <a target="_blank" href="carrentalphotos/7. Grand Cabin G (9 seaters) ph2.jpg">
                    <img src="carrentalphotos/7. Grand Cabin G (9 seaters) ph2.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>
            <div class="img">
                <a target="_blank" href="carrentalphotos/8. Grand Cabin G (9 seaters).jpg">
                    <img src="carrentalphotos/grandcabin8.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>


            <div class="img">
                <a target="_blank" href="carrentalphotos/9. Grand Cabin G interior 2.jpg">
                    <img src="carrentalphotos/grandcabin9.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>
            <div class="img">
                <a target="_blank" href="carrentalphotos/11. Commuter (14seaters).jpg">
                    <img src="carrentalphotos/11. Commuter (14seaters).jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>

            <div class="img">
                <a target="_blank" href="carrentalphotos/12. Commuter inside.jpg">
                    <img src="carrentalphotos/12. Commuter inside.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>
            <div class="img">
                <a target="_blank" href="carrentalphotos/13.Toyota Commuter.jpg">
                    <img src="carrentalphotos/13.Toyota Commuter.jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>

            <div class="img">
                <a target="_blank" href="carrentalphotos/22 seaters (1).jpg">
                    <img src="carrentalphotos/22 seaters (1).jpg" alt="Yangon Car rental" width="270" height="180">
                </a>
            </div>
            <br />


<p style="color: black;font-size: 16px;clear:both">------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>


                <br />

<p style="color: blue;font-size: 16px;">Service Include</p>
<ul style="font-size: 14px;color:#ffffff">
<li>  Fuel</li>
<li>  Driver</li>
<li>  Road Tax</li>

                </ul>
</br>
</br>
                <ul style="font-size: 14px;color:#ffffff">
                    <p style="color: red;font-size: 16px;">Remark</p>
                    <li>  Sightseeing allow maximum 10 hours per day, not later than 8pm.</li> 
                    <li>  After 10 hrs, 10% of Full day will be added to the full day amount for every extra hour.</li> 
                    <li>  100% full payments</li> 
                    <br/>
                    <li style="color: red">*The above rate will be changed according to the rate of Fuel price and peak season</li>
                </ul>

            </div>
            </div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

    <?php include 'footer.php'; ?>
