<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Inle Lake Boat Rental - Myanmar Boat Rental Service</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Inle Lake Boat Rental - Myanmar Boat Rental Service" />
    <meta name="keyword" content="Inle Lake Boat rental , myanmar boat rental service">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />

    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->

    <style>
        div.img {
            margin: 5px;
            padding: 5px;
            border: 1px solid #0000ff;
            height: auto;
            width: auto;
            float: left;
            text-align: center;
        }

        div.img img {
            display: inline;
            margin: 5px;
            border: 1px solid #ffffff;
        }

        div.img a:hover img {
            border:1px solid #0000ff;
        }

        div.desc {
            text-align: center;
            font-weight: normal;
            width: 120px;
            margin: 5px;
        }
    </style>
</head>
<body>
<div id="wrapper" >

<!--Begin Header Section-->

<div id="header" >
    <h1 id="logo"><a href="index.php" title="Green Myanmar Travel Agent"></a></h1>
    <div id="coin-slider">
        <ul>
            <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
            <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
            <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
            <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
            <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
        </ul>
    </div>
    <!-- end Slider -->

    <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
    <?php include 'menu.php'; ?>

    <!--end class dropdown-->

</div>
<!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable" >
                <h4>Green Myanmar Travels & Tours Co.,Ltd.</h4>

                <p style="font-size: 14px;">Address:NO.391 , Waizayantar Road, Thingangyun Township , Yangon.<br/>
                Phone: +95 9 420077655 , +95 9 428016234<br/>
                Fax: +95 1 8569936<br/>
                E-mail: hello@green-myanmar.com<br/>
                Website: Green-Myanmar.com </p>

               <br/><h5>***All prices are in US$*** <h5>


                <p style="color: yellow;font-size: 13px;text-align: center"><b>Inle Lake Boat Rental - Myanmar Boat Rental Service</b></p>

                <p> Inle Lake Full Day boat rental = 32 US$ </p>


                        <p> Inle + Indine boat Full Day boat rental = 42 US$ </p>

                        <p> Inle + Sagar boat Full Day boat rental = 69 US$ </p>

                        </br>


                        <div class="img">
                            <a target="_blank" href="images/boats-inle-lake.jpg">
                                <img src="images/boats-inle-lake.jpg" alt="Yangon Car Rental" width="270" height="180">
                            </a>
                        </div>




<p style="color: black;font-size: 16px;clear:both">------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>

             
           </br>
                <p style="color: blue;font-size: 16px;">Service Include</p>
<ul style="font-size: 14px;color:#ffffff">
<li>  Fuel</li>
<li>  Driver</li>
<li>  Road Tax</li>

                </ul>
</br>
</br>

                <ul style="font-size: 14px;color:#ffffff;clear: both">
                    <p style="color: red;font-size: 16px;">Remark</p>
                    <li>Full Day=10 hrs(For City Tour) </li>
                 <li>boat will be available to run only from 6 am to 6 pm for security reason</li>

                    <li>100% full payments</li>
                    <br/>

    <li style="color: red">***The above rate will be changed according to the rate of Fuel price and peak season***</li>
                </ul>


            </div>
            <!--end column body-->
            <div class="full-column-footer"></div>
            <!--end full column footer-->

        </div>
        <!--end full column -->
        <div class="clear"></div>
    </div>
    <!--end content-wrapper-->
    <div class="clear"></div>
    <!--end content section-->

<?php include 'footer.php'; ?>
