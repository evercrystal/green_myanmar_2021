<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <title>Mandalay Bagan Bus - Bagan Mandalay Bus</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Buy and Book Myanmar Bus ticket - Tickets Available Mandalay to Bagan Bus , Bagan to Inle Lake Bus, Bagan to Mandalay Bus, Yangon to Mandalay Bus">
    <meta name="keyword" content="Myanmar Bus , Yangon Bus, Mandalay Bus ,Bagan Bus, Kalaw Bus ,Mandalay to Bagan Bus , Bagan to Inle Lake Bus, Bagan to Mandalay Bus, Yangon to Mandalay Bus">    <meta content="en" http-equiv="Content-Language">
    <meta name="google" content="notranslate" />
    <!-- including Style sheets -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="css/coin-slider-styles.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/prettyPhoto.css" media="screen"  />

    <!-- including javascripts (jquery, prettyPhoto,coin-slider,Form validate) -->
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/coin-slider.min.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>

    <meta name="google-translate-customization" content="aeb12982cbee5ef6-a1d75d7fad936064-g2a3d82d4023cd784-15"></meta>

    <!-- END BuySellAds.com Ad Code -->
    <!--[if IE 6]>
    <script src="js/DD_belatedPNG_0.0.8a.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix(' #logo, .banner, #welcome, #button, #slidefeature, #submitButton, #latest-location ul li, #newsletter h2 , #commentForm textarea, #commentForm input,  #nav_main li ul, #scroller-header a, .selected,#scroller-header a:hover, a.selected, #scroller-header, #photos-header, #destinatio-header, #activities-header, #about-header,.dropdown,blockquote');
    </script>
    <![endif]-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <!-- google analyzer -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38877742-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End of google analyzer  -->


    <!-- BEGIN JIVOSITE CODE {literal} -->
    <!-- BEGIN JIVOSITE CODE v2.0 (SSL) -->
    <script type='text/javascript'>
        (function(){ var widget_id = '47889';
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
    <!-- END JIVOSITE CODE -->
</head>

<body>
<div id="wrapper" >

    <!--Begin Header Section-->

    <div id="header" >
        <h1 id="logo"><a href="index.php" title="Green Myanmar Travel & Tour"></a></h1>
        <div id="coin-slider">
            <ul>
                <li><img src="images/1.jpg" alt="shwedagon pagoda" title="shwedagon pagoda"/></li>
                <li><img src="images/2.jpg" alt="mandalay" title="mandalay"/></li>
                <li><img src="images/3.jpg" alt="inle" title="inle"/></li>
                <li><img src="images/4.jpg" alt="bagan" title="bagan"/></li>
                <li><img src="images/5.jpg" alt="bago pagoda" title="bago pagoda"/></li>
            </ul>
        </div>
        <!-- end Slider -->

        <img src="images/greenday.png" id="slidefeature" alt="greenday" width="73" height="78" />
        <?php include 'menu.php'; ?>

        <!--end class dropdown-->

    </div>
    <!--end Header-->






    <!--end Header-->

    <!-- Content Section -->

    <div id="content-wrapper" >
        <div id="content-header" >
            <h2 id="welcome"></h2>
            <form id="search-form" action="" method="post">
                <input id="searchInput" name="searchInput" type="text" />
                <input id="submitButton" name="submitButton" value="Search" type="image" src="images/search.jpg" />
            </form>
            <ul id="button">
                <li><a href="https://plus.google.com/108886326770633962604/posts" id="burss">rss</a></li>
                <li><a href="https://twitter.com/Greenmyanmart" id="butwitter">twitter</a></li>
                <li><a href="http://www.facebook.com/pages/Green-Myanmar-TT-CoLtd/523381571035373" id="bufacebook">facebook</a></li>
            </ul>
            <br class="clear"/>
        </div>
        <!--end content_header-->

        <!--begin main column-->
        <div id="full-column" >
            <div class="full-column-header"></div>
            <!--end full column header-->
            <div class="full-column-body editable">
                <div id="main">
                    <h2 style="color:orange;font-size: 20px">Mandalay to Bagan and Bagan to Manalay bus are available with 15 seaters vechicle bus</h2><br>

                    <h2 style="color:orange;font-size: 20px">Travelling by bus in Myanmar ,Book Myanmar Bus at hello@green-myanmar.com or <a href="http://myanmarbusticket.com"> Book Mandalay to Bagan bus or Bagan to Mandalay Bus online Online</a> </h2>
                    <br/>
                    <br/>
                    <a style="font-size: 16px" href="myanmar-bus-schedule.php">Check Myanmar Bus Schedule Here</a>

                    <p>Travelling by bus is the simplest way to get around Myanmar if you are on a budget or  – and it is the only way to get to certain destinations (unless you are prepared to pay for a private car). And also travelling by bus in myanmar is so safe. </p>

                    <p>Now in Myanmar , VIP coach and bus are available to For Yangon , Mandalay , Bagan , Inle Lake , Kalaw, Taunggyi and other Destinations. They are safe,  nice , clean and cosy to travel.  </p>

                    <p> Green Myanmar Travel agent provides buying and arranging Myanmar Bus Ticket for Indepedent traveller</p>

                    <p> For more information please contact hello@green-myanmar.com</p>





                    <h2 style="color:orange;font-size: 20px">It takes about 4 hours estimated time for travelling and deliver to you until Bagan hotel. </h2>

                    <h5> Mandalay to Bagan Bus Departure time (15 seaters) </h5>
                    <h6>6:00 am ,8:00 am ,13:00 pm, 16:00 pm</h6>
                    <h7>Price - 8700 kyats per person</h7>
                    <h7>One luggage allowed for one person</h7>



                    <br />
                    <br />

                    <h5> Bagan to Mandalay Bus Departure time (15 seaters)  </h5>
                    <h6>5:00 am ,8:00 am ,13:00 pm, 16:00 pm</h6>
                    <h7>Price - 8700 kyats per person</h7>
                    <h7>One luggage allowed for one person</h7>



                    <br />
                    <br />

                    <h5> Bagan to Mandalay bus , Mandalay to Bagan bus with 45 seaters</h5>
                    <h6>7.30 am , 9 pm - Time may be changed </h6>

                    <p>If you would like to book please contact hello@green-myanmar.com , buy from <a href="http://myanmarbusticket.com">myanmarbusticket.com</a> </p>





                </div>

                <!--end column body-->
                <div class="full-column-footer"></div>
                <!--end full column footer-->

            </div>
            <!--end full column -->
            <div class="clear"></div>
        </div>
        <!--end content-wrapper-->
        <div class="clear"></div>
        <!--end content section-->

        <?php include 'footer.php'; ?>
