<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\OrderController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('page/{page}', [HomeController::class, 'page'])->name('page');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

Route::get('order-form', [OrderController::class, 'orderForm'])->name('order_form');
Route::get('order-form/get-collection-address/{collectionPointId}', [OrderController::class, 'getCollectionPointAddress'])->name('get_address');
Route::get('order-form/get-collection-detail/{collectionPointId}', [OrderController::class, 'getCollectionPointDetail'])->name('get_collection_detail');
Route::get('order-form/get-city/{countryId}', [OrderController::class, 'getCities'])->name('get_city');
Route::get('order-form/get-city-in-register/{stateId}', [OrderController::class, 'getCitiesinRegister'])->name('get_city');
Route::get('order-form/get-state/{countryId}', [OrderController::class, 'getStates'])->name('get_state');
Route::get('order-form/get-township/{cityId}', [OrderController::class, 'getTownships'])->name('get_township');
Route::get('order-form/get-recent-order', [OrderController::class, 'getRecentOrder'])->name('get_recent_order');
Route::get('order-form/get-collection/{collectionId}', [OrderController::class, 'getCollection'])->name('get_collection');
Route::get('order-form/get-collections', [OrderController::class, 'getAllCollection'])->name('get_collection');
Route::get('order-form/get-township-and-collection', [OrderController::class, 'getTownshipAndCollection'])->name('get_township_and_collection');
Route::post('order-form/calculate-price', [OrderController::class, 'calculatePrice'])->name('claculate_price');
Route::post('order-form/save', [OrderController::class, 'saveOrder'])->name('save_order');
Route::post('order-form/save-payment', [OrderController::class, 'saveOrderPayment'])->name('save_payment');
Route::get('order/payment-confirmation/{method}/{encrypt_order_payment_id}', 'OrderController@paymentConfirmation');
Route::get('order/receiver-payment-confirmation/{encrypt_order_id}', 'OrderController@receiverPaymentConfirmation');
Route::any('order/payment-complete/{method}/{encrypt_id?}', [OrderController::class, 'paymentComplete']);
Route::any('order/payment-status/{method}/{encrypt_id?}', [OrderController::class, 'paymentStatus']);
Route::get('order/track', [OrderController::class, 'trackOrder'])->name('order.track');
Route::get('order/track-detail', [OrderController::class, 'trackDetail'])->name('order.trackdetail');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        Route::post('dashboard/get-order-list',  [DashboardController::class, 'getOrderList'])->name('dashboard.get_order');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
